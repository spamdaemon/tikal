#include "gpc.h"

#include <tikal/Point2D.h>
#include <tikal/BoundingBox2D.h>
#include <tikal/Region2DOps.h>
#include <tikal/Region2D.h>
#include <tikal/Area2D.h>
#include <tikal/Contour2D.h>
#include <tikal/Polygon2D.h>
#include <tikal/TriangleStrip2D.h>
#include <tikal/util.h>

#include <timber/logging.h>

#include <vector>
#include <iostream>
#include <cstdio>
#include <cmath>

// need the round function from C99
#include <math.h>

using namespace ::timber;
using namespace ::timber::logging;
using namespace ::tikal;

#define ENABLE_GPC_DEBUG 0

namespace {
#if ENABLE_GPC_DEBUG == 1
  static bool writeGPCPolygons = false;
#endif

  static Log log() { return Log("tikal.gpc"); }

  
  
  /**
   * Create a polygon from tristrip in specified in a vertex list.
   * @param v a vertex list representing a tristrip
   * @param dir the polygon orientation (1=CW, -1=CCW)
   */
  static Pointer<TriangleStrip2D> createTristripPolygon(const gpc_vertex_list& vlist)
  {
    const size_t sz = vlist.num_vertices;
    ::std::vector< Point2D> pts(sz);
    for (size_t i=0;i<sz;++i) {
      pts[i] = Point2D(vlist.vertex[i].x,vlist.vertex[i].y);
    }
    try {
      return TriangleStrip2D::create(pts);
    }
    catch (...) {
      return Pointer<TriangleStrip2D>();
    }
  }

  /**
   * Create a polygon from a vertex list.
   * @param v a vertex list
   */
  static Pointer<Polygon2D> createPolygon(const gpc_vertex_list& vlist)
  {
    ::std::vector< Point2D> pts;
    pts.reserve(vlist.num_vertices);
    for (int i=0;i<vlist.num_vertices;++i) {
      const Point2D pt(vlist.vertex[i].x,vlist.vertex[i].y);
      assert(!isnan(pt.x()) && !isnan(pt.y()));
      if (pts.empty() || pt!=pts[pts.size()-1]) {
	pts.push_back(pt);
      }
    }
    while (pts.size()>1 && pts[0]==pts[pts.size()-1]) {
      pts.pop_back();
    }
    if (pts.size()<3) {
      return Pointer<Polygon2D>();
    }
    return Polygon2D::create(pts);
  }

  /**
   * A tri-strip polygons
   * @param v a vertex list
   */
  static void createAreas(const gpc_tristrip& strip, ::std::vector<Reference<Area2D> >& areas)
  {
    const size_t nStrips = strip.num_strips;
    areas.reserve(areas.size()+nStrips*3); // at least this manyu points will be inserted
    for (size_t i=0;i<nStrips;++i) {
      Pointer<Area2D> A = createTristripPolygon(strip.strip[i]);
      if (A) {
	areas.push_back(A);
      }
    }
  }


  static Pointer<Region2D> createRegion(gpc_polygon& poly)
  {
    ::std::vector<Reference<Polygon2D> > internal,external;
    for (int i=0;i<poly.num_contours;++i) {
      Pointer<Polygon2D> p(createPolygon(poly.contour[i]));
      if (p) {
	if (poly.hole[i]) {
	  internal.push_back(p);
	}
	else {
	  external.push_back(p);
	}
      }
    }
    ::std::vector< Reference<Contour2D> > holes;
    ::std::vector< Reference<Area2D> > areas;
    for (size_t i=0;i<external.size();++i) {
      for (size_t j=0;j<internal.size();) {
	if (testContainment(external[i],internal[j])) {
	  holes.push_back(internal[j]);
	  internal[j] = internal[internal.size()-1];
	  internal.pop_back();
	}
	else {
	  ++j;
	}
      }
      areas.push_back(Area2D::create(external[i],holes));
      holes.clear();
    }
      
    if (areas.empty()) {
      return Pointer<Region2D>();
    }
    return Region2D::create(areas);
  }

  static void reducePrecision(Point2D& pt, double prec, gpc_vertex*& last, gpc_vertex*& out)
  {
    out->x = prec * round(pt.x()/prec);
    out->y = prec * round(pt.y()/prec);
    if (!last || (out->x != last->x) || (out->y != last->y)) {
      last= out;
      ++out;
    }
  }


  static gpc_vertex_list* createContour (const Contour2D& c)
  {
    if (c.maxDegree()!=1) {
      LogEntry(log()).warn() << "Maxdegree is not 1" << c.maxDegree() << doLog;
      return 0;
    }
    const size_t sCount = c.segmentCount();
    if (sCount < 3) {
      LogEntry(log()).warn() << "Too few points for contour " << sCount << doLog;
      return 0; // should never actually happen
    }
    gpc_vertex_list* vlist = (gpc_vertex_list*)malloc(sizeof(gpc_vertex_list));
    vlist->num_vertices = sCount;
    vlist->vertex = (gpc_vertex*)malloc(sCount*sizeof(gpc_vertex));

    for (size_t i=0;i<sCount;++i) {
      Point2D pts[2];
      size_t deg = c.segment(i,pts);
      assert (deg==1 && "Segments of degree > 1 are not supported at this time");
      vlist->vertex[i].x = pts[0].x();
      vlist->vertex[i].y = pts[0].y();
    }
    return vlist;
  }

  static gpc_vertex_list* createContour (const Contour2D& c, double prec)
  {
    if (prec==0) {
      return createContour(c);
    }

    if (c.maxDegree()!=1) {
      LogEntry(log()).warn() << "Maxdegree is not 1" << c.maxDegree() << doLog;
      return 0;
    }
    const size_t sCount = c.segmentCount();
    if (sCount < 3) {
      LogEntry(log()).warn() << "Too few points for contour " << sCount << doLog;
      return 0; // should never actually happen
    }
    gpc_vertex_list* vlist = (gpc_vertex_list*)malloc(sizeof(gpc_vertex_list));
    vlist->num_vertices = sCount;
    vlist->vertex = (gpc_vertex*)malloc(sCount*sizeof(gpc_vertex));
    gpc_vertex* out=vlist->vertex;
    gpc_vertex* last(0);
    
    for (size_t i=0;i<sCount;++i) {
      Point2D pts[2];
      size_t deg = c.segment(i,pts);
      assert (deg==1 && "Segments of degree > 1 are not supported at this time");
      reducePrecision(pts[0],prec,last,out);
    }
    vlist->num_vertices = out - vlist->vertex;
    return vlist;
  }

  static bool createPolygon (const Area2D& a, double prec, gpc_polygon& result)
  {
    size_t nContours = a.contourCount();
    gpc_vertex_list** contours = new gpc_vertex_list*[1+nContours];
    for (size_t i=0;i<=nContours;++i) {
      contours[i] = 0;
    }

    bool success=false;

    contours[0] = createContour(*a.outsideContour(),prec);

    if (contours[0]==0) {
      goto FAIL;
    }
    for (size_t i=1;i<=nContours;++i) {
      contours[i] = createContour(*a.insideContour(i-1),prec);
      if (contours[i]==0) {
	goto FAIL;
      }
    }
    gpc_add_contour(&result,contours[0],false);
    for (size_t i=1;i<=nContours;++i) {
      gpc_add_contour(&result,contours[i],true);
    }
    success = true;
  FAIL:
    for(size_t i=0;i<=nContours;++i) {
      if (contours[i]!=0) {
	free(contours[i]->vertex);
	free(contours[i]);
      }
    }
    delete[] contours;
    return success;
  }
    
  static bool createPolygon(const Region2D& a, double prec, gpc_polygon& result)
  {
    bool success=true;
    for (size_t i=0;i<a.areaCount();++i) {
      if (!createPolygon(*a.area(i),prec,result)) {
	success = false;
      }
    }
    return success;
  }


  static Pointer<Region2D> doGpcOp (gpc_op op, const Region2D& a, const Region2D& b, double prec=0.0)
  {
    gpc_polygon p1 = { 0,0,0 };
    gpc_polygon p2 = { 0,0,0 };
    gpc_polygon pR = { 0,0,0 };
    
    if (!createPolygon(a,prec,p1)) {
      throw ::std::runtime_error("Could not create gpc_polygon 1");
    }
    if (!createPolygon(b,prec,p2)) {
      throw ::std::runtime_error("Could not create gpc_polygon 1");
    }

#if ENABLE_GPC_DEBUG == 1
    if (writeGPCPolygons) {
      FILE* out = fopen("polygon1","w");
      gpc_write_polygon(out,1,&p1);
      fclose(out);

      out = fopen("polygon2","w");
      gpc_write_polygon(out,1,&p2);
      fclose(out);
    }
#endif

    gpc_polygon_clip(op,&p1,&p2,&pR);
    try {
      Pointer<Region2D> result = createRegion(pR);
      gpc_free_polygon(&p1);
      gpc_free_polygon(&p2);
      gpc_free_polygon(&pR);
      return result;
    }
    catch (...) {
      gpc_free_polygon(&p1);
      gpc_free_polygon(&p2);
      gpc_free_polygon(&pR);
      throw;
    }
  }
  
    
  class GPCAdapter : public Region2DOps {
    /** The implementation class */
  public:
    inline GPCAdapter() throws() {}

    /** The destructor */
  public:
    ~GPCAdapter() throws() {}
      
    Pointer<Region2D> merge (const Pointer<Region2D>& r1, const Pointer<Region2D>& r2) throws (::std::exception)
    {
      if (r1==r2 || !r1) { return r2; }
      if (!r2) { return r1; }
      
      return doGpcOp(GPC_UNION,*r1,*r2);
    }

    Pointer<Region2D> intersect (const Pointer<Region2D>& r1, const Pointer<Region2D>& r2) throws (::std::exception)
    {
      if (r1==r2) { return r1; }
      if (!r1 || !r2) { return Pointer<Region2D>(); }
      
      return doGpcOp(GPC_INT,*r1,*r2);
    }

    Pointer<Region2D> difference (const Reference<Region2D>& r1, const Pointer<Region2D>& r2) throws (::std::exception)
    {
      if (!r2) { return r1; }
      if (r1==r2) { return Pointer<Region2D>(); }

      return doGpcOp(GPC_DIFF,*r1,*r2);
    }
      
    Pointer<Region2D> exclusiveOr (const Pointer<Region2D>& r1, const Pointer<Region2D>& r2) throws (::std::exception)
    {
      if (!r1) { return r2; }
      if (!r2) { return r1; }
      if (r1==r2) { return Pointer<Region2D>(); }

      return doGpcOp(GPC_XOR,*r1,*r2);
    }
      
    Pointer<Region2D> normalize (const Pointer<Region2D>& r) throws(::std::exception)
    {
      if (!r) { return r; }

#if 0
      return doGpcOp(GPC_UNION,*r,*r);
#else
      Reference<Region2D> r2 = Polygon2D::create(r->bounds());
      return doGpcOp(GPC_INT,*r,*r2);
#endif
    }
    
    Pointer<Region2D> reducePrecision (const Pointer<Region2D>& r, double prec) throws (::std::exception)
    {
      if (!r || prec==0) {
	return r;
      }
#if ENABLE_GPC_DEBUG == 1
      writeGPCPolygons = true;
#endif
#if 0
      Pointer<Region2D> res = doGpcOp(GPC_UNION,*r,*r,prec);
#else
      Reference<Region2D> r2 = Polygon2D::create(r->bounds());
      Pointer<Region2D> res = doGpcOp(GPC_INT,*r,*r2,prec);
#endif
#if ENABLE_GPC_DEBUG == 1
      writeGPCPolygons = false;
#endif
      return res;
    }

    ::timber::Pointer<Region2D> createTriStripRegion (const ::timber::Pointer<Polygon2D>& r) throws (::std::exception)
    {
      if (!r) { return r; }
      {
	Pointer<TriangleStrip2D> strip = r.tryDynamicCast<TriangleStrip2D>();
	if (strip) {
	  return strip;
	}
      }
      
      // create a gpc polygon and convert it to a tripstrip
      ::std::vector<Reference<Area2D> > regs;
      gpc_polygon poly = { 0,0,0 };
      gpc_tristrip strip= { 0,0 };
      if (!createPolygon(*r,0,poly)) {
	log().warn("Could not create region");
	throw ::std::runtime_error("Could not remove holes from polygon; convert to gpc_polygon failed");
      }
      gpc_polygon_to_tristrip(&poly,&strip);
      gpc_free_polygon(&poly);
      try {
	createAreas(strip,regs);
	gpc_free_tristrip(&strip);
      }
      catch (const ::std::exception& ex) {
	log().caught("Could not remove holes from region",ex);
	gpc_free_tristrip(&strip);
      }
    
#if 0
      LogEntry(log()).debugging() << "tristrips: created " << regs.size() << " regions" << doLog;
#endif
      // loop over the areas again
      return Region2D::create(regs);
      
    }



    Pointer<Region2D> removeHoles (const Pointer<Region2D>& r) throws (::std::exception)
    {
      if (!r) { return r; }
      
      // check if the region has any holes at all
      bool holeFound = false;
      for (size_t i=0,sz=r->areaCount();i<sz;++i) {
	if (r->area(i)->contourCount() > 0) {
	  holeFound=true;
	  break;
	}
      }
      // no holes and we don't need to output convex only polygons
      // then we can just return the input polygon
      if (!holeFound) {
	return r;
      }

      ::std::vector<Reference<Area2D> > regs;
      regs.reserve(r->areaCount());
      for (size_t i=0,sz=r->areaCount();i<sz;++i) {
	Reference<Area2D> A=r->area(i);
	if (A->contourCount() == 0) {
	  regs.push_back(A);
	}
	else {
	  // create a gpc polygon and convert it to a tripstrup
	  gpc_polygon poly = { 0,0,0 };
	  gpc_tristrip strip= { 0,0 };
	  if (!createPolygon(*A,0,poly)) {
	    log().warn("Could not create region");
	    throw ::std::runtime_error("Could not remove holes from polygon; convert to gpc_polygon failed");
	  }
	  gpc_polygon_to_tristrip(&poly,&strip);
	  gpc_free_polygon(&poly);
	  try {
	    createAreas(strip,regs);
	    gpc_free_tristrip(&strip);
	  }
	  catch (const ::std::exception& ex) {
	    log().caught("Could not remove holes from region",ex);
	    gpc_free_tristrip(&strip);
	  }
	}
      }

#if 0
      LogEntry(log()).debugging() << "RemoveHoles: created " << regs.size() << " regions" << doLog;
#endif
      // loop over the areas again
      return Region2D::create(regs);
    }
  };
}

namespace tikal {
  namespace gpc {
    ::std::unique_ptr<Region2DOps> createRegion2DOps()
    { return ::std::unique_ptr<Region2DOps>(new GPCAdapter()); }
  }
}
