#ifndef _TIKAL_GPC_GPCADAPTER_H
#define _TIKAL_GPC_GPCADATPER_H

#include <memory>

namespace tikal {
  class Region2DOps;
  namespace gpc {
    
    /**
     * Get an implementation of Region2DOps using gpc.
     * @return an region 2d ops implementation
     */
    ::std::unique_ptr<Region2DOps> createRegion2DOps();
  }
}
#endif
