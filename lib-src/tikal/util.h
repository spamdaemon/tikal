#ifndef _TIKAL_UTIL_H
#define _TIKAL_UTIL_H

#ifndef _TIKAL_H
#include <tikal/tikal.h>
#endif

#ifndef _TIKAL_SWEEPLINE_H
#include <tikal/SweepLine.h>
#endif

#include <vector>


namespace tikal {
  class BoundingBox2D;
  class Path2D;
  class Contour2D;
  class Area2D;
  class Polygon2D;
  class Region2D;

  /**
   * Clip a path against a region
   * @param p a path
   * @param r a region
   * @return a path that is confined to the specified region
   */
  ::timber::Pointer<Path2D> clipPath(const ::timber::Reference<Path2D>& p, const ::timber::Reference<Region2D>& r);
     
  /**
   * Clip a path against a bounding box.
   * @param p a path
   * @param b a bounding box
   * @return a path that is confined to the specified box
   */
  ::timber::Pointer<Path2D> clipPath(const ::timber::Reference<Path2D>& p, const BoundingBox2D& b);

  /**
   * Add a region to a sweepline.
   * @param region the region to be added
   * @param dir the vertex order in which the path should be added
   * @param data the data value to be associated with the region
   * @param sweepline the sweepline
   */
  void insertRegion (const ::timber::Reference<Region2D>& region, const class VertexOrder& dir, SweepLine::Data data, SweepLine& sweepline);

  /**
   * Add a region to a sweepline.
   * @param region the region to be added
   * @param data the data value to be associated with the region
   * @param sweepline the sweepline
   */
  void insertRegion (const ::timber::Reference<Region2D>& region, SweepLine::Data data, SweepLine& sweepline);

  /**
   * Add a region to a sweepline.
   * @param region the region to be added
   * @param dir the vertex order in which the path should be added
   * @param data the data value to be associated with the region
   * @param sweepline the sweepline
   */
  void insertArea (const ::timber::Reference<Area2D>& region, const class VertexOrder& dir, SweepLine::Data data, SweepLine& sweepline);

  /**
   * Add a region to a sweepline.
   * @param region the region to be added
   * @param data the data value to be associated with the region
   * @param sweepline the sweepline
   */
  void insertArea (const ::timber::Reference<Area2D>& region, SweepLine::Data data, SweepLine& sweepline);

  /**
   * Add a contour to a sweepline.
   * @param region the region to be added
   * @param dir the vertex order in which the path should be added
   * @param data the data value to be associated with the region
   * @param sweepline the sweepline
   */
  void insertContour (const ::timber::Reference<Contour2D>& region, const class VertexOrder& dir, SweepLine::Data data, SweepLine& sweepline);

  /**
   * Add a contour to a sweepline.
   * @param region the region to be added
   * @param data the data value to be associated with the region
   * @param sweepline the sweepline
   */
  void insertContour (const ::timber::Reference<Contour2D>& region, SweepLine::Data data, SweepLine& sweepline);

  /**
   * Add a region to a sweepline.
   * @param region the region to be added
   * @param dir the vertex order in which the path should be added
   * @param data the data value to be associated with the region
   * @param sweepline the sweepline
   */
  void insertPolygon (const ::timber::Reference<Polygon2D>& region, const class VertexOrder& dir, SweepLine::Data data, SweepLine& sweepline);

  /**
   * Add a region to a sweepline.
   * @param region the region to be added
   * @param data the data value to be associated with the region
   * @param sweepline the sweepline
   */
  void insertPolygon (const ::timber::Reference<Polygon2D>& region, SweepLine::Data data, SweepLine& sweepline);

  /**
   * Insert a path into a sweepline
   * @param path the path to be added
   * @param dir the vertex order in which the path should be added
   * @param data the data for the associated region
   * @param sweepline the sweepline
   */
  void insertPath (const class ::timber::Reference<Path2D>& path, const class VertexOrder& dir, SweepLine::Data data, SweepLine& sweepline);

  /**
   * Insert a path into a sweepline
   * @param path the path to be added
   * @param data the data for the associated region
   * @param sweepline the sweepline
   */
  void insertPath (const class ::timber::Reference<Path2D>& path, SweepLine::Data data, SweepLine& sweepline);

  /**
   * Create an array of contours from a path.
   * @param path a path
   * @pre REQUIRE_TRUE(path.maxDegree()==1)
   * @return a vector of contours
   */
  ::std::vector< ::timber::Reference<Contour2D> > createContours (const ::timber::Reference<Path2D>& path);

  /**
   * Determine if two non-intersecting polygons have a containment relation. This
   * function may throw an exception if it can easily determine that the contours
   * intersect or are exactly the same.
   * @param outer a contour
   * @param inner a contour
   * @return if outer encloses inner completely, false if inner lies completely outside
   * @throws ::std::exception if some sort of error occurred
   */
  bool testContainment (const ::timber::Reference<Polygon2D>& outer, const ::timber::Reference<Polygon2D>& inner) throws (::std::exception);

  /**
   * Determine if two non-intersecting contours have a containment relation. This
   * function may throw an exception if it can easily determine that the contours
   * intersect or are exactly the same.
   * @note this function does not support non-polygonal contours
   * @param outer a contour
   * @param inner a contour
   * @return if outer encloses inner completely, false if inner lies completely outside
   * @throws ::std::exception if some sort of error occurred
   */
  bool testContainment (const ::timber::Reference<Contour2D>& outer, const ::timber::Reference<Contour2D>& inner) throws (::std::exception);
}
#endif
