#ifndef _TIKAL_POLYGON2D_H
#define _TIKAL_POLYGON2D_H

#ifndef _TIKAL_CONTOUR2D_H
#include <tikal/Contour2D.h>
#endif

#include <iosfwd>
#include <vector>

namespace tikal {
  class VertexOrder;

  /** The polygon ementation class */
  class Polygon2D : public Contour2D {
    Polygon2D(const Polygon2D&);
    Polygon2D&operator=(const Polygon2D&);
    
    /**
     * Create a new polygon
     */
  protected:
    Polygon2D() throws();
    
    
    /** Destroy this ementation */
  public:
    ~Polygon2D() throws();
    
    /**
     * @name Static constructors
     * @{
     */
  
    /** 
     * Create a new polygon. The points of the polygon are encoded in the
     * array such that point (x_i,y_i) is at position pts[2*i] and pts[2*i+1],
     * respectively.
     * @param n the number of points
     * @param pts a pointer to n*2 doubles
     * @param dir -1 for CCW order, 1 for CW order, 0 if unknown
     * @pre n>2
     */
  public:
    static  ::timber::Reference<Polygon2D> create (size_t n, const double* pts, int dir=0) throws (::std::exception);
      

    /** 
     * Create a new polygon. 
     * @param n the number of points
     * @param pts a pointer to n Point2D
     * @param dir -1 for CCW order, 1 for CW order, 0 if unknown
     * @pre n>2
     */
  public:
    static  ::timber::Reference<Polygon2D> create (size_t n, const Point2D* pts, int dir=0) throws (::std::exception);

    /** 
     * Create a new polygon. 
     * @param pts a pointer to n Point2D
     * @param dir -1 for CCW order, 1 for CW order, 0 if unknown
     * @pre pts.size()>2
     */
  public:
    static  ::timber::Reference<Polygon2D> create (const ::std::vector<Point2D>& pts, int dir=0) throws (::std::exception);

    /** 
     * Create a new polygon which is a triangle. The order of the vertices
     * can either be left unspecified or can be specified as cw or ccw. If left
     * unspecified, then an order is computed.
     *
     * @param p first point
     * @param q the second point
     * @param r the third point
     * @param dir -1 for CCW order, 1 for CW order, 0 if unknown
     * @return a convex polygon
     */
  public:
    static  ::timber::Reference<Polygon2D> create (const Point2D& p, const Point2D& q, const Point2D& r, int dir=0) throws (::std::exception);
    
    /** 
     * Create a new polygon which is a triangle. The order of the vertices
     * can either be left unspecified or can be specified as cw or ccw. If left
     * unspecified, then an order is computed.
     *
     * @param x1 the x-coordinate of the first point
     * @param y1 the y-coordinate of the first point
     * @param x2 the x-coordinate of the second point
     * @param y2 the y-coordinate of the second point
     * @param x3 the x-coordinate of the third point
     * @param y3 the y-coordinate of the third point
     * @pre (x1!=x2 || y1!=y2)
     * @pre (x2!=x3 || y2!=y3)
     * @pre (x1!=x3 || y1!=y3)
     * @param dir -1 for CCW order, 1 for CW order, 0 if unknown
     * @return a convex polygon
     */
  public:
    static  ::timber::Reference<Polygon2D> create (double x1,double y1, double x2, double y2, double x3, double y3, int dir=0) throws (::std::exception);
    
    
    /**
     * Create a new polygon corresponding to the specified 
     * bounding box. The order in which the vertices of the
     * bounding box should be enumerated, is by default clockwise.
     * 
     * @param box a bounding box
     * @param vo the desired vertex order for the box vertices
     * @return a convex polygon
     */
  public:
    static  ::timber::Reference<Polygon2D> create (const BoundingBox2D& box, const VertexOrder vo = VertexOrder::CW) throws();
  
    /** 
     * Create a new polygon. The points of the polygon are encoded in the
     * array such that point (x_i,y_i) is at position pts[2*i] and pts[2*i+1],
     * respectively.
     * @param n the number of points
     * @param pts a pointer to n*2 doubles
     * @param dir -1 for CCW order, 1 for CW order, 0 if unknown
     * @pre n>2
     * @return a polygon
     */
  public:
    static  ::timber::Reference<Polygon2D> create (size_t n, const double* pts, const VertexOrder& dir) throws (::std::exception);
      

    /** 
     * Create a new polygon. 
     * @param n the number of points
     * @param pts a pointer to n Point2D
     * @param dir -1 for CCW order, 1 for CW order, 0 if unknown
     * @pre n>2
      * @return a polygon
    */
  public:
    static  ::timber::Reference<Polygon2D> create (size_t n, const Point2D* pts, const VertexOrder& dir) throws (::std::exception);

    /** 
     * Create a new polygon. 
     * @param pts a pointer to n Point2D
     * @param dir -1 for CCW order, 1 for CW order, 0 if unknown
     * @pre pts.size()>2
     * @return a polygon
     */
  public:
    static  ::timber::Reference<Polygon2D> create (const ::std::vector<Point2D>& pts, const VertexOrder& dir) throws (::std::exception);

    /** 
     * Create a new polygon which is a triangle 
     * @param x1 the x-coordinate of the first point
     * @param y1 the y-coordinate of the first point
     * @param x2 the x-coordinate of the second point
     * @param y2 the y-coordinate of the second point
     * @param x3 the x-coordinate of the third point
     * @param y3 the y-coordinate of the third point
     * @pre (x1!=x2 || y1!=y2)
     * @pre (x2!=x3 || y2!=y3)
     * @pre (x1!=x3 || y1!=y3)
     * @param dir -1 for CCW order, 1 for CW order, 0 if unknown
     * @return a polygon
     */
  public:
    static  ::timber::Reference<Polygon2D> create (double x1,double y1, double x2, double y2, double x3, double y3, const VertexOrder& dir) throws (::std::exception);

    /*@}*/
    
    /** 
     * Compute the square of minimum Cartesian distance between two polygons.
     * @param pt a polygon
     * @return the square of the distance between the two polygons.
     */
  public:
    double sqrDistance (const ::timber::Reference<Polygon2D>& pt) const throws();
      
    /** 
     * Compute the minimum Cartesian distance between two polygons.
     * @param p a polygon
     * @return the distance between the two polygons.
     */
  public:
    inline double distance (const ::timber::Reference<Polygon2D>& p) const throws()
    { return ::std::sqrt(sqrDistance(p)); }
    
    /**
     * Create a region from this polygon that consists only of TriangleStrip2D polygons
     * @return a region2D where each area is an instance of TriangleStrip2D or null if an error occurred
     */
  public:
    virtual ::timber::Pointer<Region2D> toTriangleStrips() const throws();

    /**
     * Get the number of vertices of this polygon.
     * @return the number of vertices
     */
  public:
    virtual size_t vertexCount() const throws() = 0;

    /**
     * Get the specified vertex.
     * @param i a vertex index
     * @return the vertex at the specified index
     */
  public:
    virtual Point2D vertex (size_t i) const throws() = 0;
    
    
    /**
     * Get the number of segments in this contour. Calls the
     * method <code>vertexCount()</code>
     * @return <code>vertexCount()</code>
     */
  public:
    size_t segmentCount() const throws();

    /**
     * Get the number of points defining the segment with the highest degree.
     * @return always 1
     */
  public:
    size_t maxDegree() const throws();

    /**
     * Ge the vertices used to define the specified segment. The
     * points array must be large enough to hold maxDegree()+1
     * points. Subclasses should override this to provide a more 
     * efficient ementation.
     * @param i a segment index
     * @param pts the vertices are stored in traversal order
     * @return always 1
     */
  public:
    size_t segment (size_t i, Point2D* pts) const throws();

    /**
     * Print this polygon
     * @param out an output stream
     */
  public:
    void print (::std::ostream& out) const;
  };
}

#endif
