#include <tikal/util.h>
#include <tikal/SweepLine.h>
#include <tikal/Region2D.h>
#include <tikal/Area2D.h>
#include <tikal/Contour2D.h>
#include <tikal/Polygon2D.h>
#include <tikal/VertexOrder.h>
#include <tikal/Path2D.h>
#include <tikal/Point2D.h>
#include <tikal/Path2DBuilder.h>
#include <tikal/BoundingBox2D.h>
#include <iostream>

using namespace ::std;
using namespace ::timber;

namespace tikal {
  namespace {
    static void checkMaxDegree (const Reference<Path2D>& p)
    {
      if (p->maxDegree()>1) {
	throw invalid_argument("Non-polygonal paths are not supported");
      }
    }
    static void checkMaxDegree (const Reference<Contour2D>& p)
    {
      if (p->maxDegree()>1) {
	throw invalid_argument("Non-polygonal contours are not supported");
      }
    }

    struct Clipper : public SweepLine::Callback {
      Clipper (const Reference<Path2D>& pp, const Reference<Region2D>& reg)
	: _region(reg)
      {
	insertRegion(reg,0,_line);
	insertPath(pp,1,_line);
      }
      Pointer<Path2D> getPath() 
      {
	_line.sweepSegments(*this);
	return _builder.path();
      }

      void notify(const Point2D& start, const Point2D& end, int code, const SweepLine::SegmentData& data)  throws()
      {
	if (data.data()==1) {
	  const Point2D mid(0.5*(start.x()+end.x()),0.5*(start.y()+end.y()));
	  if (_region->contains(mid)) {
	    _builder.moveTo(start);
	    _builder.lineTo(end);
	  }
	}
      }

    private:
      Reference<Region2D> _region;
      SweepLine _line;
      Path2DBuilder _builder;
    };
    struct BoxClipper : public SweepLine::Callback {
      BoxClipper (const Reference<Path2D>& c, const BoundingBox2D& b)
	: _region(b)
      {
	checkMaxDegree(c);

	_line.insertUnorderedSegment(b.topLeft(),b.topRight(),VertexOrder::cw(),0);
	_line.insertUnorderedSegment(b.topRight(),b.bottomRight(),VertexOrder::cw(),0);
	_line.insertUnorderedSegment(b.bottomRight(),b.bottomLeft(),VertexOrder::cw(),0);
	_line.insertUnorderedSegment(b.bottomLeft(),b.topLeft(),VertexOrder::cw(),0);

       
	vector<Point2D> pts(c->maxDegree()+1);
	Point2D last;
	int lastClassify(-1);
	bool lastInside=false;
	bool needMove = true;
	for (size_t i=0,sz=c->segmentCount();i<sz;++i) {
	  size_t n = c->segment(i,&pts[0]);
	  int classify= _region.classify(pts[0]);
	    
	  if (n==0) {
	    last = pts[0];
	    lastClassify = classify;
	    lastInside = classify==0;
	    needMove = true;
	  }
	  else if (lastInside && classify==0) {
	    if (needMove) {
	      needMove = false;
	      _builder.moveTo(last);
	    }
	    _builder.lineTo(pts[0]);
	    last = pts[0];
	    assert(lastInside);
	    assert(lastClassify==0);
	  }
	  else if ((lastClassify & classify)!=0) {
	    needMove = true;
	    assert(!lastInside);
	    lastClassify = classify;
	    last = pts[0];
	  }
	  else {
	    lastClassify = classify;
	    lastInside = false;
	    needMove = true;
	    _line.insertUnorderedSegment(last,pts[0],VertexOrder::cw(),1);
	    last = pts[0];
	  }
	}
      }
	

      Pointer<Path2D> getPath() 
      {
	_line.sweepSegments(*this);
	return _builder.path();
      }

      void notify(const Point2D& start, const Point2D& end, int code, const SweepLine::SegmentData& data)  throws()
      {
	if (data.data()==1) {
	  const Point2D mid(0.5*(start.x()+end.x()),0.5*(start.y()+end.y()));
	  if (_region.contains(mid)) {
	    _builder.moveTo(start);
	    _builder.lineTo(end);
	  }
	}
      }

    private:
      BoundingBox2D _region;
      SweepLine _line;
      Path2DBuilder _builder;
    };
  }

  Pointer<Path2D> clipPath(const Reference<Path2D>& p, const Reference<Region2D>& r)
  {
    if (r->bounds().intersects(p->bounds())) {
      Clipper cb(p,r);
      return cb.getPath();
    }
    else {
      return Pointer<Path2D>();
    }
  }

  Pointer<Path2D> clipPath(const Reference<Path2D>& p, const BoundingBox2D& b)
  {
    if (b.encloses(p->bounds())) {
      return p;
    }
    else if (b.intersects(p->bounds())) {
      BoxClipper cb(p,b);
      return cb.getPath();
    }
    return Pointer<Path2D>();
  }

    

  void insertRegion (const Reference<Region2D>& region, const VertexOrder& dir, SweepLine::Data data, SweepLine& sweepline)
  {
    const size_t aCount = region->areaCount();
    for (size_t i=0;i<aCount;++i) {
      insertArea(region->area(i),dir,data,sweepline);
    }
  }

  void insertArea (const Reference<Area2D>& area, const VertexOrder& dir, SweepLine::Data data, SweepLine& sweepline)
  {
    insertContour(area->outsideContour(),dir,data,sweepline);
    for (size_t i=0,sz=area->contourCount();i<sz;++i) {
      insertContour(area->insideContour(i),dir.reverse(),data,sweepline);
    }
  }
     
  void insertContour (const Reference<Contour2D>& c, const VertexOrder& dir, SweepLine::Data data, SweepLine& sweepline)
  {
    const size_t sCount = c->segmentCount();
    if (sCount==0) {
      return;
    }
    vector<Point2D> pts(c->maxDegree()+1);
    if (c->vertexOrder()==dir) {
      for (size_t i=0;i<sCount;++i) {
	size_t deg = c->segment(i,&pts[0]);
	assert (deg==1 && "Segments of degree > 1 are not supported at this time");
	sweepline.insertUnorderedSegment(pts[0],pts[1],c->vertexOrder(),data);
      }
    }
    else {
      for (size_t i=0;i<sCount;++i) {
	size_t deg = c->segment(i,&pts[0]);
	assert (deg==1 && "Segments of degree > 1 are not supported at this time");
	sweepline.insertUnorderedSegment(pts[1],pts[0],dir,data);
      }
    }
  }
    

  void insertPolygon (const Reference<Polygon2D>& region, const VertexOrder& dir, SweepLine::Data data, SweepLine& sweepline)
  {

    const size_t vCount = region->vertexCount();
    Point2D pt(region->vertex(0));
    if (dir==region->vertexOrder()) {
      for (size_t i=1;i<vCount;++i) {
	const Point2D tmp(region->vertex(0));
	sweepline.insertUnorderedSegment(pt,tmp,region->vertexOrder(),data);
	pt = tmp;
      }
      sweepline.insertUnorderedSegment(pt,region->vertex(0),region->vertexOrder(),data);
    }
    else {
      for (size_t i=1;i<vCount;++i) {
	const Point2D tmp(region->vertex(0));
	sweepline.insertUnorderedSegment(tmp,pt,dir,data);
	pt = tmp;
      }
      sweepline.insertUnorderedSegment(region->vertex(0),pt,dir,data);
    }
  }
    
  void insertRegion (const Reference<Region2D>& region, SweepLine::Data data, SweepLine& sweepline)
  {
    const size_t aCount = region->areaCount();
    for (size_t i=0;i<aCount;++i) {
      insertArea(region->area(i),data,sweepline);
    }
  }

  void insertArea (const Reference<Area2D>& area, SweepLine::Data data, SweepLine& sweepline)
  {
    insertContour(area->outsideContour(),data,sweepline);
    for (size_t i=0,sz=area->contourCount();i<sz;++i) {
      insertContour(area->insideContour(i),data,sweepline);
    }
  }
     
  void insertContour (const Reference<Contour2D>& c, SweepLine::Data data, SweepLine& sweepline)
  {
    const size_t sCount = c->segmentCount();
    if (sCount==0) {
      return;
    }
    vector<Point2D> pts(c->maxDegree()+1);
    for (size_t i=0;i<sCount;++i) {
      size_t deg = c->segment(i,&pts[0]);
      assert (deg==1 && "Segments of degree > 1 are not supported at this time");
      sweepline.insertUnorderedSegment(pts[0],pts[1],c->vertexOrder(),data);
    }
  }
       

  void insertPolygon (const Reference<Polygon2D>& region, SweepLine::Data data, SweepLine& sweepline)
  {
    const size_t vCount = region->vertexCount();
    Point2D pt(region->vertex(0));
    for (size_t i=1;i<vCount;++i) {
      const Point2D tmp(region->vertex(0));
      sweepline.insertUnorderedSegment(pt,tmp,region->vertexOrder(),data);
      pt = tmp;
    }
    sweepline.insertUnorderedSegment(pt,region->vertex(0),region->vertexOrder(),data);
  }
    
  void insertPath (const Reference<Path2D>& c, const VertexOrder& dir, SweepLine::Data data, SweepLine& sweepline)
  {
    checkMaxDegree(c);
       
    vector<Point2D> pts(c->maxDegree()+1);
    Point2D last;
    for (size_t i=0,sz=c->segmentCount();i<sz;++i) {
      size_t n = c->segment(i,&pts[0]);
      if (n==0) {
	last = pts[0];
      }
      else {
	sweepline.insertUnorderedSegment(last,pts[0],dir,data);
	last = pts[0];
      }
    }
  }
       
  void insertPath (const Reference<Path2D>& c, SweepLine::Data data, SweepLine& sweepline)
  { insertPath(c,VertexOrder::cw(),data,sweepline); }

  ::std::vector<Reference<Contour2D> > createContours (const Reference<Path2D>& path)
  {
    ::std::vector<Reference<Contour2D> > polys;
    if (path->maxDegree()==0) {
      return polys;
    }
    if (path->maxDegree()>1) {
      ::std::cerr << __FILE__<<":"<<__LINE__<<":paths of degrees >1 not supported" << ::std::endl;
      return polys;
    }

    ::std::vector<Point2D> v;
    Point2D points[1];
    for (size_t i=0,sz=path->segmentCount();i<sz;++i) {
      size_t deg = path->segment(i,points);
      if (deg==0) {
	if(v.size()>2) {
	  if (v[0]==v[v.size()-1]) {
	    v.pop_back();
	  }
	  if (v.size()>2) {
	    polys.push_back(Polygon2D::create(v));
	  }
	}
	v.clear();
	v.push_back(points[0]);
      }
      else {
	v.push_back(points[deg-1]);
      }
    }

    if(v.size()>2) {
      if (v[0]==v[v.size()-1]) {
	v.pop_back();
      }
      if (v.size()>2) {
	polys.push_back(Polygon2D::create(v));
      }
    }

    return polys;
  }
    

  bool testContainment (const Reference<Polygon2D>& outer, const Reference<Polygon2D>& inner) throws(::std::exception)
  {
    // find at least 1 point of inner that lies inside outer or outside outer
    for (size_t i=0,sz=inner->vertexCount();i<sz;++i) {
      int where = outer->locatePoint(inner->vertex(i));
      if (where != 0) {
	return where < 0;
      }
    }
    Point2D p=inner->vertex(inner->vertexCount()-1);
    for (size_t i=0,sz=inner->vertexCount();i<sz;++i) {
      Point2D q = inner->vertex(i);
      double x = 0.5*(q.x()+p.x());
      double y = 0.5*(q.y()+p.y());
      int where = outer->locatePoint(x,y);
      if (where != 0) {
	return where < 0;
      }
      p = q;
    }

    // all points lie on the boundary of outer
    for (size_t i=0,sz=outer->vertexCount();i<sz;++i) {
      int where = inner->locatePoint(outer->vertex(i));
      if (where != 0) {
	return where > 0;
      }
    }
    throw ::std::runtime_error("Polygons are identical");
  }
    
  bool testContainment (const Reference<Contour2D>& outer, const Reference<Contour2D>& inner) throws(::std::exception)
  {
    checkMaxDegree(outer);
    checkMaxDegree(inner);

    // find at least 1 point of inner that lies inside outer or outside outer
    Point2D tmp[2];
    for (size_t i=0,sz=inner->segmentCount();i<sz;++i) {
      size_t deg = inner->segment(i,tmp);
      int where = outer->locatePoint(tmp[0]);
      if (where != 0) {
	return where < 0;
      }
    }
    for (size_t i=0,sz=inner->segmentCount();i<sz;++i) {
      size_t deg = inner->segment(i,tmp);
      double x = 0.5*(tmp[0].x()+tmp[1].x());
      double y = 0.5*(tmp[0].y()+tmp[1].y());
      int where = outer->locatePoint(x,y);
      if (where != 0) {
	return where < 0;
      }
    }

    // all points lie on the boundary of outer
    for (size_t i=0,sz=outer->segmentCount();i<sz;++i) {
      size_t deg = outer->segment(i,tmp);
      int where = inner->locatePoint(tmp[0]);
      if (where != 0) {
	return where > 0;
      }
    }
    throw ::std::runtime_error("Contours are identical");
  }
    
}
