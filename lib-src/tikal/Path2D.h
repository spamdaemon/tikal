#ifndef _TIKAL_PATH2D_H
#define _TIKAL_PATH2D_H

#ifndef _TIKAL_H
#include <tikal/tikal.h>
#endif

#ifndef _TIKAL_POINT2D_H
#include <tikal/Point2D.h>
#endif

#include <iosfwd>

namespace tikal {
  /** A bounding box */
  class BoundingBox2D;
    
  /**
   * A path is defined by a connected set of vertices and may possibly
   * be closed.
   */
  class Path2D : public ::timber::Counted {
    Path2D(const Path2D&);
    Path2D&operator=(const Path2D&);

    /** Create a new path impl */
  protected:
    Path2D() throws();
      
    /** Destroy this path  impl */
  public:
    ~Path2D() throws();

    /**
     * Concatenate two paths.
     * @param p1 a path or null
     * @param p2 a path or null
     * @return a single path containing the paths from p1 and p2 or null
     */
  public:
    static ::timber::Pointer<Path2D> concatenate (const ::timber::Pointer<Path2D>& p1, const ::timber::Pointer<Path2D>& p2) throws();

    /**
     * Get the number of points defining the segment with the highest degree.
     * @return the maximum degree among all segments
     */
  public:
    virtual size_t maxDegree() const throws() = 0;

    /**
     * Get the number of segments in this path. A segment
     * is an arbitary polynomial curve between the two
     * segment endpoints.
     * @return the number of segments.
     */
  public:
    virtual size_t segmentCount() const throws() = 0;

    /**
     * Get the specified segment. Each segment is a series of points
     * defined from the last point of the previous segment. If the
     * degree of this new segment is 0, then it a move to a new location
     * instead. The points array must be large enough to hold maxDegree() 
     * points.
     * @param i a segment index
     * @pre i<segmentCount()
     * @param pts the vertices are stored in traversal order
     * @return the degree of the given segment.
     */
  public:
    virtual size_t segment (size_t i, Point2D* pts) const throws() = 0;
      
    /**
     * Test if this path contains the specified point.
     * @param x the x-coordinate of a point
     * @param y the y-coordinate of a point
     * @deprecated this function is not implemented and will most likely go away
     */
  public:
    virtual bool contains (double x, double y) const throws() = 0;

    /**
     * Get the bounding box for this region.
     * @return a bounding box.
     */
  public:
    virtual BoundingBox2D bounds() const throws() = 0;

    /**
     * Print this path
     */
  public:
    virtual void print (::std::ostream& out) const;

  };
}

inline ::std::ostream& operator<< (::std::ostream& out, const ::tikal::Path2D& b)
{ b.print(out); return out; }


#endif
