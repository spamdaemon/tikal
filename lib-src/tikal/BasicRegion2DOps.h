#ifndef _TIKAL_BASICREGION2DOPS_H
#define _TIKAL_BASICREGION2DOPS_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace tikal {
  class Region2DOps;
  
  /**
   * This utility class provides a default implementation of a 
   * the region2D ops interface.
   * 
   */
  class BasicRegion2DOps  {
    BasicRegion2DOps(const BasicRegion2DOps&);
    BasicRegion2DOps&operator=(const BasicRegion2DOps&);
    BasicRegion2DOps();	
    ~BasicRegion2DOps();
    
    /**
     * Create a new region ops object.
     * @return a new region ops object
     */
  public:
    static ::std::unique_ptr<Region2DOps> create() throws();
  };
}
#endif
