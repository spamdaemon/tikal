#include <tikal/Path2D.h>
#include <tikal/Point2D.h>
#include <tikal/BoundingBox2D.h>

#include <cstdlib>
#include <iostream>

using namespace ::std;
using namespace ::timber;

namespace tikal {
  namespace {
    struct PImpl : public Path2D {
      PImpl (const Reference<Path2D>& a, const Reference<Path2D>& b)
	: _first(a),_second(b),
	  _maxDegree(max(a->maxDegree(),b->maxDegree())),
	  _firstSize(a->segmentCount()),
	  _bounds(a->bounds().merge(b->bounds()))
      {
      }

      ~PImpl() throws() {}

      size_t maxDegree() const throws()
      { return _maxDegree; }
       
      BoundingBox2D bounds() const throws() 
      { return _bounds; }
       
      bool contains (double x, double y) const throws()
      { return _first->contains(x,y) || _second->contains(x,y); }

      size_t segmentCount() const throws()
      { return _firstSize+_second->segmentCount(); }
	
      size_t segment (size_t i, Point2D* pts) const throws() 
      {
	if (i < _firstSize) {
	  return _first->segment(i,pts);
	}
	return _second->segment(i-_firstSize,pts);
      }
      
    private:
      Reference<Path2D> _first,_second;
      const size_t _maxDegree;
      const size_t _firstSize;
      const BoundingBox2D _bounds;
    };
  }
  
  
  Path2D::Path2D() throws()
  {}
  
  Path2D::~Path2D() throws()
  {}

  Pointer<Path2D>  Path2D::concatenate (const Pointer<Path2D>& p1, const Pointer<Path2D>& p2) throws()
  {
    if (!p1) { return p2; }
    if (!p2) { return p1; }

    return  new PImpl(p1,p2); 
  }
  
  void Path2D::print (::std::ostream& out) const
  { 
    Point2D* x = new Point2D[1+maxDegree()];
    try {
      out << "Path2D [" << endl;
      for (size_t i=0,sz=segmentCount();i<sz;++i) {
	size_t deg = segment(i,x);
	
	out << "    ";
	if (deg==0) {
	  out << "move-to " << x[0] << endl;
	}
	else if (deg==1) {
	  out << "line-to " << x[0] << endl;
	}
	else {
	  out << "curve-to ";
	  for (size_t j=0;j<deg;++j) {
	    out << " " << x[j];
	  }
	  out << endl;
	}
      }
    }
    catch (...) {
      delete [] x;
      throw;
    }
    delete [] x;
    out << "]" << endl;
  }

}
