#include <tikal/BasicRegion2DOps.h>
#include <tikal/Region2DOps.h>
#include <tikal/AbstractRegion2D.h>
#include <tikal/AbstractArea2D.h>
#include <tikal/Contour2D.h>
#include <tikal/TriangleStrip2D.h>
#include <tikal/BoundingBox2D.h>
#include <tikal/SweepLine.h>

#include <tikal/graph/PlanarGraph.h>
#include <tikal/graph/Direction.h>
#include <tikal/graph/AbstractGraphCallback.h>
#include <tikal/graph/DirectedContourTracer.h>
#include <tikal/graph/Node.h>

#include <tikal/util.h>

#include <timber/logging.h>
#include <iostream>

#define DEBUG_OFF

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;
using namespace ::tikal::graph;

namespace tikal {
  namespace {
    static Log logger() { return Log("tikal.BasicRegion2DOps"); }

    struct BasicGraphHelper : public SweepLine::Callback {
      inline BasicGraphHelper (PlanarGraph& b)
	: _builder(b),_regionCount(0) {}
      ~BasicGraphHelper() throws() {}

      void notify(const Point2D& start, const Point2D& end, int code, const SweepLine::SegmentData& data)  throws()
      {
	if (_regionCount >0 && code!=3) {

	  if ((code & 2)==0) {
	    for (size_t i=0;i<_regionCount;++i) {
		  
	      if (i!=data.data()) {
		if (!_regions[i]->contains(start)) {
		  return;
		}
	      }
	    }
	  }
	  if ((code & 1)==0) {
	    for (size_t i=0;i<_regionCount;++i) {
	      if (i!=data.data()) {
		if (!_regions[i]->contains(end)) {
		  return;
		}
	      }
	    }
	  }
	}

	if (data.direction()!=data.sweepDirection()) {
	  _builder.addSegment(end,start,data.direction(),data.data());
	}
	else {
	  _builder.addSegment(start,end,data.direction(),data.data());
	}
      }
	
      PlanarGraph& _builder;
      size_t _regionCount;
      Pointer<Region2D> _regions[2];
    };
	

    struct BasicSweepLine : public SweepLine {
      BasicSweepLine(PlanarGraph& b) throws()
      : _code(0),_haveBox(false),_builder(b)
      {}

      ~BasicSweepLine() throws()
      {}

      inline void setBounds (const BoundingBox2D& b) throws() 
      { _box = b; _haveBox = true; }
      inline void clearBounds () throws() 
      { _haveBox=false; }
      inline void setRegion (const Reference<Region2D>& r, int code) throws() 
      {
	if (r->isConvex()) {
	  _code = code;
	  _region = r; 
	}
	else {
	  _region = Pointer<Region2D>();
	}
      }
      inline void clearRegion() throws() 
      {
	_region =Pointer<Region2D>();
      }

      void insertSegment (const Point2D& start, const Point2D& end, const VertexOrder& dir, const VertexOrder& sweepDirection, Data data) throws()
      {
	if (_haveBox && (_box.classify(start) & _box.classify(end))!=0) {
	  return;
	}
	if (_region && data!=_code) {
	  if (_region->locatePoint(start)<0 && _region->locatePoint(end)<0) {
	    if (dir!=sweepDirection) {
	      _builder.addSegment(end,start,sweepDirection,data);
	    }
	    else {
	      _builder.addSegment(start,end,sweepDirection,data);
	    }
	    return;
	  }
	}
	    
	SweepLine::insertSegment(start,end,dir,sweepDirection,data);
      }
	  
      int _code;
      bool _haveBox;
      Pointer<Region2D> _region;
      BoundingBox2D _box;
      PlanarGraph& _builder;
    };
	
    struct IntersectionContourTracer : public ContourTracer {
      IntersectionContourTracer (const VertexOrder& traceDir)
	: _traceDir(traceDir)
      {
	    
      }
      bool beginTrace(graph::Edge* e) throws() 
      {
	return e->direction().checkDirection(_traceDir);
      }

      graph::Edge* nextEdge(graph::Edge* e) const throws()
      {
	assert(e!=0);
	graph::Edge* n = e->turnCW();
	if (n->direction().checkDirection(_traceDir)) {
	  return n;
	}
	n = e->turnCCW();
	if (n->direction().checkDirection(_traceDir)) {
	  return n;
	}
	return 0;
      }

    private:
      const VertexOrder _traceDir;
    };
	
    struct DifferenceContourTracer : public DirectedContourTracer {
      typedef DirectedContourTracer super;
      DifferenceContourTracer (const Reference<Region2D>& orig, int origData, const VertexOrder& traceDir, const VertexOrder& turnDir)
	: super(traceDir,turnDir),
	  _region(orig), _data(origData) {}
      DifferenceContourTracer (const  Reference<Region2D>& orig, int origData, const VertexOrder& traceDir, const VertexOrder& turnDir, graph::Direction dir)
	: super(traceDir,turnDir,dir),
	  _region(orig), _data(origData) {}

      bool beginTrace(Edge* e) throws()
      {
	if (!e->reverse()->direction().isNone()) {
	  return false;
	}
	if (e->data()==_data) {
	  if (traceDirection().isCW() && e->direction().isCW()) {
	    return true;
	  }
	  if (traceDirection().isCCW() && e->direction().isCCW()) {
	    return true;
	  }
	}
	else {
	  const Point2D& start = e->start().point();
	  const Point2D& end = e->end().point();
	    
	  const Double x = start.x()/2+end.x()/2;
	  const Double y = start.y()/2+end.y()/2;
	    
	  if (_region->locatePoint(x,y)<0) {
	    if (traceDirection().isCW() && e->direction().isCW()) {
	      return true;
	    }
	    if (traceDirection().isCCW() && e->direction().isCCW()) {
	      return true;
	    }
	  }
	}
	return false;
      }

    private:
      const Reference<Region2D> _region;
      const int _data;
    };



    struct BasicArea : public AbstractArea2D {
      inline BasicArea(const Reference<Contour2D>& outside)
	: _bounds(outside->bounds()),_outside(outside)
      {}
	
      ~BasicArea() throws() {}

      Reference<Contour2D> outsideContour () const throws()
      { return _outside; }
      size_t contourCount() const throws()
      { return _inside.size(); }
      Reference<Contour2D> insideContour(size_t i) const throws()
      { return _inside.at(i); }

      inline void addContour (const Reference<Contour2D>& a) 
      { _inside.push_back(a);	}

      BoundingBox2D bounds() const throws()
      { return _bounds; }

    private:
      BoundingBox2D _bounds;
      Reference<Contour2D> _outside;
      ::std::vector< Reference<Contour2D> > _inside;
    };
    
    struct BasicRegion : public AbstractRegion2D {
      inline BasicRegion()
      {}
	
      ~BasicRegion() throws() {}
      size_t areaCount() const throws() { return _areas.size(); }
      Reference<Area2D> area(size_t i) const throws() { return _areas.at(i); }
      void addArea (const Reference<Area2D>& a) 
      { 
	if (_areas.empty()) {
	  _bounds = a->bounds();
	}
	else {
	  _bounds.merge(a->bounds());
	}
	_areas.push_back(a);
      }

      BoundingBox2D bounds() const throws()
      {
	return _bounds;
      }

    private:
      BoundingBox2D _bounds;
      ::std::vector<Reference<Area2D> > _areas;
    };


    class BasicOpsHelper : public AbstractGraphCallback {
    public:
      enum OP { MERGE,INTERSECT,DIFFERENCE,XOR };

      inline BasicOpsHelper() throws() 
	: _outsideContours(true) 
      {}
      ~BasicOpsHelper() throws() {}

    public:
      void traceExternalContours (graph::Graph& g, graph::ContourTracer& tracer)
      {
	_outsideContours = true;
	g.traceContours(*this,tracer);
      }

      void traceInternalContours (graph::Graph& g, graph::ContourTracer& tracer)
      {
	_outsideContours = false;
	g.traceContours(*this,tracer);
      }

      void contourExtracted(const Reference<Contour2D>& c) throws()
      { 
	if (_outsideContours) {
	  _externalContours.push_back(c);
	}
	else {
	  _internalContours.push_back(c);
	}
      }

      void reset() throws() 
      {
	_internalContours.clear();
	_externalContours.clear();
	_areas.clear();
      }


      Pointer<Region2D> buildRegion() 
      {
#ifdef DEBUG_ON
	::std::cerr << "BUILDING REGION" << ::std::endl;
#endif
	if (_externalContours.empty()) {
#ifdef DEBUG_ON
	  ::std::cerr << "NO CW contours" << ::std::endl;
#endif
	  return Pointer<Region2D>();
	}
#ifdef DEBUG_ON
	::std::cerr << _externalContours.size() << " CW contours" << ::std::endl;
#endif
	  
	// if there are no holes then just build
	// a simple region out of the individual contours
	if (_internalContours.empty()) {
	  // only one contour --> return it as the region
	  if (_externalContours.size()==1) {
	    return _externalContours[0];
	  }
	  
	  Reference<BasicRegion> res = new BasicRegion();
	  for (size_t i =0;i<_externalContours.size();++i) {
	    res->addArea(_externalContours[i]);
	  }
	  return res;
	}
	Reference<BasicRegion> res = new BasicRegion();

	for (size_t i=0;i<_externalContours.size();++i) {
	  Reference<BasicArea> tmp = new BasicArea(_externalContours[i]);
	  res->addArea(tmp);
	  _areas.push_back(tmp);
	}
	  
#ifdef DEBUG_ON
	::std::cerr << "Number of ccw contours" << _internalContours.size() << ::std::endl;
#endif
	for (size_t i=0;i<_internalContours.size();++i) {
	  const Reference<Contour2D>& c = _internalContours[i];
	  try{ 
	    for (size_t k=0;k<_externalContours.size();++k) {
	      if (testContainment(_externalContours[k],c)) {
		_areas[k]->addContour(c);
#ifdef DEBUG_ON
		::std::cerr << "Added inner contour " << i << " to area " << k << ::std::endl;
#endif
		break;
	      }
	    }
	  }
	  catch (...) {
	  }
	}
	  
	reset();
	return res;
      }

    private:
      bool _outsideContours;

      ::std::vector<Reference<Contour2D> > _externalContours;
      ::std::vector<Reference<Contour2D> > _internalContours;
      ::std::vector<Reference<BasicArea> > _areas;
      OP _op;
    };
	
    class BasicOps : public Region2DOps {
    public:
      inline BasicOps() throws() 
	: _graphHelper(_graph),_sweepline(_graph) { }
	  
      ~BasicOps() throws() {}
	  
    public:
      Pointer<Region2D> merge (const Pointer<Region2D>& r0, const Pointer<Region2D>& r1) throws (::std::exception)
      {
	if (r0==r1 || !r0) { return r1; }
	if (!r1) { return r0; }

	_sweepline.reset();
	_graph.clear();
	_helper.reset();
 	  
	insertRegion(r0,VertexOrder::cw(),0,_sweepline);
	insertRegion(r1,VertexOrder::cw(),1,_sweepline);
	  
	_graph.insertSweepline(_sweepline);
	_graph.removeDanglingSegments();
	graph::DirectedContourTracer cwTracer(VertexOrder::cw(),VertexOrder::cw(),graph::Direction::cw());
	graph::DirectedContourTracer ccwTracer(VertexOrder::ccw(),VertexOrder::cw());
	_helper.traceExternalContours(_graph,cwTracer);
	_helper.traceInternalContours(_graph,ccwTracer);
	Pointer<Region2D> result =  _helper.buildRegion();
	_sweepline.reset();
	_graph.clear();
	_helper.reset();
	return result;
      }
      
      Pointer<Region2D> intersect (const Pointer<Region2D>& r0, const Pointer<Region2D>& r1) throws (::std::exception)
      {
	if (!r1 || !r0) { return Pointer<Region2D>(); }
	if (r1==r0) { return r0; }

	BoundingBox2D box(r0->bounds());
	if (!box.intersect(r1->bounds())) {
	  return Pointer<Region2D>();
	}

	_sweepline.reset();
	_graph.clear();
	_helper.reset();
	    
	_sweepline.setBounds(box);
	if (r0->isConvex()) {
	  _sweepline.setRegion(r0,0);
	}
	else if (r1->isConvex()) {
	  _sweepline.setRegion(r1,1);
	}
	insertRegion(r0,VertexOrder::cw(),0,_sweepline);
	insertRegion(r1,VertexOrder::cw(),1,_sweepline);
	    
	_sweepline.clearBounds();
	_sweepline.clearRegion();

	_graphHelper._regionCount = 2;
	_graphHelper._regions[0] = r0;
	_graphHelper._regions[1] = r1;
	_sweepline.sweepSegments(_graphHelper);
	_graphHelper._regionCount = 0;
	_graphHelper._regions[0] = _graphHelper._regions[1] = Pointer<Region2D>();
	_graph.removeDanglingSegments();
	    

	graph::DirectedContourTracer cwTracer(VertexOrder::cw(),VertexOrder::ccw());
	graph::DirectedContourTracer ccwTracer(VertexOrder::ccw(),VertexOrder::ccw(),graph::Direction::ccw());

	_helper.traceExternalContours(_graph,cwTracer);
	_helper.traceInternalContours(_graph,ccwTracer);

	Pointer<Region2D> result =  _helper.buildRegion();
	_sweepline.reset();
	_graph.clear();
	_helper.reset();
	return result;
      }
	
      Pointer<Region2D> difference (const Reference<Region2D>& r0, const Pointer<Region2D>& r1) throws (::std::exception)
      {
	if (!r1) { return r0; }
	if (r0==r1) { return Pointer<Region2D>(); }
	
	_sweepline.reset();
	_graph.clear();
	_helper.reset();

	_sweepline.setBounds(r0->bounds());
	if (r0->isConvex()) {
	  _sweepline.setRegion(r0,0);
	}
	insertRegion(r0,VertexOrder::cw(),0,_sweepline);
	insertRegion(r1,VertexOrder::ccw(),1,_sweepline);
	_sweepline.clearBounds();
	_sweepline.clearRegion();
	_graphHelper._regionCount = 1;
	_graphHelper._regions[0] = r0;
	_sweepline.sweepSegments(_graphHelper);
	_graphHelper._regionCount = 0;
	_graphHelper._regions[0] = _graphHelper._regions[1];

	_graph.removeDanglingSegments();
	    
	DifferenceContourTracer cwTracer(r0,0,VertexOrder::cw(),VertexOrder::ccw());
	DifferenceContourTracer ccwTracer(r0,0,VertexOrder::ccw(),VertexOrder::ccw(),graph::Direction::ccw());
	_helper.traceExternalContours(_graph,cwTracer);
	_helper.traceInternalContours(_graph,ccwTracer);

	Pointer<Region2D> result =  _helper.buildRegion();
	_sweepline.reset();
	_graph.clear();
	_helper.reset();
	return result;
      }
      
      Pointer<Region2D> exclusiveOr (const Pointer<Region2D>& r0, const Pointer<Region2D>& r1) throws (::std::exception)
      {
	if (r0==r1) { return Pointer<Region2D>(); }
	if (!r1) { return r0; }
	if (!r0) { return r1; }

	_sweepline.reset();
	_graph.clear();
	_helper.reset();

	insertRegion(r0,VertexOrder::cw(),0,_sweepline);
	insertRegion(r1,VertexOrder::cw(),1,_sweepline);
	    
	_graph.insertSweepline(_sweepline);
	_graph.removeDanglingSegments();


        logger().warn("XOR not implemented");
	throw ::std::runtime_error("XOR not implemented");

	Pointer<Region2D> result =  _helper.buildRegion();
	_sweepline.reset();
	_graph.clear();
	_helper.reset();
	return result;
      }	

      ::timber::Pointer<Region2D> createTriStripRegion (const ::timber::Pointer<Polygon2D>& r) throws (::std::exception)
      {
	if (!r) { return r; }
	::timber::Pointer<TriangleStrip2D> strip = r.tryDynamicCast<TriangleStrip2D>();
	if (strip) {
	  return strip;
	}
	
        logger().warn("CreateTriStripRegions not supported");
	throw ::std::runtime_error("BasicRegion2DOps.createTriStripRegion not implemented");
      }


      Pointer<Region2D> removeHoles (const Pointer<Region2D>& r) throws (::std::exception)
      {
	if (!r) { return r; }

	// check if the region has any holes at all
	for (size_t i=0,sz=r->areaCount();i<sz;++i) {
	  if (r->area(i)->contourCount() > 0) {
            logger().warn("Removing holes not yet supported");
	    throw ::std::runtime_error("BasicRegion2DOps.removeHoles cannot yet remove holes");
	  }
	}
	// no holes, so just return the region
	return r;
      }


      Pointer<Region2D> normalize (const Pointer<Region2D>& r) throws (::std::exception)
      {
	if (!r) {
	  return r;
	}

	_sweepline.reset();
	_graph.clear();
	_helper.reset();

	insertRegion(r,VertexOrder::cw(),0,_sweepline);
	_sweepline.sweepSegments(_graphHelper);
	    
	IntersectionContourTracer cwTracer(VertexOrder::cw());
	IntersectionContourTracer ccwTracer(VertexOrder::ccw());

	_helper.traceExternalContours(_graph,cwTracer);
	_helper.traceInternalContours(_graph,ccwTracer);

	Pointer<Region2D> result =  _helper.buildRegion();
	_sweepline.reset();
	_graph.clear();
	_helper.reset();
	return result;

      }

      Pointer<Region2D> reducePrecision (const Pointer<Region2D>& r, double prec) throws (::std::exception)
      {
	if (prec==0) {
	  return r;
	}
	
	return r;
      }


    private:
      BasicOpsHelper _helper;
      PlanarGraph _graph;
      BasicGraphHelper _graphHelper;
      BasicSweepLine _sweepline;
    };
  }

  unique_ptr< Region2DOps> BasicRegion2DOps::create() throws()
  {
    return unique_ptr<Region2DOps>(new BasicOps());
  }
}
