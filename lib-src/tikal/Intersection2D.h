#ifndef _TIKAL_INTERSECTION2D_H
#define _TIKAL_INTERSECTION2D_H

#ifndef _TIKAL_SEGMENT2D_H
#include <tikal/Segment2D.h>
#endif

namespace tikal {
  
  /**
   * This object holds the segments that result when two line segments
   * intersect. 
   */
  class Intersection2D {

    /**
     * Compute the intersection of two segments.
     * @param s1 a segment
     * @param s2 a segment
     */
  public:
    inline Intersection2D (const Segment2D& s1, const Segment2D& s2) throws()
      : _num(0)
      { intersect(s1,s2); }

    /**
     * Compute the intersection of two segments.
     * @param s1 a segment
     * @param s2 a segment
     * @param eps an error associated
     */
  public:
    inline Intersection2D (const Segment2D& s1, const Segment2D& s2, double eps) throws()
      : _num(0)
      { intersect(s1,s2,eps); }

    /**
     * Create an empty intersection.
     */
  public:
    inline Intersection2D () throws()
      : _num(0)
      {}
      
    /**
     * Intersect two segments. Updates this object to contain
     * information about the intersection of the segments.
     * @param s1 a segment
     * @param s2 a segment
     * @param eps a small error (in the range 0 to 1)
     * @return number of segments or 0 if there is no intersection
     */
  public:
    size_t intersect (const Segment2D& s1, const Segment2D& s2, double eps) throws();

    /**
     * Intersect two segments. Updates this object to contain
     * information about the intersection of the segments.
     * @param s1 a segment
     * @param s2 a segment
     * @return number of segments or 0 if there is no intersection
     */
  public:
    inline size_t intersect (const Segment2D& s1, const Segment2D& s2) throws()
    { return intersect(s1,s2,1E-10); }

    /**
     * Get the total number of generated segments.
     * @return the total number of segments
     */
  public:
    inline size_t segmentCount() const throws() 
    { return _num; }
      
    /**
     * Get the specified segment.
     * @param i a segment index
     * @return the segment at the given index
     */
  public:
    inline const Segment2D& segment(size_t i) const throws()
    {
      assert(i<_num);
      return _result[i]; 
    }

    /**
     * Test which source segments contribute to a given segment.
     * @param i a segment index
     * @return 1 (segment 1 only), 2 (segment 2), or 3 (segment 1 and 2)
     */
  public:
    inline size_t source (size_t i) const throws()
    {
      assert(i<_num);
      return _source[i]; 
    }

    /** The number of line segments */
  private:
    size_t _num;

    /** The line segments */
  private:
    Segment2D _result[4];
      
    /** Indicators, which result is from which edge */
  private:
    size_t _source[4];
  };

}
#endif
