#include <tikal/tikal.h>
#include <tikal/Point2D.h>

#include <canopy/math/Precision.h>
#include <canopy/math/RoundingMode.h>
#include <canopy/math/exact.h>
#include <canopy/math/Real.h>

#include <timber/logging.h>

#include <cstdio>

using namespace ::std;
using namespace ::canopy::math;
using namespace ::timber::logging;

#define DETERMINANT_SIGN_DEBUGGING 1

namespace tikal {
  namespace {

    /**
     * Compute the exact sign of a determinant using arbitrary precision arithmetic. This result
     * is guaranteed to be correct.
     */
    static void determinantSign3x3_Real(const Point2D& p1, const Point2D& p2, const Point2D& p3, ::canopy::Int32& sign)
    {
      Real sum;

      const Real p1x = Real(p1.x());
      const Real p1y = Real(p1.y());
      const Real p2x = Real(p2.x());
      const Real p2y = Real(p2.y());
      const Real p3x = Real(p3.x());
      const Real p3y = Real(p3.y());

      sum += p1x*p2y;
      sum += p1y*p3x;
      sum += p2x*p3y;

      sum -= p3x*p2y;
      sum -= p3y*p1x;
      sum -= p2x*p1y;

      sign = sum.sign();
    }

    
    // compute the determinant of a matrix of 3 homogenous 2d points
    static bool determinantSign3x3(const Point2D& p1, const Point2D& p2, const Point2D& p3, ::canopy::Int32& sign)
    {
      Precision pmode(Precision::DOUBLE);
      RoundingMode rmode(RoundingMode::NEAREST);
      double tmp[2*6];

      ::canopy::math::testAndClearErrors();

      twoProduct(p1.x(),p2.y(),tmp[0],tmp[1]);
      twoProduct(p1.y(),p3.x(),tmp[2],tmp[3]);
      twoProduct(p2.x(),p3.y(),tmp[4],tmp[5]);

      twoProduct(-p3.x(),p2.y(),tmp[6],tmp[7]);
      twoProduct(-p3.y(),p1.x(),tmp[8],tmp[9]);
      twoProduct(-p2.x(),p1.y(),tmp[10],tmp[11]);
	
      sign = 0;
      int ok = signOfSum(12,tmp,sign);
      return ok==0;
    }

    // compute the determinant of a matrix of 3 homogenous 2d points
    static bool determinantSign3x3_inexact(const Point2D& p1, const Point2D& p2, const Point2D& p3, ::canopy::Int32& sign)
    {
      testAndClearErrors();

      double nx = p2.y()-p3.y();
      double ny = p3.x()-p2.x();
      double dx = p1.x()-p3.x();
      double dy = p1.y()-p3.y();
      
      double det = nx*dx+ny*dy;
      double absDet = abs(det);
      // the error is bounded by 5*eps(|det|)
      if (absDet > 5* eps(absDet) && testErrors()==0) {
	sign = (det > 0 ? 1 :(det < 0 ? -1 : 0));
	return true;
      }
      return false;
    }
  }

  double computeSignedPolygonArea (size_t n, const Point2D* pts) throws()
  {
    double a= 0;

    const double offX = pts[n-1].x();
    const double offY = pts[n-1].y();
    double pX = pts[0].x()-offX;
    double pY = pts[0].y()-offY;

    for (const Point2D* p = pts+1;p<pts+n-1;++p) {
      const double x= p->x() - offX;
      const double y= p->y() - offY;
      a += pX*y - pY*x;
      pX = x;
      pY = y;
    }
#ifdef DEBUG
    if (a>0.0) {
      assert (isCCWOrdering(n,pts));
    }
    else if (a<0.0) {
      assert (!isCCWOrdering(n,pts));
    }
#endif
    return 0.5*a;
  }

  bool testPointInPolygon (double x, double y, size_t n, const Point2D* v) throws(exception)
  {
    return computePointPolygonOrientation(x,y,n,v) <= 0;
  }
    
  int testPointSegmentOrientation (const Point2D& p, const Point2D& a, const Point2D& b) throws()
  {
    int orientation = 0;
    if (a.y()==b.y()) {
      // horizontal 
      if (p.y() > a.y()) {
	orientation = -1;
      }
      else if (p.y()==a.y()) {
	orientation = 0;
      }
      else {
	orientation = 1;
      }
    }
    else {
      // non-horizontal 
      if (a.x() <= b.x()) {
	if (a.y() < b.y()) {
	  orientation = computeOrientation(a,b,p);
	}
	else {
	  orientation = computeOrientation(b,a,p);
	}
      }
      else {
	if (a.y() < b.y()) {
	  orientation = computeOrientation(a,b,p);
	}
	else {
	  orientation = computeOrientation(b,a,p);
	}
      }
    }
    return orientation;

  }

  bool isConvexPolygon (size_t n, const Point2D* poly) throws()
  {
    const Point2D* b = poly;
    const Point2D* end = b+n;
    const Point2D* a = end-1;

    int orientation = 0;
    for(const Point2D* x=b+1;x<end;) {
      int tmp = computeOrientation(*a,*b,*x);
      // if the orientation changes, then it must be concave
      if (tmp!=0) {
	if (tmp==-orientation) {
	  return false;
	}
	orientation = tmp;
	a = b;
	++b;
	++x;
      }
      else if (a->equals(*b)) {
	++b;
	++x;
      }
      else if (b->equals(*x)) {
	++x;
      }
      else {
	a = b;
	++b;
	++x;
      }
    }
    return true;
  }


  int computePointPolygonOrientation (double x, double y, size_t n, const Point2D* vtx) throws(exception)
  {
    if (n<3) {
      throw invalid_argument("Too few points for polygon");
    }

    const Point2D pt(x,y);
    int rightCount=0; // number of segments to the right of pt

      
    const Point2D* v=vtx;
    const Point2D* end=v+n;
    const Point2D* p=end-1;
      
    while(p!=v && p->y()==y) {
      if (p->x()==x) {
	return 0;
      }
      --p;
    }
    if (p==v) {
      throw invalid_argument("Invalid polygon");
    }
    assert(p->y()!=y);
    bool pAbove = p->y() > y;
    p = end-1;
      
    while(v<end) {
      if (v->y()==y) {
	if (v->x()==x) {
	  return 0;
	}

	if (p->y()==y) {
	  if ((p->x() < x && v->x()>x) || (p->x() > x && v->x()<x)) {
	    return 0;
	  }
	}
      }
      else {
	bool vAbove = v->y() > y;
	if (vAbove!=pAbove) {
	  int where = testPointSegmentOrientation(pt,*p,*v);
	  if (where==0) {
	    return 0; // is this really true???
	  }
	  if (where<0) {
	    ++rightCount;
	  }
	  pAbove = vAbove;
	}
      }
      p = v++;
    }
    return (rightCount & 1) != 0 ? -1 : 1;
  }

  bool isCCWOrdering (const Point2D& p1, const Point2D& p2) throws()
  {
    if (p1.y() != p2.y()) {
      return p1.y() > p2.y();
    }
    return p2.x() > p1.x();
  }
    
  bool isCCWOrdering (const Point2D& p1, const Point2D& p2, const Point2D& p3) throws()
  {
    int sgn = computeOrientation(p1,p2,p3);
    if (sgn!=0) {
      return sgn<0;
    }
      
#if 0
    LogEntry("tikal.isCCWOrdering").debugging() << "isCCWOrdering: points are degenerate\n" 
						 << "  " << p1 << '\n'
						 << "  " << p2 << '\n'
						 << "  " << p3 << '\n'
						 << doLog;
#endif

    return isCCWOrdering(p2,p3);
  }


  bool isCCWOrdering (size_t n, const Point2D* p) throws()
  {
    assert(n>0);
    assert(p!=0);

    if (n==2) {
      return isCCWOrdering(p[0],p[1]);
    }
      
    // find the lowest left-most point
    const Point2D* lowestleft = p;
    const Point2D* end = p+n;
    for (const Point2D* x=p;++x < end;/**/) {
      if (x->x() <= lowestleft->x()) {
	if (x->x() < lowestleft->x() || x->y() < lowestleft->y()) {
	  lowestleft = x;
	}
      }
    }
      
    // find the successors of the lowest left-most point
    const size_t i=lowestleft-p;    // the index of the lowest leftmost point
    const size_t j = i+1;
    const size_t k = i-1+n;
    const Point2D* next = p+(j%n);
    const Point2D* pred = p+(k%n);
      
    return isCCWOrdering(*pred,*lowestleft,*next);
  }
    
  bool isCCWOrdering (const vector<Point2D>& p) throws()
  {
    const int n = p.size();
    assert(n>1);
    if (n==2) {
      return isCCWOrdering(p[0],p[1]);
    }
      
    // find the lowest left-most point
    vector<Point2D>::const_iterator lowestleft = p.begin();

    for (vector<Point2D>::const_iterator x=p.begin();++x < p.end();/**/) {
      if (x->x() <= lowestleft->x()) {
	if (x->x() < lowestleft->x() || x->y() < lowestleft->y()) {
	  lowestleft = x;
	}
      }
    }
      
    // find the successors of the lowest left-most point
    const size_t i=lowestleft-p.begin();    // the index of the lowest leftmost point
    const size_t j = i+1;
    const size_t k = i-1+n;
    vector<Point2D>::const_iterator next = p.begin()+(j%n);
    vector<Point2D>::const_iterator pred = p.begin()+(k%n);
      
    return isCCWOrdering(*pred,*lowestleft,*next);
  }
    
  int computeOrientation (const Point2D& p1, const Point2D& p2, const Point2D& p3) throws()
  {
    // first, calculate the determinant directly
    // this should work, but it's not been tested

    ::canopy::Int32 determinantSign;
    if (false) {
      if (determinantSign3x3_inexact(p3,p2,p1,determinantSign)) {
	return determinantSign;
      }
    }

    // try to be more exact using floating point numbers; if there is 
    // overflow or underflow, then eventually the arbitrary precision
    // arithmetic must be used
    if (determinantSign3x3(p3,p2,p1,determinantSign)) {
      return determinantSign;
    }
#if DETERMINANT_SIGN_DEBUGGING != 0
    {
      Log logger("tikal");
      const Level llevel = Level::DEBUGGING;
      
      if (logger.isLoggable(llevel)) {
    	  LogEntry e(logger,llevel);
    	  e << "Degenerate points or not enough precision available "
			<< p1 << ", " << p2 << ", " << p3
			<< doLog;
	//      ::std::printf("Degenerate points : %A %A %A %A %A %A\n",p1.x(),p1.y(),p2.x(),p2.y(),p3.x(),p3.y());
      }
    }
#endif

    determinantSign3x3_Real(p3,p2,p1,determinantSign);
    return determinantSign;
  }
  
}
