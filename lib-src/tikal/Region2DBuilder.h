#ifndef _TIKAL_REGION2DBUILDER_H
#define _TIKAL_REGION2DBUILDER_H

#ifndef _TIKAL_REGION2D_H
#include <tikal/Region2D.h>
#endif


namespace tikal {
  class Point2D;
  class Area2D;
  class Contour2D;
  class VertexOrder;

  /**
   * A region builder can be used to quickly assemble
   * arbitrary regions
   */
  class Region2DBuilder  {
    /** Nop copy methods */
    Region2DBuilder(const Region2DBuilder&);
    Region2DBuilder&operator=(const Region2DBuilder&);

    /**
     * Create a new region builder.
     */
  protected:
    Region2DBuilder() throws();
    
    /** Destructor */
  public:
    virtual ~Region2DBuilder() throws();
    
    /**
     * Create a new builder based on this one.
     * @return a new builder
     */
  public:
    static ::std::unique_ptr<Region2DBuilder> create() throws();
      
    /**
     * Reset this builder.
     */
  public:
    virtual void reset() throws() = 0;

    /**
     * Add a new area.
     * @param a a new area
     */
  public:
    virtual void addArea(const ::timber::Reference<Area2D>& a) throws (::std::exception) = 0;

    /**
     * Get the region built so far. 
     * @return a region
     */
  public:
    virtual ::timber::Pointer<Region2D> getRegion() throws() = 0;
      
    /**
     * Add an external contour to the current area.
     * @param c an external contour.
     */
  public:
    virtual void addExternalContour(const ::timber::Reference<Contour2D>& c) = 0;
      
    /**
     * Begin an external contour.
     * @param start the contours initial vertex
     * @param windDir the wind direction 
     */
  public:
    virtual void beginExternalContour(const Point2D& start, const VertexOrder& windDir) = 0;
      
    /**
     * Begin an external contour.
     * @param start the contours initial vertex
     */
  public:
    virtual void beginExternalContour(const Point2D& start) = 0;


    /**
     * Add an internal contour to the current area.
     * @param c an internal contour.
     */
  public:
    virtual void addInternalContour(const ::timber::Reference<Contour2D>& c) = 0;

    /**
     * Begin an internal contour.
     * @param start the contours initial vertex
     * @param windDir the wind direction 
     */
  public:
    virtual void beginInternalContour(const Point2D& start, const VertexOrder& windDir) = 0;

     /**
     * Begin an internal contour.
     * @param start the contours initial vertex
     */
  public:
    virtual void beginInternalContour(const Point2D& start) = 0;

   /**
     * Create a contour with a line to the specified point. If the previous
     * point is equal to this point, then nothing is done.
     * @param pt the endpoint of the line segment from the previous point to pt
     */
  public:
    virtual void lineTo(const Point2D& pt) throws (::std::exception) = 0;
  };
}

#endif
