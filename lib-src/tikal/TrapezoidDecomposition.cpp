#include <tikal/TrapezoidDecomposition.h>
#include <tikal/Segment2D.h>
#include <timber/logging.h>

#include <iostream>
#include <cassert>

#ifdef NDEBUG
#define DEBUG_OFF
#else
#define DEBUG_ON
#endif

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace tikal {
  namespace {
    /**
     * Test a point against a segment.
     * @param p a point
     * @param s a segment
     * @pre comparePoints(s.a(),s.b()) < 0
     * @return -1 if the point is to left of the segment, 0 if it's on the segment, and 1 if it's to the right
     */
    int testPointSegment (const Point2D& p, const Segment2D& s) throws()
    {
      assert(TrapezoidDecomposition::comparePoints(s.a(),s.b()) < 0);
      int x = s.testPoint(p);
      return x;
    }
  }

  TrapezoidDecomposition::Trapezoid::Trapezoid () throws()
  : _index(invalidIndex()),
    _pointBelow(invalidIndex()), _pointAbove(invalidIndex()),
    _segmentLeft(invalidIndex()), _segmentRight(invalidIndex())
  {}

  TrapezoidDecomposition::Trapezoid::Trapezoid (size_t i) throws()
  : _index(i),
    _pointBelow(invalidIndex()), _pointAbove(invalidIndex()),
    _segmentLeft(invalidIndex()), _segmentRight(invalidIndex())
  {}

  TrapezoidDecomposition::TrapezoidDecomposition(bool enableMerging) throws()
  : _mergeTrapezoids(enableMerging)
  {
    NodePtr root ( createTrapezoidNode(createTrapezoid()));
    assert(root._node==0);
  }

  TrapezoidDecomposition::TrapezoidDecomposition() throws()
  : _mergeTrapezoids(false)
  {
    NodePtr root ( createTrapezoidNode(createTrapezoid()));
    assert(root._node==0);
  }

  TrapezoidDecomposition::~TrapezoidDecomposition() throws()
  {}

  void TrapezoidDecomposition::reset() throws()
  {
    _trapezoids.clear();
    _segments.clear();
    _points.clear();
    _nodes.clear();
    NodePtr root ( createTrapezoidNode(createTrapezoid()));
  }


  size_t TrapezoidDecomposition::createTrapezoid () throws()
  {
    size_t i = _trapezoids.size();
    _trapezoids.push_back(Trapezoid(i));
    return i;
  }

  TrapezoidDecomposition::NodePtr TrapezoidDecomposition::createTrapezoidNode (size_t t) throws()
  {
    assert(isIndex(t));
    size_t i = _nodes.size();
    _nodes.push_back(Node(t));
    assert(_nodes[i]._type==Node::TRAPEZOID);
    assert(_nodes[i]._object==t);
    return NodePtr(i);
  }
      
  size_t TrapezoidDecomposition::createPoint(const Point2D& pt) throws()
  { 
    size_t i = _points.size();
    _points.push_back(pt);
    assert(_points[i] == pt);
    return i;
  }
      
  size_t TrapezoidDecomposition::createSegment(size_t lo, size_t hi) throws()
  {
    size_t i = _segments.size();
    _segments.push_back(Segment(lo,hi));
    assert(_segments[i].first==lo && _segments[i].second==hi);
    return i;
  }

  TrapezoidDecomposition::NodePtr TrapezoidDecomposition::rootNode () const throws()
  {	return NodePtr(0); }

  int TrapezoidDecomposition::comparePoints (const Point2D& p, const Point2D& q) throws()
  {
    Double a = p.y();
    Double b = q.y();
    if (a==b) {
      a = p.x();
      b = q.x();
    }
    if (a < b) {
      return -1;
    }
    if (a > b) {
      return 1;
    }
    return 0;
  }
      
  bool TrapezoidDecomposition::createValidSegment (Segment2D& s) throws()
  { 
    if (comparePoints(s.a(),s.b()) < 0) {
      return true;
    }
    s = Segment2D(s.b(),s.a());
    return false;
  }

  int TrapezoidDecomposition::testPoint (const Point2D& pt, Segment s) const throws()
  {
    const Point2D& p1 = point(s.first);
    const Point2D& p2 = point(s.second);
    const Segment2D s1(p1,p2);
    return testPointSegment(pt,s1);
  }

  int TrapezoidDecomposition::compareSegments (Segment s, const Segment2D& t) const throws()
  {
    const Point2D& p1 = point(s.first);
    const Point2D& p2 = point(s.second);
    const Segment2D s1(p1,p2);
	
    int cmp = testPointSegment(t.a(),s1);
    if (cmp==0) {
      cmp = testPointSegment(t.b(),s1);
    }
    return cmp;
  }

  size_t TrapezoidDecomposition::findTrapezoid (const Point2D& p) const throws()
  {
    NodePtr root=rootNode();

    while (true) {
      const Node& n = node(root);
      int x=0;
      switch (n._type) {
      case Node::POINT:
	x = comparePoints(p,point(n._object));
	break;
      case Node::SEGMENT: {
	x = testPoint(p,segment(n._object));
	// we need to negate, because we're actually testing
	break;
      }
      case Node::TRAPEZOID:
	return n._object;
      }
      if (x<0) {
	root = n._less;
      }
      else {
	root= n._greater;
      }
    }
	
  }


  TrapezoidDecomposition::NodePtr TrapezoidDecomposition::findTrapezoid (NodePtr root, const Segment2D& s) const throws()
  {
    assert(comparePoints(s.a(),s.b()) < 0);
#ifdef DEBUG_ON
    Log("tikal.TrapezoidDecomposition").debugging("FIND ROOT");
#endif
    while (true) {
      const Node& n = node(root);
      int x=0;
      switch (n._type) {
      case Node::POINT: {
	const Point2D& pt = point(n._object);
	x = comparePoints(s.a(),pt);
	if (x==0) {
	  x = comparePoints(s.b(),pt);
	}
#ifdef DEBUG_ON
	LogEntry("tikal.TrapezoidDecomposition").debugging() 
	  << "POINT NODE : " << pt 
	  << doLog;
#endif
	break;
      }
      case Node::SEGMENT: {
	x =compareSegments(segment(n._object),s);
	// we need to negate, because we're actually testing
#ifdef DEBUG_ON
	LogEntry("tikal.TrapezoidDecomposition").debugging() 
	  << "SEGMENT NODE : " << point(segment(n._object).first) <<  "," <<  point(segment(n._object).second) 
	  << doLog;
#endif

	break;
      }
      case Node::TRAPEZOID:
	return root;
      }
      if (x<0) {
	root = n._less;
      }
      else {
	root= n._greater;
      }
    }
  }
      
  size_t TrapezoidDecomposition::trapezoidCount() const throws()
  { return _trapezoids.size(); }

  const TrapezoidDecomposition::Trapezoid& TrapezoidDecomposition::trapezoid (size_t i) const throws()
  { return _trapezoids.at(i); }

    
  size_t TrapezoidDecomposition::insertSegment (NodePtr root, const Segment2D& s) throws (exception)
  {
    try {
      root = findTrapezoid(root,s);
	
#ifdef DEBUG_ON
      LogEntry e("tikal.TrapezoidDecomposition");
      e.debugging() << "ROOT : " << endl;
      printTrapezoid(e,root);
      e << doLog;
#endif
	
	
	
      // make sure neither the left or right segment are the same as s
      int res = splitVertically(root,s.a());
      size_t p0;
      if (res==0) {
	// ok, root was split
	Node& pointNode = node(root);
	p0 = pointNode._object;
	root = pointNode._greater;
      }
      else {
	Trapezoid& t= trapezoid(root);
	// make sure the segment is unique
	assert (res==-1);
	p0 = t._pointBelow;
	if (isIndex(t._segmentLeft)) {
	  if (point(segment(t._segmentLeft).first).equals(s.a())) {
	    if (point(segment(t._segmentLeft).second).equals(s.b())) {
#ifdef DEBUG_ON
	      LogEntry ("tikal.TrapezoidDecomposition").debugging() << "1. DUPLICATE SEGMENT " << s << doLog;
#endif
	      return t._segmentLeft;
	    }
	  }
	}

	if (isIndex(t._segmentRight)) {
	  if (point(segment(t._segmentRight).first).equals(s.a())) {
	    if (point(segment(t._segmentRight).second).equals(s.b())) {
#ifdef DEBUG_ON
	      LogEntry ("tikal.TrapezoidDecomposition").debugging() << "2. DUPLICATE SEGMENT " << s << doLog;
#endif
	      return t._segmentRight;
	    }
	  }
	}

	assert(point(p0)==s.a());
      }

      // create a new segment, but we don't know the upper point yet
      // so we fill it in as an invalid index
      size_t segIndex = createSegment(p0,invalidIndex());
      NodePtr lBelow,rBelow;
	
      while(true) {
	if (!root.isValid()) {
	  throw runtime_error("TrapezoidDecomposition: Insert failed");
	}

	const Trapezoid& t = trapezoid(root);
	int cmp = 0;
	if (isIndex(t._pointAbove)) {
	  cmp = comparePoints(point(t._pointAbove),s.b());
	}
	if (cmp<0) {
	  root = splitHorizontally(root,s,segIndex,lBelow,rBelow);
	}
	else {
	  size_t p1;
	  res = splitVertically(root,s.b());
	  if (res==0) {
	    // ok, root was split
	    Node& pointNode = node(root);
	    p1 = pointNode._object;
	    if (lBelow.isValid()) {
	      trapezoid(lBelow).repointTop(root,pointNode._less);
	      trapezoid(rBelow).repointTop(root,pointNode._less);
	    }
	    root = pointNode._less;
	  }
	  else {
	    assert(res==1);
	    p1 = trapezoid(root)._pointAbove;
	  }
	  _segments[segIndex]=Segment(p0,p1);
	      
	  splitHorizontally(root,s,segIndex,lBelow,rBelow);
	  break;
	}
      }
	
      assert (root.isValid());
#ifdef DEBUG_ON
      print(LogEntry("tikal.TrapezoidDecomposition").debugging());
#endif
      // finally, we can insert the real segment
      return segIndex;
    }
    catch (const exception& e) {
      reset();
      throw;
    }
    catch (...) {
      throw logic_error("Unknown exception caught");
    }
  }

  int TrapezoidDecomposition::splitVertically (NodePtr ptr, const Point2D& pt) throws()
  {
	
    const Trapezoid& T = trapezoid(ptr);

    // make sure the point has not yet been added
    if (isIndex(T._pointBelow) && point(T._pointBelow).equals(pt)) {
      return -1;
    }
	
    if (isIndex(T._pointAbove) && point(T._pointAbove).equals(pt)) {
      return 1;
    }

    // create two nodes, one for the top trapezoid and one
    // for the bottom trapezoid; also, an index for the point
    const NodePtr ntop = createTrapezoidNode(T._index);
    const NodePtr nbottom = createTrapezoidNode(createTrapezoid());
    const size_t pointIndex = createPoint(pt);

    // change the type of the existing tree node to a point node
    Node& pointNode = node(ptr);
    pointNode._type = Node::POINT;
    pointNode._object = pointIndex;
    pointNode._greater = ntop;
    pointNode._less = nbottom;

    // now, all memory allocations have been made we can get
    // the trapezoid references so we can set them up
    // setup the bottom and top trapezoids 
    Trapezoid& bot = trapezoid(nbottom);
    Trapezoid& top = trapezoid(ntop);
	
    // link up the outsides of the two trapezoids
    if (top._tl.isValid()) {
      Trapezoid& t = trapezoid(top._tl);
      t.repointBottomLeft(ptr,ntop);
      t.repointBottomRight(ptr,ntop);
    }	  
    if (top._tr.isValid()) {
      Trapezoid& t = trapezoid(top._tr);
      t.repointBottomLeft(ptr,ntop);
      t.repointBottomRight(ptr,ntop);
    }	  
    if (top._bl.isValid()) {
      Trapezoid& t = trapezoid(top._bl);
      t.repointTopLeft(ptr,nbottom);
      t.repointTopRight(ptr,nbottom);
    }
    if (top._br.isValid()){ 
      Trapezoid& t = trapezoid(top._br);
      t.repointTopLeft(ptr,nbottom);
      t.repointTopRight(ptr,nbottom);
    }

    // link up the bottom with the top
    bot._pointBelow = top._pointBelow;
    bot._pointAbove = top._pointBelow = pointIndex;
    bot._segmentLeft = top._segmentLeft;
    bot._segmentRight = top._segmentRight;
    bot._tr = bot._tl = ntop;
    bot._bl = top._bl;
    bot._br = top._br;

    top._br = top._bl = nbottom;

#ifdef DEBUG_ON
    LogEntry e("tikal.TrapezoidDecomposition");
    e.debugging() << "VERTICAL SPLIT TRAPEZOID" << endl;
    printTrapezoid(e,nbottom);
    printTrapezoid(e,ntop);
    e << doLog;
#endif

#ifndef NDEBUG
    verifyTrapezoid(nbottom);
    verifyTrapezoid(ntop);
    verifyTrapezoid(bot._bl);
    verifyTrapezoid(bot._br);
    verifyTrapezoid(top._tl);
    verifyTrapezoid(top._tr);
#endif

    return 0;
  }

  TrapezoidDecomposition::NodePtr TrapezoidDecomposition::splitHorizontally (const NodePtr n, const Segment2D& s, size_t segmentIndex, NodePtr& lBelow, NodePtr& rBelow) throws()
  {
    assert(lBelow.isValid()==rBelow.isValid());
    Trapezoid T(trapezoid(n));
    const size_t Tcurrent = T._index;

    NodePtr nleft,nright;

    // first, figure out if we can merge trapezoids, because it will affect
    // how we allocate new nodes and trapezoids
    if (_mergeTrapezoids && lBelow.isValid()) {
      if (trapezoid(lBelow)._segmentLeft==T._segmentLeft) {
	nleft = lBelow;
	assert (trapezoid(rBelow)._segmentRight!=T._segmentRight);
	T._bl = NodePtr();
      }
      else if (trapezoid(rBelow)._segmentRight==T._segmentRight) {
	nright = rBelow;
	T._br = NodePtr();
      }
    }

    // create the new trapezoid nodes and setup the segments and point indices
    if (nleft.isValid()) {
      // means that we've been merged with the below trapezoid
      assert(!nright.isValid());
	  
      // reuse the current trapezoid for the right trapezoid
      nright = createTrapezoidNode(Tcurrent);
      trapezoid(nleft)._pointAbove = T._pointAbove;
      trapezoid(nright)._segmentLeft = segmentIndex;
    }
    else {
      // reuse the current trapezoid for the left trapezoid
      nleft = createTrapezoidNode(Tcurrent);
      trapezoid(nleft)._segmentRight = segmentIndex;
      if (nright.isValid()) {
	trapezoid(nright)._pointAbove = T._pointAbove;
      }
      else {
	nright = createTrapezoidNode(createTrapezoid());
	Trapezoid& right = trapezoid(nright);
	right._segmentLeft = segmentIndex;
	right._segmentRight=T._segmentRight;
	right._pointAbove = T._pointAbove;
	right._pointBelow = T._pointBelow;
      }
    }

    // we've allocated the new trapezoids
    // change the type of the existing tree node to a segment node
    // and make it point to the new nodes
    Node& segmentNode = node(n);
    segmentNode._type = Node::SEGMENT;
    segmentNode._object = segmentIndex;
    segmentNode._less = nleft;
    segmentNode._greater = nright;


    // reget the trapezoid, because pointers might be out-of-date
    Trapezoid& left = trapezoid(nleft);
    Trapezoid& right = trapezoid(nright);

    // the segments involved
    const Segment seg = segment(segmentIndex);
	
    // flags to indicate if left._tr, right._tl, left._br, right._bl are already done
    bool bottomDone=false;
    bool topDone=false;

    // set up left._tl and left._bl and also repoint the trapezoids around
    {
      const Segment* leftEdge  = isIndex(T._segmentLeft) ? &segment(T._segmentLeft) : 0;
      NodePtr tl(T._tl);
      NodePtr bl(T._bl);

      if (leftEdge!=0) {
	if(leftEdge->second==seg.second) {
	  // trapezoid is closed at top
	  tl = NodePtr();
	  left._tr = NodePtr();
	  right._tl = T._tl;
	  if (T._tl.isValid()) {
	    trapezoid(T._tl).repointBottom(n,nright);
	  }
	  topDone=true;

	} else if (!rBelow.isValid() && leftEdge->first==seg.first) {
	  bl = NodePtr();
	  left._br = NodePtr();
	  right._bl = T._bl;
	  if (T._bl.isValid()) {
	    trapezoid(T._bl).repointTop(n,nright);
	  }
	  bottomDone=true;
	}
      }

      left._tl = tl;
      if (tl.isValid()) {
	trapezoid(tl).repointBottomLeft(n,nleft);
      }
      if (lBelow!=nleft) {
	left._bl = bl;
	if (bl.isValid()) {
	  trapezoid(bl).repointTopLeft(n,nleft);
	}
      }
    }
	
    // set up right._tr and right._br and also repoint the trapezoids around
    {
      const Segment* rightEdge = isIndex(T._segmentRight) ? &segment(T._segmentRight) : 0;
      NodePtr tr(T._tr);
      NodePtr br(T._br);
      if (rightEdge!=0) {
	if (rightEdge->second==seg.second) {
	  assert(!topDone || !T._tr.isValid());
	  tr = NodePtr();
	  right._tl = NodePtr();
	  left._tr = T._tr;
	  if (T._tr.isValid()){ 
	    trapezoid(T._tr).repointBottom(n,nleft);
	  }
	  topDone=true;
	}
	else if (!rBelow.isValid() && rightEdge->first==seg.first) {
	  assert(!bottomDone || !T._br.isValid());
	  br = NodePtr();
	  right._bl = NodePtr();
	  left._br = T._br;
	  if (T._br.isValid()){ 
	    trapezoid(T._br).repointTop(n,nleft);
	  }
	  bottomDone=true;
	}
      }
	  
      right._tr = tr;
      if (tr.isValid()) {
	trapezoid(tr).repointBottomRight(n,nright);
      }
      if (rBelow!=nright) {
	right._br = br;
	if (br.isValid()) {
	  trapezoid(br).repointTopRight(n,nright);
	}
      }
    }

    // finish the top if it's not done yet
    if (!topDone) {
      if (T._tl.isValid()) {
	Trapezoid& tl = trapezoid(T._tl);
	Trapezoid& tr = trapezoid(T._tr);
	tl.repointBottomLeft(n,nleft);
	tr.repointBottomRight(n,nright);
	if (T._tl==T._tr) {
	  left._tr = right._tl = T._tl;
	}
	else {
	  const Segment& topSegment = segment(tl._segmentRight);
	  if (topSegment.first==seg.second) {
	    left._tr = T._tl;
	    right._tl = T._tr;
	    tl._br = nleft;
	    tr._bl = nright;
	  }
	  else {
	    int cmp = testPointSegment(point(topSegment.first),s);
	    if (cmp==0) {
	      cmp = testPointSegment(point(topSegment.second),s);
	    }
	    assert(cmp!=0);
	    if (cmp>0) {
	      tl._br = tr._bl = nright;
	      left._tr = right._tl = T._tl;
	    }
	    else {
	      tl._br = tr._bl = nleft;
	      left._tr = right._tl = T._tr;
	    }
	  }
	}
      }
      else {
	left._tr = right._tl = NodePtr();
      }
    }

    // finish the bottom if it's not done yet
    if (!bottomDone) { 
      // setup left._br and right._bl
      if (lBelow.isValid()) {
	Trapezoid& leftBelow = trapezoid(lBelow);
	if (lBelow!=nleft) { // only link them up, if there was no merge
	  leftBelow._tr = nleft;
	  leftBelow.repointTopLeft(n,nleft);
	  left._br = lBelow;

	  if (left._bl.isValid()) {
	    trapezoid(left._bl).repointTopRight(n,nleft);
	  }
	}
	Trapezoid& rightBelow = trapezoid(rBelow);
	if (rBelow!=nright) {
	  rightBelow._tl = nright;
	  rightBelow.repointTopRight(n,nright);
	  right._bl = rBelow;

	  if (right._br.isValid()) {
	    trapezoid(right._br).repointTopLeft(n,nright);
	  }
	}
      }
      else {
	if (T._bl.isValid()) {
	  Trapezoid& bl = trapezoid(T._bl);
	  Trapezoid& br = trapezoid(T._br);
	  bl.repointTopLeft(n,nleft);
	  br.repointTopRight(n,nright);
	  if (T._bl==T._br) {
	    left._br = right._bl = T._bl;
	  }
	  else {
	    const Segment bottomSegment = segment(bl._segmentRight);
	    if (bottomSegment.second==seg.first) {
	      left._br = T._bl;
	      right._bl = T._br;
	      bl._tr = nleft;
	      br._tl = nright;
	    }
	    else {
	      int cmp = testPointSegment(point(bottomSegment.first),s);
	      if (cmp==0){ 
		cmp =  testPointSegment(point(bottomSegment.second),s);
	      }
	      assert(cmp!=0);
	      if (cmp > 0) {
		left._br = right._bl = T._br;
		bl._tr = br._tl = nleft;
	      }
	      else {
		left._br = right._bl = T._bl;
		bl._tr = br._tl = nright;
	      }
	    }
	  }
	}
	else {
	  left._br = right._bl = NodePtr();
	}
      }
    }

    // the new trapezoids become the ones below 
    lBelow = nleft;
    rBelow = nright;

    assert(right._segmentLeft==segmentIndex);
    assert(left._segmentRight==segmentIndex);

#ifdef DEBUG_ON
    LogEntry e("tikal.TrapezoidDecomposition");
    e.debugging() << "SPLIT TRAPEZOID" << endl;
    printTrapezoid(e,nleft);
    printTrapezoid(e,nright);
    e << doLog;
#endif
#ifndef NDEBUG
    verifyTrapezoid(nleft);
    verifyTrapezoid(nright);
    verifyTrapezoid(left._tl);
    verifyTrapezoid(left._tr);
    verifyTrapezoid(left._bl);
    verifyTrapezoid(left._br);
    verifyTrapezoid(right._tl);
    verifyTrapezoid(right._tr);
    verifyTrapezoid(right._bl);
    verifyTrapezoid(right._br);
#endif
    return left._tr.isValid() ? left._tr : right._tl;
  }

  void TrapezoidDecomposition::verifyTrapezoid(NodePtr ptr) throws()
  {
    if (!ptr.isValid()) {
      return;
    }

    Trapezoid& t = trapezoid(ptr);
    if (t._tl.isValid()) {
      trapezoid(t._tl);
    }
    if (t._tr.isValid()) {
      trapezoid(t._tr);
    }
    if (t._bl.isValid()) {
      trapezoid(t._bl);
    }
    if (t._br.isValid()) {
      trapezoid(t._br);
    }
  }
  void TrapezoidDecomposition::print(ostream& out , Trapezoid t) throws()
  {
    out << " ** trapezoid : " << t._index << endl;
    out << "  POINTS ";
    if (isIndex(t._pointBelow)) {
      out << point(t._pointBelow);
    }
    else {
      out << "--";
    }
    out << "  ";
    if (isIndex(t._pointAbove)) {
      out << point(t._pointAbove);
    }
    else {
      out << "--";
    }
    out << "  SEGMENTS ";
    if (isIndex(t._segmentLeft)) {
      out << "(" << point(segment(t._segmentLeft).first) << ",";
      if (isIndex(segment(t._segmentLeft).second)) {
	out <<point(segment(t._segmentLeft).second) << ") ";
      }
      else {
	out << "UNK) ";
      }
    }
    else {
      out << "--";
    }
    out << "  ";
    if (isIndex(t._segmentRight)) {
      out << "(" << point(segment(t._segmentRight).first) << ",";
      if (isIndex(segment(t._segmentRight).second)) {
	out <<point(segment(t._segmentRight).second) << ") ";
      }
      else {
	out << "UNK) ";
      }
    }
    else {
      out << "--";
    }

    out << '\n';

    if (t._tl.isValid()) {
      Trapezoid& x = trapezoid(t._tl);
      out << "** tl : " << x._index << endl;
    }
    if (t._tr.isValid()) {
      Trapezoid& x = trapezoid(t._tr);
      out << "** tr : " << x._index << endl;
    }
    if (t._bl.isValid()) {
      Trapezoid& x = trapezoid(t._bl);
      out << "** bl : " << x._index << endl;
    }
    if (t._br.isValid()) {
      Trapezoid& x = trapezoid(t._br);
      out << "** br : " << x._index << endl;
    }
  }

  void TrapezoidDecomposition::printTrapezoid(ostream& out, NodePtr ptr) throws()
  {
    out << "NODE:  ";
    if (ptr.isValid()) {
      out << ptr._node;
    }
    else {
      out << "-";
    }
    out << endl;
    print(out,trapezoid(ptr));
  }

  void TrapezoidDecomposition::print(ostream& out) throws()
  {
    out << endl << endl << endl << endl << "ALL" << endl;
    for (size_t i=0;i<trapezoidCount();++i) {
      print(out,trapezoid(i));
    }
    out << endl << endl << endl << endl;
  }


}
