#ifndef _TIKAL_SEGMENT2D_H
#define _TIKAL_SEGMENT2D_H

#ifndef _TIKAL_POINT2D_H
#include <tikal/Point2D.h>
#endif

#include <cmath>
#include <cstring>
#include <iosfwd>
#include <cassert>

namespace tikal {
    
  /**
   * This class is used to represent an individual 2D line segment. A segment
   * consists of two distinct end points. 
   */
  class Segment2D {
    /** 
     * Definitions used in the intersection test to determine when to 
     * report an intersection
     */
  public:
    enum IntersectionFlag {
      ENDPOINT_SEGMENT=1,  //< report intersection when end-point s1.a() touches the segment s2 (not in an end-point)
      ENDPOINT_ENDPOINT=2,
      SEGMENT_SEGMENT=4,
      OVERLAP=8,
      ANY_INTERSECTION=ENDPOINT_SEGMENT|ENDPOINT_ENDPOINT|SEGMENT_SEGMENT|OVERLAP
    };


    /** 
     * Create a default segment (0,0) -> (0,1)
     */
  public:
    inline Segment2D () throws()
      : _a(0,0),_b(0,1) {}
      
    /** 
     * Create a new segment 
     * @param x1 the x-coordinate of the first point
     * @param y1 the y-coordinate of the first point
     * @param x2 the x-coordinate of the second point
     * @param y2 the y-coordinate of the second point
     * @pre (x1!=x2 || y1!=y2)
     */
  public:
    inline Segment2D (double x1,double y1, double x2, double y2) throws()
      : _a(x1,y1),_b(x2,y2)
      {
	assert(x1!=x2 || y1!=y2);
      }

    /** 
     * Create a new segment 
     * @param pa the start point of the segment
     * @param pb the end point of the segment
     * @pre (pa!=pb)
     */
  public:
    inline Segment2D (const Point2D& pa, const Point2D& pb) throws()
      : _a(pa),_b(pb)
    {
      assert(pa!=pb);
    }

    /**
     * Compute a canonical segment. A canonical segment s has the property
     * <code>s.a().x() < s.b().x() || (s.a().x() == s.b().x() && s.a().y() <= s.b().y())</code>
     * @return a canonical segment
     */
  public:
    inline Segment2D canonicalSegment () const throws()
    {
      const double x1 = a().x();
      const double x2 = b().x();
      if (x1 < x2) {
	return *this;
      }
      if (x1 > x2) {
	return reverse();
      }

      const double y1 = a().y();
      const double y2 = b().y();

      if (y1 <= y2) {
	return *this;
      }
      return reverse();
    }
      
    /**
     * Test if this is a canonical segment.
     * @return true if <code>this->canonicalSegment() == *this</code>
     */
  public:
    inline bool isCanonicalSegment () const throws()
    {
      const double x1 = a().x();
      const double x2 = b().x();
      return x1 < x2 || (x1==x2 && a().y()<b().y());
    }

    /**
     * Get the x coordinate
     * @return the x-coordinate
     */
  public:
    inline const Point2D& a() const throws() { return _a; }

    /**
     * Get the y coordinate
     * @return the y-coordinate
     */
  public:
    inline const Point2D& b() const throws() { return _b; }


    /**
     * Compute the vector from a to b
     * @return the vector from a to b
     */
  public:
    inline Vector2D vector() const throws() { return _a.vectorTo(_b); }
      
    /**
     * Compute the length of this segment
     * @return the length of this segment
     */
  public:
    inline double length() const throws() { return _a.distance(_b); }

    /**
     * Compute the square of the length of this segment
     * @return the square of the length of this segment
     */
  public:
    inline double sqrLength() const throws() { return _a.sqrDistance(_b); }

    /**
     * Get the reverse of this segment
     * @return the reverse of this segment
     */
  public:
    inline Segment2D reverse() const throws() { return Segment2D(b(),a()); }

    /**
     * Create a sub-segment for values of s and t.
     * @param s a value between 0 and 1
     * @param t a value between s and 1
     * @return the segment 
     */
  public:
    Segment2D subSegment (double s, double t) const throws();

    /**
     * Determine the orientation of the specified point relative to the line 
     * defined by this segment. Above or below the line is defined by the
     * normal to this segment: above means lying on the opposite side of the normal,
     * below means lying on the same side as the normal. The normal of this segment
     * is defined as 
     * <pre>
     * nx = b().y()-a().y()
     * ny = a().x()-b().x()
     * </pre>
     * The normal vecotor n is thus defined such that the three points <code>a(),b(), and b()+n</code>
     * form a clockwise enumerated triangle.
     * The result of this operation is <em>exact</em> given the precision
     * of the floating point representation of this segment and the point.
     * @return  1 if the point is below the line through this segment (points a(),b(),pt form a CW triangle)
     *          0 if the point is on the line through this segment
     *         -1 if the point is above the line through this segment (points a(),b(),pt form a CW triangle)
     */
  public:
    inline int testPoint (const Point2D& pt) const throws()
    { return computeOrientation(a(),b(),pt); }
      
    /**
     * Determine if this segment intersects the specified segment.
     * The result of this operation is exact, i.e. intersection function
     * can be assumed to produce a proper intersection point.
     * @param s1 a segment
     * @param s2 a segment
     * @param flags report flags
     * @return the flag used to report an intersection
     */
  public:
    static int testIntersection (const Segment2D& s1, const Segment2D& s2, int flags) throws();
      
    /**
     * Determine if this segment intersects the specified segment.
     * The result of this operation is exact, i.e. intersection function
     * can be assumed to produce a proper intersection point.
     * @param s a segment
     * @return if this segment intersects s
     */
  public:
    inline bool testIntersection (const Segment2D& s) const throws()
    { return testIntersection(*this,s,ANY_INTERSECTION); }

    /** 
     * Compare two segments. 
     * @param s a segment
     * @return true if <code>a()==s.a() && b()==s.b()</code>
     */
  public:
    inline bool equals (const Segment2D& s) const throws()
    { return _a==s._a && _b==s._b; }

    /**
     * Test if the two segments are actually the same segment.
     * @param s a segment
     * @return <code>this->equals(s) || this->equals(s.reverse())</code>
     */
  public:
    inline bool isSameSegment (const Segment2D& s) const throws()
    {
      return (a()==s.a() && b()==s.b()) || (b()==s.a() && a()==s.b());
    }

    /** 
     * Compute the square of Cartesian distance between two segments.
     * @param pt a segment
     * @return the square of the distance between the two segments.
     */
  public:
    double sqrDistance (const Segment2D& pt) const throws();
      
    /** 
     * Compute the Cartesian distance between two segments.
     * @param pt a segment
     * @return the distance between the two segments.
     */
  public:
    inline double distance (const Segment2D& pt) const throws()
    { return ::std::sqrt(sqrDistance(pt)); }

    /**
     * Compute a hashvalue for this segment. The hashvalue is constructed
     * so that the segments (a,b) and (b,a) have generally different hash values
     * @return a hashvalue
     */
  public:
    inline size_t hashValue () const throws()
    { return _a.hashValue() ^ (~_b.hashValue()); }
      
    /**
     * Print this segment 
     * @param out an output stream
     */
  public:
    ::std::ostream& print (::std::ostream& out) const;

    /** The segment end points */
  private:
    Point2D _a,_b;
  };      
}

namespace canopy {
  /**
   * Copy a segment array.
   * @param src the segments to be copied
   * @param dest the destination to which to copy the segments
   * @param n the number of segments to be copied
   */
  inline void arraycopy (const ::tikal::Segment2D* src, ::tikal::Segment2D* dest, size_t n) throws()
  { ::std::memcpy(dest,src,n*sizeof(::tikal::Segment2D)); }

  /**
   * Copy a point array.
   * @param src the points to be copied
   * @param dest the destination to which to copy the points
   * @param n the number of points to be copied
   */
  inline void arraymove (const ::tikal::Segment2D* src, ::tikal::Segment2D* dest, size_t n) throws()
  { ::std::memmove(dest,src,n*sizeof(::tikal::Segment2D)); }

}

/**
 * @name Comparison operators
 * @{
 */
inline bool operator== (const ::tikal::Segment2D& a, const ::tikal::Segment2D& b) throws()
{ return a.equals(b); }
inline bool operator!= (const ::tikal::Segment2D& a, const ::tikal::Segment2D& b) throws()
{ return !a.equals(b); }

/*@}*/

inline ::std::ostream& operator<< (::std::ostream& out, const ::tikal::Segment2D& b)
{ return b.print(out); }

#endif
