#include <tikal/AbstractArea2D.h>
#include <tikal/BoundingBox2D.h>
#include <tikal/Contour2D.h>

#include <timber/logging.h>
#include <iostream>

using namespace std;
using namespace timber;
using namespace timber::logging;

namespace tikal {

  AbstractArea2D::AbstractArea2D () throws()
  {}
  
  AbstractArea2D::~AbstractArea2D () throws()
  {}
  
  int AbstractArea2D::locatePoint (double x, double y) const throws()
  {
    if (bounds().classify(x,y)!=0) {
      return 1;
    }
    
    int where = outsideContour()->locatePoint(x,y);
    if (where<0) {
      for (size_t i=0,sz=contourCount();i<sz;++i) {
	int tmp = insideContour(i)->locatePoint(x,y);
	if (tmp < 0) {
	  return 1;
	}
	if (tmp==0) {
	  return 0;
	}
      }
    }
    return where;
  }

  bool AbstractArea2D::isConvex() const throws()
  {
    return contourCount()==0 && outsideContour()->isConvex();
  }
  
  
  Pointer<Region2D> AbstractArea2D::removeHoles () const throws()
  {
    if (contourCount()>0) {
      return Region2D::removeHoles();
    }
    return toRef<Region2D>();
  }
  
  void AbstractArea2D::print (::std::ostream& out) const
  { 
    out << "Area2D {" << endl
	<< "  Outside " << *outsideContour() << endl
	<< "  Inside  " << endl;
    for (size_t i=0;i<contourCount();++i) {
      out << '[' << i << " ] ";
      out << *insideContour(i) << endl;
    }
    out << "}" << endl;
  }
  
}
