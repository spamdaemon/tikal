#ifndef _TIKAL_LINE2D_H
#define _TIKAL_LINE2D_H

#ifndef _TIKAL_POINT2D_H
#include <tikal/Point2D.h>
#endif

#include <cmath>
#include <cstring>
#include <iosfwd>

namespace tikal {
  
  /**
   * This class is used to represent an individual 2D line line. 
   */
  class Line2D {
    /** 
     * Create a default line (0,0) -> (0,1)
     */
  public:
    inline Line2D () throws()
      : _x(0,0),_n(0,1) {}
      
    /** 
     * Create a new line through two points 
     * @param x1 the x-coordinate of the first point
     * @param y1 the y-coordinate of the first point
     * @param x2 the x-coordinate of the second point
     * @param y2 the y-coordinate of the second point
     * @pre REQUIRE_TRUE(x1!=x2 || y1!=y2)
     */
  public:
    inline Line2D (double x1,double y1, double x2, double y2) throws()
      : _x(x1,y1),_n(y1-y2,x2-x1)
      {
	assert(x1!=x2 || y1!=y2);
      }

    /** 
     * Create a new line 
     * @param pt a point on the line
     * @param dir the direction vector
     * @pre REQUIRE_TRUE(x1!=x2 || y1!=y2)
     */
  public:
    inline Line2D (const Point2D& pt, const Vector2D& dir) throws()
      : _x(pt),_n(-dir.y(),dir.x())
      {
	assert(dir.x()!=0 || dir.y()!=0);
      }

    /** 
     * Create a new line through two points
     * @param pa the start point of the line
     * @param pb the end point of the line
     * @pre REQUIRE_TRUE(pa!=pb)
     */
  public:
    inline Line2D (const Point2D& pa, const Point2D& pb) throws()
      : _x(pa),_n(pa.y()-pb.y(),pb.x()-pa.x())
      {
	assert(pa!=pb);
      }

    /**
     * Test a point with respect to this line.
     * @param pt a point
     * @return -1 ,0, 1 if pt is below, on, or above, respectively
     */
  public:
    inline int testPoint (const Point2D& pt) const throws();

    /**
     * Get a point on the line.
     * @return a point on the line
     */
  public:
    inline const Point2D& point() const throws() { return _x; }

    /**
     * Get the normal
     * @return the line normal
     */
  public:
    inline const Vector2D& normal() const throws() { return _n; }
      
    /**
     * Print this line 
     * @param out an output stream
     */
  public:
    ::std::ostream& print (::std::ostream& out) const;

    /** The line end points */
  private:
    Point2D _x;
      
    /** The (non-normalized) orthogonal vector */
  private:
    Vector2D _n;
  };      
}

namespace canopy {
  /**
   * Copy a line array.
   * @param src the lines to be copied
   * @param dest the destination to which to copy the lines
   * @param n the number of lines to be copied
   */
  inline void arraycopy (const ::tikal::Line2D* src, ::tikal::Line2D* dest, size_t n) throws()
  { ::std::memcpy(dest,src,n*sizeof(::tikal::Line2D)); }
  
  /**
   * Copy a point array.
   * @param src the points to be copied
   * @param dest the destination to which to copy the points
   * @param n the number of points to be copied
   */
  inline void arraymove (const ::tikal::Line2D* src, ::tikal::Line2D* dest, size_t  n) throws()
  { ::std::memmove(dest,src,n*sizeof(::tikal::Line2D)); }
  
}


inline ::std::ostream& operator<< (::std::ostream& out, const ::tikal::Line2D& b)
{ return b.print(out); }

#endif
