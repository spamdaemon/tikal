#include <tikal/RegionDecomposition.h>
#include <tikal/AbstractRegion2D.h>
#include <tikal/Polygon2D.h>
#include <tikal/Contour2D.h>
#include <tikal/Polygon2D.h>
#include <tikal/Point2D.h>
#include <tikal/BoundingBox2D.h>
#include <tikal/Segment2D.h>

#include <timber/logging.h>

#include <cassert>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace tikal {
    
  namespace {
    struct Region : public AbstractRegion2D {
      ~Region() throws() {}
      size_t areaCount() const throws() { return _polygons.size(); }
      Reference<Area2D> area (size_t i) const throws() { return _polygons.at(i); }
      BoundingBox2D bounds() const throws() { return _bounds; }
	
      BoundingBox2D _bounds;
      vector<Reference<Area2D> > _polygons;
    };
  }

  RegionDecomposition::RegionDecomposition(const Reference<Region2D>& region) throws (::std::exception)
  : _trapezoids(true), _regionCached(false)
  {
    // build the decomposition here
    const size_t aCount = region->areaCount();
    for (size_t i=0;i<aCount;++i) {
      Reference<Area2D> area = region->area(i);
      if (!insertContour(area->outsideContour(),true)) {
	throw runtime_error("Invalid outside contour");
      }
      const size_t cCount = area->contourCount();
      for (size_t j=0;j<cCount;++j) {
	if (!insertContour(area->insideContour(j),false)) {
	  throw runtime_error("Invalid inside contour");
	}
      }
    }
    prepare();
  }
    
     
  void RegionDecomposition::prepare() throws (::std::exception)
  {
    size_t tCount = _trapezoids.trapezoidCount();
    for (size_t i=0;i<tCount;++i) {
      const Trapezoid& t = trapezoid(i);
      if (!isInteriorTrapezoid(t)) {
	continue;
      }
      // check if the trapezoid needs to be split
      size_t tl =_trapezoids.topLeft(t);
      size_t tr =_trapezoids.topRight(t);
      if (tl!=tr) {
	assert(isIndex(tl));
	assert(isIndex(tr));
	    
	size_t rs = _trapezoids.rightSegment(trapezoid(tl));
	assert(isIndex(rs));

	size_t left = _trapezoids.leftSegment(t);
	size_t right = _trapezoids.rightSegment(t);
	    
#if 0
	const Point2D& b = _segments[rs]._segment.a();
	const Point2D& xl = _segments[left]._segment.a();
	const Point2D& xr = _segments[right]._segment.a();
#else
	const Point2D& b = _trapezoids.point(_trapezoids.topPoint(t));
	const Point2D& xl = _trapezoids.point(_trapezoids.bottomPoint(t));
	const Point2D& xr = xl;
#endif
	Segment diagonal;
	if (_trapezoids.comparePoints(xl,xr) < 0) {
	  assert(_trapezoids.comparePoints(xr,b)<0);
	  diagonal._segment = Segment2D(xr,b);
	}
	else {
	  assert(_trapezoids.comparePoints(xl,b)<0);
	  diagonal._segment = Segment2D(xl,b);
	}
	diagonal._rightInside=diagonal._leftInside=true;
	    
	// actually, we don't need to get the root node, because we
	// know the trapezoids to be split, but we have no handle at this time
	TrapezoidDecomposition::NodePtr root = _trapezoids.rootNode();
	    
	size_t segment = _trapezoids.insertSegment(root,diagonal._segment);
	if (segment==_segments.size()) {
	  _segments.push_back(diagonal);
	}
      }

      const Trapezoid& u = trapezoid(i);
      size_t bl =_trapezoids.bottomLeft(u);
      size_t br =_trapezoids.bottomRight(u);
      if (bl!=br) {
	assert(isIndex(bl));
	assert(isIndex(br));
	size_t rs = _trapezoids.rightSegment(trapezoid(bl));
	assert(isIndex(rs));

	size_t left = _trapezoids.leftSegment(u);
	size_t right = _trapezoids.rightSegment(u);

#if 0
	const Point2D& a = _segments[rs]._segment.b();
	    
	const Point2D& xl = _segments[left]._segment.b();
	const Point2D& xr = _segments[right]._segment.b();
#else
	const Point2D& a = _trapezoids.point(_trapezoids.bottomPoint(t));
	const Point2D& xl = _trapezoids.point(_trapezoids.topPoint(t));
	const Point2D& xr = xl;
#endif
	Segment diagonal;
	if (_trapezoids.comparePoints(xl,xr) > 0) {
	  assert(_trapezoids.comparePoints(xr,a)>0);
	  diagonal._segment = Segment2D(a,xr);
	}
	else {
	  assert(_trapezoids.comparePoints(xl,a)>0);
	  diagonal._segment = Segment2D(a,xl);
	}
	diagonal._rightInside=diagonal._leftInside=true;
	    
	// actually, we don't need to get the root node, because we
	// know the trapezoids to be split, but we have no handle at this time
	TrapezoidDecomposition::NodePtr root = _trapezoids.rootNode();
	    
	size_t segment = _trapezoids.insertSegment(root,diagonal._segment);
	if (segment==_segments.size()) {
	  _segments.push_back(diagonal);
	}
      }


      tCount = _trapezoids.trapezoidCount();
    }
  }

  RegionDecomposition::~RegionDecomposition() throws()
  {}

  Reference<Region2D> RegionDecomposition::buildRegion() const throws()
  {
    if (!_regionCached) {
      Reference<Region> region(new Region());

      size_t tCount = _trapezoids.trapezoidCount();
      for (size_t i=0;i<tCount;++i) {
	const Trapezoid& t = trapezoid(i);
	if (!isInteriorTrapezoid(t)) {
	  continue;
	}
	if (!isIndex(_trapezoids.bottomLeft(t))) {
	  Pointer<Polygon2D> poly(buildPolygon(t));
	  if (poly) {
	    region->_polygons.push_back(poly);
	    region->_bounds.merge(poly->bounds());
	  }
	}
      }
      _region = region;
      _regionCached = true;
    }
    return _region;
  }
      
  bool RegionDecomposition::contains (double x, double y) const throws()
  { return contains(Point2D(x,y)); }


  bool RegionDecomposition::contains (const Point2D& pt) const throws()
  {
    size_t i = _trapezoids.findTrapezoid(pt);
    const TrapezoidDecomposition::Trapezoid& t = _trapezoids.trapezoid(i);
    return isInteriorTrapezoid(t);
  }

  bool RegionDecomposition::isInteriorTrapezoid (const Trapezoid& t) const throws()
  {
    size_t left=_trapezoids.leftSegment(t); 
    size_t right=_trapezoids.rightSegment(t);
    if (!isIndex(left) || !isIndex(right)) {
      return false;
    }
    return _segments[left]._rightInside && _segments[right]._leftInside;
  }


  bool RegionDecomposition::insertContour (const Reference<Contour2D>& c, bool outside) 
  {
    if (c->maxDegree()!=1) {
      return false;
    }
    Point2D pts[2];
    const bool cw = c->vertexOrder().isCW();
    const size_t sCount = c->segmentCount();
    Segment s;
    for (size_t i=0;i<sCount;++i) {
      size_t dim = c->segment(i,pts);
      s._segment=Segment2D(pts[0],pts[1]);
      bool ok = TrapezoidDecomposition::createValidSegment(s._segment);
      if (ok) {
	if (cw) {
	  if (outside) {
	    s._leftInside = false;
	    s._rightInside = true;
	  }
	  else {
	    s._leftInside = true;
	    s._rightInside = false;
	  }
	}
	else {
	  if (outside) {
	    s._leftInside = true;
	    s._rightInside = false;
	  }
	  else {
	    s._leftInside = false;
	    s._rightInside = true;
	  }
	}
      }
      else {
	if (cw) {
	  if (outside) {
	    s._leftInside = true;
	    s._rightInside = false;
	  }
	  else {
	    s._leftInside = false;
	    s._rightInside = true;
	  }
	}
	else {
	  if (outside) {
	    s._leftInside = false;
	    s._rightInside = true;
	  }
	  else {
	    s._leftInside = true;
	    s._rightInside = false;
	  }
	}
      }

      size_t segment = _trapezoids.insertSegment(_trapezoids.rootNode(),s._segment);
      if (segment==_segments.size()) {
	_segments.push_back(s);
      }
    }
	
    return true;
  }

  Pointer<Polygon2D> RegionDecomposition::buildPolygon (const Trapezoid& t) const throws(::std::exception)
  {
    vector<Point2D> verts;
    assert(isInteriorTrapezoid(t));
    assert(!isIndex(_trapezoids.bottomLeft(t)));

    const Trapezoid* u = &t;

    // move up along the left side of the polygon
    size_t lastSegment = invalidIndex();
    while(true) {
      size_t s = _trapezoids.leftSegment(*u);
      if (!isIndex(s)) {
	goto ERROR;
      }
      if (s!=lastSegment) {
	verts.push_back(_segments[s]._segment.a());
	lastSegment = s;
      }
      size_t next = _trapezoids.topLeft(*u);
      if (!isIndex(next)) {
	break;
      }
      u = &trapezoid(next);
    }
	
    // move down along the right side of the polygon
    lastSegment = invalidIndex();
    while(true) {
      size_t s = _trapezoids.rightSegment(*u);
      if (!isIndex(s)) {
	goto ERROR;
      }
      if (s!=lastSegment) {
	verts.push_back(_segments[s]._segment.b());
	lastSegment = s;
      }
      size_t next = _trapezoids.bottomRight(*u);
      if (!isIndex(next)) {
	break;
      }
      u = &trapezoid(next);
	  
    }
	
    return Polygon2D::create(verts,VertexOrder::CW);
  ERROR:
    Log("tikal.RegionDecomposition").warn("Error rebuilding polygon");
    return Pointer<Polygon2D>();
  }

}
