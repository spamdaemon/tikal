#include <tikal/Vector2D.h>
#include <iostream>

namespace {
  static short QUADRANTS[4] = { 1,2,4,3 };
  inline short computeQuadrant (double x,double y)
  { 
    int s1 = x >= 0.0 ? 0 : 1;
    int s2 = y >= 0.0 ? 0 : 2;
    return QUADRANTS[s1|s2];
  }
}

namespace tikal {
  
  ::std::ostream& Vector2D::print (::std::ostream& out) const
  { 
    return out << "( " << x() << "  " << y() << " )"; 
  }
  
  int Vector2D::quadrant () const throws()
  { return computeQuadrant(_v[0],_v[1]); }
  
  bool Vector2D::compareAngles (const Vector2D& v) const throws()
  {
    const double x1 = _v[0];
    const double y1 = _v[1];
    const double x2 = v._v[0];
    const double y2 = v._v[1];
    
    int q1 = computeQuadrant(x1,y1);
    int q2 = computeQuadrant(x2,y2);
    
    if (q1==q2) {
      return y1*x2 < y2*x1;
    }
    return q1<q2;
  }
}
