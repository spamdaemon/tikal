#ifndef _TIKAL_SWEEPLINE_H
#define _TIKAL_SWEEPLINE_H

#ifndef _TIKAL_POINT2D_H
#include <tikal/Point2D.h>
#endif

#ifndef _TIKAL_VERTEXORDER_H
#include <tikal/VertexOrder.h>
#endif

#include <set>

namespace tikal {

  /** A segment */
  class Segment2D;

  /** An intersection function */
  class IntersectionFunction;

  /** 
   * The SweepLine implements the classic Bentley-Ottman sweepline algorithm. Clients add line
   * segments to the sweepline in arbitrary order using the insertSegment() function. To compute
   * the set of non-intersecting line segments, call sweepSegments() with a callback function that will
   * be invoked for each line segment found. An optional intersection function can be specified
   * to control the accuracy with which intersection can be computed.
   */
  class SweepLine {
    SweepLine (const SweepLine&);
    SweepLine&operator=(const SweepLine&);

    /** The type item */
  public:
    typedef int Data;

    /** The line that is currently intersected by the sweep line */
    class Line;
      
    /** A line segment */
  public:
    typedef Line* Segment;
      
    /** An event */
  private:
    class Event;
    
    /** The event heap */
  private:
    typedef ::std::set<Event> EventQueue;
    
    /** The callback data that will be passed to the callback */
  public:
    class SegmentData {
      /** 
       * Create new segment data
       * @param dat the data
       * @param vo the vertex order
       * @param sweepDir the direction in which the segment will be swept
       */
    public:
      inline SegmentData (Data dat, const VertexOrder& vo, const VertexOrder& sweepDir) throws()
	: _data(dat), _direction(vo),_sweepDirection(sweepDir)
      {}
	
      /**
       * Get the data.
       * @return the data
       */
    public:
      inline Data data() const throws() { return _data; }

      /**
       * The direction of the original segment
       * @return the direction of the original segment
       */
    public:
      inline VertexOrder direction() const throws() { return _direction; }

      /** 
       * The relative direction in which the sweepline will enumerate this segment. If
       * this direction is not direction(), then the start and end vertices have been 
       * reversed when sweeping
       * @return the relative direction in which the sweepline traverses the edge
       */
    public:
      inline VertexOrder sweepDirection() const throws() { return _sweepDirection; }

    private: 
      Data _data; //< The segment identifier
	
    private:
      VertexOrder _direction; //< The original direction of the segment

    private:
      VertexOrder _sweepDirection; //< The direction of the sweep
    };
      
    /** A callback that is invoked whenever to lines become adjacent */
  public:
    class Callback {
      /** Protected destructor */
    protected:
      virtual ~Callback() throws() {}

      /** 
       * Notify clients that a new segment has been emitted
       * @param start the start point
       * @param end the end point 
       * @param code a code
       * @param data data associated with the segment
       */
    public:
      virtual void notify(const Point2D& start, const Point2D& end, int code, const SegmentData& data)  throws() = 0;
    };
      
    /**
     * Create a new sweep line.
     */
  public:
    SweepLine();
      
    /**
     * Destroy this sweep line
     */
  public:
    virtual ~SweepLine() throws();

    /**
     * Clear this sweepline. Removes all segments in the sweepline
     * and resets it
     * @pre !isSweeping()
     */
  public:
    void reset() throws();

    /**
     * Insert a new and possibly duplicate line segment. There may be multiple segments
     * that can be started at (x,y) and/or have the same slope. 
     * <p>
     * This method must be invoked if a subclass overrides it.
     * @param start the start point of the segment
     * @param end a point on the segment
     * @param dir the direction in which the start and end point are specified
     * @param sweepDirection the direction in which the segment will actually be swept
     * @param data data to be associated with the segment
     * @pre start.x()<end.x() || (start.x()==end.x() && start.y()<end.y())
     * @pre !isSweeping())
     */
  protected:
    virtual void insertSegment (const Point2D& start, const Point2D& end, const VertexOrder& dir, const VertexOrder& sweepDirection, Data data) throws();

    /**
     * Insert a new and possibly duplicate line segment. There may be multiple segments
     * that can be started at (x,y) and/or have the same slope.
     * @param start the start point of the segment
     * @param end a point on the segment
     * @param dir the direction of travel of the segment
     * @param data data to be associated with the segment
     * @pre REQUIRE_TRUE(start.x()<end.x() || (start.x()==end.x() && start.y()<end.y()))
     * @pre !isSweeping())
     */
  public:
    inline void insertSegment (const Point2D& start, const Point2D& end, const VertexOrder& dir, Data data) throws()
    { insertSegment(start,end,dir,dir,data); }

    /**
     * Insert an unordered segment. Calls insertSegment() to insert the segment
     * in the correct order.
     * @param start the start point
     * @param end an end point
     * @param dir the direction of travel of the segment
     * @param data the data
     * @return true if a segment was inserted, false if <code>start==end</code>
     * @pre !isSweeping())
     */
  public:
    bool insertUnorderedSegment (const Point2D& start, const Point2D& end, const VertexOrder& dir, Data data) throws();

    /**
     * Insert a new and possibly duplicate line segment. This method is equivalent
     * to <code>insertSegment(start,end,VertexOrder::cw(),data)</code>.
     * @param start the start point of the segment
     * @param end a point on the segment
     * @param data data to be associated with the segment
     * @pre REQUIRE_TRUE(start.x()<end.x() || (start.x()==end.x() && start.y()<end.y()))
     * @pre !isSweeping())
     */
  public:
    void insertSegment (const Point2D& start, const Point2D& end, Data data) throws();

    /**
     * Insert an unordered segment. This call is equivalent 
     * to <code>insertUnorderedSegment(start,end,VertexOrder::cw(),data)</code>.
     * @param start the start point
     * @param end an end point
     * @param data the data
     * @return true if a segment was inserted, false if <code>start==end</code>
     * @pre !isSweeping())
     */
  public:
    bool insertUnorderedSegment (const Point2D& start, const Point2D& end, Data data) throws();

    /**
     * Perform the sweep of the segments in order of increasing x.
     * @param cb the callback to be invoked for each segment
     * @pre !isSweeping())
     */
  public:
    void sweepSegments(Callback& cb) throws();

    /**
     * Perform the sweep of the segments in order of increasing x.
     * @param ifunc an intersection function to be used
     * @param cb the callback to be invoked for each segment
     * @pre !isSweeping())
     */
  public:
    void sweepSegments(const IntersectionFunction& ifunc, Callback& cb) throws();
      
    /**
     * Determine if the line is currently sweeping segments. 
     * @return true if this sweepline is currently processing all segments
     */
  public:
    inline bool isSweeping() const throws() { return _sweeping; }

    /**
     * Start a new line segment. There may be multiple segments
     * that can be started at (x,y) and/or have the same slope.
     * @param line the segment to be started
     * @param cb a callback
     * @return an id representing the new segment
     * @pre REQUIRE_GREATER_OR_EQUAL(x0,position())
     * @pre REQUIRE_GREATER(dx,0.0);
     * @pre REQUIRE_FALSE(y()==NAN)
     */
  private:
    Segment startSegment (Line* line, const IntersectionFunction& isector, Callback& cb) throws();

    /**
     * Terminate a segment and remove it from this sweep line.
     * @param segment the segment to be terminated
     * @param cb a callback that will be invoked for 
     * @pre REQUIRE_GREATER_OR_EQUAL(x0,position())
     * @pre REQUIRE_FALSE(y()==NAN)
     */
  private:
    void terminateSegment (Segment segment, const IntersectionFunction& isector,Callback& cb)  throws();
      
    /**
     * Start a new vertical segment.
     * @param line the line
     * @param cb a callback
     * @pre REQUIRE_TRUE(pt.y()==NAN || (pt.x()==position() && pt.y() >= positionY()))
     * @return an id representing the new segment
     */
  private:
    Segment startVerticalSegment (Line* line, const IntersectionFunction& isector, Callback& cb)  throws();

    /**
     * Terminate a vertical segment and remove it from this sweep line.
     * @param segment the segment to be terminated
     * @param pt the segment's endpoint
     * @param cb a callback that will be invoked for 
     * @pre REQUIRE_EQUAL(pt.x(),position())
     * @pre REQUIRE_GREATER_OR_EQUAL(pt.y(),positionY())
     */
  private:
    void terminateVerticalSegment (Segment segment, const IntersectionFunction& isector, Callback& cb) throws();
      
    /**
     * Two lines have become adjacent.
     * @param p a line
     * @param n a line
     * @param isector the intersection function
     */
  private:
    void notifyAdjacent (Line* p, Line* n, const IntersectionFunction& isector)  throws();
      
    /**
     * Invoke the callback.
     * @param pt new end point
     * @param intersectionPoint true if pt is an intersection point
     * @param line a line
     * @param cb a callback
     */
  private:
    void notify (const Point2D& pt, bool intersectionPoint, Line* line, Callback& cb) throws();

    /**
     * Process an intersection event.
     * @param pt a point
     * @param p the line 
     * @param n the line
     * @param segment the segment
     * @param cb a callback
     */
  private:
    void swapSegments (const Point2D& pt, Line* p, Line* n, const IntersectionFunction& isector, Callback& cb) throws();

    /** Verify this structure at the current position */
  private:
    void verify() const throws();

    /**
     * Enqueue an event.
     * @param e an event
     */
  private:
    void newEvent (const Event& e) throws();

    /**
     * Get the next event.
     * @return the next event for processing
     */
  private:
    Event nextEvent () throws();
      
    /**
     * Determine if there are more event.
     * @return true if there are more events
     */
  private:
    bool hasNextEvent() const throws();

    /**
     * The number of items in the queue.
     * @return the number of items in the queue
     */
  private:
    size_t eventCount() const throws();

    /**
     * Get the current x-position
     */
  private:
    inline double x() const throws() 
    { return _x; }

    /**
     * Get the current position
     */
  private:
    inline double y() const throws() 
    { return _y; }

    /** True if currently sweeping */
  private:
    bool _sweeping;

    /** The current sweepline position */
  private:
    double _x;

    /** The y-position */
  private:
    double _y;

    /** The event queue */
  private:
    EventQueue _eventQueue;

    /** The lines that are currently intersected by the sweep line */
  private:
    Line* _sweepline;

    /** The vertical line segment */
  private:
    Line* _verticals;

    /** The start of the verticals */
  private:
    Line* _currentVertical;
  };
}

#endif
