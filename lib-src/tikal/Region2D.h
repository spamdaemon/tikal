#ifndef _TIKAL_REGION2D_H
#define _TIKAL_REGION2D_H

#ifndef _TIKAL_H
#include <tikal/tikal.h>
#endif

#include <iosfwd>
#include <vector>

namespace tikal {

  /** A point */
  class Point2D;

  /** A contour */
  class Path2D;

  /** An area */
  class Area2D;

  /** A vertex ordering */
  class VertexOrder;

  /** A bounding box */
  class BoundingBox2D;

  /**
   * An region is a geometry object defined as a set of areas 
   * which are possibly overlapping.
   */
  class Region2D : public ::timber::Counted {

    /** 
     * Create an empty region.
     */
  protected:
    Region2D () throws();

    /**
     * Destructor
     */
  public:
    ~Region2D() throws();

    /**
     * Create a new region from a path. This method
     * will determine the internal and external contours
     * of areas and combine them into a single region.
     * @param p a path
     * @param outside the vertex ordering of outside region in the path
     * @return a region
     */
  public:
    static ::timber::Pointer<Region2D> create (const ::timber::Reference<Path2D>& p, const VertexOrder& outside);

    /**
     * Create a new region from a set of non-intersecting areas.
     * @param areas a list of non-intersecting areas
     * @return a region
     */
  public:
    static ::timber::Reference<Region2D> create (const ::std::vector< ::timber::Reference<Area2D> >& areas);

    /**
     * Create a new region from a set of non-intersecting regions.
     * @param areas a list of non-intersecting regions
     * @return a region
     */
  public:
    static ::timber::Reference<Region2D> create (const ::std::vector< ::timber::Reference<Region2D> >& areas);

    /**
     * Create a new region from a path. All regions defined by the path will
     * be merged into a single region. The vertex ordering of the individual areas 
     * will be undefined.
     * @param p a path
     * @return a region
     */
  public:
    static ::timber::Pointer<Region2D> createMergedPath (const ::timber::Reference<Path2D>& p);

    /**
     * Create a new region from a path. All regions defined by the path will
     * be intersected into a single region. The vertex ordering of the individual areas 
     * will be undefined.
     * @param p a path
     * @return a region or null if the region is empty
     */
  public:
    static ::timber::Pointer<Region2D> createIntersectionPath (const ::timber::Reference<Path2D>& p);

    /**
     * Create a new region from a path. All regions defined by the path will
     * be xor'ed into a single region. The vertex ordering of the individual areas 
     * will be undefined.
     * @param p a path
     * @return a region or null if the region is empty
     */
  public:
    static ::timber::Pointer<Region2D> createXORPath (const ::timber::Reference<Path2D>& p);

    /**
     * @name Standard region operations
     * @{
     */
      
    /**
     * Intersect a path with this region.
     * @param p a path
     * @return a path that is clipped to this region
     */
  public:
    virtual ::timber::Pointer<Path2D> clipPath (const ::timber::Reference<Path2D>& p) const throws();

    /**
     * Intersect two regions.
     * @param p a region
     * @return the intersection region
     */
  public:
    virtual ::timber::Pointer<Region2D> intersect (const ::timber::Reference<Region2D>& p) const throws();

    /**
     * Compute the union of two regions
     * @param p a region
     * @return a region representing the union
     */
  public:
    virtual ::timber::Reference<Region2D> merge (const ::timber::Reference<Region2D>& p) const throws();

    /**
     * Compute the difference of two regions
     * @param p a region
     * @return a region representing the difference between two regions
     */
  public:
    virtual ::timber::Pointer<Region2D> subtract (const ::timber::Reference<Region2D>& p) const throws();
      
    /**
     * Compute the exclusive-or between two regions
     * @param p a region
     * @return a region that represents the exclusive or of two regions
     */
  public:
    virtual ::timber::Pointer<Region2D> exclusiveOR (const ::timber::Reference<Region2D>& p) const throws();
       
    /**
     * Normalize this region.
     * @return a new region that is no longer self-intersecting and consists of non-overlapping regions or
     * null if an error occurred
     */
  public:
    virtual ::timber::Pointer<Region2D> normalize() const throws();

    /**
     * Reduce the (perceived) accuracy of this region. This will result in
     * a slightly modified region whose vertices have less precision as they
     * are rounded to the nearest precision.
     * If the precision is 0, then this region is returned.
     * @param prec the nearest precision 
     * @pre prec >= 0
     * @return a new region or 0 if an error occurred
     */
  public:
    virtual ::timber::Pointer<Region2D> reducePrecision(double prec) const throws();

    /*@}*/

    /**
     * Convert this region into an equivalent region that does not contain any holes
     * @return a new region without holes in areas or null if an error occurred
     */
  public:
    virtual ::timber::Pointer<Region2D> removeHoles () const throws();

    /**
     * Get the number of areas in this region. If the region is empty then
     * this method must retun 0, otherwise at least 1.
     * @return the number of areas in this region
     */
  public:
    virtual size_t areaCount() const throws() = 0;

    /**
     * Get the area at the specified index.
     * @param i the index of a some area
     * @pre REQUIRE_RANGE(i,0,areaCount()-1)
     * @return the given area
     */
  public:
    virtual ::timber::Reference<Area2D> area(size_t i) const throws() = 0;
	
    /**
     * Test where a point is with respect to  the region
     * @param pt a point
     * @return true if the point is inside
     */
  public:
    bool contains (const Point2D& pt) const throws();
      
    /**
     * Test where a point is with respect to  the region
     * @param x a x-coordinate
     * @param y a y-coordinate
     * @return true if the point is inside
     */
  public:
    virtual bool contains (double x, double y) const throws();

    /**
     * Determine the orientation of a point with respect to this region.
     * @param pt a point
     * @return -1 if pt lies inside this region, 0 if it lies on the boundary, and 1 if it lies outside
     */
  public:
    int locatePoint(const Point2D& pt) const throws();
      
    /**
     * Determine the orientation of a point with respect to this region.
     * @param x a x-coordinate
     * @param y a y-coordinate
     * @return -1 if pt lies inside this region, 0 if it lies on the boundary, and 1 if it lies outside
     */
  public:
    virtual int locatePoint (double x, double y) const throws() = 0;

    /**
     * Get the bounding box for this region.
     * @return a bounding box.
     */
  public:
    virtual BoundingBox2D bounds() const throws() = 0;
      
    /**
     * Test if this region is a convex region.
     * @return true if this region is a convex region
     */
  public:
    virtual bool isConvex() const throws() = 0;

    /**
     * Print this region 
     * @param out an output stream
     */
  public:
    virtual void print (::std::ostream& out) const = 0;
  };
}


inline ::std::ostream& operator<< (::std::ostream& out, const ::tikal::Region2D& b)
{
  b.print(out);
  return out;
}


#endif
