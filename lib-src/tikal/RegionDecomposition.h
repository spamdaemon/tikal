#ifndef _TIKAL_REGIONDECOMPOSITION_H
#define _TIKAL_REGIONDECOMPOSITION_H

#ifndef _TIKAL_TRAPEZOIDDECOMPOSITION_H
#include <tikal/TrapezoidDecomposition.h>
#endif

#ifndef _TIKAL_REGION2D_H
#include <tikal/Region2D.h>
#endif

#ifndef _TIKAL_POINT2D_H
#include <tikal/Point2D.h>
#endif

#ifndef _TIKAL_SEGMENT2D_H
#include <tikal/Segment2D.h>
#endif

#include <vector>

namespace tikal {
  class Segment2D;
  class Point2D;
  class Contour2D;
  class Polygon2D;

  /**
   * A default datastructure for building trapezoid decompositions
   * of for arbitrary non-intersecting line segments. The interface exposed
   * by this class allows this decomposition to serve in implementing
   * Seidel's randomized polygon triangulation algorithm.
   */
  class RegionDecomposition {
    RegionDecomposition&operator=(const RegionDecomposition&);
    RegionDecomposition(const RegionDecomposition&);
	
    /** A trapezoid */
  private:
    typedef TrapezoidDecomposition::Trapezoid Trapezoid;

    /** The segment information */
  private:
    struct Segment {
      friend class 	 RegionDecomposition;
      /** The points of the segment */
    private:
      Segment2D _segment;
	  
      /** True if the point inside lies on the specified side. */
      bool _leftInside,_rightInside;
    };

    /**
     * Create a polygon decomposition. This decomposition will not
     * hold on to the region beyond this constructor. 
     * @param r a region 
     * @throws ::std::exception if the region contains non-linear segments
     * @throws ::std::exception if a valid trapezoid decomposition cannot be created
     */
  public:
    RegionDecomposition(const ::timber::Reference<Region2D>& r) throws (::std::exception);

    /** Destroy this polygon decomposition  */
  public:
    ~RegionDecomposition() throws();
	
    /**
     * Build a region that covers the same are area the original region. The 
     * new region will not contain holes. Each area contained in the region
     * will be a {@link ::tikal::Polygon2D}.
     * @return a region that does not contain any holes
     */
  public:
    ::timber::Reference<Region2D> buildRegion() const throws();
	
    /**
     * Determine if a point lies with the original region.
     * @param x a x-coordinate
     * @param y a y-coordinate
     * @return true if the point lies within the original region
     */
  public:
    bool contains (double x, double y) const throws();

    /**
     * Determine if a point lies with the original region.
     * @param pt a point
     * @return true if the point lies within the original region
     */
  public:
    bool contains (const Point2D& pt) const throws();

    /**
     * Test if the specified index is a valid index 
     * @param i an index
     * @return true if i is a valid index
     */
  private:
    inline static size_t invalidIndex () throws()
    { return ~size_t(0); }

    /**
     * Test if the specified index is a valid index 
     * @param i an index
     * @return true if i is a valid index
     */
  private:
    inline static bool isIndex (size_t i) throws()
    { return ptrdiff_t(i) >= 0; }

    /** 
     * Insert a contour into the trapezoidation
     * @param c a contour
     * @param outside true if the contour is an outside contour
     * @return true if th contour was completely added, false otherwise
     * @throws ::std::exception if an error occurred
     */
  private:
    bool insertContour (const ::timber::Reference<Contour2D>& c, bool outside);
	
    /**
     * Create a clockwise oriented polygon by following the links of a trapezoid. 
     * @param t a trapezoid
     * @return a polygon
     */
  private:
    ::timber::Pointer<Polygon2D> buildPolygon (const Trapezoid& t) const throws (::std::exception);

    /**
     * Determine if a trapezoid is an interior trapezoid
     * @param t a trapezoid
     * @return true if t is an interior trapezoid
     */
  private:
    bool isInteriorTrapezoid (const Trapezoid& t) const throws();

    /**
     * Prepare the trapezoids for extracting polygons.
     * @throws ::std::exception if an error occurred
     */
  private:
    void prepare() throws (::std::exception);

    /**
     * Get a trapezoid.
     * @param i a trapezoid index
     * @return a trapezoid
     */
  private:
    inline const Trapezoid& trapezoid(size_t i) const throws()
    { return _trapezoids.trapezoid(i); }
	
    /** The default decomposition used to build the trapezoids */
  private:
    TrapezoidDecomposition _trapezoids;

    /** The cached region */
  private:
    mutable ::timber::Pointer<Region2D> _region;
	
    /** True if the region has been cached */
  private:
    mutable bool _regionCached;

    /** A vector */
  private:
    ::std::vector<Segment> _segments;
  };
}

#endif
