#ifndef _TIKAL_CONTOUR2D_H
#define _TIKAL_CONTOUR2D_H

#ifndef _TIKAL_VERTEXORDER_H
#include <tikal/VertexOrder.h>
#endif

#ifndef _TIKAL_AREA2D_H
#include <tikal/Area2D.h>
#endif


#include <iosfwd>

namespace tikal {
  class BoundingBox2D;
  class Point2D;

  /**
   * A contour is defined by a connected set of vertices and may possibly
   * be closed.
   */
  class Contour2D : public Area2D {
    Contour2D&operator=(const Contour2D&);
    Contour2D(const Contour2D&);

    /** Create a new contour  */
  protected:
    Contour2D() throws();
      
    /** Destroy this contour   */
  public:
    ~Contour2D() throws();

    /**
     * Get this contour as a path.
     * @return a path representing this contour.
     */
  public:
    virtual ::timber::Reference<Path2D> getContourPath() const throws();

    /**
     * Get the traversal direction.
     * @return the order in which vertices are enumerated
     */
  public:
    virtual VertexOrder vertexOrder () const throws() = 0;

    /**
     * Get the number of segments in this contour
     * @return the number of segments in this contour
     */
  public:
    virtual size_t segmentCount() const throws() = 0;

    /**
     * Get the number of points defining the segment with the highest degree.
     * @return the maximum degree among all segments
     */
  public:
    virtual size_t maxDegree() const throws() = 0;

    /**
     * Ge the vertices used to define the specified segment. The
     * points array must be large enough to hold maxDegree()+1
     * points.
     * @param i a segment index
     * @pre REQUIRE_RANGE(i,0,segmentCount()-1)
     * @param pts the vertices are stored in traversal order
     * @return the degree of the given segment.
     */
  public:
    virtual size_t segment (size_t i, Point2D* pts) const throws() = 0;

    /**
     * Get the outside contour.
     * @return this contour
     */
  public:
    ::timber::Reference<Contour2D> outsideContour() const throws();

    /**
     * Get the number of contours. This can actually be 0, which 
     * might be the result of a difference operation.
     * @return 0
     */
  public:
    size_t contourCount() const throws();
      
    /**
     * Get an internal contour
     * @param i the index of an internal contour
     * @return asserts
     */
  public:
    ::timber::Reference<Contour2D> insideContour(size_t i) const throws();

    /**
     * Remove holes from this region.
     * @return this polygon
     */
  public:
    ::timber::Pointer<Region2D> removeHoles () const throws();
    
    /**
     * Print this contour 
     * @param out an output stream
     */
  public:
    void print (::std::ostream& out) const;
  };
}

#endif
