#ifndef _TIKAL_ABSTRACTREGION2D_H
#define _TIKAL_ABSTRACTREGION2D_H

#ifndef _TIKAL_REGION2D_H
#include <tikal/Region2D.h>
#endif

namespace tikal {
  /**
   * An region is a geometry object defined as a set of areas.
   */
  class AbstractRegion2D : public Region2D {

    /** 
     * Create an empty region.
     */
  protected:
    AbstractRegion2D () throws();

    /**
     * Destructor
     */
  public:
    ~AbstractRegion2D() throws();

    /**
     * Determine the orientation of a point with respect to this region.
     * @param x a x-coordinate
     * @param y a y-coordinate
     * @return -1 if pt lies inside this region, 0 if it lies on the boundary, and 1 if it lies outside
     */
  public:
    int locatePoint (double x, double y) const throws();

    /**
     * Test if this region is a convex region.
     * @return true if this region is a convex region
     */
  public:
    bool isConvex() const throws();

    /**
     * Print this region 
     * @param out an output stream
     */
  public:
    void print (::std::ostream& out) const;
  };
}

#endif
