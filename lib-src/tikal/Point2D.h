#ifndef _TIKAL_POINT2D_H
#define _TIKAL_POINT2D_H

#ifndef _TIKAL_VECTOR2D_H
#include <tikal/Vector2D.h>
#endif

#include <cstring>

namespace tikal {
  
  /**
   * This class represents point on a planar cartesian coordinate system.
   * This class has been kept small on purpose, because
   * calculations should be performed using a math package.
   */
  class Point2D {

    /** Create a point from a vector */
  public:
    inline Point2D (const Vector2D& v) throws()
      : _x(v)
    {}

    /** Create a default point */
  public:
    inline Point2D () throws()
    {}

    /** 
     * Create a new point 
     * @param fx x-coordinate of the point
     * @param fy y-coordinate of the point
     */
  public:
    inline Point2D (double fx, double fy) throws()
      : _x(fx,fy)
      {}


    /**
     * Get the coordinate at the specified index.
     * @param i an index of 0 (=x) or 1 (=y)
     * @return the coordinate
     */
  public:
    inline double operator[] (size_t i) const throws() 
    { return _x[i]; }
      
    /**
     * Get the x coordinate
     * @return the x-coordinate
     */
  public:
    inline double x() const throws() 
    { return _x.x(); }
      
    /**
     * Get the y coordinate
     * @return the y-coordinate
     */
  public:
    inline double y() const throws() 
    { return _x.y(); }
      

    /**
     * Set the x value
     * @param xx the new x-value
     */
  public:
    inline void setX(double xx) throws() 
    { _x.setX(xx); }

    /**
     * Set the y value
     * @param yy the new y-value
     */
  public:
    inline void setY(double yy) throws()
    { _x.setY(yy); }

    /**
     * Set the x and y values
     * @param xx the new x-value
     * @param yy the new y-value
     */
  public:
    inline void set(double xx, double yy) throws() 
    { _x.set(xx,yy);}

    /**
     * Translate this point by a given vector
     * @param dx the translation along x
     * @param dy the translation along y
     * @return *this point
     */
  public:
    inline Point2D& translate(double dx, double dy) throws()
    { _x.set(dx+x(),dy+y()); return *this; }
      
    /**
     * Translate this point by a given vector
     * @param v a translation vector
     * @return *this point
     */
  public:
    inline Point2D& translate(const Vector2D& v) throws()
    { return translate(v.x(),v.y()); }

    /** Compare two points. */
  public:
    inline bool equals (const Point2D& pt) const throws()
    { return _x.equals(pt._x); }

    /**
     * Determine if two points are exactly the same.
     * @param pt a point
     * @return true if this point is exactly the same as pt
     */
  public:
    inline bool operator== (const Point2D& pt) const throws()
    { return x()==pt.x() && y() == pt.y(); }

    /**
     * Determine if two points are different.
     * @param pt a point
     * @return true if this point is different from pt
     */
  public:
    inline bool operator!= (const Point2D& pt) const throws()
    { return x()!=pt.x() || y() != pt.y(); }

    /**
     * Determine if this point is before another one.
     * @param pt a point
     * @return true if this x() < pt.x() || (x()==pt.x() &&  y() < pt.y())
     */
  public:
    inline bool operator< (const Point2D& pt) const throws()
    { return x()<pt.x() || (x()==pt.x() && y()< pt.y()); }
    
    /** 
     * Compute the square of Cartesian distance between two points.
     * @param pt a point
     * @return the square of the distance between the two points.
     */
  public:
    inline double sqrDistance (const Point2D& pt) const throws()
    { return _x.sqrDistance(pt._x); }
      
    /** 
     * Compute the Cartesian distance between two points.
     * @param pt a point
     * @return the distance between the two points.
     */
  public:
    inline double distance (const Point2D& pt) const throws()
    { return _x.distance(pt._x); }
      
    /**
     * Compute a hashvalue for this point.
     * @return a hashvalue
     */
  public:
    inline size_t hashValue () const throws()
    { return _x.hashValue(); }

    /**
     * Create a point translated by a given offset
     * @param xoff the offset along x
     * @param yoff the offset along y
     * @return the point x()+xoff, y()+yoff
     */
  public:
    inline Point2D translate (double xoff, double yoff) const throws()
    { Point2D p(*this); p._x.translate(xoff,yoff); return p; }

    /**
     * Create a point scaled along each axis.
     * @param xscale the scale factor along x
     * @param yscale the scale factor along y
     * @return the point x()*xscale, y()+yscale
     */
  public:
    inline Point2D scale (double xscale, double yscale) const throws()
    { Point2D p(*this); p._x.scale(xscale,yscale); return p; }
      
    /**
     * Create a point uniformly scaled along each axis.
     * @param s the scale factor along x and y
     * @return the point x()*s, y()*s
     */
  public:
    inline Point2D scale (double s) const throws()
    { Point2D p(*this); p._x.scale(s,s); return p; }
      
    /**
     * Create a point counter-clockwise rotated around the origin by a given angle.
     * @param theta the angle of rotation in radians
     * @return a rotated point
     */
  public:
    inline Point2D rotate (double theta) const throws()
    { Point2D p(*this); p._x.rotate(theta); return p; }
      
    /**
     * Compute the vector to the specified ponit
     * @param p a point
     * @return the vector to p
     */
  public:
    inline Vector2D vectorTo(const Point2D& p) const throws()
    { return p._x-_x; }
      
    /**
     * Print this point 
     * @param out an output stream
     */
  public:
    inline ::std::ostream& print (::std::ostream& out) const
    { return _x.print(out); }
      
    /** The point coordinates */
  private:
    Vector2D _x;
  };
  
}

namespace canopy {
  /**
   * Copy a point array.
   * @param src the points to be copied
   * @param dest the destination to which to copy the points
   * @param n the number of points to be copied
   */
  inline void arraycopy (const ::tikal::Point2D* src, ::tikal::Point2D* dest, size_t n) throws()
  { ::std::memcpy(dest,src,n*sizeof(::tikal::Point2D)); }
  
  /**
   * Copy a point array.
   * @param src the points to be copied
   * @param dest the destination to which to copy the points
   * @param n the number of points to be copied
   */
  inline void arraymove (const ::tikal::Point2D* src, ::tikal::Point2D* dest, size_t n) throws()
  { ::std::memmove(dest,src,n*sizeof(::tikal::Point2D)); }
}


inline ::tikal::Vector2D operator- (const ::tikal::Point2D& a, const ::tikal::Point2D& b) throws()
{ return b.vectorTo(a); }

inline ::std::ostream& operator<< (::std::ostream& out, const ::tikal::Point2D& b)
{ return b.print(out); }

#endif
