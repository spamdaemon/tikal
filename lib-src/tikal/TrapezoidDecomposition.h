#ifndef _TIKAL_TRAPEZOIDDECOMPOSITION_H
#define _TIKAL_TRAPEZOIDDECOMPOSITION_H

#ifndef _TIKAL_POINT2D_H
#include <tikal/Point2D.h>
#endif

#include <algorithm>
#include <vector>
#include <cassert>
#include <iosfwd>

namespace tikal {
  class Segment2D;
  /**
   * A default datastructure for building trapezoid decompositions
   * of for arbitrary non-intersecting line segments. The interface exposed
   * by this class allows this decomposition to serve in implementing
   * Seidel's randomized polygon triangulation algorithm.
   */
  class TrapezoidDecomposition {
    TrapezoidDecomposition(const TrapezoidDecomposition&);
    TrapezoidDecomposition&operator=(const TrapezoidDecomposition&);

    /** A segment is just a couple of indices */
  private:
    typedef ::std::pair<size_t,size_t> Segment;

    /** A pointer to a node in the tree */
  public:
    class NodePtr {
      friend class TrapezoidDecomposition;

      /** The default constructor */
    public:
      inline NodePtr() throws() : _node(invalidIndex()) {}

      /** Constructor from an index */
    private:
      inline explicit NodePtr (size_t i) throws() : _node(i) {}

      /** Node comparator */
    public:
      inline bool operator==(const NodePtr& n) const throws() { return _node==n._node; }

      /** Node comparator */
    public:
      inline bool operator!=(const NodePtr& n) const throws() { return _node!=n._node; }

    public:
      inline bool isValid() const throws() { return isIndex(_node); }

    private:
      size_t _node;
    };

    /** A tree node */
  private:
    class Node {
      /** The default composition is a friend */
      friend class TrapezoidDecomposition;

      /** There are three types of nodes */
      enum Type { SEGMENT,POINT,TRAPEZOID };
	  
      /**
       * Create a trapezoid node
       * @param trapezoidIndex the index of a trapezoid
       */
      inline Node (size_t trapezoidIndex) throws()
	: _type(TRAPEZOID), _object(trapezoidIndex) {}
	  
	  
    private:
      Type _type; //< The node type
	  
    private:
      size_t _object; //< either a point, a segment, or a trapezoid node
	  
    private:
      NodePtr _less; //< the nodes left/top and right/bottom

    private:
      NodePtr _greater;
    };

    /** A trapezoid */
  public:
    class Trapezoid {
      /** The default composition is a friend */
      friend class TrapezoidDecomposition;

      /**
       * Create a default trapezoid
       */
    public:
      Trapezoid() throws();
 
      /**
       * Constructor
       * @param i the index or this trapezoid
       */
    private:
      Trapezoid (size_t i) throws();


      /**
       * Update the top-left trapezoid to point to <code>nt</code>
       * if it currently poinst to <code>ot</code>.
       * @param ot an old trapezoid pointer
       * @param nt a new trapezoid pointer
       */
    private:
      void repointTopLeft(NodePtr ot, NodePtr nt) throws()
      {
	if (_tl==ot) {
	  _tl = nt;
	}
      }

      /**
       * Update the top-right trapezoid to point to <code>nt</code>
       * if it currently poinst to <code>ot</code>.
       * @param ot an old trapezoid pointer
       * @param nt a new trapezoid pointer
       */
    private:
      void repointTopRight(NodePtr ot, NodePtr nt) throws()
      {
	if (_tr==ot) {
	  _tr=nt;
	}
      }
	  
	   
      /**
       * Repoint the top 
       * @param ot an old trapezoid pointer
       * @param nt a new trapezoid pointer
       */
    private:
      void repointTop(NodePtr ot, NodePtr nt) throws()
      { 
	repointTopLeft(ot,nt);repointTopRight(ot,nt);
      }

      /**
       * Update the bottom-left trapezoid to point to <code>nt</code>
       * if it currently poinst to <code>ot</code>.
       * @param ot an old trapezoid pointer
       * @param nt a new trapezoid pointer
       */
    private:
      void repointBottomLeft(NodePtr ot, NodePtr nt) throws()
      {
	if (_bl==ot) {
	  _bl = nt;
	}
      }
	  
      /**
       * Update the bottom-right trapezoid to point to <code>nt</code>
       * if it currently poinst to <code>ot</code>.
       * @param ot an old trapezoid pointer
       * @param nt a new trapezoid pointer
       */
    private:
      void repointBottomRight(NodePtr ot, NodePtr nt) throws()
      {
	if (_br==ot) {
	  _br=nt;
	}
      }
	  
      /**
       * Repoint the bottom 
       * @param ot an old trapezoid pointer
       * @param nt a new trapezoid pointer
       */
    private:
      void repointBottom(NodePtr ot, NodePtr nt) throws()
      { 
	repointBottomLeft(ot,nt);repointBottomRight(ot,nt);
      }

      /** The index of this trapezoid */
    private:
      size_t _index;
 
      /** The point below and above this trapezoid or invalidIndex */
    private:
      size_t _pointBelow,_pointAbove;

      /** The segment the left and the right, or invalidIndex */
    private:
      size_t _segmentLeft, _segmentRight;

      /** The trapezoid nodes above and below this trapezoid */
    private:
      NodePtr _tl,_tr,_bl,_br;
    };

    /**
     * Create a new default decomposition.
     */
  public:
    TrapezoidDecomposition() throws();

    /**
     * Create a new decomposition.
     * @param enableMerging true to enable merging of trapezoids
     */
  public:
    TrapezoidDecomposition(bool enableMerging) throws();

    /**
     * Destroy this decompositon
     */
  public:
    ~TrapezoidDecomposition() throws();

    /**
     * Clear this decomposition so that it may be reused.
     */
  public:
    void reset() throws();

    /**
     * Get the root node index
     * @return the index of the root node
     */
  public:
    NodePtr rootNode () const throws();

    /**
     * Get the number of trapezoids
     * @return the number of trapezoids
     */
  public:
    size_t trapezoidCount() const throws();

    /**
     * Get the i'th trapezoid. The reference returned may not persists 
     * across insertSegment() calls.
     * @param i a trapezoid index
     * @pre REQUIRE_RANGE(i,0,trapezoidCount()-1)
     * @return the trapezoid at the specified index
     */
  public:
    const Trapezoid& trapezoid (size_t i) const throws();

    /**
     * Get the trapezoid node that can be reached by moving up on the left edge
     * @param t a trapezoid
     * @return the top-left trapezoid or an invalid index
     */
  public:
    inline size_t topLeft(const Trapezoid& t) const throws() { return t._tl.isValid() ? node(t._tl)._object : invalidIndex(); }
	  
    /**
     * Get the trapezoid node that can be reached by moving down on the left edge
     * @param t a trapezoid
     * @return the bottom-left trapezoid  or an invalid index
     */
  public:
    inline size_t bottomLeft(const Trapezoid& t) const throws() { return t._bl.isValid() ? node(t._bl)._object : invalidIndex(); }

    /**
     * Get the trapezoid node that can be reached by moving up on the right edge
     * @param t a trapezoid
     * @return the top-right trapezoid  or an invalid index
     */
  public:
    inline size_t topRight(const Trapezoid& t) const throws() { return t._tr.isValid() ? node(t._tr)._object : invalidIndex(); }

    /**
     * Get the trapezoid node that can be reached by moving down on the right edge
     * @param t a trapezoid
     * @return the bottom-right trapezoid  or an invalid index
     */
  public:
    inline size_t bottomRight(const Trapezoid& t) const throws() { return t._br.isValid() ? node(t._br)._object : invalidIndex(); }
	
    /**
     * Get the index of the left edge. If this trapezoid is unbounded
     * on the left side, then this method returns an invalid index.
     * @param t a trapezoid
     * @return the index of the segment that comprises the left edge or invalidIndex
     */
  public:
    inline size_t leftSegment(const Trapezoid& t) const throws() { return t._segmentLeft; }

    /**
     * Get the index of the right edge. If this trapezoid is unbounded
     * on the right side, then this method returns an invalid index.
     * @param t a trapezoid
     * @return the index of the segment that comprises the right edge or invalidIndex
     */
  public:
    inline size_t rightSegment(const Trapezoid& t) const throws() { return t._segmentRight; }

    /**
     * Get the index of the point that bounds this trapezoid at the top. If this
     * trapezoid is unbouned at the top, then an invalid index is returned.
     * @param t a trapezoid
     * @return the index of the point bounding the trapezoid or invalid index
     */
  public:
    inline size_t topPoint(const Trapezoid& t) const throws() { return t._pointAbove; }

    /**
     * Get the index of the point that bounds this trapezoid at the bottom. If this
     * trapezoid is unbouned at the bottom, then an invalid index is returned.
     * @param t a trapezoid
     * @return the index of the point bounding the trapezoid or invalid index
     */
  public:
    inline size_t bottomPoint(const Trapezoid& t) const throws() { return t._pointBelow; }

    /**
     * Get a point .
     * @param i a point index
     * @return the point
     */
  public:
    const Point2D& point (size_t i) const throws() { return _points[i]; }

    /**
     * Find the trapezoid that contains the specified point
     * @param p a point
     * @return the index of a trapezoid
     */
  public:
    size_t findTrapezoid (const Point2D& p) const throws();

    /**
     * Find a node that contains the lower point of a segment under the given root node
     * @param root a node pointer
     * @param s a segment
     * @pre REQUIRE_TRUE(comparePoints(s.a(),s.b()) < 0)
     * @return a node index
     */
  public:
    NodePtr findTrapezoid (NodePtr root, const Segment2D& s) const throws();

    /**
     * Determine the ordering of a point with respect to another point
     * @param p a point
     * @param q a point
     * @return -1 if p is below q, 0 if p is the same point as q, or 1 if p is above q
     */
  public:
    static int comparePoints (const Point2D& p, const Point2D& q) throws();

    /**
     * Ensure that the specified segment is a segment that can be passed to insertSegment() and findTrapezoid()
     * @param s a segment
     * @return true if the segment was valid, false if it was modified and is now valid
     */
  public:
    static bool createValidSegment (Segment2D& s) throws();

    /**
     * Insert a segment into the decomposition rooted at the specified root node. If 
     * an exception is thrown, then the decomposition is automatically reset.
     * @param root the root node
     * @param s a segment
     * @pre comparePoints(s.a(),s.b()) < 0
     * @return a unique index for the segment
     * @throws ::std::exception if an error occurred
     */
  public:
    size_t insertSegment (NodePtr root, const Segment2D& s) throws (::std::exception);

    /**
     * Insert a segment.
     * @param s a segment
     * @pre comparePoints(s.a(),s.b()) < 0
     * @return a unique index for the segment
     * @throws ::std::exception if an error occurred
     */
  public:
    inline size_t insertSegment (const Segment2D& s) throws(::std::exception)
    { return insertSegment(rootNode(),s); }

    /**
     * Print this decomposition for debuggnig purposes
     * @param out an output stream
     */
  private:
    void print(::std::ostream& out) throws();

    /**
     * Print this decomposition for debuggnig purposes
     * @param out an output stream
     * @param t a trapezoid index
     */
  private:
    void print(::std::ostream& out, Trapezoid t) throws();

    /**
     * Get a segment
     * @param i a segment index
     */
  private:
    const Segment& segment (size_t i) const throws() { return _segments[i]; }

    /**
     * Access the node pointer
     * @param ptr a node pointer
     * @pre REQUIRE_TRUE(ptr.isValid())
     * @return a node reference
     */
  private:
    inline Node& node(NodePtr ptr) throws() 
    { 
      assert(ptr.isValid());
      return  _nodes[ptr._node]; 
    }

    /**
     * Access the node pointer
     * @param ptr a node pointer
     * @pre REQUIRE_TRUE(ptr.isValid())
     * @return a node reference
     */
  private:
    inline const Node& node(NodePtr ptr) const throws() 
    { 
      assert(ptr.isValid()); 
      return _nodes[ptr._node]; 
    }

    /**
     * Get the trapezoid corresponding from a node pointer
     * @param n a node pointer
     * @pre REQUIRE_TRUE(ptr.isValid())
     * @return the trapezoid node for the given node
     */
  private:
    inline Trapezoid& trapezoid(NodePtr ptr) throws()
    {
      Node& n = node(ptr);
      assert(n._type==Node::TRAPEZOID);
      return _trapezoids[n._object]; 
    }
	
    /**
     * Get a point .
     * @param ptr a point node
     * @return the point
     */
  private:
    const Point2D& point (NodePtr ptr) const throws() 
    {
      const Node& n = node(ptr);
      assert(n._type==Node::POINT);
      return point(n._object);
    }

    /**
     * Get a segment
     * @param ptr a segment node
     */
  private:
    const Segment& segment (NodePtr ptr) const throws() 
    {
      const Node& n = node(ptr);
      assert(n._type==Node::SEGMENT);
      return segment(n._object);
    }
	
    /**
     * Split a trapezoid vertically.
     * @param n a node to a trapezoid
     * @return -1 if the point is the bottom point of the trapezoid,
     *          1 if the point is the top point of the trapezoid
     *          0 if the point was inserted
     */
  private:
    int splitVertically (NodePtr n, const Point2D& p) throws();

    /**
     * Split a trapezoid horizontally.
     * @param n a node to a trapezoid
     */
  private:
    NodePtr splitHorizontally (NodePtr n, const Segment2D& s, size_t sIndex, NodePtr& lBelow, NodePtr& rBelow) throws();

    /**
     * Create a new tree node
     * @param t a trapezoid node
     * @return a tree node index
     */
  private:
    NodePtr createTrapezoidNode (size_t t) throws();
	
    /**
     * Create a new trapezoid
     * @return the index of the new trapezoid
     */
  private:
    size_t createTrapezoid () throws();

    /**
     * Create a point
     * @param pt a new point
     * @return the point index
     */
  private:
    size_t createPoint(const Point2D& pt) throws();
	
    /**
     * Create a point
     * @param pt a new point
     * @return the point index
     */
  private:
    size_t createSegment(size_t lo, size_t hi) throws();

    /**
     * Determine the orientation of a segment to another segment
     * @param s a segment
     * @param t a 2D segment
     * @return -1 if t lies to the left of s, 1 if t lies to the right of s, or 0
     * if the lines (but not the segments) cross.
     */
  private:
    int compareSegments (Segment s, const Segment2D& t) const throws();

    /**
     * Test a point against a segment
     * @param pt a point
     * @param s a segment
     * @return -1 if pt lies to the left of s, 0 if lies on s, and 1 if it lies to the right of s
     */
  private:
    int testPoint (const Point2D& pt, Segment s) const throws();

    /**
     * Test if the specified index is a valid index 
     * @param i an index
     * @return true if i is a valid index
     */
  private:
    inline static size_t invalidIndex () throws()
    { return ~size_t(0); }

    /**
     * Test if the specified index is a valid index 
     * @param i an index
     * @return true if i is a valid index
     */
  private:
    inline static bool isIndex (size_t i) throws()
    { return ptrdiff_t(i) >= 0; }
      
    /** Verify a trapezoid */
  private:
    void verifyTrapezoid(NodePtr ptr) throws();

    /** Print a trapezoid */
  private:
    void printTrapezoid(::std::ostream& out, NodePtr ptr) throws();

    /** The nodes to be inserted */
  private:
    ::std::vector<Node> _nodes;

    /** The points */
  private:
    ::std::vector<Point2D> _points;
	
    /** The segments represented as pairs of points */
  private:
    ::std::vector<Segment> _segments;

    /** The current set of trapezoids */
  private:
    ::std::vector<Trapezoid> _trapezoids;

    /** True if trapezoid merging should be enabled */
  private:
    bool _mergeTrapezoids;
  };
}

#endif
