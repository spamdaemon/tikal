#include <tikal/BoundingBox2D.h>
#include <tikal/Segment2D.h>

#include <cmath>
#include <iostream>

namespace tikal {

  BoundingBox2D& BoundingBox2D::insert (double x, double y) throws()
  {
    if (x < _minX) {
      _minX = x;
    }
    else if (x>_maxX) {
      _maxX = x;
    }
    if (y < _minY) {
      _minY = y;
    }
    else if (y>_maxY) {
      _maxY = y;
    }
    return *this;
  }

  BoundingBox2D& BoundingBox2D::merge (const BoundingBox2D& b) throws()
  { 
    _minX = ::std::min(_minX,b._minX);
    _maxX = ::std::max(_maxX,b._maxX);
    _minY = ::std::min(_minY,b._minY);
    _maxY = ::std::max(_maxY,b._maxY);
    return *this;
  }

  bool BoundingBox2D::intersect (const BoundingBox2D& b) throws()
  {
    double minX = ::std::max(_minX,b._minX);
    double maxX = ::std::min(_maxX,b._maxX);
    if (minX > maxX) {
      return false;
    }

    double minY = ::std::max(_minY,b._minY);
    double maxY = ::std::min(_maxY,b._maxY);

    if (minY > maxY) {
      return false;
    }
    _minX = minX;
    _minY = minY;
    _maxX = maxX;
    _maxY = maxY;
    return true;
  }


  bool BoundingBox2D::intersects (const BoundingBox2D& b) const throws()
  {
    if (_minX> b._maxX) {
      return false;
    }
    if (_minY> b._maxY) {
      return false;
    }
    if (_maxX < b._minX) {
      return false;
    }
    if (_maxY < b._minY) {
      return false;
    }
    return true;
  }

  bool BoundingBox2D::encloses (const BoundingBox2D& b) const throws()
  {
    if (_minX<= b._minX && b._maxX <= _maxX) {
      if (_minY<= b._minY && b._maxY <= _maxY) {
	return true;
      }
    }
    return false;
  }

  bool BoundingBox2D::containsSegment (const Point2D& a, const Point2D& b) const throws()
  {
    BoundingBox2D bbox(a);
    bbox.insert(b);
    // if the bounding box do not intersect, then there's no containment
    if (!intersects(bbox)) {
      return false;
    }
    if (contains(a) || contains(b)) {
      return true;
    }
    const Segment2D s(a,b);
    int tl = s.testPoint(topLeft());
    int tr = s.testPoint(topRight());
    if (tl!=tr || tl==0) {
      return true;
    }
    int bl = s.testPoint(bottomLeft());
    if (tl!=bl) {
      return true;
    }
    int br = s.testPoint(bottomRight());
    if (tr!=br || tr==0) {
      return true;
    }
    if (br!=bl || br==0) {
      return true;
    }
    return false;
  }

  int BoundingBox2D::classify (double x, double y) const throws()
  {
    int res = 0;
    if (x < _minX) {
      res = 0x08;
    }
    else if (x > _maxX) {
      res = 0x04;
    }
    if (y < _minY) {
      res = res | 0x02;
    }
    else if (y>_maxY) {
      res = res | 0x01;
    }
    return res;
  }

  ::std::ostream& BoundingBox2D::print(::std::ostream& out) const 
  {
    out << "BBox2D min ( " << _minX << ", " << _minY << ") -> max ( " << _maxX << ", " << _maxY << ")";
    return out;
  }

}
