#include <tikal/Area2D.h>
#include <tikal/AbstractArea2D.h>
#include <tikal/BoundingBox2D.h>
#include <tikal/Contour2D.h>

#include <iostream>

using namespace std;
using namespace timber;

namespace tikal {
  namespace {
    struct Area : public AbstractArea2D {
      inline Area (const Reference<Contour2D>& outside)
	: _bounds(outside->bounds()),_outside(outside)
      {}
      
      inline Area (const Reference<Contour2D>& outside, 
		   const ::std::vector<Reference<Contour2D> >& holes)
	: _bounds(outside->bounds()),_outside(outside),_inside(holes)
      {}
      
      ~Area () throws()
      {
      }
      
      size_t contourCount() const throws()
      { return _inside.size(); }
      
      Reference<Contour2D> insideContour(size_t i) const throws()
      {
	assert(i<_inside.size()); 
	return _inside[i]; 
      }
      
      Reference<Contour2D> outsideContour() const throws()
      { return _outside; }
      
      void addContour (const Reference<Contour2D>& c)
      {
	_inside.push_back(c);
      }
      
      BoundingBox2D bounds() const throws() 
      { return _bounds; }
      
    private:
      BoundingBox2D _bounds;
      const Reference<Contour2D> _outside;
      ::std::vector<Reference<Contour2D> > _inside;
    };
  }

  Area2D::Area2D () throws()
  {}
  
  Area2D::~Area2D () throws()
  {}
  
  Reference<Area2D> Area2D::create (const Reference<Contour2D>& outside, 
				    const ::std::vector<Reference<Contour2D> >& holes) throws()
  {
    if (holes.empty()) {
      return outside;
    }
    return new Area(outside,holes);
  }

  Reference<Area2D> Area2D::area(size_t i) const throws()
  {
    assert(i==0);
    return toRef<Area2D>();
  }

  size_t Area2D::areaCount() const throws()
  { return 1; }

  
}
