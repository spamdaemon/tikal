#ifndef _TIKAL_INTERSECTIONFUNCTION_H
#define _TIKAL_INTERSECTIONFUNCTION_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

namespace tikal {
  class Line2D;
  class Segment2D;
  class Point2D;

  /**
   * Compute the intersection point of two non-parallel segments.
   */
  class IntersectionFunction {
    IntersectionFunction&operator=(const IntersectionFunction&);
    IntersectionFunction(const IntersectionFunction&);

    /**
     * Default constructor
     */
  protected:
    IntersectionFunction () throws();
      
    /**
     * The destructor
     */
  public:
    virtual ~IntersectionFunction() throws() = 0;

    /**
     * Intersect two non-parallel lines. The result of this operation
     * is undefined if the two segments are parallel or degenerate
     * @param s1 a line
     * @param s2 a line
     * @return the intersection point
     */
  public:
    virtual Point2D intersect (const Line2D& s1, const Line2D& s2) const = 0;

    /**
     * Intersect two non-parallel, intersecting segments. The result of this operation
     * is undefined if the two segments are parallel or degenerate. If the intersection
     * point is outside the two segments, then it is clamped into a segment end-point
     * @param s1 a segment
     * @param s2 a segment
     * @return the intersection point of the two segments
     */
  public:
    virtual Point2D intersect (const Segment2D& s1, const Segment2D& s2) const = 0;
      
    /**
     * Get a default instance of an intersection function.
     * @return a default intersection function
     */
  public:
    static const IntersectionFunction& getInstance();
  };
}
#endif
