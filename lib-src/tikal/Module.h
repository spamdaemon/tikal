#ifndef _TIKAL_MODULE_H
#define _TIKAL_MODULE_H

#ifndef _TIKAL_REGION2DOPS_H
#include <tikal/Region2DOps.h>
#endif

extern "C" {

  /** 
   * The module structure. It has a single function which creates 
   * region2d ops object.
   */
  struct Module {
    /** The creation function */
    typedef ::std::unique_ptr< ::tikal::Region2DOps> (*Create)();

    /** The creation function */
    Create create;
  };
  
}


#define TIKAL_MODULE_NAME "RegionOps"

#define TIKAL_MODULE(create) struct Module RegionOps = { create }

#endif
