#include <tikal/IntersectionFunction.h>
#include <tikal/Segment2D.h>
#include <tikal/Line2D.h>
#include <tikal/Point2D.h>

#include <cmath>

using namespace ::std;

namespace tikal {
   namespace {

      inline double det2x2(double x00, double x01, double x10, double x11)
      {
         // could use 2product here
         return x00 * x11 - x10 * x01;
      }

      inline Point2D intersectLines(const Point2D& ra, const Point2D& rb, const Point2D& sa, const Point2D& sb)
      {
         double x1 = ra.x();
         double y1 = ra.y();
         double x2 = rb.x();
         double y2 = rb.y();

         double x3 = sa.x();
         double y3 = sa.y();
         double x4 = sb.x();
         double y4 = sb.y();

         double a00 = det2x2(x1, y1, x2, y2);
         double a10 = det2x2(x3, y3, x4, y4);

         double denom = det2x2(x1 - x2, y1 - y2, x3 - x4, y3 - y4);

         assert(denom != 0.0 && "Lines seem to be parallel" != 0);

         double x = det2x2(a00, x1 - x2, a10, x3 - x4) / denom;
         double y = det2x2(a00, y1 - y2, a10, y3 - y4) / denom;
         return Point2D(x, y);
      }
   }

   IntersectionFunction::IntersectionFunction() throws()
   {
   }

   IntersectionFunction::~IntersectionFunction() throws()
   {
   }

   const IntersectionFunction& IntersectionFunction::getInstance()
   {
      struct IFunc : public IntersectionFunction
      {
            IFunc() throws()
            {
            }
            ~IFunc() throws()
            {
            }

            Point2D intersect(const Line2D& R, const Line2D& S) const
            {
               const Point2D rb(R.point().x() + R.normal().y(), R.point().y() - R.normal().x());
               const Point2D sb(S.point().x() + S.normal().y(), S.point().y() - S.normal().x());
               return intersectLines(R.point(), rb, S.point(), sb);
            }

            Point2D intersect(const Segment2D& R, const Segment2D& S) const
            {
               Point2D pt = intersectLines(R.a(), R.b(), S.a(), S.b());

               double p[] = { pt.x(), pt.y() };

               for (size_t i = 0; i < 2; ++i) {
                  double min = (R.a()[i] <= R.b()[i]) ? R.a()[i] : R.b()[i];
                  double max = (R.a()[i] <= R.b()[i]) ? R.b()[i] : R.a()[i];
                  p[i] = ::std::max(p[i], min);
                  p[i] = ::std::min(p[i], max);
                  min = (S.a()[i] <= S.b()[i]) ? S.a()[i] : S.b()[i];
                  max = (S.a()[i] <= S.b()[i]) ? S.b()[i] : S.a()[i];
                  p[i] = ::std::max(p[i], min);
                  p[i] = ::std::min(p[i], max);
               }
               return Point2D(p[0], p[1]);
            }
      };
      static const IFunc _isector;
      return _isector;
   }

}
