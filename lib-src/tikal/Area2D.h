#ifndef _TIKAL_AREA2D_H
#define _TIKAL_AREA2D_H

#ifndef _TIKAL_REGION2D_H
#include <tikal/Region2D.h>
#endif

#include <iosfwd>

namespace tikal {
  
  /** A contour2D */
  class Contour2D;
  
  /** The area ementation class */
  class Area2D : public Region2D {
    Area2D(const Area2D&);
    Area2D&operator=(const Area2D&);
      
    /**
     * Default constructor.
     */
  protected:
    Area2D() throws();
    
    
    /** Destroy this area */
  public:
    ~Area2D() throws();
    
    /**
     * @name Static constructors
     * @{
     */
    
    /*@}*/
    
    /**
     * Create a new area consisting of an outside contour and several external contours.
     * @param outside an outside contour
     * @param holes holes in the contour
     * @return an area
     */
  public:
    static ::timber::Reference<Area2D> create (const ::timber::Reference<Contour2D>& outside, 
					       const ::std::vector< ::timber::Reference<Contour2D> >& holes) throws();
    
    /**
     * Get the number of areas.
     * @return 1
     */
  public:
    size_t areaCount() const throws();
    
    /**
     * Return this area
     * @param i an area index
     * @pre REQUIRE_RANGE(i,0,0)
     * @return this area
     */
  public:
    ::timber::Reference<Area2D> area (size_t i) const throws();
    
    /**
     * Get the outside contour.
     * @return the single outside contour
     */
  public:
    virtual  ::timber::Reference<Contour2D> outsideContour () const throws() = 0;

    /**
     * Get the number of internal contours. This is actually just the number
     * of holes in the area.
     * @return the number of internal contours bounding holes in the area
     */
  public:
    virtual size_t contourCount() const throws() = 0;
    
    /**
     * Get an internal contour
     * @param i the index of an internal contour
     * @return the given contour
     * @pre i<contourCount()
     */
  public:
    virtual  ::timber::Reference<Contour2D> insideContour(size_t i) const throws() = 0;
  };
}

#endif
