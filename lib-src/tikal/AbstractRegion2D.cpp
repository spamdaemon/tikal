#include <tikal/AbstractRegion2D.h>
#include <tikal/Point2D.h>
#include <tikal/Area2D.h>

#include <iomanip>
#include <iostream>
#include <cassert>

using namespace ::std;
using namespace ::timber;

namespace tikal {
  
  AbstractRegion2D::AbstractRegion2D () throws()
  {}
  
  AbstractRegion2D::~AbstractRegion2D () throws()
  {}

  int AbstractRegion2D::locatePoint (double x, double y) const throws()
  {
    for (size_t i=0,sz=areaCount();i<sz;++i) {
      Reference<Area2D> ai = area(i);
       int where = ai->locatePoint(x,y);
      if (where <= 0) {
	return where;
      }
    }
    return 1; // outside all of the regions
  }
  
  bool AbstractRegion2D::isConvex() const throws()
  { return areaCount()==1 && area(0)->isConvex(); }
  
  
  void AbstractRegion2D::print(::std::ostream& out) const 
  {
    out << "Region2D {" << endl;
    for (size_t i=0,sz=areaCount();i<sz;++i) {
      Reference<Area2D> ai = area(i);
      ai->print(out);
    }
    out << "}" << endl;
  }
  
}
