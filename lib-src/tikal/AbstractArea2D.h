#ifndef _TIKAL_ABSTRACTAREA2D_H
#define _TIKAL_ABSTRACTAREA2D_H

#ifndef _TIKAL_AREA2D_H
#include <tikal/Area2D.h>
#endif

#include <iosfwd>

namespace tikal {
  
  /** The area ementation class */
  class AbstractArea2D : public Area2D {
    AbstractArea2D(const AbstractArea2D&);
    AbstractArea2D&operator=(const AbstractArea2D&);
      
    /**
     * Default constructor.
     */
  protected:
    AbstractArea2D() throws();
    
    
    /** Destroy this ementation */
  public:
    ~AbstractArea2D() throws();
    
    /**
     * @name Static constructors
     * @{
     */
    
    /*@}*/
    
    public:
      int locatePoint (double x, double y) const throws();

      /**
       * Test if this region is a convex region.
       * @return true if this region is a convex region
       */
    public:
      bool isConvex() const throws();

      /**
       * Print this contour 
       * @param out an output stream
       */
    public:
      void print (::std::ostream& out) const;

      /**
       * Remove holes from this area. If this area does not have any holes,
       * then it returns itself. If it has holes, then it invokes Region2D::removeHoles().
       * @param convexOnly if true then the region must consist of convex area only.
       * @return a region without holes
       */
    public:
      ::timber::Pointer<Region2D> removeHoles () const throws();
  };
}

#endif
