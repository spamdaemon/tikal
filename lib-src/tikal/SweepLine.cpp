#include <tikal/SweepLine.h>
#include <tikal/Segment2D.h>
#include <tikal/Line2D.h>
#include <tikal/VertexOrder.h>
#include <tikal/IntersectionFunction.h>

#include <timber/logging.h>

#include <cmath>
#include <iomanip>

#define ENABLE_SWEEPLINE_VERIFY_OFF
#define ENABLE_DEBUG_FF

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace tikal {
  namespace {
    /** The event type */
    enum EventType { 
      INTERSECTION = 0,
      START_SEGMENT = 1,
      START_VERTICAL_SEGMENT = 2,
      END_VERTICAL_SEGMENT = 4,
      END_SEGMENT = 8,
    };
  }

  struct SweepLine::Line
  {
    Line (const Point2D& startPt, const Point2D& endPt, const VertexOrder& dir, const VertexOrder& origDirection, SweepLine::Data d) throws()
    : segment(startPt,endPt),
      line(startPt,endPt),
      lastPoint(startPt),
      isIntersectionPoint(false),
      end(endPt),
      minY(::std::min(startPt.y(),endPt.y())),
      maxY(::std::max(startPt.y(),endPt.y())),
      next(0),pred(0),
      nextParallel(0),predParallel(0),
      owner(0),
      segmentData(d,origDirection,dir),
      active(true)
    {
    }
  public:      
    ~Line() throws() {}

  public:
    double y(double x) const throws()
    { 
      double slopeX = line.normal().y();
      double slopeY = -line.normal().x();
      return (x-line.point().x())*slopeY/slopeX + line.point().y();
    }
      
  public:
    int compareTo(const Line& g) const throws()
    {
      int p = segment.testPoint(g.segment.a());
      if (p==0) {
	p = segment.testPoint(g.segment.b());
      }
      // if this line is below the given line, then testPoint() will return -1 because
      // at least 1 point is above this line
      return p;
    }

  public:
    bool isLessThan(const Line& g) const throws()
    { return compareTo(g) < 0; } 

  public:
    Segment2D segment;
    Line2D line;
    Point2D lastPoint;
    bool isIntersectionPoint;
    Point2D end;
    double minY,maxY;
    Line* next;
    Line* pred;
    Line* nextParallel;
    Line* predParallel;
    Line* owner; // if parallel
    SegmentData segmentData;
    bool active;
  };

  /** An event */
  class SweepLine::Event {
    /** Create a new intersection event */
  public:
    inline Event (Segment s, Segment t, const Point2D& pt)
      : _type(INTERSECTION),_segmentA(s),_segmentB(t),_point(pt) {}
      
    /** Create a new intersection event */
  public:
    inline Event (EventType t, Segment s, const Point2D& pt)
      : _type(t),_segmentA(s),_segmentB(s),_point(pt) 
    {
      assert(t!=INTERSECTION);
    }
      
    /**
     * Test if this is a vertical event.
     * @return true if the type is either START_VERTICAL_SEGMENT or END_VERTICAL_SEGMENT
     */
  public:
    inline bool isVerticalEventType() const throws()
    { return (_type & (START_VERTICAL_SEGMENT|END_VERTICAL_SEGMENT))!=0; }
      
    /**
     * Get the event type 
     * @return the event type
     */
  public:
    EventType type() const throws() { return _type; }
      
    /**
     * Get the segment involved
     * @return the segment involved
     */
  public:
    Segment segmentA() const throws() { return _segmentA; }
      
    /**
     * Get the segment involved
     * @return the segment involved
     */
  public:
    Segment segmentB() const throws() { return _segmentB; }
      
    /**
     * Get a refernce to the point
     * @return a reference to the point
     */
  public:
    const Point2D& point() const throws() { return _point; }
      
    /**
     * Test if an event is less than some other event.
     * @param e an event
     */
  public:
    inline bool isLessThan(const Event& e) const throws()
    {
      // sort primarily by x
      if (_point.x()!=e._point.x()) {
	return _point.x() < e._point.x();
      }
	
      if (isVerticalEventType() && e.isVerticalEventType() && _point.y() != e._point.y()) { 
	return _point.y() < e._point.y();
      }
#ifdef USE_MULTISET
      return _type<e._type;
#else
      if (_type!=e._type) {
	return _type < e._type;
      }
      if (_point.y()!=e._point.y()) {
	return _point.y() < e._point.y();
      }
      if (_segmentA != e._segmentA) {
	return _segmentA < e._segmentA;
      }
      return _segmentB < e._segmentB;
#endif
    }
      
    inline bool operator< (const Event& e) const throws()
    { return isLessThan(e); }

    /** The event type */
  private:
    EventType _type;
      
    /** The segments involved */
  private:
    Segment _segmentA,_segmentB;
      
    /** The x,y value assocaited with the event */
  private:
    Point2D _point;
  };
    
  SweepLine::SweepLine ()
    : _sweeping(false)
  {
    _y = _x = NAN;
    _sweepline = _verticals = _currentVertical = 0;
  }


  SweepLine::~SweepLine() throws()
  {
    reset();
  }

  inline void SweepLine::newEvent (const Event& e) throws()
  { 
    _eventQueue.insert(e); 
  }
      
  inline SweepLine::Event SweepLine::nextEvent() throws()
  {
    EventQueue::iterator i = _eventQueue.begin();
    SweepLine::Event event = *i;
    _eventQueue.erase(i);
    return event;
  }

  inline bool SweepLine::hasNextEvent() const throws()
  {
    return !_eventQueue.empty(); 
  }
    
  inline size_t SweepLine::eventCount() const throws()
  {
    return _eventQueue.size();
  }

  void SweepLine::verify() const throws()
  {
    Line* line = _sweepline;
    while (line!=0) {
      assert (line->active);
      Line* next = line->next;
      if (next!=0) {
	assert (line==next->pred);
	double y0= line->y(x());
	double y1= next->y(x());
	assert (y0<y1 || ::std::abs(y0-y1)<=(1E-10)*::std::abs(y0));
      }
      line = next;
    }
  }

  void SweepLine::notifyAdjacent (Line* p, Line* n, const IntersectionFunction& isector) throws()
  {

    if (p->maxY <= n->minY) {
      return;
    }
    if (p->minY >= n->maxY) {
      return;
    }

    if (p->segment.b().x() <= n->segment.a().x()) {
      return;
    }
    const int iFlags = Segment2D::ENDPOINT_SEGMENT|Segment2D::SEGMENT_SEGMENT;
      
    if (Segment2D::testIntersection(p->segment,n->segment,iFlags)==0) {
      return;
    }

    // 1. compute the intersection point of p and n
    // 2. insert the intersection point event
    Point2D pt=isector.intersect(p->segment,n->segment);
    if (pt.x()<x()) {
      // intersection is before the current sweepline
      return;
    }
    if (p->segmentData.data() >= 0 && p->segmentData.data()==n->segmentData.data()) {
      LogEntry("tikal.SweepLine").warn() << "Warning : self-intersection not ignored : " 
					  << p->segment << " with  " << n->segment << " in " << pt 
					  << doLog;
      //return;
    }
    newEvent(Event(p,n,pt));
  }

  SweepLine::Segment SweepLine::startVerticalSegment (Line* line, const IntersectionFunction& isector,Callback& cb) throws()
  {
    const Point2D& pt(line->lastPoint);
    assert(_verticals==0 || (pt.y()>=y()));
    assert(!(_x> pt.x()));
    if (!(_x >= pt.x())) {  // same as if (_x<pt.x()), but works also for NaN
      _currentVertical = 0;
    }
    _x = pt.x();
    _y = pt.y();
      
    // if there are no verticals yet, then find the horizontal line that
    // is just above this vertical
    if (_verticals==0) {
      if (_currentVertical==0) {
	_currentVertical = _sweepline;
      }
      while (_currentVertical!=0 && _currentVertical->segment.testPoint(pt)<0) {
	_currentVertical = _currentVertical->next;
      }
    }
      
    int sgn=0;
    while (_currentVertical!=0 && (sgn=_currentVertical->segment.testPoint(pt))<=0) {
      const Point2D tmp(x(),sgn==0 ? pt.y() : _currentVertical->y(x()));
      //assert(_currentVertical->segment.testPoint(tmp)==sgn);
      notify(tmp,true,_currentVertical,cb);
      for (Line* g = _verticals;g!=0;g=g->next) {
	notify(tmp,true,g,cb);
      }
      _currentVertical=_currentVertical->next;
    }
    for (Line* g = _verticals;g!=0;g=g->next) {
      notify(pt,false,g,cb);
    }
    line->next = _verticals;
    if (_verticals!=0) {
      _verticals->pred = line;
    }
    _verticals = line;
    return line;
  }
    
  void SweepLine::terminateVerticalSegment (Line* segment, const IntersectionFunction& isector, Callback& cb) throws()
  {
    Point2D pt(segment->end);
    assert(_verticals!=0);
    assert(pt.y()>=y());
    assert(pt.x()==x());
    _y = pt.y();

    while (_currentVertical!=0 && _currentVertical->segment.testPoint(pt)<=0) {
      const Point2D tmp(x(),_currentVertical->y(x()));
      notify(tmp,true,_currentVertical,cb);
      for (Line* g = _verticals;g!=0;g=g->next) {
	notify(tmp,true,g,cb);
      }
      _currentVertical=_currentVertical->next;
    }
    Line* pred = 0;
    for (Line* g = _verticals;g!=0;g=g->next) {
      notify(pt,false,g,cb);
      pred = g;
    }
    Line* n = segment->next;
    Line* p = segment->pred;
    if (n!=0) {
      n->pred = p;
    }
    if (p!=0) {
      p->next = n;
    }
    else {
      _verticals = n;

      if (n==0) {
	_y = NAN;
      }
    }
    delete segment;
  }

  void SweepLine::reset() throws()
  {
    struct DummyCB : public Callback {
      ~DummyCB() throws() {}
      void notify(const Point2D&, const Point2D&,Int,const SegmentData&)  throws()
      {}
    };
    assert(!isSweeping());
    DummyCB cb;
    sweepSegments(cb);
    _y = _x = NAN;
    _sweepline = _verticals = _currentVertical = 0;
  }

  void SweepLine::insertSegment (const Point2D& start, const Point2D& end, const VertexOrder& dir, const VertexOrder& orig, Data data)  throws()
  {
    assert(!isSweeping());
    assert(start.x()<end.x() || (start.x()==end.x() && start.y()<end.y()));

    // start a new segment
    Line* s = new Line(start,end,dir,orig,data);
    if (start.x()==end.x()) {
      newEvent(Event(START_VERTICAL_SEGMENT,s,start));
    }
    else {
      newEvent(Event(START_SEGMENT,s,start));
    }
  }

  void SweepLine::insertSegment (const Point2D& start, const Point2D& end, Data data)  throws()
  { insertSegment(start,end,VertexOrder::cw(),VertexOrder::cw(),data); }

  bool SweepLine::insertUnorderedSegment (const Point2D& start, const Point2D& end, const VertexOrder& dir, Data data) throws()
  {
    assert(!isSweeping());
#if 0
    {
      LogEntry("tikal.SweepLine").info() 
	<< setprecision(20) 
	<< setw(30)
	<< "sweepline.insertUnorderedSegment("
	<< "Point2D("<<start.x()<<","<<start.y()<<"),"
	<< "Point2D("<<end.x()<<","<<end.y()<<"),"
	<< "VertexOrder::" << (dir.isCCW() ? "ccw()":"cw()") << ','
	<< data
	<< ")"
	<< doLog;
    }
#endif

    if (start.x() < end.x()) {
      insertSegment(start,end,dir,dir,data);
    }
    else if (start.x() > end.x()) {
      insertSegment(end,start,dir.reverse(),dir,data);
    }
    else if (start.y() < end.y()) {
      insertSegment(start,end,dir,dir,data);
    }
    else if (start.y() > end.y()) {
      insertSegment(end,start,dir.reverse(),dir,data);
    }
    else {
      return false;
    }
    return true;
  }

  bool SweepLine::insertUnorderedSegment (const Point2D& start, const Point2D& end, Data data) throws()
  { return insertUnorderedSegment(start,end,VertexOrder::cw(),data); }

    
  void SweepLine::sweepSegments (Callback& cb)  throws()
  {
    assert(!isSweeping());
    sweepSegments(IntersectionFunction::getInstance(),cb);
  }

  void SweepLine::sweepSegments (const IntersectionFunction& ifunc, Callback& cb)  throws()
  {
    assert(!isSweeping());
    if (_eventQueue.empty()) {
      return;
    }

    _sweeping = true;

    // initialize x and y
    const Event& xe = *_eventQueue.begin();
    _x = xe.point().x()-1;
    _y = xe.point().y()-1;

    while (hasNextEvent()) {
      const Event e(nextEvent());
      assert (!(e.point().x() < _x));
      Line* segA = e.segmentA();
      Line* segB = e.segmentB();
      switch (e.type()) {
      case INTERSECTION:
	if (segA->owner!=0) {
	  segA = segA->owner;
	}
	if (segB->owner!=0) {
	  segB = segB->owner;
	}
	assert(segA->active && segB->active);
	if (segA->active && segB->active) {
	  if (segA->next==segB) {
	    assert (segB->pred==segA);
	    // need to be adjacent
	    swapSegments(e.point(),segA,segB,ifunc,cb);
	  }
	}
	break;
      case START_SEGMENT:
	assert (segA==segB);
#ifdef ENABLE_SWEEPLINE_VERIFY
	verify();
#endif
#ifdef ENABLE_DEBUG
	LogEntry("tikal.SweepLine").info() << "START " << x() << ' ' << segA->segmentData.data() << " " << e.point() << doLog;
#endif
	startSegment(segA,ifunc,cb);
#ifdef ENABLE_SWEEPLINE_VERIFY
	verify();
#endif
	newEvent(Event(END_SEGMENT,segA,segA->end));
	break;
      case START_VERTICAL_SEGMENT:
	assert (segA==segB);
#ifdef ENABLE_SWEEPLINE_VERIFY
	verify();
#endif
#ifdef ENABLE_DEBUG
	LogEntry("tikal.SweepLine").info() << "VSTART " << x() << ' '<< segA->segmentData.data() << " " << e.point() << doLog;
#endif
	startVerticalSegment(segA,ifunc,cb);
#ifdef ENABLE_SWEEPLINE_VERIFY
	verify();
#endif
	newEvent(Event(END_VERTICAL_SEGMENT,segA,segA->end));
	break;
      case END_VERTICAL_SEGMENT:
	assert (segA==segB);
#ifdef ENABLE_SWEEPLINE_VERIFY
	verify();
#endif
#ifdef ENABLE_DEBUG
	LogEntry("tikal.SweepLine").info() <<  "VEND " << x() << ' '<< segA->segmentData.data() << " " << e.point() << doLog;
#endif
	terminateVerticalSegment(segA,ifunc,cb);
#ifdef ENABLE_SWEEPLINE_VERIFY
	verify();
#endif
	break;
      case END_SEGMENT:
	assert (segA==segB);
#ifdef ENABLE_SWEEPLINE_VERIFY
	verify();
#endif
#ifdef ENABLE_DEBUG
	LogEntry("tikal.SweepLine").info() <<  "END " << x() << ' '<< segA->segmentData.data() << " " << e.point() << doLog;
#endif
	terminateSegment(segA,ifunc,cb);
#ifdef ENABLE_SWEEPLINE_VERIFY
	verify();
#endif
	break;
      default:
	assert(false);
      }
    }
    _y = _x = NAN;
    _sweepline = _verticals = _currentVertical = 0;
    _sweeping = false;
  }

  SweepLine::Segment SweepLine::startSegment (Line* line, const IntersectionFunction& isector, Callback& cb)  throws()
  {
    const Point2D& start = line->line.point();
    const Point2D& end = line->end;
    assert(start.x()>=x());
    assert(start.x()<end.x() || (start.x()==end.x() && start.y()<end.y()));
    assert(end.x()>start.x());
    assert(start.x()>=x());
    assert(_verticals==0);
      
    _currentVertical = 0;
    assert(!(_x >line->lastPoint.x()));
    _x = line->lastPoint.x();

    Line* next = _sweepline;
    if (next==0) {
      _sweepline = line;
      return line;
    }

    Line* pred = 0;
    while (next!=0 && next->isLessThan(*line)) {
      pred = next;
      next = next->next;
    }
    if (next==0) {
      assert (pred!=0);
      pred->next = line;
      line->pred = pred;
      notifyAdjacent(pred,line,isector);
    }
    else {
      int cmp = next->compareTo(*line);
      assert (cmp>=0);
      if (cmp > 0) {
	line->pred = pred;
	line->next = next;
	next->pred = line;
	if (pred!=0) {
	  pred->next = line;
	  notifyAdjacent(pred,line,isector);
	}
	else {
	  _sweepline=line;
	}
	notifyAdjacent(line,next,isector);
      }
      else { // lines are parallel
	Line* par = next->nextParallel;
	if (par==0) {
	  // make a copy of the current parallel segment
	  par = next;
	  next = new Line(*next);
	  par->owner= next;
	  if (pred==0) {
	    _sweepline = next;
	  }
	  else {
	    pred->next = next;
	  }
	  assert (par->pred==next->pred);
	  assert (par->next==next->next);
	  if (next->next!=0) {
	    next->next->pred = next;
	  }
	  next->nextParallel = par;
	  assert (next->predParallel==0);
	}
	if (next->end.x() < line->end.x()) {
	  next->end = end;
	  next->segment=Segment2D(next->segment.a(),end);
	}
	notify(start,false,next,cb);
	line->owner = next;
	line->nextParallel = par;
	par->predParallel = line;
	next->nextParallel = line;
	// no notification is necessary
	if (next->pred!=0) {
	  notifyAdjacent(next->pred,line,isector);
	}
	if (next->next!=0) {
	  notifyAdjacent(line,next->next,isector);
	}
      }
    }
#ifdef ENABLE_SWEEPLINE_VERIFY
    verify();
#endif
    return line;
  }

  void SweepLine::terminateSegment (Segment segment,const IntersectionFunction& isector, Callback& cb)  throws()
  {
    Point2D pt(segment->end);
    assert(_verticals==0);
    assert(pt.x()>=x());
    _currentVertical=0;
    _x = pt.x();
    notify(pt,false,segment,cb);
    assert(segment->active);

    Line* owner = segment->owner;
    segment->active=false;
    if (owner!=0) {
      assert(owner->active);
      Line* p = segment->predParallel;
      Line* n = segment->nextParallel;

      if (p==0) {
	owner->nextParallel=n;
	if (n!=0) {
	  n->predParallel = 0;
	}
      }
      else if (n==0) {
	p->nextParallel = 0;
      }
      else {
	p->nextParallel = n;
	n->predParallel = p;
      }
      delete segment;
      if(owner->nextParallel!=0) {
	return;
      }
      segment = owner;
      segment->active=false;
    }
    Line* p = segment->pred;
    Line* n = segment->next;
      
    if (p==0) {
      _sweepline=n;
      if (n!=0) {
	n->pred = 0;
      }
    }
    else if (n==0) {
      p->next = 0;
    }
    else {
      p->next = n;
      n->pred = p;
      notifyAdjacent(p,n,isector);
    }
    delete segment;
#ifdef ENABLE_SWEEPLINE_VERIFY
    verify();
#endif
  }
    
  void SweepLine::swapSegments (const Point2D& pt, Line* p, Line* n, const IntersectionFunction& isector, Callback& cb) throws()
  {
    assert (pt.x() >= _x);
    _x = pt.x();
#if 0
    LogEntry("tikal.SweepLine").info() 
      << setprecision(20) 
      << setw(30)
      << "Intersection between:\n"
      << ' ' << p->segment << '\n'
      << ' ' << n->segment << '\n'
      << ' ' << pt << '\n'
      << ' ' << _x 
      << doLog;
#endif
    
    if (p->segment.testPoint(n->segment.b())<=0) {
      return;
    }

    //      assert (p->y(x()-1) < n->y(x()-1));
    //      assert (n->y(x()+1) < p->y(x()+1));
    assert (p->next==n);
    assert (n->pred==p);

    p->next = n->next;
    n->pred = p->pred;
    n->next = p;
    p->pred = n;
    if (n->pred==0) {
      _sweepline = n;
    }
    else {
      n->pred->next = n;
      notifyAdjacent(n->pred,n,isector);
    }

    if (p->next!=0) {
      p->next->pred = p;
      notifyAdjacent(p,p->next,isector);
    }
    notify(pt,true,p,cb);
    notify(pt,true,n,cb);

  }
    
  void SweepLine::notify (const Point2D& pt, bool isIntersectionPoint, Line* line, Callback& cb) throws()
  {
    if (line->owner!=0) {
      line = line->owner;
    }
    if (pt.equals(line->lastPoint)) {
      return;
    }
    if (pt.equals(line->end) || pt.equals(line->line.point())) {
      isIntersectionPoint = false;
    }
    int code = (line->isIntersectionPoint ? 2 : 0) | (isIntersectionPoint ? 1 : 0);
    if (line->nextParallel==0) {
#if 0
       LogEntry ("tikal.SweepLine") 
	 << setprecision(20) 
	 << setw(30)
	 << "Emit segment :\n" 
	 << "    " << line->lastPoint << '\n'
	 << " -> " << pt
	 << doLog;
#endif
      cb.notify(line->lastPoint,pt, code, line->segmentData);
    }
    else for (Line* g = line->nextParallel;g!=0;g=g->nextParallel) {
#if 0
	LogEntry ("tikal.SweepLine") 
	  << setprecision(20) 
	  << setw(30)
	  << "Emit segment :\n" 
	  << "    " << line->lastPoint << '\n'
	  << " -> " << pt
	  << doLog;
#endif
	cb.notify(line->lastPoint,pt,code,g->segmentData);
      }
    line->lastPoint = pt;
    line->isIntersectionPoint = isIntersectionPoint;
  }

}
