#include <tikal/Region2D.h>
#include <tikal/AbstractRegion2D.h>
#include <tikal/Point2D.h>
#include <tikal/Contour2D.h>
#include <tikal/Area2D.h>
#include <tikal/Path2D.h>
#include <tikal/VertexOrder.h>
#include <tikal/BoundingBox2D.h>
#include <tikal/Region2DOps.h>
#include <tikal/util.h>

#include <iomanip>
#include <iostream>
#include <cassert>

using namespace ::std;
using namespace ::timber;

namespace tikal {
  namespace {
    struct Region : public AbstractRegion2D {
      Region(const BoundingBox2D& bnds)
	: _bounds(bnds) 
      {}

      Region(const vector<Reference<Area2D> >& areas)
	: _areas(areas)
      {
	assert(areas.size()>0);
	_bounds = areas[0]->bounds();
	for (size_t i=1;i<areas.size();++i) {
	  _bounds.merge(areas[i]->bounds());
	}
      }
      Region(const vector<Reference<Region2D> >& regs)
      {
	assert(regs.size()>0);
	for (size_t i=0;i<regs.size();++i) {
	  const Reference<Region2D>& r = regs[i];
	  for (size_t j=0,sz=r->areaCount();j<sz;++j) {
	    Reference<Area2D> a = r->area(j);
	    _areas.push_back(a);
	    if( _areas.empty()) {
	      _bounds = a->bounds();
	    }
	    else {
	      _bounds.merge(a->bounds());
	    }
	  }
	}
      }
      
      ~Region() throws() {}
      size_t areaCount() const throws() { return _areas.size(); }
      Reference<Area2D> area(size_t i) const throws()
      { return _areas.at(i); }
      
      BoundingBox2D bounds() const throws()
      { return _bounds; }
      
    private:
      BoundingBox2D _bounds;
      vector<Reference<Area2D> > _areas;
    };
  }

    
  Region2D::Region2D () throws()
  {}

  Region2D::~Region2D () throws()
  {}
    
  Reference<Region2D> Region2D::create (const vector<Reference<Area2D> >& areas)
  {
    if (areas.empty()) {
      throw ::std::invalid_argument("No area specified");
    }
    if (areas.size()==1) {
      return areas[0];
    }
    return new Region(areas);
  }
      
  Reference<Region2D> Region2D::create (const vector<Reference<Region2D> >& regs)
  {
    if (regs.empty()) {
      throw ::std::invalid_argument("No regions specified");
    }
    if (regs.size()==1) {
      return regs[0];
    }
    return new Region(regs);
  }


  Pointer<Region2D> Region2D::create (const Reference<Path2D>& p, const VertexOrder& outside) 
  {
    if (p->maxDegree()==0) {
      throw ::std::invalid_argument("Not a valid path");
    }
    if (p->maxDegree()>1) {
      throw ::std::invalid_argument("Paths of degrees >1 not supported");
    }
    
    vector<Reference<Contour2D > > polys(createContours(p));
    if (polys.empty()) {
      throw ::std::invalid_argument("Path does not define valid boundaries");
    }

    Pointer<Region2D> cw,ccw;
    while (!polys.empty()) {
      Reference<Contour2D> poly = polys.back();
      polys.pop_back();
      
      if (poly->vertexOrder()==VertexOrder::CW) {
	if (cw) {
	  cw = cw->merge(poly);
	}
	else {
	  cw = poly;
	}
      }
      else {
	if (ccw) {
	  ccw = ccw->merge(poly);
	}
	else {
	  ccw = poly;
	}
      }
    }

    if (outside.isCCW()) {
      if (!ccw) {
	return Pointer<Region2D>();
      }
      if (cw) {
	return ccw->subtract(cw);
      }
      else {
	return ccw;
      }
    }
    else {
      if (!cw) {
	return Pointer<Region2D>();
      }
      if (ccw) {
	return cw->subtract(ccw);
      }
      else {
	return cw;
      }
    }
  }

  Pointer<Region2D> Region2D::createMergedPath (const Reference<Path2D>& p)
  {
    vector<Reference<Contour2D> > polys(createContours(p));
    
    if (polys.empty()) {
      throw ::std::invalid_argument("Path does not define valid boundaries");
    }
    Reference<Region2D> r = polys[0];
    if (polys.size()>1) {
      //@todo fix
      for (size_t i=1;i<polys.size();++i) {
	r = r->merge(polys[i]);
      }
    }
    return r;
  }

  Pointer<Region2D> Region2D::createIntersectionPath (const Reference<Path2D>& p)
  {
    vector<Reference<Contour2D> > polys(createContours(p));
    if (polys.empty()) {
      throw ::std::invalid_argument("Path does not define valid boundaries");
    }
    Reference<Region2D> r = polys[0];
    if (polys.size()>1) {
      //@todo fix
      for (size_t i=1;i<polys.size();++i) {
	r = r->intersect(polys[i]);
      }
    }
    return r;
  }

  Pointer<Region2D> Region2D::createXORPath (const Reference<Path2D>& p)
  {
    vector<Reference<Contour2D> > polys(createContours(p));
    if (polys.empty()) {
      throw ::std::invalid_argument("Path does not define valid boundaries");
    }
    Reference<Region2D> r = polys[0];
    if (polys.size()>1) {
      //@todo fix
      for (size_t i=1;i<polys.size();++i) {
	r = r->exclusiveOR(polys[i]);
      }
    }
    return r;
  }

  bool Region2D::contains (const Point2D& pt) const throws()
  {
    return locatePoint(pt.x(),pt.y())<=0;
  }
    
  bool Region2D::contains (double x, double y) const throws()
  {
    return locatePoint(x,y)<=0;
  }
    
  Int Region2D::locatePoint(const Point2D& pt) const throws()
  { return locatePoint(pt.x(),pt.y()); }
      
  Pointer<Path2D> Region2D::clipPath (const Reference<Path2D>& p) const throws()
  {
    return ::tikal::clipPath(p,toRef<Region2D>());
  }

  Pointer<Region2D> Region2D::intersect (const Reference<Region2D>& p) const throws()
  {
    return Region2DOps::create()->intersect(toRef<Region2D>(),p);
  }
    
  Reference<Region2D> Region2D::merge (const Reference<Region2D>& p) const throws()
  { 
    return Region2DOps::create()->merge(toRef<Region2D>(),p);
  }

  Pointer<Region2D> Region2D::subtract (const Reference<Region2D>& p) const throws()
  { 
    Reference<Region2D> THIS(const_cast<Region2D*>(this));
    return Region2DOps::create()->difference(toRef<Region2D>(),p);
  }

  Pointer<Region2D> Region2D::normalize() const throws()
  {
    try {
      return Region2DOps::create()->normalize(toRef<Region2D>());
    }
    catch (...) {
      return Pointer<Region2D>();
    }
  }

  ::timber::Pointer<Region2D> Region2D::reducePrecision(double prec) const throws()
  {
    try {
      return Region2DOps::create()->reducePrecision(toRef<Region2D>(),prec);
    }
    catch (...) {
      return Pointer<Region2D>();
    }
  }
  

  Pointer<Region2D> Region2D::removeHoles() const throws()
  {
    try {
      return Region2DOps::create()->removeHoles(toRef<Region2D>());
    }
    catch (...) {
      return Pointer<Region2D>();
    }
  }

  Pointer<Region2D> Region2D::exclusiveOR (const Reference<Region2D>& p) const throws()
  { 
    return Region2DOps::create()->exclusiveOr(toRef<Region2D>(),p);
  }

  
}
