#include <tikal/Polygon2D.h>
#include <tikal/Point2D.h>
#include <tikal/VertexOrder.h>
#include <tikal/BoundingBox2D.h>
#include <tikal/Region2DOps.h>

#include <timber/ios/Guard.h>

#include <iostream>
#include <iomanip>

using namespace ::std;
using namespace ::timber;

namespace tikal {
  namespace {

    struct Triangle : public Polygon2D {
	
      static const size_t VERTEX_COUNT = 3;

      Triangle (double x0,double y0, double x1, double y1, double x2,double y2, int dir) throws(::std::exception)
      : _bounds(x0,y0)
      {
	_bounds.insert(x1,y1).insert(x2,y2);
	_x[0]=Point2D(x0,y0);
	_x[1]=Point2D(x1,y1);
	_x[2]=Point2D(x2,y2);

	if (_x[0]==_x[1] || _x[0]==_x[2] || _x[1]==_x[2]) {
	  throw invalid_argument("Points do not form a triangle");
	}

	if (dir==0) {
	  _order=VertexOrder(_x[0],_x[1],_x[2]);
	}
	else if (dir<0) {
	  _order = VertexOrder::CCW;
	}
	else {
	  _order = VertexOrder::CW;;
	}
      }

      Triangle (const Point2D& p0, const Point2D& p1, const Point2D& p2, int dir) throws(::std::exception)
      : _bounds(p0)
      {
	_x[0] = p0;
	_x[1] = p1;
	_x[2] = p2;
	if (_x[0]==_x[1] || _x[0]==_x[2] || _x[1]==_x[2]) {
	  throw invalid_argument("Points do not form a triangle");
	}

	_bounds.insert(p1);
	_bounds.insert(p2);
	if (dir==0) {
	  _order=VertexOrder(_x[0],_x[1],_x[2]);
	}
	else if (dir<0) {
	  _order = VertexOrder::CCW;
	}
	else {
	  _order = VertexOrder::CW;;
	}
      }


      ~Triangle () throws()
      {}

      VertexOrder vertexOrder() const throws()
      { return _order; }

      size_t vertexCount() const throws() { return 3; }
      Point2D vertex(size_t v) const throws() 
      { 
	assert(v<3);
	return _x[v]; 
      }
      int locatePoint (double x, double y) const throws()
      {
	if (_bounds.classify(x,y)!=0) {
	  return 1;
	}
	return computePointPolygonOrientation(x,y,VERTEX_COUNT,_x); 
      }

      size_t segmentCount() const throws()
      { return 3; }
      size_t segment (size_t i, Point2D* pts) const throws()
      {
	assert(i<3);
	pts[0] = _x[i];
	pts[1] = _x[(i+1)%3];
	assert(pts[0]!=pts[1]);
	return 1; 
      }
	
      BoundingBox2D bounds() const throws()
      { return _bounds; }

      bool isConvex() const throws() { return true; }

    private:
      Point2D _x[3];
      VertexOrder _order;
      BoundingBox2D _bounds;
    };

    struct BoundsPoly : public Polygon2D {
      inline BoundsPoly (const BoundingBox2D& box, VertexOrder vo) throws()
	: _bounds(box),_vertexOrder(vo)
      {
	_vertex[0] = box.topLeft();
	_vertex[2] = box.bottomRight();
	if (vo==VertexOrder::CW) {
	  _vertex[1] = box.topRight();
	  _vertex[3] = box.bottomLeft();
	}
	else {
	  _vertex[1] = box.bottomLeft();
	  _vertex[3] = box.topRight();
	}
	
      }

      ~BoundsPoly () throws()
      {}

      VertexOrder vertexOrder() const throws()
      { return _vertexOrder; }

      bool isConvex() const throws() 
      { return true; }

      size_t vertexCount() const throws() { return 4; }
	
      Point2D vertex(size_t i) const throws() 
      { 
	assert(i<4 && "Invalid point index");
	return _vertex[i];
      }

      int locatePoint (double x, double y) const throws()
      {
	if (x < _bounds.minx() || x > _bounds.maxx() || y < _bounds.miny() || y > _bounds.maxy()) {
	  return 1;
	}
	if (x == _bounds.minx() || x==_bounds.maxx() || y==_bounds.miny() || y==_bounds.maxy()) {
	  return 0;
	}
	return -1;
      }

      size_t segmentCount() const throws()
      { return 4; }
	
      size_t segment (size_t i, Point2D* pts) const throws()
      {
	assert(i<4);
	pts[0] = _vertex[i];
	 // can use (i+1) & 0x3 instead of %4 if that would be faster
	pts[1] = _vertex[(i+1)%4];
	return 1;
      }
	
      BoundingBox2D bounds() const throws()
      { return _bounds; }
	
    private:
      const BoundingBox2D _bounds;
      const VertexOrder _vertexOrder;
      Point2D _vertex[4];
    };

    struct SePolygon : public Polygon2D {
	
      SePolygon (size_t n, const double* pts, int dir) throws(::std::exception)
      : _convexStatus(-1)
      {
	if (n<3) {
	  throw invalid_argument("Not enough points for a polygon");
	}

	_vertices = new Point2D[n];
	_vertices[0] = Point2D(pts[0],pts[1]);
	_bounds = BoundingBox2D(_vertices[0]);
	size_t j=0;
	for (size_t i=1;i<n;++i) {
	  const Point2D tmp(pts[2*i],pts[2*i+1]);
	  if (tmp!=_vertices[j]) {
	    _vertices[++j] = tmp;
	    _bounds.insert(tmp);
	  }
	}
	while (_vertices[j]==_vertices[0] && j > 0) {
	  --j;
	}
	_count = j+1;

	if (_count<3) {
	  delete [] _vertices;
	  throw invalid_argument("Too many repeated points");
	}

	if (dir==0) {
	  _order = VertexOrder(_count,_vertices);
	}
	else if (dir<0) {
	  _order = VertexOrder::CCW;
	}
	else {
	  _order = VertexOrder::CW;
	}
      }
	
      SePolygon (size_t n, const Point2D* pts, int dir)throws(::std::exception)
      : _convexStatus(-1)
      {
	if (n<3) {
	  throw invalid_argument("Not enough points");
	}

	_vertices = new Point2D[n];
	_vertices[0] = Point2D(pts[0]);
	_bounds = BoundingBox2D(_vertices[0]);
	size_t j =0;
	for (size_t i=1;i<n;++i) {
	  if (pts[i]!=_vertices[j]) {
	    _vertices[++j] = pts[i];
	    _bounds.insert(pts[i]);
	  }
	}
	while (_vertices[j]==_vertices[0] && j > 0) {
	  --j;
	}

	//@todo check that vertex order is correct
	_count = j+1;

	if (_count<3) {
	  delete [] _vertices;
	  throw invalid_argument("Too many repeated points");
	}


	if (dir==0) {
	  _order = VertexOrder(_count,_vertices);
	}
	else if (dir<0) {
	  _order = VertexOrder::CCW;
	}
	else {
	  _order = VertexOrder::CW;
	}
      }
		
      ~SePolygon () throws()
      {
	delete [] _vertices;
      }
      VertexOrder vertexOrder() const throws()
      { return _order; }
      size_t vertexCount() const throws() 
      { return _count; }
      Point2D vertex(size_t v) const throws() 
      {
	assert(v<_count);
	return _vertices[v]; 
      }
      int locatePoint (double x, double y) const throws()
      { 
	if (_bounds.classify(x,y)!=0) {
	  return 1;
	}
	return computePointPolygonOrientation(x,y,_count,_vertices); 
      }

      size_t segmentCount() const throws()
      { return _count; }
      size_t segment (size_t i, Point2D* pts) const throws()
      {
	assert(i<_count);
	pts[0] = _vertices[i];
	pts[1] = _vertices[(i+1)%_count];

	assert(pts[0]!=pts[1]);
	return 1; 
      }

      BoundingBox2D bounds() const throws()
      { return _bounds; }

      bool isConvex() const throws() 
      {
	if (_convexStatus==-1) {
	  _convexStatus = isConvexPolygon(_count,_vertices) ? 1 : 0;
	}
	return _convexStatus; 
      }

      /** The vertex list */
    private:
      mutable int _convexStatus;
      size_t _count;
      VertexOrder _order;
      Point2D* _vertices;
      BoundingBox2D _bounds;
    };

      
  }      
    
  Polygon2D::Polygon2D () throws()
  {}
    
  Polygon2D::~Polygon2D () throws()
  {}
    
  size_t Polygon2D::segmentCount() const throws()
  { return vertexCount(); }

  size_t Polygon2D::maxDegree() const throws() 
  { return 1; }

  size_t Polygon2D::segment (size_t i, Point2D* pts) const throws()
  {
    pts[0] = vertex(i);
    pts[1] = vertex((i+1)%vertexCount());
    assert(pts[0]!=pts[1]);
    return 1; 
  }
    
  Reference<Polygon2D> Polygon2D::create (size_t n, const double* pts, int dir) throws(::std::exception)
  {
    if (n==3) {
      return new Triangle(pts[0],pts[1],pts[2],pts[3],pts[4],pts[5],dir);
    }
    else {
      return new SePolygon(n,pts,dir); 
    }
  }
  
  Reference<Polygon2D> Polygon2D::create (size_t n,  const Point2D* pts, int dir) throws(::std::exception)
  {
    if (n==3) {
      return new Triangle(pts[0],pts[1],pts[2],dir);
    }
    else {
      return new SePolygon(n,pts,dir); 
    }
  }
  
  Reference<Polygon2D> Polygon2D::create (const ::std::vector<Point2D>& pts, int dir) throws(::std::exception)
  { return create(pts.size(),&pts[0],dir); }
    
  Reference<Polygon2D> Polygon2D::create (double x1,double y1, double x2, double y2, double x3, double y3, int dir) throws(::std::exception)
  { return new Triangle(x1,y1,x2,y2,x3,y3,dir); }

  Reference<Polygon2D> Polygon2D::create (const Point2D& p, const Point2D& q, const Point2D& r, int dir) throws (::std::exception)
  { return new Triangle(p,q,r,dir); }

  Reference<Polygon2D> Polygon2D::create (size_t n, const double* pts, const VertexOrder& dir) throws(::std::exception)
  { return create(n,pts,dir.isCCW() ? -1 : 1); }
  Reference<Polygon2D> Polygon2D::create (size_t n,  const Point2D* pts, const VertexOrder& dir) throws(::std::exception)
  { return create(n,pts,dir.isCCW() ? -1 : 1); }
  Reference<Polygon2D> Polygon2D::create (const ::std::vector<Point2D>& pts, const VertexOrder& dir) throws(::std::exception)
  { return create(pts,dir.isCCW() ? -1 : 1); }
    
  Reference<Polygon2D> Polygon2D::create (double x1,double y1, double x2, double y2, double x3, double y3, const VertexOrder& dir) throws(::std::exception)
  { return create(x1,y1,x2,y2,x3,y3,dir.isCCW() ? -1 : 1); }

  Reference<Polygon2D> Polygon2D::create (const BoundingBox2D& box, const VertexOrder vo) throws()
  {
    return new BoundsPoly(box,vo); 
  }

  void Polygon2D::print (::std::ostream& out) const 
  {
    ::timber::ios::Guard g(out);
    out << ::std::setprecision(20) << ::std::setw(30);
    out << "class Polygon2D"<<this<< endl << '{' << endl;
    out << "public:" << endl << "Polygon2D"<<this<<"(double xoff, double yoff)"<<endl << '{' << endl;
    out << "::tikal::Point2D pts[] = {" << endl;
    size_t sz = vertexCount();
    for (size_t i=0;i<sz;++i) {
      const Point2D v=vertex(i);
      out << "::tikal::Point2D(" << v.x() << "+xoff,"<<v.y()<<"+yoff)," << endl;
    }
    out << "};" << endl;
    out << "_poly= ::tikal::Polygon2D::create("<<sz<<",pts);"<<endl<<"}" << endl;
    out << "::tikal::Polygon2D poly() const throws() { return _poly; }" << endl;
    out << "private:\n::timber::Reference< ::tikal::Polygon2D> _poly;"<<endl<<"};" << endl;
  }

  double Polygon2D::sqrDistance (const Reference<Polygon2D>& p) const throws()
  {
    assert ("FIXME: Polygon2D::sqrDistance not implemented"==0);
    return 0.0;
  }

  Pointer<Region2D> Polygon2D::toTriangleStrips() const throws()
  {
    try {
      return Region2DOps::create()->createTriStripRegion(toRef<Polygon2D>());
    }
    catch (...) {
      return Pointer<Region2D>();
    }
  }
 
}
