#ifndef _TIKAL_VERTEXORDER_H
#define _TIKAL_VERTEXORDER_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#include <iosfwd>
#include <vector>

namespace tikal {
  
  /** Forward declare a point */
  class Point2D;

  /** 
   * This class defines the possible orderings of a sequence of points. There
   * are two orders, clockwise (CW) and counter-clockwise (CCW).
   */
  class VertexOrder {
      
    /** The ordering */
  public:
    enum Order {
      CW=0, ///< clockwise vertex order
      CCW=1,///< counter-clockwise vertex order
    };

    /**
     * Create a CCW vertex order
     */
  public:
    inline VertexOrder () : _ccw(true) {}
      
    /**
     * Create an ordering
     * @param x a vertex ordering
     */
  public:
    inline VertexOrder (Order x) throws() 
      : _ccw(x==CCW) {}

    /**
     * Compute a vertex ordering for two points
     * @param p a start point
     * @param q an end point
     */
  public:
    VertexOrder (const Point2D& p, const Point2D& q) throws();

    /**
     * Compute a vertex ordering for three vertices
     * @param p a triangle vertex
     * @param q a triangle vertex
     * @param r a triangle vertex
     */
  public:
    VertexOrder (const Point2D& p, const Point2D& q, const Point2D& r) throws();

    /**
     * Compute a vertex ordering for a set of points
     * @param n the number of points
     * @param p the points
     */
  public:
    VertexOrder (size_t n, const Point2D* p) throws();
      
    /**
     * Compute a vertex ordering for a set of points
     * @param p the points
     */
  public:
    VertexOrder (const ::std::vector<Point2D>& p) throws();
      

    /** Destroy this ordering */
  public:
    inline ~VertexOrder() throws() {}

    /**
     * The CCW order
     * @return CCW ordering
     */
  public:
    inline static VertexOrder ccw() throws()
    { return VertexOrder(CCW); }

    /**
     * The CW order
     * @return CW ordering
     */
  public:
    inline static VertexOrder cw() throws()
    { return VertexOrder(CW); }

    /**
     * Compare two orders
     * @param v an order
     * @return true if the vertex orders are the same
     */
  public:
    inline bool operator==(const VertexOrder& v) const throws()
    { return _ccw ==  v._ccw; }

    /**
     * Compare two orders
     * @param v an order
     * @return true if the vertex orders are different
     */
  public:
    inline bool operator!=(const VertexOrder& v) const throws()
    { return _ccw !=  v._ccw; }
       
    /**
     * Invert this order
     * @return the reverse ordering
     */
  public:
    inline VertexOrder reverse() const throws()
    { VertexOrder r; r._ccw= !_ccw; return r; }
      
    /** 
     * Is the order CCW 
     * @return true if the order is CCW
     */
  public:
    inline bool isCCW () const throws() { return _ccw; }

    /** 
     * Is the order CCW 
     * @return true if the order is CCW
     */
  public:
    inline bool isCW () const throws() { return !_ccw; }
      
      
    /** The vertex order */
  private:
    bool _ccw;
  };
}

/**
 * Print out either CCW or CW.
 * @param out an output stream
 * @param x a vertex ordering
 * @return out
 */
::std::ostream& operator << (::std::ostream& out, const ::tikal::VertexOrder& x);

#endif
