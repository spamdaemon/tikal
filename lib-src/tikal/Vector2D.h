#ifndef _TIKAL_VECTOR2D_H
#define _TIKAL_VECTOR2D_H

#ifndef _TIKAL_H
#include <tikal/tikal.h>
#endif

#ifndef _CANOPY_HASH_H
#include <canopy/hash.h>
#endif

#include <cmath>
#include <iosfwd>
#include <cstdlib>
#include <cstring>
#include <cassert>

namespace tikal {
  
  /**
   * This class represents vector on a planar cartesian coordinate system.
   * This class has been kept small on purpose, because
   * calculations should be performed using a math package.
   */
  class Vector2D {
    /** Create the vector(0,0) */
  public:
    inline Vector2D () throws()
    {
      _v[0]=_v[1]=0;
    }
    
    
    /** 
     * Create a new vector 
     * @param fx x-coordinate of the vector
     * @param fy y-coordinate of the vector
     */
  public:
    inline Vector2D (double fx, double fy) throws()
    {
      _v[0]=fx;
      _v[1]=fy;
    }
    
    
    /**
     * Get the coordinate at the specified index.
     * @param i an index of 0 (=x) or 1 (=y)
     * @return the coordinate
     */
  public:
    inline double operator[] (size_t i) const throws() 
    {
      assert(i==0 || i==1);
      return _v[i]; 
    }
    
    /**
     * Get the x coordinate
     * @return the x-coordinate
     */
  public:
    inline double x() const throws() { return _v[0]; }

    /**
     * Get the y coordinate
     * @return the y-coordinate
     */
  public:
    inline double y() const throws() { return _v[1]; }

    /**
     * Set the x value
     * @param xx the new x-value
     */
  public:
    inline void setX(double xx) throws() { _v[0] = xx; }

    /**
     * Set the y value
     * @param yy the new y-value
     */
  public:
    inline void setY(double yy) throws() { _v[1] = yy; }

    /**
     * Set the x and y values
     * @param xx the new x-value
     * @param yy the new y-value
     */
  public:
    inline void set(double xx, double yy) throws() 
    { 
      _v[0] = xx; 
      _v[1] = yy; 
    }
      
    /**
     * Get the quadrant that a vector lies in. The quadrants are
     * the standard quadrants, enumerated in counter clockwise order.
     * <pre>
     *   II:x()<0 && y()>=0   I:x()>=0 && y()>=0
     *   III:x()<0 && y()<0   IV:x()>0 && y()<0
     * </pre>.
     */
  public:
    int quadrant () const throws();

    /** 
     * Compare two vectors for equality by component.
     * @param v a vector
     * @return <code>x()==v.x() && y()==v.y()</code>
     */
  public:
    inline bool equals (const Vector2D& v) const throws()
    { return _v[0]==v._v[0] && _v[1]==v._v[1]; }
      
    /**
     * Determine a vector is less than some other vector. The
     * comparison is based on a comparison of the angles of the
     * two vector.
     * @param v a vector.
     * @return <code>angle() < v.angle()</code>
     * @note this method may not actually compute the angle
     */
  public:
    bool compareAngles (const Vector2D& v) const throws();
      
    /**
     * Compute the angle with the horizontal. The resulting
     * angle is normalized to lie between 0 and 2*M_PI;
     * @return <code>::std::atan2(y,x)</code> mapped to 0..2*M_PI
     */
  public:
    inline double angle () const throws()
    {
      const double a = ::std::atan2(_v[1],_v[0]);
      return (a < .0 ? a+(2.0*M_PI) : a);
    }

    /**
     * Compute the dot product
     * @param v a vector
     * @return the dot product between two vectors
     */
  public:
    inline double dot(const Vector2D& v) const throws()
    { return _v[0]*v._v[0]+_v[1]*v._v[1]; }

    /**
     * Compute the cross product.
     * @param v a vector
     * @return <code>x()*v.y()-y()*v.x()</code>
     */
  public:
    inline double cross (const Vector2D& v) const throws()
    { return _v[0]*v._v[1]-_v[1]*v._v[0]; }

    /**
     * Compute the square of the length of this vector or <code>this->dot(*this)</code>.
     * @return the square of the length of this vector
     */
  public:
    inline double sqrLength() const throws()
    { return _v[0]*_v[0]+_v[1]*_v[1]; }
      
    /**
     * Compute the lenth of this vector
     * @return the length of this vector
     */
  public:
    inline double length() const throws()
    { return ::std::sqrt(sqrLength()); }
      
    /**
     * Create a vector normal to this one. If this vector is
     * Vector2D(0,0), then the normal vector is also Vector2D(0,0).
     * @note the following invariance always holds <code>length()==normal().length()</code>
     * @return Vector2D(-y(),x())
     */
  public:
    inline Vector2D normal () const throws()
    { const Vector2D n(-_v[1],_v[0]); assert (n._v[1]*n._v[1]+n._v[0]*n._v[0] == _v[0]*_v[0]+_v[1]*_v[1]); return n; }
      
    /** 
     * Compute the square of Cartesian distance between two vectors.
     * @param pt a vector
     * @return the square of the distance between the two vectors.
     */
  public:
    inline double sqrDistance (const Vector2D& pt) const throws()
    {
      double dx = _v[0]-pt._v[0];
      double dy = _v[1]-pt._v[1];
      return dx*dx+dy*dy;
    }
    
    /** 
     * Compute the Cartesian distance between two vectors.
     * @param pt a vector
     * @return the distance between the two vectors.
     */
  public:
    inline double distance (const Vector2D& pt) const throws()
    { return ::std::sqrt(sqrDistance(pt)); }
      
    /**
     * Compute a hashvalue for this vector.
     * @return a hashvalue
     */
  public:
    inline size_t hashValue () const throws()
    { return ::canopy::hashValue(_v[0]) ^ ::canopy::hashValue(_v[1]); }

    /**
     * Normalize this vector
     */
  public:
    Vector2D& normalize() throws()
      {
	const double len = length();
	assert(len>0. || "Zero length vector cannot be normalized");
	_v[0] /= len;
	_v[1] /= len;
	return *this;
      }
      
    /**
     * Negate this vector
     */
  public:
    Vector2D& negate () throws()
      { _v[0]=-_v[0]; _v[1]=-_v[1]; return *this; }
      
    /**
     * Add the specified value to this vector
     * @param xoff the offset along x
     * @param yoff the offset along y
     */
  public:
    inline Vector2D& translate (double xoff, double yoff) throws()
    { _v[0] += xoff; _v[1]+=yoff; return *this; }

    /**
     * Scale this vector vector scaled along each axis.
     * @param xscale the scale factor along x
     * @param yscale the scale factor along y
     */
  public:
    inline Vector2D&  scale (double xscale, double yscale) throws()
    { _v[0]*=xscale; _v[1]*=yscale; return *this; }
      
    /**
     * Scale a vector uniformly scaled along each axis.
     * @param s the scale factor along x and y
     */
  public:
    inline Vector2D&  scale (double s) throws()
    { return scale(s,s); }
      
    /**
     * Create a vector counter-clockwise rotated around the origin by a given angle.
     * @param theta the angle of rotation in radians
     * @return a rotated vector
     */
  public:
    inline Vector2D&  rotate (double theta) throws()
    {
      const double c = ::std::cos(theta);
      const double s = ::std::sin(theta);
      const double tx = c*_v[0]-s*_v[1];
      const double ty = s*_v[0]+c*_v[1];
      _v[0] = tx;
      _v[1] = ty;
      return *this;
    }

    /**
     * Print this vector 
     * @param out an output stream
     */
  public:
    ::std::ostream& print (::std::ostream& out) const;

    /** The vector coordinates */
  private:
    double _v[2];
  };

}

/**
 * Copy a point array.
 * @param src the points to be copied
 * @param dest the destination to which to copy the points
 * @param n the number of points to be copied
 */
namespace canopy {
  inline void arraycopy (const ::tikal::Vector2D* src, ::tikal::Vector2D* dest, size_t n) throws()
  { ::std::memcpy(dest,src,n*sizeof(::tikal::Vector2D)); }

  /**
   * Copy a point array.
   * @param src the points to be copied
   * @param dest the destination to which to copy the points
   * @param n the number of points to be copied
   */
  inline void arraymove (const ::tikal::Vector2D* src, ::tikal::Vector2D* dest, size_t n) throws()
  { ::std::memmove(dest,src,n*sizeof(::tikal::Vector2D)); }
}

/**
 * @name Comparison operators
 * @{
 */
inline bool operator== (const ::tikal::Vector2D& a, const ::tikal::Vector2D& b) throws()
{ return a.equals(b); }
inline bool operator!= (const ::tikal::Vector2D& a, const ::tikal::Vector2D& b) throws()
{ return !a.equals(b); }

/*@}*/

/**
 *@name Common vector operators
 *@{
 */
inline ::tikal::Vector2D& operator+=(::tikal::Vector2D& a, const ::tikal::Vector2D& b) throws()
{ return a.translate(b.x(),b.y()); }

inline ::tikal::Vector2D operator+(const ::tikal::Vector2D& a, const ::tikal::Vector2D& b) throws()
{ return ::tikal::Vector2D(a.x()+b.x(),a.y()+b.y()); }

inline ::tikal::Vector2D& operator-=(::tikal::Vector2D& a, const ::tikal::Vector2D& b) throws()
{ return a.translate(-b.x(),-b.y()); }

inline ::tikal::Vector2D operator-(const ::tikal::Vector2D& a, const ::tikal::Vector2D& b) throws()
{ return ::tikal::Vector2D(a.x()-b.x(),a.y()-b.y()); }

inline ::tikal::Vector2D operator-(const ::tikal::Vector2D& a) throws()
{ return ::tikal::Vector2D(-a.x(),-a.y()); }

/*@}*/

inline ::std::ostream& operator<< (::std::ostream& out, const ::tikal::Vector2D& b)
{ return b.print(out); }

#endif
