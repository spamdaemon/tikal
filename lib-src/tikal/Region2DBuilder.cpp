#include <tikal/Region2DBuilder.h>
#include <tikal/BoundingBox2D.h>
#include <tikal/Point2D.h>
#include <tikal/Contour2D.h>
#include <tikal/AbstractRegion2D.h>
#include <tikal/AbstractArea2D.h>
#include <tikal/Polygon2D.h>
#include <tikal/VertexOrder.h>

#include <vector>

using namespace ::timber;

namespace tikal {
   namespace {
      struct Region : public AbstractRegion2D
      {
            inline Region()
            {
            }

            ~Region() throws()
            {
            }

            Pointer< Region2D> clone() const throws()
            {
               if (_areas.empty()) {
                  return Pointer< Region2D>();
               }
               if (_areas.size() == 1) {
                  return _areas[0];
               }

               Reference< Region> r = new Region();
               r->_areas = _areas;
               r->_bounds = _bounds;
               return r;
            }

            size_t areaCount() const throws()
            {
               return _areas.size();
            }

            Reference< Area2D> area(size_t i) const throws()
            {
               return _areas.at(i);
            }

            BoundingBox2D bounds() const throws()
            {
               return _bounds;
            }

            void addArea(const Reference< Area2D>& a) throws()
            {
               if (_areas.empty()) {
                  _bounds = a->bounds();
               }
               else {
                  _bounds.merge(a->bounds());
               }
               _areas.push_back(a);
            }

         private:
            BoundingBox2D _bounds;
            ::std::vector< Reference< Area2D> > _areas;
      };

      struct Area : public AbstractArea2D
      {
            inline Area(const Reference< Contour2D>& c)
                  : _outside(c), _bounds(c->bounds())
            {
            }

            ~Area() throws()
            {
            }

            size_t contourCount() const throws()
            {
               return _inside.size();
            }

            Reference< Contour2D> insideContour(size_t i) const throws()
            {
               return _inside.at(i);
            }

            Reference< Contour2D> outsideContour() const throws()
            {
               return _outside;
            }

            void addInternalContour(const Reference< Contour2D>& c)
            {
               _inside.push_back(c);
               _bounds.merge(c->bounds());
            }

            BoundingBox2D bounds() const throws()
            {
               return _bounds;
            }

         private:
            const Reference< Contour2D> _outside;
            BoundingBox2D _bounds;
            ::std::vector< Reference< Contour2D> > _inside;
      };

      /**
       * A region builder can be used to quickly assemble
       * arbitrary regions
       */
      class BasicBuilder : public Region2DBuilder
      {

            /**
             * Create a new region builder.
             */
         public:
            BasicBuilder() throws()
                  : _externalContour(false), _haveWindDirection(false)
            {
            }

            /** Destructor */
         public:
            ~BasicBuilder() throws()
            {
            }

            void reset() throws()
            {
               _region = nullptr;
               _area = nullptr;
               _points.clear();
            }

            void addArea(const Reference< Area2D>& a) throws (::std::exception)
            {
               endContour(true);
               if (_region == nullptr) {
                  _region = new Region();
               }
               _region->addArea(a);
            }

            Pointer< Region2D> getRegion() throws()
            {
               endContour(true);
               if (_region != nullptr) {
                  return _region->clone();
               }
               else {
                  return Pointer< Region2D>();
               }
            }

            void addExternalContour(const Reference< Contour2D>& c)
            {
               endContour(true);
               _area = new Area(c);
            }

            void beginExternalContour(const Point2D& start)
            {
               endContour(true);
               _points.push_back(start);
               _externalContour = true;
               _haveWindDirection = false;
            }

            void beginExternalContour(const Point2D& start, const VertexOrder& windDir)
            {
               endContour(true);
               _points.push_back(start);
               _externalContour = true;
               _haveWindDirection = true;
               _windDirection = windDir;
            }

            void addInternalContour(const Reference< Contour2D>& c)
            {
               endContour(false); // end the current contour
               if (_area != nullptr) {
                  _area->addInternalContour(c);
               }
            }

            void beginInternalContour(const Point2D& start)
            {
               endContour(false); // end the current contour
               _points.push_back(start);
               _externalContour = false;
               _haveWindDirection = false;
            }

            void beginInternalContour(const Point2D& start, const VertexOrder& windDir)
            {
               endContour(false); // end the current contour
               _points.push_back(start);
               _externalContour = false;
               _haveWindDirection = true;
               _windDirection = windDir;
            }

            void lineTo(const Point2D& pt) throws(::std::exception)
            {
               if (pt == _points[_points.size() - 1]) {
                  return;
               }

               _points.push_back(pt);
            }

            Pointer< Contour2D> buildContour() throws()
            {
               if (_points.size() > 2) {
                  if (_points[0] == _points[_points.size() - 1]) {
                     _points.pop_back();
                  }
                  if (_points.size() > 2) {
                     try {
                        if (_haveWindDirection) {
                           return Polygon2D::create(_points, _windDirection);
                        }
                        else {
                           return Polygon2D::create(_points);
                        }
                     }
                     catch (const ::std::exception& e) {
                        // ignore
                     }
                  }
               }
               return Pointer< Contour2D>();
            }

            void endContour(bool areaComplete) throws()
            {
               Pointer< Contour2D> c = buildContour();
               if (c) {
                  if (_externalContour) {
                     _area = new Area(c);
                  }
                  else {
                     assert(_area != nullptr);
                     _area->addInternalContour(c);
                  }
               }
               _points.clear();

               if (areaComplete) {
                  if (_area != nullptr) {
                     if (_region == nullptr) {
                        _region = new Region();
                     }
                     _region->addArea(_area);
                     _area = nullptr;
                  }
               }
            }

         private:
            Pointer< Region> _region;

         private:
            Pointer< Area> _area;

         private:
            bool _externalContour;

         private:
            ::std::vector< Point2D> _points;

         private:
            bool _haveWindDirection;
         private:
            VertexOrder _windDirection;
      };
   }

   Region2DBuilder::Region2DBuilder() throws()
   {
   }

   Region2DBuilder::~Region2DBuilder() throws()
   {
   }

   ::std::unique_ptr< Region2DBuilder> Region2DBuilder::create() throws()
   {
      return ::std::unique_ptr < Region2DBuilder > (new BasicBuilder());
   }
}

