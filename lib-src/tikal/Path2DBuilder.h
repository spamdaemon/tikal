#ifndef _TIKAL_PATH2DBUILDER_H
#define _TIKAL_PATH2DBUILDER_H

#ifndef _TIKAL_PATH2D_H
#include <tikal/Path2D.h>
#endif

#ifndef _GPS_GEOMERY_POINT2D_H
#include <tikal/Point2D.h>
#endif

#include <vector>

namespace tikal {
  /**
   * A path builder can be used to quickly assemble
   * arbitrary paths
   */
  class Path2DBuilder {
    Path2DBuilder&operator=(const Path2DBuilder&);
    Path2DBuilder(const Path2DBuilder&);
      
    /** A path point */
  public:
    struct WayPoint;

    /**
     * Create a new path builder.
     * @param cap the expected number of points used to construct the path
     * @pre REQUIRE_GREATER(cap,0)
     */
  public:
    Path2DBuilder(size_t cap=128) throws();

    /** Destructor */
  public:
    ~Path2DBuilder() throws();

    /**
     * Reset this builder.
     */
  public:
    void reset() throws();

    /**
     * Create a path consisting of a single polyline.
     * @param n the number of points in the polyline
     * @param pts the points in the polyline
     * @return a new polyline path
     */
  public:
    static ::timber::Pointer<Path2D> createPolylinePath (size_t n, const Point2D* pts);

    /**
     * Create a path consisting of a single polyline.
     * @param pts the points in the polyline
     * @return a new polyline path
     */
  public:
    static ::timber::Pointer<Path2D> createPolylinePath (const ::std::vector<Point2D>& pts);

    /**
     * Move to a new position.
     * @param x a point
     */
  public:
    inline void moveTo (const Point2D& x) throws()
    { moveTo(x.x(),x.y()); }

    /**
     * Move to a new position.
     * @param x x-coordinate of a point
     * @param y y-coordinate of a point
     */
  public:
    void moveTo (double x, double y) throws();

    /**
     * Create a line from the last point to the specified point.
     * @param x a point
     * @throws ::std::exception if there was no prior moveTo() call
     */
  public:
    inline void lineTo (const Point2D& x) throws (::std::exception)
    { lineTo(x.x(),x.y()); }

    /**
     * Create a line from the last point to the specified point.
     * @param x x-coordinate of a point
     * @param y y-coordinate of a point
     * @throws ::std::exception if there was no prior moveTo() call
     */
  public:
    void lineTo (double x, double y) throws (::std::exception);

    /**
     * Close the path. Connect to the most recent
     * moveTo().
     * @throws Exception if there was no prior moveTo() call
     */
  public:
    void close() throws (::std::exception);

    /**
     * Get the path as built up to now.
     * @return the path built so far
     */
  public:
    ::timber::Pointer<Path2D> path() const throws();

    /**
     * Add new path points
     * @param n the number of points to add
     * @param p n path points
     */
  private:
    void addWayPoint (size_t n, const WayPoint* p) throws();

    /** True if we need a move to */
  private:
    bool _needMoveTo;

    /** The most recent start point */
  private:
    double _x, _y;

    /** The current path */
  private:
    mutable ::timber::Pointer<Path2D> _path;

    /** The number of individual paths */
  private:
    size_t _pathCount;

    /** The maximum degree */
  private:
    size_t _maxDegree;

    /** The path points */
  private:
    ::std::vector<WayPoint> _points;
  };
}


#endif
