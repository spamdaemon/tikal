#ifndef _TIKAL_BOUNDINGBOX2D_H
#define _TIKAL_BOUNDINGBOX2D_H

#ifndef _TIKAL_H
#include <tikal/tikal.h>
#endif

#ifndef _TIKAL_POINT2D_H
#include <tikal/Point2D.h>
#endif

#include <iosfwd>

namespace tikal {
  
  /**
   * A 2D bounding box.
   */
  class BoundingBox2D {
    
    /**
     * The default constructor. Creates a bounding box 
     * with a width and height of 0.
     */
  public:
    inline BoundingBox2D() throws()
      : _minX(0),_maxX(0),_minY(0),_maxY(0)
      {}
    
    /**
     * Create a new bounding box containing a single point
     * @param minX the left x-coordinate
     * @param minY the botton y-coordinate
     * @param maxX the right x-coordinate
     * @param maxY the top y-coordinate
     * @pre minX<=maxX;
     * @pre minY<=maxY;
     */
  public:
    inline BoundingBox2D (double minX, double minY, double maxX, double maxY)  throws()
      : _minX(minX),_maxX(maxX),_minY(minY),_maxY(maxY)
    {
      assert(minX <= maxX);
      assert(minY <= maxY);
    }
      
    /**
     * Create a new bounding box containing a single point
     * @param pt the point
     */
  public:
    inline BoundingBox2D (const Point2D& pt)  throws()
      : _minX(pt.x()),_maxX(pt.x()),_minY(pt.y()),_maxY(pt.y())
      {}
      
    /**
     * Create a new bounding box containing a single point
     * @param x a x-coordinate
     * @param y a y-coordinate
     */
  public:
    inline BoundingBox2D (double x, double y)  throws()
      : _minX(x),_maxX(x),_minY(y),_maxY(y)
    {}
      
    /**
     * Insert a new point into this box.
     * @param x x-coordinate of a point
     * @param y y-coordinate of a point
     */
  public:
    BoundingBox2D& insert (double x, double y) throws();

    /**
     * Insert a point into this box.
     * @param pt a point
     */
  public:
    inline BoundingBox2D& insert (const Point2D& pt) throws()
    { return insert(pt.x(),pt.y()); }

    /**
     * Merge this box with the specified box.
     * @param b a bounding box
     * @return *this
     */
  public:
    BoundingBox2D& merge (const BoundingBox2D& b) throws();

    /**
     * Intersect this box with the specified box. This box is not
     * modified if the two boxes do not intersect.
     * @param b a bounding box
     * @return true if <code> intersects(b) </code>, false otherwise
     */
  public:
    bool intersect (const BoundingBox2D& b) throws();
      
    /**
     * Determine if this box intersects the specified box.
     * @param b a bounding box
     * @return true this box and b intersect
     */
  public:
    bool intersects (const BoundingBox2D& b) const throws();
      

    /**
     * Determine if this box encloses the specified box.
     * @param b a bounding box
     * @return true this box contains b inside
     */
  public:
    bool encloses (const BoundingBox2D& b) const throws();
      

    /**
     * Determine if this box contains the specified point.
     * @param x x-coordinate of a point
     * @param y y-coordinate of a point
     * @return true if (x,y) is inside this box
     */
  public:
    inline bool contains (double x, double y) const throws()
    { return _minX<=x && x<=_maxX && _minY<=y && y<=_maxY; }
      
    /**
     * Classify a point. If the point is inside this box, then 0
     * is returned, otherwise, the result will encode the location of the point such that:
     * <ul>
     * <li>Bit 3 will be set if the point is to the left of this box
     * <li>Bit 2 will be set if the point is to the right of this box
     * <li>Bit 1 will be set if the point is below of this box
     * <li>Bit 0 will be set if the point is above this box
     * </ul>
     * This method may be used to efficiently test for instance if two points a and b
     * are entirely outside this box: 
     * <code>outside =  (classify(a) & classify(b))!=0;</code> 
     *
     * @param x x-coordinate of the point to be classified
     * @param y y-coordinate of the point to be classified
     * @return 0 if the point is inside, non-zero otherwise.
     */
  public:
    int classify (double x, double y) const throws();
      
    /**
     * Classify a point.
     * @param pt a point
     * @return 0 if the point is inside, non-zero otherwise.
     */
  public:
    inline int classify (const Point2D& pt) const throws()
    { return classify(pt.x(),pt.y()); }
      
      
    /**
     * Determine if this box contains the specified point.
     * @param pt a point
     * @return true if this box contains pt
     */
  public:
    inline bool contains (const Point2D& pt) const throws()
    { return contains(pt.x(),pt.y()); }

    /**
     * Determine if this box intersects the specified line segment.
     * @param a an endpoint of a line segment
     * @param b the second endpoint of the line segment
     * @return true the line lies partially inside this box
     */
  public:
    bool containsSegment (const Point2D& a, const Point2D& b) const throws();
      
    /**
     * Get the left side of this bounding box.
     * @return the left side of this bounding box
     */
  public:
    inline double minx() const throws() { return _minX; }

    /**
     * Get the bottom side of this bounding box.
     * @return the bottom side of this bounding box
     */
  public:
    inline double miny() const throws() { return _minY; }

    /**
     * Get the right side of this bounding box.
     * @return the right side of this bounding box
     */
  public:
    inline double maxx() const throws() { return _maxX; }

    /**
     * Get the top side of this bounding box.
     * @return the top side of this bounding box
     */
  public:
    inline double maxy() const throws() { return _maxY; }

    /**
     * Get the width of this box.
     * @return the width of this box.
     */
  public:
    inline double width() const throws() { return _maxX-_minX; }

    /**
     * Get the height of this box.
     * @return the height of this box.
     */
  public:
    inline double height() const throws() { return _maxY-_minY; }

    /**
     * Get the top-left corner
     * @return the top-left corner point
     */
  public:
    inline Point2D topLeft() const throws() { return Point2D(_minX,_maxY); }

    /**
     * Get the top-right corner
     * @return the top-right corner point
     */
  public:
    inline Point2D topRight() const throws() { return Point2D(_maxX,_maxY); }

    /**
     * Get the botton-left corner
     * @return the botton-left corner point
     */
  public:
    inline Point2D bottomLeft() const throws() { return Point2D(_minX,_minY); }
      
    /**
     * Get the botton-right corner
     * @return the botton-right corner point
     */
  public:
    inline Point2D bottomRight() const throws() { return Point2D(_maxX,_minY); }

    /** 
     * Print this bounding box to the specified outputs stream.
     * @param out a stream
     * @return out
     */
  public:
    ::std::ostream& print (::std::ostream& out) const;

    /** The x-bounds */
  private:
    double _minX,_maxX;

    /** The y-bounds */
  private:
    double _minY,_maxY;
  };
}

inline ::std::ostream& operator<< (::std::ostream& out, const ::tikal::BoundingBox2D& b) 
{ return b.print(out); }

#endif
