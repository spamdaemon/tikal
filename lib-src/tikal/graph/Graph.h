#ifndef _TIKAL_GRAPH_GRAPH_H
#define _TIKAL_GRAPH_GRAPH_H

#ifndef _TIKAL_GRAPH_EDGE_H
#include <tikal/graph/Edge.h>
#endif

#include <iosfwd>
#include <vector>
#include <map>

namespace tikal {
  class VertexOrder;
  class Point2D;

  namespace graph {
    class Node;
    class ContourTracer;
    class Direction;
    
    class Graph {
      Graph&operator=(const Graph&);
      Graph(const Graph&);

      /** The data associated with each segment */
    public:
      typedef Edge::Data Data;
      
      /** The node map */
    private:
      typedef ::std::map<Point2D,Node*> NodeMap;

      /** An edge list */
    private:
      typedef ::std::vector<Edge*> EdgeList;

      /**
       * A callback that is invoked when finding contours.
       */
    public:
      class Callback {
	/** Default constructor */
      public:
	inline Callback() throws() {}

	/** The destructor is protected */
      protected:
	virtual ~Callback() throws() {}

	/**
	 * Begin a new contour.
	 * @param pt the first point of the contour
	 */
      public:
	virtual void beginContour (const Point2D& pt) throws() = 0;

	/**
	 * End the current contour. 
	 */
      public:
	virtual void endContour () throws() = 0;

	/** 
	 * Add a new segment to the current contour. The curve starts
	 * at the last point that was specified and terminates at pts[n-1]
	 * The points pts[0]..pts[n-2] are control points of a bezier curve.
	 * The dimension of the curve is <em>n</em>.
	 * @param n the number of points passed in the pts array
	 * @param pts the points of the segment without the first point
	 */
      public:
	virtual void emitCurveSegment (size_t n, const Point2D* pts) throws() = 0;

	/**
	 * Add a series of linear segments. The first segment
	 * starts at previous point and terminates at pts[0];
	 * the second segment starts at pts[0] and terminates at pts[1],
	 * the last segments starts point pts[n-2] and terminates at pts[n-1]
	 * @param n the number of linear segments specified
	 * @param pts the points 
	 */
      public:
	virtual void emitLinearSegments (size_t n, const Point2D* pts) throws() = 0;

	/**
	 * Discard the current contour.
	 */
      public:
	virtual void discardContour() throws() = 0;
      };

      /** Default constructor */
    public:
      Graph() throws();
      
      /** Destructor */
    public:
      virtual ~Graph() throws();
      
      /**
       * Remove all segments from this graph.
       */
    public:
      void clear() throws();

      /**
       * Remove dangling segments. Dangling segments are those which will not contribute to contours.
       * Before tracing out contours, dangling edges should be removed because they interfere with
       * finding the contours.
       */
    public:
      void removeDanglingSegments();

      /**
       * Insert a line segment. The vertex order specified both, the direction
       * in which the segment's vertices are specified, and the allowed
       * direction of travel on that segment. This method must be called from the overriden
       * method to actually add the segment.
       * @param start the start vertex
       * @param end the end vertex
       * @param vo the vertex order in which start and end are specified.
       * @param data user date that can be associated with the segment
       */
    protected:
      void addSegment (const Point2D& start, const Point2D& end, const VertexOrder& vo, Data data) throws();

      /**
       * Trace contours in the graph.
       * @param cb a callback that will be invoked while traversing contours
       * @param nTracers the number of tracers
       * @param tracers the tracers
       * @return the number of contours traced.
       */
    public:
      virtual size_t traceContours (Callback& cb, size_t nTracers, ContourTracer** tracers) throws();

      /**
       * Trace contours in the graph.
       * @param cb a callback that will be invoked while traversing contours
       * @param tracer the tracers
       * @return the number of contours traced.
       */
    public:
      virtual size_t traceContours (Callback& cb, ContourTracer& tracer) throws();

    public:
      virtual void print(::std::ostream& out) const;

      /**
       * Validate this graph.
       */
    public:
      virtual void validateGraph();

      /**
       * Determine the vertex order of the endpoints of the
       * given edges.
       * @param end1 the end vertex of edge 1
       * @param end2 the end vertex of edge 2
       * @return the vertex order
       */
    private:
      static VertexOrder computeOrientation (Edge& e1, Edge& e2) throws();
	
      /**
       * Determine the vertex order of the endpoints of the
       * given edges.
       * @param end1 the end vertex of edge 1
       * @param end2 the end vertex of edge 2
       * @param end3 the end vertex of edge 3
       * @return the vertex order
       */
    private:
      static VertexOrder computeOrientation (Edge& e1, Edge& e2, Edge& e3) throws();
	
    private:
      Node* makeNode (const Point2D& v);

    private:
      void unlinkEdgeFromStartNode(Edge* e);

    private:
      void linkEdgeToStartNode (Edge* e);

    private:
      Edge* makeEdge (Node* start, Node* end, VertexOrder edgeDir, Direction travelDir, Data data);
      
    private:
      void checkDanglingInvariant();

    private:
      void markDangling(Node* n);

    private:
      NodeMap _nodes;

    private:
      Node* _danglingNode;

    private:
      EdgeList _marked;
    };
    
  }
}


#endif

