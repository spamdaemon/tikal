#include <tikal/graph/AbstractGraphCallback.h>
#include <tikal/Point2D.h>
#include <tikal/Polygon2D.h>

#include <iostream>
#include <cstdlib>

namespace tikal {
  namespace graph {

    AbstractGraphCallback::AbstractGraphCallback () throws()
    {}

    AbstractGraphCallback::~AbstractGraphCallback () throws()
    {}


    void AbstractGraphCallback::beginContour(const Point2D& pt) throws()
    {
      _maxDegree = 0;
      _pts.push_back(pt);
      _segments.push_back(0);
    }
	  
    void AbstractGraphCallback::emitLinearSegments (size_t n, const Point2D* pts) throws()
    {
      _maxDegree = ::std::max(_maxDegree,size_t(1));
      for (size_t i=0;i<n;++i) {
	_pts.push_back(pts[i]);
	_segments.push_back(_pts.size());
      } 
    }
	  
    void AbstractGraphCallback::emitCurveSegment (size_t n, const Point2D* pts) throws() 
    {
      _maxDegree = ::std::max(_maxDegree,n);
      for (size_t i=0;i<n;++i) {
	_pts.push_back(pts[i]);
      }
      _segments.push_back(_pts.size());
    }

    void AbstractGraphCallback::endContour () throws()
    {
      if (_maxDegree==1) {
	::timber::Reference<Polygon2D> poly = Polygon2D::create(_pts);
	contourExtracted(poly);
      }
      else {
	::std::cerr << "Non polygonal contours not yet supported";
      }

      _pts.clear();
      _segments.clear();
    }
	  
    void AbstractGraphCallback::discardContour() throws()
    {
      _pts.clear();
      _segments.clear();
    }
 
  }
}
