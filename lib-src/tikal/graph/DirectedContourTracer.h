#ifndef _TIKAL_GRAPH_DIRECTEDCONTOURTRACER_H
#define _TIKAL_GRAPH_DIRECTEDCONTOURTRACER_H

#ifndef _TIKAL_GRAPH_CONTOURTRACER_H
#include <tikal/graph/ContourTracer.h>
#endif

#ifndef _TIKAL_GRAPH_DIRECTION_H
#include <tikal/graph/Direction.h>
#endif

#ifndef _TIKAL_VERTEXORDER_H
#include <tikal/VertexOrder.h>
#endif

namespace tikal {
  namespace graph {
      
    /**
     * A contour tracer. Contour tracers are stateful!
     */
    class DirectedContourTracer : public ContourTracer {
      /**
       * Create a directed contour tracker. This tracer will start
       * only on edges that support the specified trace direction. At 
       * each vertex, turn are performed either clockwise or counter clockwise. The
       * direction of the edge onto which one turns may not be NONE.
       * @param traceDir the direction of the first edge
       * @param turnDir the turn direction when moving from edge to edge
       */
    public:
      DirectedContourTracer(const VertexOrder& traceDir, const VertexOrder& turnDir) throws();

      /**
       * Create a directed contour tracker. This tracer will start
       * only on edges that support the specified trace direction. At 
       * each vertex, turn are performed either clockwise or counter clockwise. The
       * direction of the edge onto which one turns may not be NONE.
       * @param traceDir the direction of the first edge
       * @param turnDir the turn direction when moving from edge to edge
       * @param allowedEdgeDir the allowed direction of edges when turning
       */
    public:
      DirectedContourTracer(const VertexOrder& traceDir, const VertexOrder& turnDir, Direction allowedEdgeDir) throws();

      /** Destructor */
    public:
      ~DirectedContourTracer() throws();

      /** 
       * Determine in which direction a contour starting at the specified edge
       * should be traced out.
       * @param e an edge
       * @return a true if this may be the start of a contour
       */
    public:
      bool beginTrace(Edge* e) throws();
	
	
      /**
       * Get the trace direction
       * @return the trace direction
       */
    public:
      inline Direction traceDirection() const throws()
      { return _traceDir; }

      /**
       * Get the trace direction
       * @return the trace direction
       */
    public:
      inline VertexOrder turnDirection() const throws()
      { return _turnDir; }

      /**
       * Get the allowed edge direction
       * @return the allowed edge direction
       */
    public:
      inline Direction allowedEdgeDirection() const throws()
      { return _allowedEdgeDirection; }

      /**
       * Get the next edge for this trace. If this method
       * returns 0, no more calls to nextEdge() will succeed.
       * @param e an edge
       * @return an edge or 0 if no valid turn can be made
       */
    public:
      Edge* nextEdge(Edge* e) const throws();

      /** The trace direction */
    private:
      const Direction _traceDir;

      /** The allowed direction of the edges onto which a turn is made */
    private:
      const Direction _allowedEdgeDirection;

      /** The turn direction */
    private:
      const VertexOrder _turnDir;
    };
  }
}
#endif
