#ifndef _TIKAL_GRAPH_PLANARGRAPH_H
#define _TIKAL_GRAPH_PLANARGRAPH_H

#ifndef _TIKAL_GRAPH_GRAPH_H
#include <tikal/graph/Graph.h>
#endif


namespace tikal {
  class SweepLine;
  class Point2D;

  namespace graph {
      
    /**
     * The segment graph is a set of non-crossing 2D segments. Segments
     * may be contact each other only at their vertices. The primary
     * purpose of this is to extract closed contours.
     */
    class PlanarGraph : public Graph {
      PlanarGraph(const PlanarGraph&);
      PlanarGraph&operator=(const PlanarGraph&);
	
      /**
       * Create a new planar graph
       */
    public:
      PlanarGraph () throws();

      /**
       * Create a new planar graph from the segments that result from sweeping a 
       * a SweepLine.
       * @param sweepline a sweepline
       */
    public:
      PlanarGraph (SweepLine& sweepline);

      /**
       * Destructor
       */
    public:
      ~PlanarGraph () throws();

      /**
       * Insert a sweepline into this graph. This method will invoke sweepline.sweepSegments().
       * @param sweepline a sweepline
       */
    public:
      void insertSweepline (SweepLine& sweepline);
	
      /**
       * Insert a line segment. The vertex order specified both, the direction
       * in which the segment's vertices are specified, and the allowed
       * direction of travel on that segment.
       * @param start the start vertex
       * @param end the end vertex
       * @param vo the vertex order in which start and end are specified.
       * @param data user date that can be associated with the segment
       */
    public:
      inline void addSegment (const Point2D& start, const Point2D& end, const VertexOrder& vo, Data data) throws()
      { Graph::addSegment(start,end,vo,data); }
    };
  }
}
#endif
