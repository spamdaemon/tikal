#include <tikal/graph/Node.h>
#include <tikal/graph/Edge.h>
#include <iostream>

namespace tikal {
  namespace graph {
    Node::~Node() throws()
    {
      Edge* e = _firstCCW;
      if (e!=0) {
	do {
	  Edge* n = e->_nextCCW;
	  delete e;
	  e = n;
	} while (e!=_firstCCW);
      }
    }
    
    ::std::ostream& operator<< (::std::ostream& out, const Node& n)
    { return n.point().print(out); }
  }
}


