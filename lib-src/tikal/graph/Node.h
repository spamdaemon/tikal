#ifndef _TIKAL_GRAPH_NODE_H
#define _TIKAL_GRAPH_NODE_H

#ifndef _TIKAL_POINT2D_H
#include <tikal/Point2D.h>
#endif

#include <iosfwd>

namespace tikal {
  namespace graph {
    class Graph;
    class Edge;
       
    /**
     * A graph node
     */
    class Node {
      /** No copying allowed */
    private:
      Node&operator=(const Node&);
      Node(const Node&);
	
      friend class Graph;
	
    private:
      inline Node( const Point2D& pt)
	: _point(pt),_firstCCW(0),
	_nextDangling(0),_predDangling(0),_dangling(false) {}
	
    private:
      ~Node() throws();

      /**
       * Get the vertex point.
       * @return the 2D point
       */
    public:
      inline const Point2D& point() const throws() { return _point; }

      /**
       * Get the first edge.
       * @return the first edge
       */
    public:
      inline Edge* edges() const throws() { return _firstCCW; }

    private:
      Point2D _point;
	
    private:
      Edge* _firstCCW;
	
    private:
      Node* _nextDangling;
      Node* _predDangling;
      bool _dangling;
    };

    ::std::ostream& operator<< (::std::ostream& out, const Node& n);
  }
}


#endif
