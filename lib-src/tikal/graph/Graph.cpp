#include <tikal/graph/Graph.h>
#include <tikal/graph/Edge.h>
#include <tikal/graph/Node.h>
#include <tikal/VertexOrder.h>
#include <tikal/graph/Direction.h>
#include <tikal/graph/ContourTracer.h>

#include <timber/ios/Guard.h>
#include <timber/logging.h>

#include <iostream>
#include <iomanip>
#include <cassert>

#define EDGE_DATA_WARNING_OFF
#define CLOSE_POINT_WARNING_OFF

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace tikal {
  namespace graph {
    
    Graph::Graph() throws()
    : _danglingNode(0)
    {}
      
    Graph::~Graph() throws()
    { clear(); }
      
    VertexOrder Graph::computeOrientation (Edge& e1, Edge& e2, Edge& e3) throws()
    {
      assert(e1._start==e2._start);
      assert(e2._start==e3._start);
      assert(e1._end!=e2._end);
      assert(e1._end!=e3._end);
      assert(e2._end!=e3._end);
      return VertexOrder(e1._end->_point,e2._end->_point,e3._end->_point);
    }
	
    VertexOrder Graph::computeOrientation (Edge& e1, Edge& e2) throws()
    {
      assert(e1._start==e2._start);
      assert(e1._end!=e2._end);
      return VertexOrder(e1._start->_point,e1._end->_point,e2._end->_point);
    }
	
    Node* Graph::makeNode (const Point2D& v)
    {
      pair<NodeMap::iterator,bool> status = _nodes.insert(make_pair(v,static_cast<Node*>(0)));
      if (status.second) {
	status.first->second = new Node(v);
      }
      return status.first->second;
    }

    void Graph::unlinkEdgeFromStartNode(Edge* e)
    {
      Node* v = e->_start;
      Edge* n = e->_nextCCW;
      Edge* p = e->_predCCW;
      e->_nextCCW = e->_predCCW = 0;
      if (n==e) {
	v->_firstCCW = 0;
      }
      else {
	n->_predCCW = p;
	p->_nextCCW = n;
	v->_firstCCW = n; // just reset the first - it doesn't matter
      }
    }

    void Graph::linkEdgeToStartNode (Edge* e)
    {
      Node* n = e->_start;
      Edge* current = n->edges();
      Edge* first = current;
      if (current==0) {
	// no edge attached yet
	n->_firstCCW = e;
	e->_nextCCW = e->_predCCW = e;
      }
      else if (current->_nextCCW==current) {
	// there is only one attached edge so far
	e->_nextCCW = e->_predCCW = current;
	current->_nextCCW = current->_predCCW = e;
      }
      else if (computeOrientation(*first,*e).isCCW()) {
	while (first!=current->_nextCCW && computeOrientation(*first,*current->_nextCCW).isCCW() ) {
	  if (computeOrientation(*current,*e,*current->_nextCCW).isCCW()) {
	    break;
	  }
	  current=current->_nextCCW;
	  assert(current!=n->edges());
	}
	e->_nextCCW = current->_nextCCW;
	e->_predCCW = current;
	current->_nextCCW = e;
	e->_nextCCW->_predCCW = e;
      }
      else {
	while (first!=current->_predCCW && computeOrientation(*first,*current->_predCCW).isCW() ) {
	  if (computeOrientation(*current,*e,*current->_predCCW).isCW()) {
	    break;
	  }
	  current=current->_predCCW;
	  assert(current!=n->edges());
	}
	e->_predCCW = current->_predCCW;
	e->_nextCCW = current;
	current->_predCCW = e;
	e->_predCCW->_nextCCW = e;
      }

    }

    Edge* Graph::makeEdge (Node* start, Node* end, VertexOrder edgeDir, Direction travelDir, Data data)
    {
	
      Edge* e = start->edges();
      if (start->edges()!=0) {
	do {
	  assert(e->_start==start);
	  if (e->_end==end) {
	    e->_direction += travelDir;
	    if (e->data()!=data) {
#ifdef EDGE_DATA_WARNING
	      LogEntry("tikal.Graph").warn() << "Edge::Data conflict : existing " << e->data() << ", new " << data << doLog;
#endif
	      e->setData(min(e->data(),data));
#ifdef EDGE_DATA_WARNING
	      LogEntry("tikal.Graph").warn()  << "  Resolution : keeping smaller data" << e->data() << doLog;
#endif
	    }
	    return e;
	  }
	  e = e->_nextCCW;
	} while (e!=start->edges());
      }

      e = new Edge(start,end,edgeDir,travelDir,data);
      linkEdgeToStartNode(e);
      return  e;
    }
      
    void Graph::checkDanglingInvariant()
    {
      Node*n = _danglingNode;
      while(n!=0) {
	assert(n->_dangling);
	n = n->_nextDangling;
      }
    }

    void Graph::markDangling(Node* n)
    {
      if (n->edges()==0 || n->edges()->_nextCCW==n->edges()) {
	if (!(n->_dangling)) {
	  n->_nextDangling = _danglingNode;
	  if (_danglingNode!=0) {
	    _danglingNode->_predDangling = n;
	  }
	  assert(n->_predDangling==0);
	  _danglingNode = n;
	  n->_dangling=true;
	}
      }
      else if (n->_dangling) {
	if (n==_danglingNode) {
	  _danglingNode = n->_nextDangling;
	}
	else {
	  n->_predDangling->_nextDangling = n->_nextDangling;
	}

	if (n->_nextDangling!=0) {
	  n->_nextDangling->_predDangling = n->_predDangling;
	}

	n->_nextDangling=n->_predDangling = 0;
	n->_dangling=false;
      }
      checkDanglingInvariant();
    }

    void Graph::removeDanglingSegments()
    {
      while (_danglingNode!=0) {
	Node* n = _danglingNode;
	assert(n->_dangling);

	_danglingNode = n->_nextDangling;
	if (_danglingNode!=0) {
	  _danglingNode->_predDangling = 0;
	}
	n->_nextDangling = 0;
	assert(n->_predDangling==0);
	  
	Edge* e = n->edges();
	if (e!=0) {
	  assert (e->_nextCCW==e->_predCCW);
	  Node* end = e->_end;

	  // unlink the undirected edge 
	  unlinkEdgeFromStartNode(e);
	  unlinkEdgeFromStartNode(e->_reverse);

	  markDangling(end);
	    
	  assert (e->start().edges()==0);
	  delete e->_reverse;
	  delete e;
	}
	_nodes.erase(n->_point);
	delete n;
      }
    }

    void Graph::addSegment (const Point2D& start, const Point2D& end, const VertexOrder& vo, Data data) throws()
    {
      if (start.equals(end)) { 
	return;
      }

      Node* nStart = makeNode(start);
      Node* nEnd   = makeNode(end);

      assert(nStart!=0);
      assert(nEnd!=0);
      assert(nStart!=nEnd);

      Edge* se = makeEdge(nStart,nEnd,vo,Direction(vo),data);
      Edge* es = makeEdge(nEnd,nStart,vo.reverse(),Direction(),data);

      se->_reverse = es; 
      es->_reverse = se;

      markDangling(nStart);
      markDangling(nEnd);
#if 0
      LogEntry("tikal.Graph").warn() << "Inserting segment : " << start << " " << end << " " << data << doLog;
#endif 
    }

    size_t Graph::traceContours (Callback& cb, ContourTracer& tracer) throws()
    {
      ContourTracer* tracers = &tracer;
      return traceContours(cb,1,&tracers);
    }

    size_t Graph::traceContours (Callback& cb, size_t nTracers, ContourTracer** tracers) throws()
    {
      size_t traced = 0;
      for (size_t k=0;k<nTracers;++k) {
	ContourTracer& tracer= *tracers[k];
	for (NodeMap::const_iterator i = _nodes.begin();i!=_nodes.end();++i) {
	  Node* n = i->second;
	  Edge* e = n->edges();
	  // loop over all edges of the current node and try to start paths for each
	  do {
	    Edge* g= e;
	    assert (g->_start==n);
	    if (!g->_marked) {
	      if (tracer.beginTrace(g)) {
		cb.beginContour(n->point());
		g->_marked = true;
		_marked.push_back(g);
		g = tracer.nextEdge(g);
		  
		while (g!=0 && g!=e) {
		  cb.emitLinearSegments(1,&g->start().point());
		  g->_marked = true;
		  _marked.push_back(g);
		  g = tracer.nextEdge(g);
		};
		  
		if (g==0) {
		  cb.discardContour();
		}
		else {
		  ++traced;
		  cb.endContour();
		}
	      }
	    }
	    e = e->_nextCCW;
	  } while(e!=n->edges());
	}
      }
	
      for (EdgeList::iterator i = _marked.begin();i!=_marked.end();++i) {
	(*i)->_marked = false;
      }
      _marked.clear();
      return traced;
    }

    void Graph::clear() throws()
    {
      for (NodeMap::iterator i=_nodes.begin();i!=_nodes.end();++i) {
	delete i->second;
      }
      _nodes.clear();
    }

    void Graph::validateGraph()
    {
      for (NodeMap::const_iterator i = _nodes.begin();i!=_nodes.end();++i) {
	Node* n = i->second;
	assert (n!=0);
	Edge* e= n->edges();
	assert (e!=0);
	Node* end = e->_end;
	assert(_nodes.count(end->point())==1);
	do {
	  end = e->_end;
	  assert(_nodes.count(end->point())==1);
	  Edge* cw = e->turnCW();
	  Edge* ccw = e->turnCCW();
	  assert(e->_nextCCW != 0);
	  assert(e->_predCCW != 0);
	  assert(cw!=0);
	  assert(ccw!=0);
	  assert(cw->_start!=0);
	  assert(cw->_end!=0);
	  assert(ccw->_start!=0);
	  assert(ccw->_end!=0);
	  assert(cw->_start->edges()!=0);
	  assert(ccw->_start->edges()!=0);

	  e = e->_nextCCW;
	} while(e!=n->edges());
      }	
    }


    void Graph::print(::std::ostream& out) const
    {
      ::timber::ios::Guard guard(out);
      out << ::std::setw(100) << ::std::setprecision(20);
      for (NodeMap::const_iterator i = _nodes.begin();i!=_nodes.end();++i) {
	out << " Node : " << i->first << '@' << i->second <<  ::std::endl;
	Edge* x = i->second->edges();
	do {
	  if (x->_end->_dangling) {
	    out <<  "  *" << x->_end->_point << '@' << x->_end << ::std::endl;
	  }
	  else {
	    out <<  "  " << x->_end->_point << '@' << x->_end << ::std::endl;
	  }
	  x = x->_nextCCW;
	} while (x!=i->second->edges());
      }
    }
  }
}



