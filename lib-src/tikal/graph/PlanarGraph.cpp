#include <tikal/graph/PlanarGraph.h>
#include <tikal/graph/Graph.h>
#include <tikal/graph/ContourTracer.h>
#include <tikal/graph/Edge.h>
#include <tikal/graph/Direction.h>
#include <tikal/VertexOrder.h>
#include <tikal/SweepLine.h>

namespace tikal {
  namespace graph {
    
    PlanarGraph::PlanarGraph () throws()
    {}
    
    PlanarGraph::PlanarGraph (SweepLine& sweepline)
    {
      insertSweepline(sweepline);
    }
    
    PlanarGraph::~PlanarGraph() throws()
    {
    }

    void PlanarGraph::insertSweepline (SweepLine& sweepline)
    {
      struct CB : public SweepLine::Callback {
	inline CB (PlanarGraph& b)
	  : _builder(b) {}

	void notify(const Point2D& start, const Point2D& end, int code, const SweepLine::SegmentData& data)  throws()
	{
	  if (data.direction()!=data.sweepDirection()) {
	    _builder.addSegment(end,start,data.direction(),data.data());
	  }
	  else {
	    _builder.addSegment(start,end,data.direction(),data.data());
	  }
	}
	
      private:
	PlanarGraph& _builder;
      };
      
      CB cb(*this);
      sweepline.sweepSegments(cb);
    }

  }
}
