#include <tikal/graph/DebugGraphCallback.h>
#include <tikal/VertexOrder.h>
#include <tikal/Point2D.h>
#include <tikal/Contour2D.h>

#include <iostream>

namespace tikal {
  namespace graph {
      
    void DebugGraphCallback::beginContour(const Point2D& pt) throws()
    {
      ::std::cerr << "begin contour , pt=";
      pt.print(::std::cerr) << ::std::endl;
      AbstractGraphCallback::beginContour(pt);
    }
      
    void DebugGraphCallback::emitLinearSegments (size_t n, const Point2D* pts) throws()
    {
      ::std::cerr << "emit linear segments [n=" << n  << ", last=";
      pts[n-1].print(::std::cerr) <<" ]" << ::std::endl;
      AbstractGraphCallback::emitLinearSegments(n,pts);
    }
      
    void DebugGraphCallback::emitCurveSegment (size_t n, const Point2D* pts) throws() 
    {
      ::std::cerr << "emit curve segment [d=" << n  << "]" << ::std::endl;
      AbstractGraphCallback::emitCurveSegment(n,pts);
    }
      
    void DebugGraphCallback::endContour () throws()
    {
      ::std::cerr << "End contour" << ::std::endl;
      AbstractGraphCallback::endContour();
    }
      
    void DebugGraphCallback::discardContour() throws()
    {
      ::std::cerr << "Discard contour" << ::std::endl;
      AbstractGraphCallback::discardContour();
    }

    void DebugGraphCallback::contourExtracted(const ::timber::Reference<Contour2D>& c) throws()
    {
      ::std::cerr << "Contour extracted [maxdeg=" << c->maxDegree() << ", n=" << c->segmentCount() << " ]" << ::std::endl;
    }

  }
}
