#include <tikal/graph/Edge.h>
#include <tikal/graph/Node.h>

#include <iostream>

namespace tikal {
  namespace graph {
    
    ::std::ostream& operator<< (::std::ostream& out, const Edge& e)
    { return out << "{ " << e.start() << " -> " << e.end() << " }"; }
    
  }
}
