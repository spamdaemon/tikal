#ifndef _TIKAL_GRAPH_EDGE_H
#define _TIKAL_GRAPH_EDGE_H


#ifndef _TIKAL_VERTEXORDER_H
#include <tikal/VertexOrder.h>
#endif

#ifndef _TIKAL_GRAPH_DIRECTION_H
#include <tikal/graph/Direction.h>
#endif

#include <iosfwd>
#include <cassert>

namespace tikal {
  namespace graph {
    class Node;
    class Graph;

    /**
     * An edge in the planar graph
     */
    class Edge {
      Edge&operator=(const Edge&);
      Edge(const Edge&);
	
      friend class Graph;
      friend class Node;

    public:
      typedef int Data;
	
    private:
      inline ~Edge() throws()
      {}
	
    private:
      inline Edge(Node* s, Node* e, VertexOrder edgeDir, Direction travelDir, Data d) throws()
	: _data(d),
	_start(s),_end(e),_linkDirection(edgeDir),_direction(travelDir),
	_reverse(0),_nextCCW(0),_predCCW(0),
	_marked(false)
	  {
	    assert(s!=0);
	    assert(e!=0);
	    assert(s!=e);
	  }

      /**
       * Get the start node
       * @return the start node
       */
    public:
      inline Node& start() const throws()
      { return *_start; }

      /**
       * Get the end node
       * @return the end node
       */
    public:
      inline Node& end() const throws()
      { return *_end; }

      /**
       * Get the data on on this edge
       * @return the data on this edge
       */
    public:
      inline Data data() const throws() 
      { return _data; }

      /**
       * Set the data for this edge.
       * @param d the new data
       */
    private:
      inline void setData (Data d) throws()
      { _data = d; }
	
      /**
       * Get the direction in which this edge is specified.
       * @return the relative ordering of the vertices
       */
    public:
      inline VertexOrder vertexOrder() const throws()
      { return _linkDirection; }
	
      /**
       * Moving along this edge, turn on the edge
       * that lies in the counter clockwise direction.
       * @return the edge that is reached by turning counter clockwise
       */
    public:
      inline Edge* reverse() 
      {
	return _reverse;
      }
      
      /**
       * Get the allowed trace direction.
       * @return the allowed trace direction
       */
    public:
      inline Direction direction() const throws()
      { return _direction; }

      
      /**
       * Moving along this edge, turn on the edge
       * that lies in the counter clockwise direction.
       * @return the edge that is reached by turning counter clockwise
       */
    public:
      inline Edge* turnCCW() 
      {
	return _reverse->_nextCCW;
      }
      
      /**
       * Moving along this edge, turn on the edge
       * that lies in the clockwise direction.
       * @return the edge that is reached by turning clockwise
       */
    public:
      inline Edge* turnCW() { return _reverse->_predCCW; }
      
      /**
       * Move along this edge, turning in the specified direction
       * @param vo the turn direction as a vertex order object
       * @return the edge that is reached by turning
       */
    public:
      inline Edge* turn (const VertexOrder& vo) 
      { 
	if (vo.isCW()) {
	  return turnCW();
	}
	else {
	  return turnCCW();
	}
      }
    
      /** The data on this edge */
    private:
      Data _data;

      /** 
       * @name Edge info about the nature of the edge
       * @{
       */
    private:
      Node*       _start;         // start vertex of the canonical segment
    private:
      Node*       _end;           // end vertex of the canonical segment
    private:
      VertexOrder _linkDirection; // the enumeration order of start and end
    private:
      Direction _direction;       // the allowed travel direction
      
      /*@}*/

      /** The reverse segment */
    private:
      Edge* _reverse;

      /** The next edge that is connected to the start node */
    private:
      Edge* _nextCCW;
    private:
      Edge* _predCCW;
	
      /** A flag */
    private:
      bool _marked;
    };

    ::std::ostream& operator<< (::std::ostream& out, const Edge& e);
  }
}



#endif
