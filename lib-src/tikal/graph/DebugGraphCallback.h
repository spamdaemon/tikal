#ifndef _TIKAL_GRAPH_DEBUGGRAPHCALLBACK_H
#define _TIKAL_GRAPH_DEBUGGRAPHCALLBACK_H

#ifndef _TIKAL_GRAPH_ABSTRACTGRAPHCALLBACK_H
#include <tikal/graph/AbstractGraphCallback.h>
#endif

namespace tikal {

  namespace graph {
      
    /** A basic implementation of a graph callback */
    class DebugGraphCallback : public AbstractGraphCallback
    {
      DebugGraphCallback(const DebugGraphCallback&);
      DebugGraphCallback&operator=(const DebugGraphCallback&);

      /**
       * Default constructor.
       */
    public:
      inline DebugGraphCallback () throws()
      {}

      /**
       * Destructor
       */
    public:
      inline ~DebugGraphCallback() throws()
      {}

    public:
      void beginContour(const Point2D& pt) throws();
	  
    public:
      void emitLinearSegments (size_t n, const Point2D* pts) throws();
	  
    public:
      void emitCurveSegment (size_t n, const Point2D* pts) throws() ;

    public:
      void endContour () throws();
	  
    public:
      void discardContour() throws();
 
      /** 
       * A contour was extracted 
       * @param c the extract contour
       */
    protected:
      void contourExtracted(const ::timber::Reference<Contour2D>& c) throws();
    };
  }
}

#endif
