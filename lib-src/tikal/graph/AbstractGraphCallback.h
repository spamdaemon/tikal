#ifndef _TIKAL_GRAPH_ABSTRACTGRAPHCALLBACK_H
#define _TIKAL_GRAPH_ABSTRACTGRAPHCALLBACK_H

#ifndef _TIKAL_GRAPH_GRAPH_H
#include <tikal/graph/Graph.h>
#endif

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#include <vector>

namespace tikal {

  /** A contour */
  class Contour2D;

  namespace graph {
      
    /** A basic implementation of a graph callback */
    class AbstractGraphCallback : public Graph::Callback
      {
	AbstractGraphCallback&operator=(const AbstractGraphCallback&);
	AbstractGraphCallback(const AbstractGraphCallback&);

	/**
	 * Default constructor.
	 */
      protected:
	AbstractGraphCallback () throws();

	/**
	 * Destructor
	 */
      protected:
	~AbstractGraphCallback() throws();

      public:
	void beginContour(const Point2D& pt) throws();
	  
      public:
	void emitLinearSegments (size_t n, const Point2D* pts) throws();
	  
      public:
	void emitCurveSegment (size_t n, const Point2D* pts) throws() ;

      public:
	void endContour () throws();
	  
      public:
	void discardContour() throws();
 
	/** 
	 * A contour was extracted 
	 * @param c the extract contour
	 */
      protected:
	virtual void contourExtracted(const ::timber::Reference<Contour2D>& c) throws() = 0;

	/** The max degree of the segments */
      private:
	size_t _maxDegree;

	/** The points */
      private:
	::std::vector<Point2D> _pts;

	/** The segments */
      private:
	::std::vector<size_t> _segments;
      };
  }
}

#endif
