#ifndef _TIKAL_GRAPH_CONTOURTRACER_H
#define _TIKAL_GRAPH_CONTOURTRACER_H

#ifndef _TIKAL_GRAPH_DIRECTION_H
#include <tikal/graph/Direction.h>
#endif

namespace tikal {
  namespace graph {
      
    class Edge;
      
    /**
     * A contour tracer. Contour tracers are stateful!
     */
    class ContourTracer {
      /** Default constructor */
    protected:
      inline ContourTracer() throws() {}

      /** Destructor */
    protected:
      virtual  ~ContourTracer() throws() {}

      /** 
       * Determine in which direction a contour starting at the specified edge
       * should be traced out.
       * @param e an edge
       * @return true if this edge can start a contour
       */
    public:
      virtual bool beginTrace(Edge* e) throws() = 0;
	
      /**
       * Get the next edge for this trace. If this method
       * returns 0, no more calls to nextEdge() will succeed.
       * @param e an edge
       * @return an edge or 0 if no valid turn can be made
       */
    public:
      virtual Edge* nextEdge(Edge* e) const throws() = 0;
    };
      
  }
}



#endif
