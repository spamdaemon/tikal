#ifndef _TIKAL_GRAPH_DIRECTION_H
#define _TIKAL_GRAPH_DIRECTION_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#ifndef _TIKAL_VERTEXORDER_H
#include <tikal/VertexOrder.h>
#endif

namespace tikal {
  namespace graph {
    
    class Direction {
    public:
      enum Type {
	NONE= 0,
	CW  = 1,
	CCW = 2,
	BOTH= CW|CCW
      };
      
      /**
       * Create a default direction NONE.
       */
    public:
      inline Direction ()
	: _value(NONE) 
      {}
	
      /**
       * Create a direction of the specified type.
       * @param type the direction  type
       */
    public:
      inline Direction (Type type)
	: _value(type) 
      {}
	
      /**
       * Create a direction corresponding to the specified vertex order.
       * @param dir a vertex order
       */
    public:
      inline Direction (const VertexOrder& dir)
	: _value(dir.isCW() ? CW : CCW)
	{}


      /**
       * The no direction value
       * @return no direction
       */
    public:
      inline static Direction none() throws()
      { return Direction(NONE); }

      /**
       * The CW and CCW direction
       * @return CW and CCW direction
       */
    public:
      inline static Direction both() throws()
      { return Direction(BOTH); }

      /**
       * The CCW direction
       * @return CCW direction
       */
    public:
      inline static Direction ccw() throws()
      { return Direction(CCW); }

      /**
       * The CW direction
       * @return CW direction
       */
    public:
      inline static Direction cw() throws()
      { return Direction(CW); }


      /**
       * Test if the direction is neither CW nor CCW or CCW&CW
       * @return true if the direction is CW nor CCW or CCW&CW
       */
    public:
      inline bool isNone() const throws()
      { return _value==0; }

      /**
       * Test if the direction is either both CW and CCW
       * @return true if the direction is CW and CCW
       */
    public:
      inline bool isBoth() const throws()
      { return _value==BOTH; }

      /**
       * Test if the direction is CW
       * @return true if the direction is CW
       */
    public:
      inline bool isCW() const throws()
      { return _value==CW; }

      /**
       * Test if the direction is CCW
       * @return true if the direction is CCW
       */
    public:
      inline bool isCCW() const throws()
      { return _value==CCW; }

      /**
       * Determine if this direction is directed.
       * @return true if the direction is either CW or CCW (not both)
       */
    public:
      inline bool isDirected() const throws()
      { return isCW() || isCCW(); }

      /**
       * Get the direction as a vertex order object
       * @pre REQUIRE_TRUE(isDirected()).
       * @return a vertex order 
       */
    public:
      inline VertexOrder direction() const throws()
      { return isCW() ?  VertexOrder::cw() : VertexOrder::ccw(); }
	
    public:
      inline Direction& operator+= (const Direction& d)
      { _value |= d._value; return *this; }
	
    public:
      inline bool checkDirection (const Direction& d) const throws()
      { return (_value & d._value)!=0; }

    private:
      ::canopy::Int16 _value;
    };
  }
}

#endif
