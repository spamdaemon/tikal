#include <tikal/graph/DirectedContourTracer.h>
#include <tikal/graph/Edge.h>
#include <iostream>

namespace tikal {
  namespace graph {
    
    DirectedContourTracer::DirectedContourTracer(const VertexOrder& traceDir, const VertexOrder& turnDir) throws()
    : _traceDir(traceDir),_allowedEdgeDirection(Direction::both()),_turnDir(turnDir)
    {}

    DirectedContourTracer::DirectedContourTracer(const VertexOrder& traceDir, const VertexOrder& turnDir, Direction edgeDir) throws()
    : _traceDir(traceDir),_allowedEdgeDirection(edgeDir),_turnDir(turnDir)
    {}

    DirectedContourTracer::~DirectedContourTracer() throws()
    {}

    bool DirectedContourTracer::beginTrace(Edge* e) throws()
    {
      return e->direction().checkDirection(_traceDir);
    }
      
    Edge* DirectedContourTracer::nextEdge(Edge *e)  const throws()
    {
      assert(e!=0);
      Edge* n = e->turn(_turnDir);
      if (!n->direction().checkDirection(_allowedEdgeDirection)) {
#if 0
	::std::cerr << "Cannot turn on edge " << *e << " onto " << *n  << ::std::endl;
#endif
	n = 0;
      }
      else {
#if 0 
	::std::cerr << "Turning turn on edge " << *n << " onto " << *n  << ::std::endl;
#endif
      }
      return n;
    }
  }
}
