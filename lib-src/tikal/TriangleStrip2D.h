#ifndef _TIKAL_TRIANGLESTRIP2D_H
#define _TIKAL_TRIANGLESTRIP2D_H

#ifndef _TIKAL_POLYGON2D_H
#include <tikal/Polygon2D.h>
#endif

namespace tikal {

  /** 
   * A triangle strip class. One important note: the number of vertices of the polygon may not necessarily
   * be the same as the number of vertices in the triangle strip!
   */
  class TriangleStrip2D : public Polygon2D {
    TriangleStrip2D(const TriangleStrip2D&);
    TriangleStrip2D&operator=(const TriangleStrip2D&);
    
    /**
     * Create a new polygon
     */
  protected:
    TriangleStrip2D() throws();
    
    
    /** Destroy this ementation */
  public:
    ~TriangleStrip2D() throws();
    
    /**
     * @name Static constructors
     * @{
     */
  
    /** 
     * Create a new polygon. The points of the polygon are encoded in the
     * array such that point (x_i,y_i) is at position pts[2*i] and pts[2*i+1],
     * respectively.
     * @param n the number of points
     * @param pts a pointer to n*2 doubles
     * @param dir -1 for CCW order, 1 for CW order, 0 if unknown
     * @pre n>2
     */
  public:
    static  ::timber::Reference<TriangleStrip2D> create (size_t n, const double* pts) throws (::std::exception);
      

    /** 
     * Create a new polygon. 
     * @param n the number of points
     * @param pts a pointer to n Point2D
     * @param dir -1 for CCW order, 1 for CW order, 0 if unknown
     * @pre n>2
     */
  public:
    static  ::timber::Reference<TriangleStrip2D> create (size_t n, const Point2D* pts) throws (::std::exception);

    /** 
     * Create a new polygon. 
     * @param pts a pointer to n Point2D
     * @param dir -1 for CCW order, 1 for CW order, 0 if unknown
     * @pre pts.size()>2
     */
  public:
    static  ::timber::Reference<TriangleStrip2D> create (const ::std::vector<Point2D>& pts) throws (::std::exception);

    /** 
     * Create a new polygon which is a triangle. The order of the vertices
     * can either be left unspecified or can be specified as cw or ccw. If left
     * unspecified, then an order is computed.
     *
     * @param p first point
     * @param q the second point
     * @param r the third point
     * @param dir -1 for CCW order, 1 for CW order, 0 if unknown
     * @return a convex polygon
     */
  public:
    static  ::timber::Reference<TriangleStrip2D> create (const Point2D& p, const Point2D& q, const Point2D& r) throws (::std::exception);
    
    /** 
     * Create a new polygon which is a triangle. The order of the vertices
     * can either be left unspecified or can be specified as cw or ccw. If left
     * unspecified, then an order is computed.
     *
     * @param x1 the x-coordinate of the first point
     * @param y1 the y-coordinate of the first point
     * @param x2 the x-coordinate of the second point
     * @param y2 the y-coordinate of the second point
     * @param x3 the x-coordinate of the third point
     * @param y3 the y-coordinate of the third point
     * @pre (x1!=x2 || y1!=y2)
     * @pre (x2!=x3 || y2!=y3)
     * @pre (x1!=x3 || y1!=y3)
     * @param dir -1 for CCW order, 1 for CW order, 0 if unknown
     * @return a convex polygon
     */
  public:
    static  ::timber::Reference<TriangleStrip2D> create (double x1,double y1, double x2, double y2, double x3, double y3) throws (::std::exception);

    /*@}*/

    /**
     * Copy the vertices of out in tri-strip order.
     * @return the vertices in  tri-strip order
     */
  public:
    virtual const ::std::vector<Point2D>& stripVertices() const throws() = 0;

    /**
     * Return this polygon.
     * @return this polygon
     */
  public:
    ::timber::Pointer<Region2D> toTriangleStrips() const throws();

 
    
  };
}

#endif
