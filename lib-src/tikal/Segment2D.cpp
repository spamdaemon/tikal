#include <tikal/Segment2D.h>
#include <iostream>

namespace tikal {

  Segment2D Segment2D::subSegment (double s, double t) const throws()
  {
    if (s==0 && t==1) {
      return *this;
    }
    else if (s==0) {
      double dx = _b.x()-_a.x();
      double dy = _b.y()-_a.y();
      return Segment2D(a(),Point2D(_a.x()+t*dx,_a.y()+t*dy));
    }
    else if (t==1) {
      double dx = _b.x()-_a.x();
      double dy = _b.y()-_a.y();
      return Segment2D(Point2D(_a.x()+s*dx,_a.y()+s*dy),b());
    }
    else {
      double dx = _b.x()-_a.x();
      double dy = _b.y()-_a.y();
      return Segment2D(Point2D(_a.x()+s*dx,_a.y()+s*dy),Point2D(_a.x()+t*dx,_a.y()+t*dy));
    }
  }
    
  int Segment2D::testIntersection (const Segment2D& s1, const Segment2D& s2, int flags) throws()
  {
    const int x1 = s2.testPoint(s1.a());
    const int x2 = s2.testPoint(s1.b());
    const int x = x1*x2;
    if( x>0) {
      return 0;
    }
    const int y1 = s1.testPoint(s2.a());
    const int y2 = s1.testPoint(s2.b());
    const int y = y1*y2;
      
    if (y>0) {
      return 0;
    }
      
    if (x*y > 0) {
      // only report the intersection if we want an interior intersection
      return flags&SEGMENT_SEGMENT;
    }

    if ((flags&(OVERLAP|ENDPOINT_SEGMENT|ENDPOINT_ENDPOINT))==0) {
      // can abort right now
      return 0;
    }
    if (x==0 && y==0) {
      if ((flags&(OVERLAP|ENDPOINT_ENDPOINT))==0) {
	return 0;
      }
	
      if (x1!=0 || x2!=0 || y1!=0 || y2!=0) {
	return flags & ENDPOINT_ENDPOINT;
      }

      assert(x1==0 || x2==0 || y1==0 || y2==0);
	
      const size_t i = (s1.a().x()!=s1.b().x()) ? 1 : 0;

      double min1 = (s1.a()[i] <= s1.b()[i]) ? s1.a()[i] : s1.b()[i];
      double max1 = (s1.a()[i] <= s1.b()[i]) ? s1.b()[i] : s1.a()[i];
      double min2 = (s2.a()[i] <= s2.b()[i]) ? s2.a()[i] : s2.b()[i];
      double max2 = (s2.a()[i] <= s2.b()[i]) ? s2.b()[i] : s2.a()[i];
	
      if (max1 < min2 || min1 > max2) {
	return 0;
      }
      if (max1==min2 || min1==max2) {
	return flags & ENDPOINT_ENDPOINT;
      }
      return flags & OVERLAP;
    }
    return flags & ENDPOINT_SEGMENT;
  }

  double Segment2D::sqrDistance(const Segment2D& ) const throws()
  {
    assert ("sqrDistance not implemented"==0);
    return 0.0;
  }

  ::std::ostream& Segment2D::print (::std::ostream& out) const
  { return out << "[ " << _a << " ->  " << _b << " ]"; }

}
