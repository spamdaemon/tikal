#include <tikal/TriangleStrip2D.h>
#include <tikal/Point2D.h>
#include <tikal/BoundingBox2D.h>

using namespace ::std;
using namespace ::timber;

namespace tikal {
  namespace {

  /**
   * Create a polygon from tristrip in specified in a vertex list.
   * @param v vertices representing a strip 
   * @param dir the polygon orientation (1=CW, -1=CCW)
   */
    static Pointer<Polygon2D> createTristripPolygon(const Point2D* v, const size_t sz)
    {
      ::std::vector< Point2D> pts(sz);

    // first, copy all the vertices at even indices
    for (size_t i=0,j=0;i<sz;i+=2) {
      pts[j++] = v[i];
    }
    // then, copy all vertices at odd index, but fill the the
    // in the reverse order
    for (size_t i=1,j=sz;i<sz;i+=2) {
      pts[--j] = v[i];
    }

    // go through the list and make sure there are no duplicates
    // this is just another loop, copy points
    for (size_t i=1,j=0;i<sz;++i) {
      if (pts[j]!=pts[i]) {
	pts[++j] = pts[i];
      }
    }

    while (pts.size()>1 && pts[0]==pts[pts.size()-1]) {
      pts.pop_back();
    }

    if (pts.size()<3) {
      return Pointer<Polygon2D>();
    }

    return Polygon2D::create(pts);
    }

    struct DefaultTriangleStrip : public TriangleStrip2D {
      DefaultTriangleStrip(const Reference<Polygon2D>& p, const vector<Point2D>& vtx) throws()
      : _polygon(p),_vertices(vtx)
      {
      }

      DefaultTriangleStrip(const Reference<Polygon2D>& p, vector<Point2D>& vtx) throws()
      : _polygon(p)
      {
	_vertices.swap(vtx);
      }

      ~DefaultTriangleStrip() throws() {}
      
      int locatePoint (double x, double y) const throws() { return _polygon->locatePoint(x,y); }
      BoundingBox2D bounds() const throws() { return _polygon->bounds(); }
      bool isConvex() const throws() {  return _polygon->isConvex(); }
      size_t vertexCount() const throws()  { return _polygon->vertexCount(); }
      Point2D vertex (size_t i) const throws() { return _polygon->vertex(i); }
      VertexOrder vertexOrder() const throws() { return _polygon->vertexOrder(); }

      const ::std::vector<Point2D>& stripVertices() const throws() { return _vertices; }

    private:
      const Reference<Polygon2D> _polygon;
      vector<Point2D> _vertices;
    };

  }
  TriangleStrip2D::TriangleStrip2D() throws()
  {}
    
  TriangleStrip2D::~TriangleStrip2D() throws()
  {}
    
   Reference<TriangleStrip2D> TriangleStrip2D::create (size_t n, const double* pts) throws (::std::exception)
   {
     vector<Point2D> vtx;
     vtx.resize(n);
     const double* end = pts+2*n;
     vector<Point2D>::iterator i=vtx.begin();
     while(pts!=end) {
       double x = *pts++;
       double y = *pts++;
       *i = Point2D(x,y);
       ++i;
     }
     Pointer<Polygon2D> P = createTristripPolygon(&vtx[0],n);
     if (!P) {
       throw ::std::runtime_error("Could not create polygon");
     }
     return new DefaultTriangleStrip(P,vtx);
   }
  
      
  Reference<TriangleStrip2D> TriangleStrip2D::create (size_t n, const Point2D* pts) throws (::std::exception)
  {
    vector<Point2D> vtx(pts,pts+n);
    Pointer<Polygon2D> P = createTristripPolygon(&vtx[0],n);
    if (!P) {
      throw ::std::runtime_error("Could not create polygon");
    }
    return new DefaultTriangleStrip(P,vtx);
  }
  
  Reference<TriangleStrip2D> TriangleStrip2D::create (const ::std::vector<Point2D>& vtx) throws (::std::exception)
  {
    Pointer<Polygon2D> P = createTristripPolygon(&vtx[0],vtx.size());
    if (!P) {
      throw ::std::runtime_error("Could not create polygon");
    }
    return new DefaultTriangleStrip(P,vtx);
  }
  
  Reference<TriangleStrip2D> TriangleStrip2D::create (const Point2D& p, const Point2D& q, const Point2D& r) throws (::std::exception)
  {
    vector<Point2D> vtx;
    vtx.resize(3);
    vtx[0] = p;
    vtx[1] = q;
    vtx[2] = r;

    Pointer<Polygon2D> P = createTristripPolygon(&vtx[0],vtx.size());
    if (!P) {
      throw ::std::runtime_error("Could not create polygon");
    }
    return new DefaultTriangleStrip(P,vtx);
    
  }
  
   Reference<TriangleStrip2D> TriangleStrip2D::create (double x1,double y1, double x2, double y2, double x3, double y3) throws (::std::exception)
   {
    vector<Point2D> vtx;
    vtx.resize(3);
    vtx[0] = Point2D(x1,y1);
    vtx[1] = Point2D(x2,y2);
    vtx[2] = Point2D(x3,y3);

    Pointer<Polygon2D> P = createTristripPolygon(&vtx[0],vtx.size());
    if (!P) {
      throw ::std::runtime_error("Could not create polygon");
    }
    return new DefaultTriangleStrip(P,vtx);
    
   }

  ::timber::Pointer<Region2D> TriangleStrip2D::toTriangleStrips() const throws()
  { return toRef<Region2D>(); }
  

}

