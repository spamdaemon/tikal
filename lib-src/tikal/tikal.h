#ifndef _TIKAL_H
#define _TIKAL_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#include <cmath>
#include <vector>
#include <cassert>

namespace tikal {
  /** A 2d point */
  class Point2D;
  
  /**
   * Compute the area of a polygon. The sign of the returned
   * area indicates the winding direction of the polygon (CCW or CW).
   * If the reported area is negative, then the vertices are ordered
   * in clockwise (CW) order, otherwise they are ordered in CCW order.
   * If 0.0 is returned, then no order can be established.
   * @param n the number of points in the polygon
   * @param pts the points of the polygon
   * @return the area of the polygon 
   */
  double computeSignedPolygonArea (size_t n, const Point2D* pts) throws();
  
  /**
   * Compute the relative location of a point to a polygon.
   * @param x the x-coordinate of the point to be tested
   * @param y the y-coordinate of the point to be tested
   * @param n the number of vertices in the polygon
   * @param v the polygon vertices
   * @return -1 if the point lies inside the polygon, 0 if it lies on the boundary, and 1 if it lies outside
   */
  int computePointPolygonOrientation (double x, double y, size_t n, const Point2D* v) throws (::std::exception);
  
  
  /**
   * Test if a point is contained within a polygon defined by
   * a set of points
   * @param x the x-coordinate of the point to be tested
   * @param y the y-coordinate of the point to be tested
   * @param n the number of vertices in the polygon
   * @param v the polygon vertices
   * @return @code computePointPolygonOrientation(x,y,n,v)<=0 @endcode
   */
  bool testPointInPolygon (double x, double y, size_t n, const Point2D* v) throws (::std::exception);

  /**
   * Compute the orientation of a point with respect to an undirected line segment. The result
   * of this operation are LEFT/UP,ON,or RIGHT/DOWN and does not depend on the order of the segment
   * vertices. 
   * @param p a point
   * @param a a segment endpoint
   * @param b a segment endpoint
   * @return -1 if p is to the left of the line segment, 0 if it is on the line segment, and 1 if it is on the right.
   */
  int testPointSegmentOrientation (const Point2D& p, const Point2D& a, const Point2D& b) throws();

  /**
   * Test if the specified polygon is convex. If n < 3, then this 
   * function returns true.
   * @param n the number of points in the polygon
   * @param poly the vertices of the polygon
   * @return true if the polygon is convex, false otherwise
   */
  bool isConvexPolygon (size_t n, const Point2D* poly) throws();

  /**
   * Compute the relative ordering of two points. Two points are in CCW
   * order if the normal points in the negative x-direction or straight
   * downward.
   * @param p1 a point
   * @param p2 a point
   * @return true if the points are in counter-clockwise order
   */
  bool isCCWOrdering (const Point2D& p1, const Point2D& p2) throws();

  /**
   * Compute the relative ordering of three (non-colinear) points. 
   * @param p1 a point
   * @param p2 a point
   * @param p3 a point
   * @return true if the points are in counter-clockwise order
   */
  bool isCCWOrdering (const Point2D& p1, const Point2D& p2, const Point2D& p3) throws();

  /**
   * Test if the points are ordered in counter-clockwise direction.
   * @param n the number of points
   * @param p the points
   * @pre REQUIRE_GREATER(n,1)
   * @pre REQUIRE_NON_ZERO(p)
   * @return true if the points are enumerated in CCW direction, false if in CW direcetion
   */
  bool isCCWOrdering (size_t n, const Point2D* p) throws();
    
  /**
   * Test if the points are ordered in counter-clockwise direction.
   * @param p the points
   * @pre REQUIRE_NON_ZERO(p)
   * @return true if the points are enumerated in CCW direction, false if in CW direcetion
   */
  bool isCCWOrdering (const ::std::vector<Point2D>& p) throws();
    
  /**
   * Compute the cosine of the angle between two sides of a triangle.
   * @param a the first side of a triangle
   * @param b the second side of a triangle
   * @param c the third side of a triangle
   * @return the angle between a and b
   */
  inline double computeCosAngle (double a, double b, double c) throws()
  {
    assert(a>0 && b>0 && c>0);
	
    return (a*a - c*c + b*b)/(2.0*b*a);
  }
      
  /**
   * Compute the angle between two sides of a triangle.
   * @param a the first side of a triangle
   * @param b the second side of a triangle
   * @param c the third side of a triangle
   * @return the angle between a and b
   */
  inline double computeAngle (double a, double b, double c) throws()
  { return ::std::acos(computeCosAngle(a,b,c)); }

  /**
   * Compute the relative ordering of three (possibly co-linear) points. This method
   * returns -1 if p1->p2->p3 are enumerated in counter-clockwise order, or 1 if
   * they are enumerated in clockwise order. If the vertices all lie in a straight line,
   * then 0 is returned. 
   * @param p1 a point
   * @param p2 a point
   * @param p3 a point
   * @return -1 for CCW, 0, for a straight line, and 1 for CW ordering
   */
  int computeOrientation (const class Point2D& p1, const class Point2D& p2, const class Point2D& p3) throws();
    
}

#endif
