#ifndef _TIKAL_REGION2DOPS_H
#define _TIKAL_REGION2DOPS_H

#ifndef _TIKAL_H
#include <tikal/tikal.h>
#endif

namespace tikal {
  class Region2D;
  class Polygon2D;

  /**
   * This class provides an interface to perform 
   * the four fundamental operations on 2D regions.
   * The four operators are: union, intersection, difference,
   * exclusive or
   */
  class Region2DOps  {
    Region2DOps(const Region2DOps&);
    Region2DOps&operator=(const Region2DOps&);

    /** The implementation class */
  protected:
    Region2DOps() throws();
      
    /** The destructor */
  public:
    virtual ~Region2DOps() throws();
    
    /**
     * Create the default region 2D ops
     * @return the default region 2D ops
     */
  public:
    static ::std::unique_ptr<Region2DOps> create() throws();
    
    /**
     * Merge two regions. If r1==r2, then this may return r1 or r2 and not perform any work.
     * @param r1 a region.
     * @param r2 a region.
     * @return a region that is the union of r1 and r2.
     * @throws ::std::exception if the operation failed
     */
  public:
    virtual ::timber::Pointer<Region2D> merge (const ::timber::Pointer<Region2D>& r1, const ::timber::Pointer<Region2D>& r2) throws (::std::exception) = 0;
	
    /**
     * Intersect two regions. If r1==r2, then this may return r1 or r2 and not perform any work.
     * @param r1 a region.
     * @param r2 a region.
     * @return a region that is the intersection of r1 with r2.
     * @throws ::std::exception if the operation failed
     */
  public:
    virtual ::timber::Pointer<Region2D> intersect (const ::timber::Pointer<Region2D>& r1, const ::timber::Pointer<Region2D>& r2) throws (::std::exception) = 0;
	
    /**
     * Compute the difference of two regions. 
     * @param r1 a region.
     * @param r2 a region.
     * @return the region of r1 that is not also contained in r2
     * @throws ::std::exception if the operation failed
     */
  public:
    virtual ::timber::Pointer<Region2D> difference (const ::timber::Reference<Region2D>& r1, const ::timber::Pointer<Region2D>& r2) throws (::std::exception) = 0;
      
	
    /**
     * Compute the exclusive OR of two regions. 
     * @param r1 a region.
     * @param r2 a region.
     * @return the regions of r1 and r2 which are not contained inside the other region
     * @throws ::std::exception if the operation failed
     */
  public:
    virtual ::timber::Pointer<Region2D> exclusiveOr (const ::timber::Pointer<Region2D>& r1, const ::timber::Pointer<Region2D>& r2) throws (::std::exception) = 0;

    /**
     * Normalize a region. This method ensures that the specified region has no self-intersections
     * and does not consist of mutually overlapping areas.
     * @note if r is non-null, then the output must also be non-null.
     * @param r a region
     * @return a region without any self intersections or overlapping areas
     * @throws ::std::exception if the operation failed
     */
  public:
    virtual ::timber::Pointer<Region2D> normalize (const ::timber::Pointer<Region2D>& r) throws (::std::exception) =0;

    /**
     * Compute an region that is requivalent to the specified region and contains no holes. If the region 
     * has no holes, then it should be returned as to avoid an data copying.
     * @note if r is non-null, then the output must also be non-null.
     * @param r a region possibly with holes (or null)
     * @return a region without holes (or null)
     * @throws ::std::exception if the operation failed
     */
  public:
    virtual ::timber::Pointer<Region2D> removeHoles (const ::timber::Pointer<Region2D>& r) throws (::std::exception) =0;


    /**
     * Create a region consisting of TriangleStrip2D areas only.
     * @note if a polygon
     * @return a region without holes (or null)
     * @throws ::std::exception if the operation failed
     */
  public:
    virtual ::timber::Pointer<Region2D> createTriStripRegion (const ::timber::Pointer<Polygon2D>& r) throws (::std::exception) =0;

    /**
     * Reduce the precision of the specified region.
     * @note if r is non-null, then the output must also be non-null.
     * @param r a region possibly with holes (or null)
     * @param prec the desired precisoin for vertices
     * @return a region without holes (or null)
     * @throws ::std::exception if the operation failed
     */
  public:
    virtual ::timber::Pointer<Region2D> reducePrecision (const ::timber::Pointer<Region2D>& r, double prec) throws (::std::exception) =0;
  };
}
#endif
