#include <tikal/Line2D.h>
#include <canopy/math/exact.h>
#include <iostream>

using namespace ::canopy::math;

namespace tikal {
  
  ::std::ostream& Line2D::print (::std::ostream& out) const
  { return out << "[ x=" << _x << ", n=" << _n << " ]"; }
  
  inline int Line2D::testPoint (const Point2D& pt) const throws()
  {
#if 0
    double dx = pt.x()-_x.x();
    double dy = pt.y()-_x.y();
    double p  = dx * _n.x() + dy*_n.y();
    return p < 0 ? -1 : (p>0 ? 1 : 0);
#else
    double tmp[2*4];
    scaleSum(_n.x(),pt.x(),-_x.x(),tmp);
    scaleSum(_n.y(),pt.y(),-_x.y(),tmp+4);
    ::canopy::Int32 s=0;
    int ok = signOfSum(8,tmp,s);
    if (!ok) {
      //FIXME: degenerate or not enough precision
      s = 0;
    }
    return s;
#endif
  }
}
