#include <tikal/Path2DBuilder.h>
#include <tikal/BoundingBox2D.h>

#include <cstdlib>
#include <cassert>
#include <iostream>

using namespace ::std;
using namespace ::timber;

namespace tikal {
  struct Path2DBuilder::WayPoint {
    inline WayPoint () {}
      
    inline WayPoint (size_t x) 
    { _point._degree=x; }
    inline WayPoint (double x, double y) 
    { _point._x[0] = x; _point._x[1] = y; }
      
    union {
      size_t _degree;
      double _x[2];
    } _point;
  };

  namespace {
    struct PolylinePath : public Path2D {
      PolylinePath (const vector<Path2DBuilder::WayPoint>& p)
	: _bounds(p[1]._point._x[0],p[1]._point._x[1])
      {
	_pts.reserve(p.size()/2);
	
	// the first point has degree 0 (move-to)
	assert (p[0]._point._degree==0);
	for (vector<Path2DBuilder::WayPoint>::const_iterator i=p.begin();i!=p.end(); /**/ ) {
	  ++i; // skip the first point as it only contains the degree
	  _pts.push_back(Point2D(i->_point._x[0],i->_point._x[1]));
	  _bounds.insert(*_pts.rbegin());
	  ++i;
	  assert (i==p.end() || i->_point._degree==1);
	}
      }
      
      PolylinePath (size_t k, const Point2D* p)
	: _pts(p,p+k),_bounds(*p)
      {
	for (const Point2D* end=p+k;p<end;++p) {
	  _bounds.insert(*p);
	}
      }

      PolylinePath (const vector<Point2D>& p)
	:  _bounds(p[0])
      {
	_pts.reserve(p.size());
	for (::std::vector<Point2D>::const_iterator i=p.begin();i!=p.end();++i) {
	  const Point2D& pt = *i;
	  _pts.push_back(pt);
	  _bounds.insert(pt);
	}
      } 

      ~PolylinePath() throws() {  }
	
      size_t segmentCount() const throws() { return _pts.size(); }

      size_t maxDegree() const throws()
      { return 1; }

      size_t segment (size_t i, Point2D* p) const throws()
      { 
	p[0] = _pts.at(i);
	return i==0 ? 0 : 1;
      }
	
      bool contains (double, double) const throws()
      {
	assert ("Not implemented"==0);
      }
	
      BoundingBox2D bounds() const throws()
      { return _bounds; }

    private:
      vector<Point2D> _pts;
      BoundingBox2D _bounds;
    };
      
    struct GeneralPath : public Path2D {
      GeneralPath (const vector<Path2DBuilder::WayPoint>& pts) throws()
      : _segmentCount(0),_maxDegree(0),_bounds(pts[1]._point._x[0],pts[1]._point._x[1])
      {
	assert (pts.size()>1);
	// count the number of segments
	for (vector<Path2DBuilder::WayPoint>::const_iterator i=pts.begin();i!=pts.end();) {
	  ++_segmentCount;
	  i += 1+ max(i->_point._degree,size_t(1));
	}
	  
	_pointIndex.reserve(_segmentCount);
	_segType.reserve(_segmentCount);
	_points.reserve(pts.size()-_segmentCount);
	  
	size_t k = 0;
	size_t i = 0;
	for (size_t s = 0;s<_segmentCount;++s) {
	  const size_t deg = pts[i]._point._degree;
	  _maxDegree =max(_maxDegree,deg);
	  _segType.push_back(deg);
	  _pointIndex.push_back(k);
	  ++i;
	  const size_t end_i = i+deg;
	  do {
	    _points.push_back(Point2D(pts[i]._point._x[0],pts[i]._point._x[1]));
	    _bounds.insert(*_points.rbegin());
	    ++k;
	    ++i;
	  } while (i<end_i);
	}
      }
	
      ~GeneralPath() throws() 
      {}
	
      size_t maxDegree() const throws()
      { return _maxDegree; }
	
      bool contains (double, double) const throws()
      { return false; }
	
      size_t segmentCount() const throws()
      { return _segmentCount; }
	
      size_t segment (size_t i, Point2D* pts) const throws()
      {
	assert(i<_segmentCount);
	size_t k = _pointIndex[i];
	pts[0] = _points[k];
	size_t deg = _segType[i];
	for (size_t j=1;j<deg;++j) {
	  pts[j] = _points[++k];
	}
	return deg;
      }
	
      BoundingBox2D bounds() const throws()
      { return _bounds; }

    private:
      vector<Point2D> _points;
      vector<size_t>  _pointIndex;
      vector<int>     _segType;
      size_t _segmentCount;
      size_t _maxDegree;
      BoundingBox2D _bounds;
    };
  }


  Path2DBuilder::Path2DBuilder(size_t cap) throws()
  {
    _points.reserve(cap);
    reset();
  }
   
  Path2DBuilder::~Path2DBuilder() throws()
  {}
    
  void Path2DBuilder::reset() throws()
  {
    _maxDegree=0;
    _pathCount=0;
    _points.clear();
    _path = Pointer<Path2D>();
    _needMoveTo = true;
  }
    

  void Path2DBuilder::addWayPoint (size_t n, const WayPoint* p) throws()
  {
    if (n==0) {
      throw ::std::invalid_argument("Need at least one path point");
    }
    _points.insert(_points.end(),p,p+n);
    _path = Pointer<Path2D>();
  }

  void Path2DBuilder::moveTo (double x, double y) throws()
  {
    _needMoveTo = false;
    const WayPoint pt[2] = {
      WayPoint(0),WayPoint(x,y)
    };
    addWayPoint(2,pt);
    _x = x;
    _y = y;
    ++_pathCount;
  }
    
  void Path2DBuilder::lineTo (double x, double y) throws (::std::exception)
  {
    if (_needMoveTo) {
      throw ::std::logic_error("lineTo requires a prior call to moveTo");
    }
    // ignore duplicates
    if (_points.rbegin()->_point._x[0] == x && _points.rbegin()->_point._x[1]==y) {
      return;
    }
    const WayPoint pt[2] = {
      WayPoint(1),WayPoint(x,y)
    };

    addWayPoint(2,pt);
    _maxDegree = max(_maxDegree,size_t(1));
  }
    
  void Path2DBuilder::close() throws (::std::exception)
  {
    if (_needMoveTo) {
      throw ::std::logic_error("close requires a prior call to moveTo");
    }
    lineTo(_x,_y);
  }

  Pointer<Path2D> Path2DBuilder::createPolylinePath (size_t n, const Point2D*pts)
  {
    if (n==0) {
      return Pointer<Path2D>();
    }
    return new PolylinePath(n,pts);
  }

  Pointer<Path2D> Path2DBuilder::createPolylinePath (const ::std::vector<Point2D>& pts)
  { return createPolylinePath(pts.size(),&pts[0]); }

  Pointer<Path2D> Path2DBuilder::path() const throws()
  {
    if (_path!=nullptr || _points.empty()) {
      return _path;
    }
    if (_pathCount==1 && _maxDegree==1) {
      _path = new PolylinePath(_points);
    }
    else {
      _path  = new GeneralPath(_points);
    }
    return _path;
  }
    
}
