#include <tikal/Intersection2D.h>
#include <tikal/Point2D.h>
#include <cmath>

using namespace ::std;

namespace tikal {
  namespace {
    static inline int sign(double x) { return x < 0 ? -1 : (x>0 ? 1 : 0); }
  }

   
  size_t Intersection2D::intersect (const Segment2D& R, const Segment2D& S, double eps) throws()
  {
    /** 0. Initialize return value */
    _num = 0;
    
    /** 1. compute the normal to segment R */
    const Vector2D vectorR (R.vector());
    const Vector2D normalR = vectorR.normal();
    const double sqrLenR = normalR.sqrLength();
    
    /** 2. compute the vector vectorS as S.b()-S.a() */
    const Vector2D vectorS(S.vector()); 
    const Vector2D normalS(vectorS.normal());
    const Vector2D vectorRS (R.a().vectorTo(S.a()));
       
    const double DOT_S = normalR.dot(vectorS);
    const double DOT_RS= normalR.dot(vectorRS);

    /** 3. if dot(normalR,vectorS)==0 --> parallel */
    if (abs(DOT_S) <= eps* sqrt(sqrLenR*vectorS.sqrLength())) {
	
      /** 3.1 if RS is NOT orthogonal to normalR, then S does not lie on the same line as R ==> no intersections */
      if (abs(DOT_RS) > eps * sqrt(sqrLenR*vectorRS.sqrLength())) {
	return 0;
      }
      /** 3.2 let r1 be the parameter such that R.a()+t*vectorR = s.a() */
      const size_t i = abs(vectorR.x()) > abs(vectorR.y()) ? 0 : 1;
      double r1 = (S.a()[i]-R.a()[i])/vectorR[i];
      /** 3.3 let r2 be the parameter such that R.a()+t*vectorR = s.b() */
      double r2 = (S.b()[i]-R.a()[i])/vectorR[i];

      /** 3.4 if r2 < t1, then S is specified in the opposite direction of R */
      if (r2 < r1) {
	/** 3.4.1 test if segments overlap at all */
	//fixme: do we need to use eps here?
	if (r1 <= .0 || r2 >= 1.0) {
	  return 0;
	}
	/** 3.4.2 remove degnerate cases */
	if (r2==0.0 && R.a()!=S.b()) {
	  r2 = -1; // force an order
	}
	if (r1==1.0 && R.b()!=S.a()) {
	  r1 = 2; // force an order
	}

	/** 3.4.3 map r2 and r1 to a single value to enumerate all combinations */
	const size_t f = (sign(r1-1.0)+1) | ((sign(r2)+1)<<2);
	switch (f) {
	case 0:   //(-1,-1)
	  _result[2] = Segment2D(R.a(),S.b());
	  _result[1] = Segment2D(R.a(),S.a());
	  _result[0] = Segment2D(S.a(),R.b());
	  _source[2] = 2;
	  _source[1] = 3;
	  _source[0] = 1;
	  return  _num = 3;
	case 1:   //(-1,0)
	  _result[1] = Segment2D(R.a(),S.b());
	  _result[0] = R;
	  _source[1] = 2;
	  _source[0] = 3;
	  return _num = 2;
	case 2: //(-1,1)
	  _result[1] = Segment2D(R.a(),S.b());
	  _result[0] = R;
	  _result[2] = Segment2D(S.a(),R.b());
	  _source[2] = _source[1] = 2;_source[0] = 3;
	  return _num = 3;
	case 4:  //(0,-1)
	  _result[0] = Segment2D(R.a(),S.a());
	  _result[1] = Segment2D(S.a(),R.b());
	  _source[0] = 3;_source[1] = 1;
	  return _num = 2;
	case 5: //(0,0)
	  _result[0] = R;
	  _source[0] = 3;
	  return _num = 1;
	case 6: //(0,1)
	  _result[0] = R;
	  _result[1] = Segment2D(S.a(),R.b());
	  _source[0] = 3;
	  _source[1] = 2;
	  return _num = 2;
	case 8://(1,-1)
	  _result[0] = Segment2D(R.a(),S.b());
	  _result[2] = Segment2D(S.b(),S.a());
	  _result[1] = Segment2D(S.a(),R.b());
	  _source[1] = _source[0] = 1;_source[2] = 3;
	  return _num = 3;
	case 9: //(1,0)
	  _result[0] = Segment2D(R.a(),S.b());
	  _result[1] = Segment2D(S.b(),R.b());
	  _source[0] = 1;_source[1] = 3;
	  return _num = 2;
	case 10:  //(1,1)
	  _result[0] = Segment2D(R.a(),S.b());
	  _result[1] = Segment2D(S.b(),R.b());
	  _result[2] = Segment2D(S.a(),R.b());
	  _source[0] = 1; _source[1] = 3; _source[2] = 2; 
	  return  _num = 3;
	default:
	  assert ("Fall through switch statement is impossible"==0);
	  return 0;
	}
      }
      /** 3.5. R and S are specified in the same direction */
      else {
	/** 3.5.1 test if segments overlap at all */
	if (r2 <= 0.0 || r1 >= 1.0) {
	  return 0;
	}

	/** 3.5.2 get rid of degenerate cases */
	if (r2==1.0 && R.b()!=S.b()) {
	  r2 = 2; // force an order
	}
	if (r1==0.0 && R.a()!=S.a()) {
	  r1 = -1; // force an order
	}
	/** 3.5.3 map r2 and r1 to a single value and enumerate all combinations */
	const int f = (sign(r2-1.0)+1) | ((sign(r1)+1)<<2);
	switch (f) {
	case 0:   //(-1,-1)
	  _result[2] = Segment2D(S.a(),R.a());
	  _result[1] = Segment2D(R.a(),S.b());
	  _result[0] = Segment2D(S.b(),R.b());
	  _source[2] = 2;_source[1]=3;_source[0]=1;
	  return  _num = 3;
	case 1:   //(-1,0)
	  _result[1] = Segment2D(S.a(),R.a());
	  _result[0] = R;
	  _source[1] = 2;_source[0] = 3;
	  return _num = 2;
	case 2: //(-1,1)
	  _result[1] = Segment2D(S.a(),R.a());
	  _result[0] = R;
	  _result[2] = Segment2D(R.b(),S.b());
	  _source[2] = _source[1] = 2;_source[0] = 3;
	  return _num = 3;
	case 4:  //(0,-1)
	  _result[1] = Segment2D(R.a(),S.b());
	  _result[0] = Segment2D(S.b(),R.b());
	  _source[1] = 3;_source[0] = 1;
	  return _num = 2;
	case 5: //(0,0)
	  _result[0] = R;
	  _source[0] = 3;
	  return _num = 1;
	case 6: //(0,1)
	  _result[0] = R;
	  _result[1] = Segment2D(R.b(),S.b());
	  _source[0] = 3;
	  _source[1] = 2;
	  return _num = 2;
	case 8://(1,-1)
	  _result[0] = Segment2D(R.a(),S.a());
	  _result[2] = S;
	  _result[1] = Segment2D(S.b(),R.b());
	  _source[1] = _source[0] = 1;_source[2] = 3;
	  return _num = 3;
	case 9: //(1,0)
	  _result[0] = Segment2D(R.a(),S.a());
	  _result[1] = Segment2D(S.a(),R.b());
	  _source[0] = 1;_source[1] = 3;
	  return _num = 2;
	case 10:  //(1,1)
	  _result[0] = Segment2D(R.a(),S.a());
	  _result[1] = Segment2D(S.a(),R.b());
	  _result[2] = Segment2D(R.b(),S.b());
	  _source[0] = 1; _source[1] = 3; _source[2] = 2; 
	    
	  return  _num = 3;
	default:
	  assert ("Fall through switch statement is impossible"==0);
	  return 0;
	}
      }
      assert (false);
    }
      
    /** 
     * 3.6 compute the intersection point of two non-parallel segments:
     *  dot(S.a() + t*vectorS-R.a(), normalR)==0
     */
    const double t = -DOT_RS/DOT_S;

    /** 3.7 test if intersection point is within segment S */
    if (t<=eps || t > 1.0+eps) {
      // intersection is not on S
      return 0;
    }

    /** 3.8 compute the intersection point */
    Point2D pt ( S.a().translate(t*vectorS.x(),t*vectorS.y()));
    const size_t i = abs(vectorR.x()) > abs(vectorR.y()) ? 0 : 1;
    const double s = (pt[i]-R.a()[i])/vectorR[i];
    if (s <= eps || s > 1.0+eps) {
      return 0;
    }
    if (s>(1.0-eps)) {
      pt=R.b();
    }
    else if (t>1.0-eps) {
      pt = S.b();
    }
    if (pt==R.a() || pt==S.a()) {
      return 0;
    }
      
    if (pt==R.b()) {
      _result[0] = R;
      _source[0] = 1;
      if (pt==S.b()) {
	_result[1] = S;
	_source[1] = 2;
	return _num=2;
      }
      _result[1] = Segment2D(S.a(),pt);
      _result[2] = Segment2D(pt,S.b());
      _source[1] = _source[2] = 2;
      return _num=3;
    }
    else if (pt==S.b()) {
      _result[0] = Segment2D(R.a(),pt);
      _result[1] = Segment2D(pt,R.b());
      _result[2] = S;
      _source[0]=_source[1]=1;
      _source[2]=2;
      return _num=3;
    }
    else {
      _result[0] = Segment2D(R.a(),pt);
      _result[1] = Segment2D(pt,R.b());
      _result[2] = Segment2D(S.a(),pt);
      _result[3] = Segment2D(pt,S.b());
      _source[0] = _source[1] = 1;
      _source[2] = _source[3] = 2;
      return _num=4;
    }

  }


}
