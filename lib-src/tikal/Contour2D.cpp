#include <tikal/Contour2D.h>
#include <tikal/Point2D.h>
#include <tikal/Path2D.h>
#include <tikal/BoundingBox2D.h>
#include <timber/ios/Guard.h>

#include <iostream>
#include <iomanip>

using namespace ::std;
using namespace ::timber;

namespace tikal {

  Contour2D::Contour2D() throws()
  {}
  
  Contour2D::~Contour2D() throws()
  {}

  Reference<Path2D> Contour2D::getContourPath() const throws()
  {
    struct xPath : public Path2D {
      xPath(const Reference<Contour2D>& c) throws()
      : _contour(c)
      {}

      ~xPath() throws() {}
      size_t maxDegree() const throws() { return _contour->maxDegree(); }
      size_t segmentCount() const throws() { return _contour->segmentCount(); }
      size_t segment (size_t i, Point2D* pts) const throws()
      { return _contour->segment(i,pts); }
      
      bool contains (double x, double y) const throws()
      { return false; }

      BoundingBox2D bounds() const throws() { return _contour->bounds(); }

    private:
      const Reference<Contour2D> _contour;
    };

    Path2D* thePath = new xPath(toRef<Contour2D>());
    return thePath;
  }

  
  Reference<Contour2D> Contour2D::outsideContour() const throws()
  { return toRef<Contour2D>(); }
  
  Reference<Contour2D> Contour2D::insideContour(size_t) const throws()
  { 
    assert (false); 
    return Pointer<Contour2D>(); 
  }
  
  size_t Contour2D::contourCount() const throws()
  { return 0; }
  
  Pointer<Region2D> Contour2D::removeHoles () const throws()
  {
    return toRef<Region2D>();
  }

  void Contour2D::print (::std::ostream& out) const
  { 
    ::timber::ios::Guard g(out);
    out << ::std::setprecision(20) << ::std::setw(30);
    out << "Contour2D {" << endl;
    Point2D* pts = new Point2D[maxDegree()+1];
    try {
      for (size_t i=0;i<segmentCount();++i) {
	size_t n = segment(i,pts);
	out << "   [" << i << "] {";
	for (size_t j=0;j<=n;++j) {
	  out << "    " << pts[j] << endl;
	}
	out << "}" << endl;
      }
      out << "}" << endl;
    }
    catch (...) {
      delete[] pts;
      throw;
    }
    delete[] pts;
  }
}
