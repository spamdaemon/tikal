#include <tikal/Region2DOps.h>
#include <tikal/BasicRegion2DOps.h>
#include <tikal/Module.h>

#include <canopy/dso/DSO.h>
#include <timber/logging.h>

using namespace ::std;
using namespace timber;
using namespace timber::logging;
using canopy::dso::DSO;

#define MAKE_STRING2(X) "" #X 
#define MAKE_STRING(X) MAKE_STRING2(X)

namespace tikal {
  namespace {
    static const char DEFAULT_MODULE_PATH[] = MAKE_STRING(TIKAL_REGION2DOPS_MODULE_PATH);

    static Module* loadModule (const char* path) 
    {
      unique_ptr<DSO> dso;
      try {
	// keep the dso open
	dso = DSO::load(path,false);
	Module* m = static_cast<Module*>(dso->symbol(TIKAL_MODULE_NAME));
	Log("tikal::Region2DOps").info(string("Loaded module ")+path);
	return m;
      }
      catch (const ::std::exception& e) {
	// if the dso was indeed loaded, then close it now
	if (dso.get()) {
	  dso->close();
	}
	Log("tikal::Region2DOps").caught(string("Could not load module ")+path,e);
      }
      return 0;
    }

    static Module* getModule()
    {
         static Module* module = loadModule(DEFAULT_MODULE_PATH);
	 return module;
    }
  }
  
  Region2DOps::Region2DOps() throws()
  {}
      
  Region2DOps::~Region2DOps() throws()
  {}
    
  unique_ptr<Region2DOps> Region2DOps::create() throws()
  {
    unique_ptr<Region2DOps> res;
    Module* module = getModule();
    if (module) {
      res = module->create();
    }
    if (res.get()==0) {
      res = BasicRegion2DOps::create();
    }
    else {
      // Log("tikal.Region2DOps").info(string("Using region2D ops : ")+typeid(*res).name());
    }
    return res;
  }
}
