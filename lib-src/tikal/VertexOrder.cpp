#include <tikal/VertexOrder.h>
#include <tikal/Point2D.h>
#include <tikal/tikal.h>
#include <iostream>

namespace tikal {
  
  VertexOrder::VertexOrder (const Point2D& p, const Point2D& q) throws()
  : _ccw(::tikal::isCCWOrdering(p,q))
  {}
  
  VertexOrder::VertexOrder (const Point2D& p, const Point2D& q, const Point2D& r) throws()
  : _ccw(::tikal::isCCWOrdering(p,q,r))
  {}
  
  VertexOrder::VertexOrder (size_t n, const Point2D* p) throws()
  : _ccw(::tikal::isCCWOrdering(n,p))
  {}
  
  VertexOrder::VertexOrder (const ::std::vector<Point2D>& pts) throws()
  : _ccw(::tikal::isCCWOrdering(pts))
  {}
}

::std::ostream& operator << (::std::ostream& out, const ::tikal::VertexOrder& x)
{
  if (x.isCCW()) {
    return out << "CCW";
  }
  else {
    return out << "CW";
  }
}
