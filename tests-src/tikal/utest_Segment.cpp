#include <tikal/Segment2D.h>
#include <tikal/Point2D.h>

#include <cmath>
#include <iostream>

using namespace std;
using namespace tikal;

static bool testPoint (const Point2D& pt, double x, double y)
{
  double EPS = 1E-6;
  return abs(pt.x()-x) < EPS && abs(pt.y()-y) < EPS;
}

void utest_1()
{
  Segment2D s(Point2D(0,0),Point2D(2,0));
  assert (s.testPoint(Point2D(1,1)) < 0);
  assert (s.testPoint(Point2D(1,0)) == 0);
  assert (s.testPoint(Point2D(1,-1)) > 0);
}
