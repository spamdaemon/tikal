#include <tikal/graph/PlanarGraph.h>
#include <tikal/graph/DirectedContourTracer.h>
#include <tikal/SweepLine.h>
#include <tikal/VertexOrder.h>
#include <tikal/Point2D.h>
#include <tikal/util.h>

#include <iostream>

using namespace std;
using namespace timber;
using namespace tikal;
using namespace tikal::graph;

class MyCallback : public Graph::Callback
{
  void beginContour (const Point2D& pt) throws()
  {
    cerr << "BEGIN CONTOUR" << endl << "  " << pt << endl;
  }

  void endContour () throws()
  {
    cerr << "CONTOUR COMPLETE " << endl;
  }

  void emitCurveSegment (size_t n, const Point2D* pts) throws()
  {}

  void emitLinearSegments (size_t n, const Point2D* pts) throws()
  {
    for (size_t i=0;i<n;++i) {
    cerr << "  " << pts[i] << endl;
    }
  }

  void discardContour() throws()
  {
    cerr << "CONTOUR CANCELLED " << endl;
  }
  
};
 
void utest_1()
{
  cerr << "TEST 1" << endl;
  PlanarGraph B;
  B.addSegment(Point2D(0,0),Point2D(0,1),VertexOrder::cw(),0);
  B.addSegment(Point2D(0,1),Point2D(1,1),VertexOrder::cw(),0);
  B.addSegment(Point2D(1,1),Point2D(1,0),VertexOrder::cw(),0);
  B.addSegment(Point2D(1,0),Point2D(0,0),VertexOrder::cw(),0);

  MyCallback  cb;
  DirectedContourTracer tracer(VertexOrder::cw(),VertexOrder::cw());
  B.traceContours(cb,tracer);
}

void utest_2()
{
  cerr << "TEST 2" << endl;
  SweepLine S;
  S.insertUnorderedSegment(Point2D(0,-1),Point2D(0,2),VertexOrder::cw(),0);
  S.insertUnorderedSegment(Point2D(-1,1),Point2D(2,1),VertexOrder::cw(),0);
  S.insertUnorderedSegment(Point2D(1,2),Point2D(1,-1),VertexOrder::cw(),0);
  S.insertUnorderedSegment(Point2D(2,0),Point2D(-1,0),VertexOrder::cw(),0);

  PlanarGraph B(S);
  MyCallback  cb;
  B.removeDanglingSegments();
  DirectedContourTracer tracer(VertexOrder::cw(),VertexOrder::cw());
  B.traceContours(cb,tracer);
}

void utest_3()
{
  cerr << "TEST 3" << endl;
  PlanarGraph B;
  B.addSegment(Point2D(0,0),Point2D(1,-1),VertexOrder::ccw(),0);
  B.addSegment(Point2D(0,0),Point2D(-1,-1),VertexOrder::ccw(),0);

  cerr << "CHECKPOINT #1";B.print(cerr);

  B.addSegment(Point2D(0,0),Point2D(1,0),VertexOrder::cw(),0);
  cerr << "CHECKPOINT #2";B.print(cerr);
  B.addSegment(Point2D(0,0),Point2D(-1,0),VertexOrder::ccw(),0);
  cerr << "CHECKPOINT #3";B.print(cerr);
  
}
