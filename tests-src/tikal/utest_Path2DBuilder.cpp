#include <tikal/Path2DBuilder.h>
#include <iostream>
#include <cassert>


using namespace std;
using namespace timber;
using namespace tikal;


static void checkInvariants (const Reference<Path2D>& p)
{
  Point2D* pts = new Point2D[1+p->maxDegree()];
  assert (p->segmentCount()>0);
  assert (p->segment(0,pts)==0);
  delete [] pts;
}

void utest_1()
{
  Pointer<Path2D> path;
  {
    Path2DBuilder B;
    
    B.moveTo(0,0);
    B.lineTo(1,0);
    B.lineTo(1,1);
    B.lineTo(0,1);
    B.close();
    path = B.path(); // should be a linear path
    path->print(cerr);
    cerr << endl;
    checkInvariants(path);

    B.moveTo(0,.5);
    B.lineTo(.5,1);
    B.lineTo(1,.5);
    B.lineTo(.5,0);
    B.close();
    
    path = B.path();
    path->print(cerr);
    cerr << endl;

    checkInvariants(path);
    assert (path->maxDegree()==1);
  }
}

void utest_2()
{
  Pointer<Path2D> first;
  {
    Path2DBuilder B;
    B.moveTo(0,0);
    B.lineTo(0,1);
    B.lineTo(1,1);
    B.lineTo(1,0);
    first = B.path();
  }

  Pointer<Path2D> second;
  {
    Path2DBuilder B;
    B.moveTo(0,0);
    B.lineTo(0,-1);
    B.lineTo(1,-1);
    B.lineTo(1,0);
    second = B.path();
  }
  
  Pointer<Path2D> combined = Path2D::concatenate(first,second);

  cerr << "MAX DEG " << combined->maxDegree() << endl;
  assert (combined->maxDegree()==1);
  Point2D pts[1];
  size_t i = 0;
  size_t n=combined->segment(i,pts);
  assert(n==0 && pts[0]==Point2D(0,0));
  n=combined->segment(++i,pts);
  assert(n==1);
  assert(pts[0]==Point2D(0,1));
  n=combined->segment(++i,pts);
  assert(n==1);
  assert(pts[0]==Point2D(1,1));
  n=combined->segment(++i,pts);
  assert(n==1);
  assert(pts[0]==Point2D(1,0));
  n=combined->segment(++i,pts);
  assert(n==0 && pts[0]==Point2D(0,0));
  n=combined->segment(++i,pts);
  assert(n==1);
  assert(pts[0]==Point2D(0,-1));
  n=combined->segment(++i,pts);
  assert(n==1);
  assert(pts[0]==Point2D(1,-1));
  n=combined->segment(++i,pts);
  assert(n==1);
  assert(pts[0]==Point2D(1,0));
}
