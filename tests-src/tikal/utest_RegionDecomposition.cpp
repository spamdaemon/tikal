#include <tikal/RegionDecomposition.h>
#include <tikal/Area2D.h>
#include <tikal/Path2D.h>
#include <tikal/Path2DBuilder.h>
#include <tikal/VertexOrder.h>
#include <tikal/Region2D.h>
#include <iostream>
#include <cassert>

using namespace ::std;
using namespace ::timber;
using namespace ::tikal;

void utest_1()
{
  ::std::cerr << "TEST 1" << ::std::endl;
  Path2DBuilder pb;
  pb.moveTo(0,0);
  pb.lineTo(3,0);
  pb.lineTo(3,3);
  pb.lineTo(0,3);
  pb.close();
  
  Reference<Region2D> regionWithHole = Region2D::create(pb.path(),VertexOrder::CCW);
  assert(regionWithHole->contains(1.5,1.5));
  assert(regionWithHole->contains(0.5,0.5));
  assert(!regionWithHole->contains(-0.5,1));

  assert (regionWithHole->areaCount()==1);
  assert (regionWithHole->area(0)->contourCount()==0);
  RegionDecomposition rd(regionWithHole);

  assert(rd.contains(1.5,1.5));
  assert(rd.contains(0.5,0.5));
  assert(!rd.contains(-0.5,1));

  Reference<Region2D> regionWithoutHole = rd.buildRegion();
  for (size_t i=0;i<regionWithoutHole->areaCount();++i) {
    assert (regionWithoutHole->area(i)->contourCount()==0);
    ::std::cerr << "Region " << i << ::std::endl 
		<< *regionWithoutHole->area(i) << ::std::endl;
  }
}

void utest_2()
{
  ::std::cerr << "TEST 2" << ::std::endl;
  Path2DBuilder pb;
  pb.moveTo(0,0);
  pb.lineTo(3,0);
  pb.lineTo(3,3);
  pb.lineTo(0,3);
  pb.close();
  pb.moveTo(1,1);
  pb.lineTo(1,2);
  pb.lineTo(2,2);
  pb.lineTo(2,1);
  pb.close();
  
  Reference<Region2D> regionWithHole = Region2D::create(pb.path(),VertexOrder::CCW);
  assert(!regionWithHole->contains(1.5,1.5));
  assert(regionWithHole->contains(0.5,0.5));
  assert(!regionWithHole->contains(-0.5,1));

  assert (regionWithHole->areaCount()==1);
  assert (regionWithHole->area(0)->contourCount()==1);
  RegionDecomposition rd(regionWithHole);

  assert(!rd.contains(1.5,1.5));
  assert(rd.contains(0.5,0.5));
  assert(!rd.contains(-0.5,1));

  Reference<Region2D> regionWithoutHole = rd.buildRegion();
  assert(regionWithoutHole->areaCount()>1);
  for (size_t i=0;i<regionWithoutHole->areaCount();++i) {
    assert (regionWithoutHole->area(i)->contourCount()==0);
    ::std::cerr << "Region " << i << ::std::endl 
		<< *regionWithoutHole->area(i) << ::std::endl;
  }
}

void utest_3()
{
  ::std::cerr << "TEST 3" << ::std::endl;
  Path2DBuilder pb;
  pb.moveTo(0,0);
  pb.lineTo(5,5);
  pb.lineTo(10,0);
  pb.lineTo(5,2);
  pb.close();

  Reference<Region2D> region = Region2D::create(pb.path(),VertexOrder::CW);
  RegionDecomposition rd(region);
  
  Reference<Region2D> res = rd.buildRegion();
  assert(res->areaCount()>0);
  for (size_t i=0;i<res->areaCount();++i) {
    assert (res->area(i)->contourCount()==0);
    ::std::cerr << "Region " << i << ::std::endl 
		<< *res->area(i) << ::std::endl;
  }
}

