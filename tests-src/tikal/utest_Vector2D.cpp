#include <tikal/Vector2D.h>
#include <cassert>

using namespace tikal;

void utest_1()
{
  {
    Vector2D a(2,2);
    Vector2D b(2,1);
    assert (a.quadrant()==1);
    assert (a.quadrant()==b.quadrant());
    assert (b.compareAngles(a));
  }

  {
    Vector2D a(-2,2);
    Vector2D b(-2,1);
    assert (a.quadrant()==2);
    assert (a.quadrant()==b.quadrant());
    assert (a.compareAngles(b));
  }

  {
    Vector2D a(-2,-2);
    Vector2D b(-2,-1);
    assert (a.quadrant()==3);
    assert (a.quadrant()==b.quadrant());
    assert (b.compareAngles(a));
  }

  {
    Vector2D a(2,-2);
    Vector2D b(2,-1);
    assert (a.quadrant()==4);
    assert (a.quadrant()==b.quadrant());
    assert (a.compareAngles(b));
  }
}

void utest_2()
{
  {
    Vector2D a(1,0);
    assert (a.quadrant()==1);
  }
  {
    Vector2D a(0,1);
    assert (a.quadrant()==1);
  }
  {
    Vector2D a(0,-1);
    assert (a.quadrant()==4);
  }
  {
    Vector2D a(-1,0);
    assert (a.quadrant()==2);
  }
  {
    Vector2D a(-1,1);
    assert (a.quadrant()==2);
  }
  {
    Vector2D a(-1,-1);
    assert (a.quadrant()==3);
  }
}

