#include <tikal/tikal.h>
#include <tikal/Point2D.h>
#include <cassert>
#include <iostream>

using namespace std;
using namespace tikal;

void utest_1()
{
  Point2D p1(0,0);
  Point2D p2(1,0);
  Point2D p3(1,1);

  assert (isCCWOrdering(p1,p2,p3));
  assert (!isCCWOrdering(p3,p2,p1));
}

void utest_2()
{
  Point2D p1(0,0);
  Point2D p2(2,1);
  Point2D p3(4,2);
  
  assert (!isCCWOrdering(p1,p2,p3));
  assert (isCCWOrdering(p3,p2,p1));
}

void utest_3()
{
  Point2D pts[] = { Point2D(0,0),Point2D(0,1),Point2D(1,1),Point2D(1,0) };
  double area = computeSignedPolygonArea(4,pts);
  assert (abs(abs(area)-1.0) < 1E-6);
  cerr << "AREA : " << area << endl;
  assert (area < 0.0);
  assert (!isCCWOrdering(4,pts));

  pts[1] = Point2D(1,0);
  pts[3] = Point2D(0,1);
  double  area1= computeSignedPolygonArea(4,pts);
  cerr << "AREA : " << area1 << endl;
  assert (area1 > 0.0);
  assert (abs(area1+area)<1E-6);
  assert (isCCWOrdering(4,pts));
}

void utest_4()
{
  double a = computeAngle(2,2,2);
  assert ( abs(a-M_PI/3.0) < 1E-5);
  a = computeAngle(2,2,sqrt(8));
  assert ( abs(a-M_PI/2.0) < 1E-5);
  a = computeAngle(2,sqrt(2),sqrt(2));
  assert ( abs(a-M_PI/4.0) < 1E-5);
}

void utest_5()
{
  Point2D pts[] = { Point2D(0,0), Point2D(10,0), Point2D(10,10), Point2D(0,10) };
  for (int i=0;i<4;++i) {
    int tmp = computePointPolygonOrientation(pts[i].x(),pts[i].y(),4,pts);
    assert(tmp==0);
  }
  
}

void utest_6()
{
  Point2D pts[] = { Point2D(-1,5), Point2D(2,0), Point2D(3,0), Point2D(4,0), Point2D(6,-2),Point2D(-1,-2) };
  int tmp = computePointPolygonOrientation(0,0,6,pts);
  assert(tmp<0);
}

void utest_7()
{
  Point2D pts[] = { Point2D(-1,5), Point2D(2,0), Point2D(3,0), Point2D(4,0), Point2D(6,5) };
  int tmp = computePointPolygonOrientation(0,0,5,pts);
  assert(tmp>0);
}

void utest_8()
{
  Point2D a(0,0);
  Point2D b(1,0);
  Point2D c(0,1);

  assert(computeOrientation(a,b,c) < 0 );
}

void utest_9()
{
  Point2D pts[] = { Point2D(1,0),Point2D(2,0),Point2D(3,0),Point2D(4,0), Point2D(5,1),
		    Point2D(6,0), Point2D(7,-1),Point2D(8,0), Point2D(9,1),Point2D(10,1),
		    Point2D(9,2),Point2D(-10,1),Point2D(-10,0),Point2D(-9,-1),Point2D(-8,2),
		    Point2D(-7,-2) };
  size_t n = sizeof(pts)/sizeof(Point2D);
  vector<Point2D> v;
  for (int i=0;i<n;++i) {
    v.push_back(pts[i]);
  }
  for( size_t i=0;i<n;++i) {
    const Point2D* p = &v[0];

    int tmp = computePointPolygonOrientation(0,0,n,p);
    assert(tmp<0);
    tmp = computePointPolygonOrientation(0,1,n,p);
    assert(tmp<0);
    tmp = computePointPolygonOrientation(9.5,1,n,p);
    assert(tmp==0);
    tmp = computePointPolygonOrientation(-10,2,n,p);
    assert(tmp>0);
    for (size_t j=0;j<n;++j) {
      tmp = computePointPolygonOrientation(pts[j].x(),pts[j].y(),n,p);
      assert(tmp==0);
    }
    v.push_back(v[0]);
    v.erase(v.begin());
  }
}
