#include <tikal/Polygon2D.h>
#include <iostream>

using namespace std;
using namespace tikal;
using namespace timber;

void utest_1()
{
  Reference<Polygon2D> triangle = Polygon2D::create(0.,0.,0.,5.,5.,0.);
  assert (triangle->contains(1,1));
  assert (!triangle->contains(-1,5));
  assert (!triangle->contains(-1,0));
  assert (!triangle->contains(6,0));
}

void utest_2()
{
  const double p1[] = { 0,0,-5,0,-5,10,0,10,0,5,10,5,10,10,15,10,15,0 };
  const double p2[] = { 0,5,10,5,10,10,0,10 };
  Reference<Polygon2D> P1=Polygon2D::create(9,p1);
  Reference<Polygon2D> P2=Polygon2D::create(4,p2);

  ::std::cerr << "P1 " << *P1;
  ::std::cerr << "P2 " << *P2;

  assert (P1->contains(2,2));
  assert (!P2->contains(2,2));
  assert (!P1->contains(7,7));
  assert (P2->contains(7,7));
  assert (P1->contains(-3,5));
  assert (!P2->contains(-3,5));
  assert (!P1->contains(-6,5));
  assert (!P2->contains(-6,5));

  assert (P1->contains(0,7));
  assert (P2->contains(0,7));
  assert (P1->contains(15,7));
  assert (P1->contains(10,7));
  assert (P2->contains(10,7));
}

void utest_3()
{
  // test self-intersecting polygon
  const double p[] = { 0,0,0,6,6,0,6,6 };
  Reference<Polygon2D> P=Polygon2D::create(4,p);

  assert (P->contains(2,3));
  assert (P->contains(4,3));
  assert (!P->contains(3,2));
  assert (!P->contains(3,4));
  assert (!P->contains(-1,6));
  assert (!P->contains(3,6));
  assert (P->contains(6,6));
  assert (P->contains(3,3));
}

void utest_4()
{
  const double p1[] = {  0, 0, 0,1,1, 1,1, 0 };
  const double p2[] = { -1, 0,-1,1,0, 1,0, 0 };
  const double p3[] = { -1,-1,-1,0,0, 0,0,-1 };
  const double p4[] = {  0,-1, 0,0,1, 0,1,-1 };
  Reference<Polygon2D> P1=Polygon2D::create(4,p1);
  Reference<Polygon2D> P2=Polygon2D::create(4,p2);
  Reference<Polygon2D> P3=Polygon2D::create(4,p3);
  Reference<Polygon2D> P4=Polygon2D::create(4,p4);

  bool a,b;
  a = P1->contains(0.,0.5);
  b = P2->contains(0.,0.5);
  assert (a || b);

  a = P2->contains(-.5,0);
  b = P3->contains(-.5,0);
  assert (a || b);
  
  a = P3->contains(-.5,-.5);
  b = P4->contains(-.5,-.5);
  assert (a || b);
  
  a = P4->contains(.5,0);
  b = P1->contains(.5,0);
  assert (a || b);
}
