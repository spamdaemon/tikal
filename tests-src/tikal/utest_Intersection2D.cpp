#include <tikal/Segment2D.h>
#include <tikal/Intersection2D.h>
#include <tikal/Point2D.h>
#include <iostream>
#include <cmath>

using namespace ::std;
using namespace ::tikal;

union DOUBLE {
  double _double;
  struct  {
    ::canopy::UInt32   _int1;
    ::canopy::UInt32   _int2;
  } _int;
};


static bool testPoints (const Point2D& p, const Point2D& q)
{
  double EPS = 1E-6;
  return p.distance(q) < EPS;
}

static bool testPoint (const Point2D& pt, double x, double y)
{
  double EPS = 1E-6;
  return pt.distance(Point2D(x,y)) < EPS;
}

static void checkPoint (const Point2D& pt, double x, double y)
{
  double EPS = 1E-6;
  assert (abs(pt.x()-x) < EPS || "X-Coordinate differs"==0);
  assert (abs(pt.y()-y) < EPS || "Y-Coordinate differs"==0);
}

static bool testSegment (const Segment2D& s, const Segment2D& t)
{
  if (testPoints(s.a(),t.a()) && testPoints(s.b(),t.b())) {
    return true;
  }
  if (testPoints(s.a(),t.b()) && testPoints(s.b(),t.a())) {
    return true;
  }
  return false;
}

static Intersection2D intersect(Point2D ax, Point2D ay, Point2D bx, Point2D by)
{
  Segment2D a(ax,ay);
  Segment2D b(bx,by);
  Intersection2D X(a,b);
  ::std::cerr << "Intersecting segments:\n" 
	      << "Segment 1 : " << a << '\n'
	      << "Segment 2 : " << b << '\n'
	      << ::std::flush;
  for (size_t i=0;i<X.segmentCount();++i) {
    ::std::cerr << "  " << X.segment(i) << "  " << X.source(i) << ::std::endl;
  }
  
  return X;
}

static Intersection2D intersect(double ax1, double ax2, double bx1, double bx2, double y=0)
{
  return intersect(Point2D(ax1,y),Point2D(ax2,y),Point2D(bx1,y),Point2D(bx2,y)); 
}


static void assertSegment (const Segment2D& s, const Intersection2D& seg)
{
  for (size_t j=0;j<seg.segmentCount();++j) {
    if (testSegment(s,seg.segment(j))) {
      return;
    }
  }
  assert ("Segment not found"==0);
}

static void checkIntersection (const Segment2D& a, const Segment2D& b, size_t iCount, Intersection2D& isect)
{
  Intersection2D i1(a,b);
  Intersection2D i2(b,a);

  ::std::cerr << "Intersecting segments:\n" 
	      << "Segment 1 : " << a << '\n'
	      << "Segment 2 : " << b << '\n'
	      << ::std::flush;
  assert (i1.segmentCount()==i2.segmentCount());
  size_t countI1[3] = { 0,0,0};
  size_t countI2[3] = { 0,0,0};

  ::std::cerr << "Result:\n";
  for (size_t i=0;i<i1.segmentCount();++i) {
    ::std::cerr << "  " << i1.segment(i) << "  <-->  " << i2.segment(i) << ::std::endl;
    ++countI1[i1.source(i)-1];
    ++countI2[i2.source(i)-1];
  }
  assert (countI1[0]==countI2[1]);
  assert (countI1[1]==countI2[0]);
  assert (countI1[2]==countI2[2]);
  
  ::std::cerr << "INTERSECTION\n";
  for (size_t i=0;i<i1.segmentCount();++i) {
    bool found = false;
    for (size_t j=0;!found && j<i2.segmentCount();++j) {
      if ((i1.source(i)==3 && i2.source(j)==3) || (i1.source(i)!=i2.source(j))) {
	found = testSegment(i1.segment(i),i2.segment(j));
      }
    }
    assert (found && "Segment not found");
    ::std::cerr << "   " << i1.segment(i) << ::std::endl;
  }
  assert (i1.segmentCount()==iCount);
  isect = i1;
}

void utest_1()
{
  Intersection2D seg;
  {
    // 1. straight intersection test
    Segment2D s1(-1,0,1,0);
    Segment2D s2(0,-1,0,1);
    checkIntersection(s1,s2,4,seg);
    assertSegment(Segment2D(-1,0,0,0),seg);
    assertSegment(Segment2D(0,0,1,0),seg);
    assertSegment(Segment2D(0,-1,0,0),seg);
    assertSegment(Segment2D(0,0,0,1),seg);
  }
  
  {
    // 2. parallel segments, not overlapping
    Segment2D s1(-1,0,1,0);
    Segment2D s2(-2,1,4,1);
    checkIntersection(s1,s2,0,seg);
  }

  {
    // 3. orthogonal segments, not intersecting
    Segment2D s1(-1,0,1,0);
    Segment2D s2(0,1,0,2);
    checkIntersection(s1,s2,0,seg);
  }

  {
    // 4. parallel segments, overlapping
    Segment2D s1(-1,0,1,0);
    Segment2D s2(0,0,2,0);
    checkIntersection(s1,s2,3,seg);
    assertSegment(Segment2D(-1,0,0,0),seg);
    assertSegment(Segment2D(0,0,1,0),seg);
    assertSegment(Segment2D(1,0,2,0),seg);
  }

  {
    // 5. parallel segments, overlapping
    Segment2D s1(0,0,2,0);
    Segment2D s2(1,0,-1,0);
    checkIntersection(s1,s2,3,seg);
    assertSegment(Segment2D(0,0,1,0),seg);
    assertSegment(Segment2D(0,0,-1,0),seg);
    assertSegment(Segment2D(1,0,2,0),seg);
  }


  {
    // 6. parallel segments, overlapping
    Segment2D s1(-1,0,1,0);
    Segment2D s2(-2,0,2,0);
    checkIntersection(s1,s2,3,seg);
    assertSegment(Segment2D(-2,0,-1,0),seg);
    assertSegment(Segment2D(-1,0,1,0),seg);
    assertSegment(Segment2D(1,0,2,0),seg);
  }

  {
    // 7. identical segments
    Segment2D s1(-1,0,1,0);
    Segment2D s2(1,0,-1,0);
    checkIntersection(s1,s2,1,seg);
    assertSegment(Segment2D(-1,0,1,0),seg);
  }

  {
    // 8. a T-intersection
    Segment2D s1(-1,0,1,0);
    Segment2D s2(0,1,0,0);
    checkIntersection(s1,s2,3,seg);
    assertSegment(Segment2D(-1,0,0,0),seg);
    assertSegment(Segment2D(0,0,1,0),seg);
    assertSegment(Segment2D(0,1,0,0),seg);
  }

  {
    // 9. an intersection between two end-segments
    Segment2D s1(-5,2,-2,2);
    Segment2D s2(-2,-5,-2,2);
    checkIntersection(s1,s2,2,seg);
    assertSegment(Segment2D(-5,2,-2,2),seg);
    assertSegment(Segment2D(-2,-5,-2,2),seg);

  }
}

void utest_2()
{
  Point2D s1[] = { Point2D(4,-1), Point2D(4,2)};
  Point2D s2[] = { Point2D(-4,1), Point2D(4,1)};
  Intersection2D i(Segment2D(s1[0],s1[1]),Segment2D(s2[0],s2[1]));
  assert (i.segmentCount()==3);
}

void utest_3()
{
  Intersection2D X;

  {
    X = intersect( Point2D(-1,1),Point2D(1,-1),Point2D(1,1),Point2D(-1,-1));
    assert(X.segmentCount()==4);
    X = intersect( Point2D(0,0),Point2D(0,-1),Point2D(1,0),Point2D(-1,0));
    assert(X.segmentCount()==0);
    X = intersect( Point2D(0,1),Point2D(0,-1),Point2D(0,0),Point2D(-1,0));
    assert(X.segmentCount()==0);
    X = intersect( Point2D(0,1),Point2D(0,0),Point2D(1,0),Point2D(-1,0));
    assert(X.segmentCount()==3);
    X = intersect( Point2D(0,1),Point2D(0,-1),Point2D(1,0),Point2D(0,0));
    assert(X.segmentCount()==3);
  }
  
  // case 0:
  {
    X = intersect(-1,1,-2,0);
    assert(X.segmentCount()==3);
    X = intersect(-1,1,0,-2);
    assert(X.segmentCount()==3);
  }
  
  // case 1:
  {
    X = intersect(-1,1,-2,1);
    assert(X.segmentCount()==2);
    X = intersect(-1,1,1,-2);
    assert(X.segmentCount()==2);
  }
  
  // case 2:
  {
    X = intersect(-1,1,-2,2);
    assert(X.segmentCount()==3);
    X = intersect(-1,1,2,-2);
    assert(X.segmentCount()==3);
  }
  
  // case 3:
  {
    X = intersect(-1,1,-1,0);
    assert(X.segmentCount()==2);
    X = intersect(-1,1,0,-1);
    assert(X.segmentCount()==2);
  }
  
  // case 4:
  {
    X = intersect(-1,1,-1,1);
    assert(X.segmentCount()==1);
    X = intersect(-1,1,1,-1);
    assert(X.segmentCount()==1);
  }
  
  // case 5:
  {
    X = intersect(-1,1,-1,2);
    assert(X.segmentCount()==2);
    X = intersect(-1,1,2,-1);
    assert(X.segmentCount()==2);
  }
  
  // case 6:
  {
    X = intersect(-2,2,-1,1);
    assert(X.segmentCount()==3);
    X = intersect(-2,2,1,-1);
    assert(X.segmentCount()==3);
  }
  
  // case 7:
  {
    X = intersect(-2,1,-1,1);
    assert(X.segmentCount()==2);
    X = intersect(-2,1,1,-1);
    assert(X.segmentCount()==2);
  }
  
  // case 8:
  {
    X = intersect(-2,1,-1,2);
    assert(X.segmentCount()==3);
    X = intersect(-2,1,2,-1);
    assert(X.segmentCount()==3);
  }
}

void utest_4()
{
  ::std::cerr << "It is possible that this test fails, especially, "
	      << "if the byte order for integers is different than that for doubles"
	      << ::std::endl;

  DOUBLE sx1,sy1,sx2,sy2;
  DOUBLE tx1,ty1,tx2,ty2;

  sx1._int._int1 = 0; sx1._int._int2 = (::canopy::UInt32)3222536192U;
  sy1._int._int1 = 0; sy1._int._int2 = (::canopy::UInt32)3221225472U;
  sx2._int._int1 = 0; sx2._int._int2 = (::canopy::UInt32)3222536192U;
  sy2._int._int1 = 0; sy2._int._int2 = (::canopy::UInt32)1073741824U;
  
  tx1._int._int1 = 0; tx1._int._int2 = (::canopy::UInt32)3222536192U;
  ty1._int._int1 = (::canopy::UInt32)4294967295U; ty1._int._int2 = (::canopy::UInt32)3221225471U;
  tx2._int._int1 = 0; tx2._int._int2 = (::canopy::UInt32)3222536192U;
  ty2._int._int1 = 1; ty2._int._int2 = (::canopy::UInt32)1073741824U;

  
  Point2D sa(sx1._double,sy1._double);
  Point2D sb(sx2._double,sy2._double);
  Point2D ta(tx1._double,ty1._double);
  Point2D tb(tx2._double,ty2._double);
  
  Segment2D s(sa,sb);
  Segment2D t(ta,tb);

  ::std::cerr << "s "  << s << ::std::endl;
  ::std::cerr << "t "  << t << ::std::endl;

  Intersection2D I;
  size_t n = I.intersect(s,t,0);
  for (size_t i=0;i<n;++i) {
    ::std::cerr << "Segment " << I.segment(i) << ::std::endl;
  }
}
