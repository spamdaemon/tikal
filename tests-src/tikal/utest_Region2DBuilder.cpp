#include <tikal/Region2DBuilder.h>
#include <tikal/Region2D.h>
#include <tikal/Polygon2D.h>
#include <tikal/Contour2D.h>
#include <tikal/Area2D.h>
#include <tikal/Path2D.h>
#include <tikal/Point2D.h>

#include <cassert>
#include <iostream>

using namespace std;
using namespace timber;
using namespace tikal;

void test1()
{
  Point2D pts [] = { 
    Point2D(0,0),
    Point2D(0,1),
    Point2D(1,1),
    Point2D(1,0),
    Point2D(0,0) 
  };
  unique_ptr<Region2DBuilder> B=Region2DBuilder::create();
  B->beginExternalContour(pts[0]);
  B->lineTo(pts[1]);
  B->lineTo(pts[2]);
  B->lineTo(pts[3]);
  B->lineTo(pts[4]);
  
  Reference<Region2D> r = B->getRegion();
  Reference<Contour2D> c = r->area(0)->outsideContour(); 
  ::std::cerr << "Contour vertex order : " << c->vertexOrder() << ::std::endl;
  assert (c->vertexOrder()==VertexOrder::CW);
  assert (c->segmentCount()==4);
}

void test2()
{
  Point2D pts [] = { 
    Point2D(0,0),
    Point2D(0,1),
    Point2D(1,1),
    Point2D(1,0),
    Point2D(0,0) 
  };
  Point2D pts2 [] = { 
    Point2D(.25,.25),
    Point2D(.25,.75),
    Point2D(.75,.75),
    Point2D(.75,.25),
    Point2D(.25,.25) 
  };

  unique_ptr<Region2DBuilder> B=Region2DBuilder::create();
  B->beginExternalContour(pts[0]);
  B->lineTo(pts[1]);
  B->lineTo(pts[2]);
  B->lineTo(pts[3]);
  B->lineTo(pts[4]);
  B->beginInternalContour(pts2[0]);
  B->lineTo(pts2[1]);
  B->lineTo(pts2[2]);
  B->lineTo(pts2[3]);
  B->lineTo(pts2[4]);

  Pointer<Region2D> r = B->getRegion();
  assert(r->areaCount()==1);
  Reference<Contour2D> c = r->area(0)->outsideContour(); 
  assert (c->vertexOrder()==VertexOrder::CW);
  assert (c->segmentCount()==4);
  assert(r->area(0)->contourCount()==1);
  c = r->area(0)->insideContour(0);
  assert (c->vertexOrder()==VertexOrder::CW);
  assert (c->segmentCount()==4);
}
