#include <canopy/time/Time.h>
#include <timber/ios/Guard.h>
#include <tikal/Segment2D.h>
#include <tikal/SweepLine.h>

#include <iomanip>
#include <iostream>
#include <cmath>
#include <cstdlib>

using namespace std;
using namespace timber;
using namespace canopy::time;
using namespace tikal;

struct Printer : SweepLine::Callback {
  Printer() : _count(0) {}
  ~Printer() throws() 
  {
    ::std::cerr << "Number of result segments : " << _count << ::std::endl; 
  }

  void notify (const Point2D& a, const Point2D& b, int, const SweepLine::SegmentData& data) throws()
  {
    ::timber::ios::Guard g(cerr);
    cerr << setprecision(20) << setw(30);
    cerr << "Segment (" << data.data() << ") " << a << " --> " << b << endl;
    ++_count;
  }

private:
  size_t _count;
};

struct Expected : public Printer {
private:
  vector<Segment2D> segments;
public:
  Expected () {}
  ~Expected() throws() { assert (segments.empty()); }

  void add (const Point2D& p1, const Point2D& p2)
  { segments.push_back(Segment2D(p1,p2)); }


  void notify (const Point2D& a, const Point2D& b, int  code,const SweepLine::SegmentData& data) throws()
  {
    Printer::notify(a,b,code,data);
    size_t i;
    for (i=0;i<segments.size();++i) {
      const Segment2D& s = segments[i];
      if (s.a()==a && s.b()==b) {
	segments.erase(segments.begin()+i);
	return;
      }
    }
    assert("NOT FOUND"==0);
  }
};


void utest_1()
{
  cerr << "Test 1" << endl;
  SweepLine B;
  B.insertSegment(Point2D(0,0),Point2D(10,10),0);
  B.insertSegment(Point2D(0,10),Point2D(10,0),1);

  Expected expected;
  expected.add(Point2D(0,0),Point2D(5,5));
  expected.add(Point2D(5,5),Point2D(10,10));
  expected.add(Point2D(0,10),Point2D(5,5));
  expected.add(Point2D(5,5),Point2D(10,0));
  B.sweepSegments(expected);

  

}

void utest_2()
{

  cerr << "Test 2" << endl;
  SweepLine B;

  B.insertSegment(Point2D(0,0),Point2D(10,10),0);
  B.insertSegment(Point2D(0,5),Point2D(11,5),1);
  B.insertSegment(Point2D(3,7),Point2D(7,3),2);
  B.insertSegment(Point2D(3,8),Point2D(10,8),3);
  B.insertSegment(Point2D(5,2),Point2D(6,2),4);
  Printer  PRINTER;
  B.sweepSegments(PRINTER);
}

void utest_3()
{
  cerr << "Test 3 (vertical segment)" << endl;
  SweepLine B;
  B.insertSegment(Point2D(0,1),Point2D(10,1),0);
  B.insertSegment(Point2D(0,5),Point2D(10,5),1);
  B.insertSegment(Point2D(0,9),Point2D(10,9),2);

  B.insertSegment(Point2D(5,0),Point2D(5,10),3);
  Printer  PRINTER;
  B.sweepSegments(PRINTER);
}

void utest_4()
{
  cerr << "Test 4 (overlapping vertical segments)" << endl;
  SweepLine B;
  B.insertSegment(Point2D(0,1),Point2D(10,1),0);
  B.insertSegment(Point2D(0,5),Point2D(10,5),1);
  B.insertSegment(Point2D(0,9),Point2D(10,9),2);

  B.insertSegment(Point2D(5,0),Point2D(5,10),3);
  B.insertSegment(Point2D(5,0),Point2D(5,7),4);
  B.insertSegment(Point2D(5,0),Point2D(5,3),5);
  Printer  PRINTER;
  B.sweepSegments(PRINTER);

}

void utest_5()
{
  cerr << "Test 5 (only overlapping vertical segments)" << endl;
  SweepLine B;

  B.insertSegment(Point2D(5,0),Point2D(5,3),0);
  B.insertSegment(Point2D(5,0),Point2D(5,7),1);
  B.insertSegment(Point2D(5,0),Point2D(5,10),2);
  Printer  PRINTER;
  B.sweepSegments(PRINTER);

}

void utest_6()
{
  cerr << "Test 6 (overlapping segments)" << endl;
  SweepLine B;
  
  B.insertSegment(Point2D(0,0),Point2D(10,10),0);
  B.insertSegment(Point2D(3,3),Point2D(6,6),1);
  Printer  PRINTER;
  B.sweepSegments(PRINTER);
}

void utest_7()
{
  cerr << "Test 7 (overlapping segments)" << endl;
  SweepLine B;

  B.insertSegment(Point2D(0,0),Point2D(10,10),0);
  B.insertSegment(Point2D(0,0),Point2D(10,0),1);
  B.insertSegment(Point2D(0,0),Point2D(4,0),2);
  B.insertSegment(Point2D(3,3),Point2D(6,6),3);
  B.insertSegment(Point2D(4,0),Point2D(6,0),4);
  B.insertSegment(Point2D(6,0),Point2D(10,0),6);
  Printer  PRINTER;
  B.sweepSegments(PRINTER);
}

void utest_8()
{
  cerr << "Test 8 (non intersecting segments)" << endl;
  SweepLine B;

  B.insertSegment(Point2D(0,0),Point2D(10,10),0);
  B.insertSegment(Point2D(0,10),Point2D(10,0),1);
  Printer  PRINTER;
  B.sweepSegments(PRINTER);
}

void utest_9()
{
  cerr << "Test 9 (segments)" << endl;
  SweepLine B;

  B.insertSegment(Point2D(0,0),Point2D(10,10),0);
  B.insertSegment(Point2D(0,5),Point2D(11,5),1);
  B.insertSegment(Point2D(3,7),Point2D(7,3),2);
  B.insertSegment(Point2D(3,8),Point2D(10,8),3);
  B.insertSegment(Point2D(5,2),Point2D(6,2),4);
  Printer  PRINTER;
  B.sweepSegments(PRINTER);

}

void utest_10()
{
  cerr << "Test 10 (segments)" << endl;
  SweepLine B;


  B.insertSegment(Point2D(0,6),Point2D(8,8),0);
  B.insertSegment(Point2D(3,8),Point2D(8,3),1);
  B.insertSegment(Point2D(3,1),Point2D(3,2),2);
  B.insertSegment(Point2D(5,0),Point2D(6,5),4);
  B.insertSegment(Point2D(6,9),Point2D(9,7),6);
  B.insertSegment(Point2D(6,7),Point2D(9,8),7);
  B.insertSegment(Point2D(6,1),Point2D(6,3),8);
  B.insertSegment(Point2D(7,6),Point2D(8,7),10);
  B.insertSegment(Point2D(8,3),Point2D(8,5),14);
  Printer  PRINTER;
  B.sweepSegments(PRINTER);
}

void utest_11()
{
  cerr << "Test 11" << endl;
  SweepLine B;

  B.insertSegment(Point2D(0,4),Point2D(6,6),0);
  B.insertSegment(Point2D(0,5),Point2D(9,1),1);
  B.insertSegment(Point2D(1,5),Point2D(2,3),2);
  Printer  PRINTER;
  B.sweepSegments(PRINTER);
}

void utest_12()
{
  struct CB : SweepLine::Callback {
    CB() : _count(0) {}
    ~CB() throws() 
    {
      cerr << "Number of result segments : " << _count << endl; 
    }
    void notify (const Point2D&, const Point2D&, int , const SweepLine::SegmentData&) throws()
    {
      ++_count;
    }

  size_t _count;

  };
  
  cerr << "Test 12 (random segments)" << endl;
  srand48(0);
  int x = -1;
  for (int i=0;i<5;++i) {
    cerr << "I="<<i<< endl;
    SweepLine B;
    size_t n=0;
    for (size_t j=0;j<200;++j) {
      double x0 = ((drand48()-.5)*100);
      double y0 = ((drand48()-.5)*100); 
      double x1 = ((drand48()-.5)*100);
      double y1 = ((drand48()-.5)*100);
      if (x0!=x1 || y0!=y1) {
	if (i==x) {
	  cerr << "Segment : (" << x0<<","<<y0<<") --> ("<<x1<<","<<y1<<")"<< endl;
	}
	Segment2D seg = Segment2D(Point2D(x0,y0),Point2D(x1,y1)).canonicalSegment();
	B.insertSegment(seg.a(),seg.b(),j);
	++n;
      }
    }
    if (x<0 || x==i) {
      Time start = Time::now();
      cerr << "Compute ... " << flush;
      CB cb;
      B.sweepSegments(cb);
      cerr << "done " << endl;
      Time end = Time::now();
      cerr << "Performance : " 
	   << n/((end.time()-start.time()).count()/1000000000.0)
	   << "  "
	   << cb._count/((end.time()-start.time()).count()/1000000000.0)
	   << endl;
    }
  }
}

void utest_13()
{
  cerr << "Test 13" << endl;
  SweepLine B;

  B.insertSegment(Point2D(-5,-2),Point2D(-5,2),0);
  B.insertSegment(Point2D(-6,1),Point2D(-4,-1),1);
  Expected exp;
  exp.add(Point2D(-6,1),Point2D(-5,0));
  exp.add(Point2D(-5,-2),Point2D(-5,0));
  exp.add(Point2D(-5,0),Point2D(-5,2));
  exp.add(Point2D(-5,0),Point2D(-4,-1));

  B.sweepSegments(exp);
}

void utest_14()
{
  cerr << "Test 14" << endl;
  SweepLine B;

  B.insertSegment(Point2D(0,1),Point2D(M_PI,1),0);
  B.insertSegment(Point2D(M_PI,-2),Point2D(M_PI,2),1);
  Printer  PRINTER;
  B.sweepSegments(PRINTER);
}

void utest_15()
{
  cerr << "Test 15" << endl;
  SweepLine B;
  
  double x=  -5;

  B.insertSegment(Point2D(x,1),Point2D(x+1,1),1);
  B.insertSegment(Point2D(x,-2),Point2D(x,2),0);
  B.insertSegment(Point2D(x,-2),Point2D(x,2),2);

  Expected exp;
  exp.add(Point2D(x,-2),Point2D(x,1));
  exp.add(Point2D(x,1),Point2D(x,2));
  exp.add(Point2D(x,-2),Point2D(x,1));
  exp.add(Point2D(x,1),Point2D(x,2));
  exp.add(Point2D(x,1),Point2D(x+1,1));

  B.sweepSegments(exp);
}

void utest_16()
{
  cerr << "Test 15" << endl;
  SweepLine sweepline;
  sweepline.insertUnorderedSegment(Point2D(-3.1341458129875428007,0.61699929673280240472),Point2D(-3.2879461103354405971,0.61431181761576969524),VertexOrder::cw(),1);
  sweepline.insertUnorderedSegment(Point2D(-3.2879461103354405971,0.61431181761576969524),Point2D(-3.4415099876456793915,0.61234670835078730544),VertexOrder::cw(),1);
  sweepline.insertUnorderedSegment(Point2D(-3.4415099876456793915,0.61234670835078730544),Point2D(-3.5949147740559048714,0.61114927960598985912),VertexOrder::cw(),1);
  sweepline.insertUnorderedSegment(Point2D(-3.5949147740559048714,0.61114927960598985912),Point2D(-3.7482395848183371889,0.61074705908186299741),VertexOrder::cw(),1);
  sweepline.insertUnorderedSegment(Point2D(-3.7482395848183371889,0.61074705908186299741),Point2D(-3.9015643955807695065,0.61114927960598985912),VertexOrder::cw(),1);
  sweepline.insertUnorderedSegment(Point2D(-3.9015643955807695065,0.61114927960598985912),Point2D(-4.0549691819909945423,0.61234670835078730544),VertexOrder::cw(),1);
  sweepline.insertUnorderedSegment(Point2D(-4.0549691819909945423,0.61234670835078730544),Point2D(-4.2085330593012333367,0.61431181761576969524),VertexOrder::cw(),1);
 sweepline.insertUnorderedSegment(Point2D(-4.2085330593012333367,0.61431181761576969524),Point2D(-4.3623333566491302449,0.61699929673280240472),VertexOrder::cw(),1);
 sweepline.insertUnorderedSegment(Point2D(-4.3623333566491302449,0.61699929673280240472),Point2D(-4.5164445668454087368,0.6203469037243237727),VertexOrder::cw(),1);
 sweepline.insertUnorderedSegment(Point2D(-4.5164445668454087368,0.6203469037243237727),Point2D(-4.6709371247788666892,0.62427665424172096298),VertexOrder::cw(),1);
 sweepline.insertUnorderedSegment(Point2D(-4.6709371247788666892,0.62427665424172096298),Point2D(-4.82587598585168287,0.62869634379935634971),VertexOrder::cw(),1);
 sweepline.insertUnorderedSegment(Point2D(-4.82587598585168287,0.62869634379935634971),Point2D(-4.9813189998873017572,0.63350139701438534789),VertexOrder::cw(),1);
 sweepline.insertUnorderedSegment(Point2D(-4.9813189998873017572,0.63350139701438534789),Point2D(-5.1373151050574339394,0.63857703392300491085),VertexOrder::cw(),1);
 sweepline.insertUnorderedSegment(Point2D(-5.1373151050574339394,0.63857703392300491085),Point2D(-5.2939023993227269926,0.64380073781908808694),VertexOrder::cw(),1);
 sweepline.insertUnorderedSegment(Point2D(-5.2939023993227269926,0.64380073781908808694),Point2D(-5.4511061816236416888,0.64904500079430960113),VertexOrder::cw(),1);
 sweepline.insertUnorderedSegment(Point2D(-5.4511061816236416888,0.64904500079430960113),Point2D(-5.6089370885266571776,0.65418031175008395373),VertexOrder::cw(),1);
 sweepline.insertUnorderedSegment(Point2D(-5.6089370885266571776,0.65418031175008395373),Point2D(-5.767389480088597864,0.65907833696626727527),VertexOrder::cw(),1);
 sweepline.insertUnorderedSegment(Point2D(-5.767389480088597864,0.65907833696626727527),Point2D(-5.9264402463501042462,0.66361522579979592695),VertexOrder::cw(),1);
 sweepline.insertUnorderedSegment(Point2D(-5.9264402463501042462,0.66361522579979592695),Point2D(-6.0860482078301876285,0.66767495496082807094),VertexOrder::cw(),1);
 sweepline.insertUnorderedSegment(Point2D(-6.0860482078301876285,0.66767495496082807094),Point2D(-6.246154265101623082,0.67115260611500560106),VertexOrder::cw(),1);
 sweepline.insertUnorderedSegment(Point2D(-6.246154265101623082,0.67115260611500560106),Point2D(-6.4066824114202400864,0.67395745603905843346),VertexOrder::cw(),1);
 sweepline.insertUnorderedSegment(Point2D(-6.4066824114202400864,0.67395745603905843346),Point2D(-6.5675416592294624252,0.67601574932953201191),VertexOrder::cw(),1);
 sweepline.insertUnorderedSegment(Point2D(-6.5675416592294624252,0.67601574932953201191),Point2D(-6.7286288511805958379,0.67727302365294106412),VertexOrder::cw(),1);
 sweepline.insertUnorderedSegment(Point2D(-6.7286288511805958379,0.67727302365294106412),Point2D(-6.8898322384081289727,0.67769586877543142034),VertexOrder::cw(),1);
 sweepline.insertUnorderedSegment(Point2D(-6.8898322384081289727,-3.141592653589793116),Point2D(-6.8898322384081289727,3.141592653589793116),VertexOrder::cw(),3);
 sweepline.insertUnorderedSegment(Point2D(-6.8898322384081289727,-3.141592653589793116),Point2D(-6.8898322384081289727,3.141592653589793116),VertexOrder::ccw(),3);
 sweepline.insertUnorderedSegment(Point2D(-0.60664693122854362883,3.141592653589793116),Point2D(-0.60664693122854362883,-3.141592653589793116),VertexOrder::cw(),3);
 sweepline.insertUnorderedSegment(Point2D(-0.60664693122854362883,3.141592653589793116),Point2D(-0.60664693122854362883,-3.141592653589793116),VertexOrder::ccw(),3);
 
  Printer  PRINTER;
  sweepline.sweepSegments(PRINTER);
}

void utest_17()
{
  cerr << "Test 17" << endl;
  SweepLine sweepline;
  sweepline.insertUnorderedSegment(Point2D(-3,0),Point2D(-1.5,0),VertexOrder::cw(),1);
  sweepline.insertUnorderedSegment(Point2D(-1.5,0),Point2D(0,0),VertexOrder::cw(),1);
  sweepline.insertUnorderedSegment(Point2D(-1,-1),Point2D(0.5,1),VertexOrder::cw(),1);
  Printer  PRINTER;
  sweepline.sweepSegments(PRINTER);
  
}

void utest_18()
{
  cerr << "Test 18" << endl;
  SweepLine sweepline;
  sweepline.insertUnorderedSegment(Point2D(-3,0),Point2D(-1,0),VertexOrder::cw(),1);
  sweepline.insertUnorderedSegment(Point2D(-1,0),Point2D(1,0),VertexOrder::cw(),1);
  sweepline.insertUnorderedSegment(Point2D(-2,1),Point2D(2,-1),VertexOrder::cw(),1);
  
  Expected expected;
  expected.add(Point2D(-3,0),Point2D(-1,0));
  expected.add(Point2D(-1,0),Point2D(0,0));
  expected.add(Point2D(0,0),Point2D(1,0));
  expected.add(Point2D(-2,1),Point2D(0,0));
  expected.add(Point2D(0,0),Point2D(2,-1));

  sweepline.sweepSegments(expected);
}

