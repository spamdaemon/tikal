#include <tikal/TrapezoidDecomposition.h>
#include <tikal/Segment2D.h>
#include <iostream>
#include <cassert>

using namespace ::std;
using namespace ::timber;
using namespace ::tikal;

static bool enableMerge=true;

static void insertSegment(TrapezoidDecomposition& td, Segment2D s)
{
  TrapezoidDecomposition::NodePtr root(td.rootNode());
  TrapezoidDecomposition::createValidSegment(s);
  td.insertSegment(root,s);
}


void utest_0()
{
  ::std::cerr << "TEST 0" << ::std::endl;
   TrapezoidDecomposition td(enableMerge);
  insertSegment(td,Segment2D(Point2D(0,0),Point2D(0,5)));
  ::std::cerr << "Number of trapezoids : " << td.trapezoidCount() << ::std::endl;

  assert(td.trapezoidCount()==4);
}

void utest_0A()
{
  ::std::cerr << "TEST 0A" << ::std::endl;
   TrapezoidDecomposition td(enableMerge);
  insertSegment(td,Segment2D(Point2D(0,0),Point2D(0,10)));
  insertSegment(td,Segment2D(Point2D(2,4),Point2D(2,6)));
  ::std::cerr << "Number of trapezoids : " << td.trapezoidCount() << ::std::endl;
  assert(td.trapezoidCount()==7);
}

void utest_0B()
{
  ::std::cerr << "TEST 4" << ::std::endl;
   TrapezoidDecomposition td(enableMerge);
  insertSegment(td,Segment2D(Point2D(-5,-5),Point2D(-5,3)));
  insertSegment(td,Segment2D(Point2D(5,5),Point2D(5,-3)));
  insertSegment(td,Segment2D(Point2D(-5,3),Point2D(5,5)));

  ::std::cerr << "Number of trapezoids : " <<td.trapezoidCount() << ::std::endl;
  assert(enableMerge || td.trapezoidCount()==9);
  assert(!enableMerge || td.trapezoidCount()==8);
}

void utest_1()
{
  ::std::cerr << "TEST 1" << ::std::endl;
   TrapezoidDecomposition td(enableMerge);
  insertSegment(td,Segment2D(Point2D(0,0),Point2D(0,10)));
  ::std::cerr << "1.Number of trapezoids : " <<td.trapezoidCount() << ::std::endl;
  insertSegment(td,Segment2D(Point2D(2,4),Point2D(2,6)));
  ::std::cerr << "2.Number of trapezoids : " <<td.trapezoidCount() << ::std::endl;
  insertSegment(td,Segment2D(Point2D(1,1),Point2D(1,9)));
  ::std::cerr << "3.Number of trapezoids : " <<td.trapezoidCount() << ::std::endl;
  assert(enableMerge || td.trapezoidCount()==12);
  assert(!enableMerge || td.trapezoidCount()==10);
}


void utest_2()
{
  ::std::cerr << "TEST 2" << ::std::endl;
   TrapezoidDecomposition td(enableMerge);
  insertSegment(td,Segment2D(Point2D(2,2),Point2D(5,5)));
  insertSegment(td,Segment2D(Point2D(-2,1),Point2D(-5,4)));
  insertSegment(td,Segment2D(Point2D(0,0),Point2D(0,6)));
  ::std::cerr << "Number of trapezoids : " <<td.trapezoidCount() << ::std::endl;
  assert(enableMerge || td.trapezoidCount()==15);
  assert(!enableMerge || td.trapezoidCount()==10);
}

void utest_3()
{
  ::std::cerr << "TEST 3" << ::std::endl;
   TrapezoidDecomposition td(enableMerge);
  insertSegment(td,Segment2D(Point2D(-2,0),Point2D(-2,5)));
  insertSegment(td,Segment2D(Point2D(2,1),Point2D(2,6)));
  ::std::cerr << "Number of trapezoids : " <<td.trapezoidCount() << ::std::endl;
  assert(enableMerge || td.trapezoidCount()==8);
  assert(!enableMerge || td.trapezoidCount()==7);
}

void utest_4A()
{
  ::std::cerr << "TEST 4" << ::std::endl;
   TrapezoidDecomposition td(enableMerge);
  insertSegment(td,Segment2D(Point2D(-5,-5),Point2D(-5,3)));
  insertSegment(td,Segment2D(Point2D(5,5),Point2D(5,-3)));
  insertSegment(td,Segment2D(Point2D(-5,3),Point2D(5,5)));
  insertSegment(td,Segment2D(Point2D(5,-3),Point2D(-5,-5)));

  ::std::cerr << "Number of trapezoids : " <<td.trapezoidCount() << ::std::endl;
  assert(enableMerge || td.trapezoidCount()==10);
  assert(!enableMerge || td.trapezoidCount()==9);
}

void utest_4()
{
  ::std::cerr << "TEST 4" << ::std::endl;
   TrapezoidDecomposition td(enableMerge);
  insertSegment(td,Segment2D(Point2D(-5,-5),Point2D(-5,3)));
  insertSegment(td,Segment2D(Point2D(5,5),Point2D(5,-3)));
  insertSegment(td,Segment2D(Point2D(-5,3),Point2D(5,5)));
  insertSegment(td,Segment2D(Point2D(5,-3),Point2D(-5,-5)));

  insertSegment(td,Segment2D(Point2D(-2,-2),Point2D(-2,1)));
  insertSegment(td,Segment2D(Point2D(2,2),Point2D(2,-1)));
  insertSegment(td,Segment2D(Point2D(-2,1),Point2D(2,2)));
  insertSegment(td,Segment2D(Point2D(2,-1),Point2D(-2,-2)));

  ::std::cerr << "Number of trapezoids : " <<td.trapezoidCount() << ::std::endl;
  assert(enableMerge || td.trapezoidCount()==19);
  assert(!enableMerge || td.trapezoidCount()==17);
}

void utest_5()
{
  ::std::cerr << "TEST 5" << ::std::endl;
   TrapezoidDecomposition td(enableMerge);
  insertSegment(td,Segment2D(Point2D(-2,0),Point2D(-5,5)));
  insertSegment(td,Segment2D(Point2D(2,0),Point2D(5,5)));
  ::std::cerr << "Number of trapezoids : " <<td.trapezoidCount() << ::std::endl;
  assert(enableMerge || td.trapezoidCount()==8);
  assert(!enableMerge || td.trapezoidCount()==7);
}

void utest_6()
{
  ::std::cerr << "TEST 6" << ::std::endl;
  {
   TrapezoidDecomposition td(enableMerge);
    insertSegment(td,Segment2D(Point2D(2,0),Point2D(0,5)));
    insertSegment(td,Segment2D(Point2D(-2,0),Point2D(0,5)));
  ::std::cerr << "Number of trapezoids : " <<td.trapezoidCount() << ::std::endl;
  assert(enableMerge || td.trapezoidCount()==7);
  assert(!enableMerge || td.trapezoidCount()==6);
  }
  {
   TrapezoidDecomposition td(enableMerge);
    insertSegment(td,Segment2D(Point2D(-2,0),Point2D(0,5)));
    insertSegment(td,Segment2D(Point2D(2,0),Point2D(0,5)));
    ::std::cerr << "Number of trapezoids : " <<td.trapezoidCount() << ::std::endl;
    assert(enableMerge || td.trapezoidCount()==6);
    assert(!enableMerge || td.trapezoidCount()==6);
  }
}

void utest_7()
{
  ::std::cerr << "TEST 7" << ::std::endl;
   TrapezoidDecomposition td(enableMerge);
  insertSegment(td,Segment2D(Point2D(0,0),Point2D(5,5)));
  insertSegment(td,Segment2D(Point2D(10,0),Point2D(5,5)));
  insertSegment(td,Segment2D(Point2D(10,0),Point2D(5,2)));
  insertSegment(td,Segment2D(Point2D(0,0),Point2D(5,2)));
  ::std::cerr << "Number of trapezoids : " <<td.trapezoidCount() << ::std::endl;
  assert(enableMerge || td.trapezoidCount()==10);
  assert(!enableMerge || td.trapezoidCount()==9);
}

void utest_8()
{
  ::std::cerr << "TEST 8" << ::std::endl;
   TrapezoidDecomposition td(enableMerge);
  insertSegment(td,Segment2D(Point2D(-5,-5),Point2D(-5,3)));
  insertSegment(td,Segment2D(Point2D(5,5),Point2D(5,-3)));
  insertSegment(td,Segment2D(Point2D(-5,3),Point2D(5,5)));
  insertSegment(td,Segment2D(Point2D(5,-3),Point2D(-5,-5)));

  insertSegment(td,Segment2D(Point2D(-2,-2),Point2D(-2,3)));
  insertSegment(td,Segment2D(Point2D(2,2),Point2D(2,-3)));
  insertSegment(td,Segment2D(Point2D(-2,3),Point2D(2,2)));
  insertSegment(td,Segment2D(Point2D(2,-3),Point2D(-2,-2)));

  ::std::cerr << "Number of trapezoids : " <<td.trapezoidCount() << ::std::endl;
  assert(enableMerge || td.trapezoidCount()==23);
  assert(!enableMerge || td.trapezoidCount()==17);
}

void utest_9()
{
  ::std::cerr << "TEST 9" << ::std::endl;
   TrapezoidDecomposition td(enableMerge);
  insertSegment(td,Segment2D(Point2D(-5,-5),Point2D(-5,3)));
  insertSegment(td,Segment2D(Point2D(5,5),Point2D(5,-3)));
  ::std::cerr << "Number of trapezoids : " <<td.trapezoidCount() << ::std::endl;
  assert(enableMerge || td.trapezoidCount()==8);
  assert(!enableMerge || td.trapezoidCount()==7);
}

void utest_10()
{
  ::std::cerr << "TEST 10" << ::std::endl;
   TrapezoidDecomposition td(enableMerge);
  insertSegment(td,Segment2D(Point2D(-5,-2),Point2D(-5,2)));
  insertSegment(td,Segment2D(Point2D(5,0),Point2D(5,5)));
  insertSegment(td,Segment2D(Point2D(0,-3),Point2D(0,2)));
  insertSegment(td,Segment2D(Point2D(5,0),Point2D(0,2)));
  ::std::cerr << "Number of trapezoids : " <<td.trapezoidCount() << ::std::endl;
  assert(enableMerge || td.trapezoidCount()==16);
  assert(!enableMerge || td.trapezoidCount()==11);
}

void utest_11()
{
  ::std::cerr << "TEST 11" << ::std::endl;
   TrapezoidDecomposition td(enableMerge);
  insertSegment(td,Segment2D(Point2D(-2,2),Point2D(-2,4)));
  insertSegment(td,Segment2D(Point2D(0,0),Point2D(0,3)));
  ::std::cerr << "Number of trapezoids : " <<td.trapezoidCount() << ::std::endl;
  assert(enableMerge || td.trapezoidCount()==8);
  assert(!enableMerge || td.trapezoidCount()==7);
}

void utest_12()
{
  ::std::cerr << "TEST 12" << ::std::endl;
   TrapezoidDecomposition td(enableMerge);
  insertSegment(td,Segment2D(Point2D(0,0),Point2D(0,10)));
  insertSegment(td,Segment2D(Point2D(2,4),Point2D(2,6)));
  ::std::cerr << "Number of trapezoids : " <<td.trapezoidCount() << ::std::endl;
  assert(enableMerge || td.trapezoidCount()==7);
  assert(!enableMerge || td.trapezoidCount()==7);
}

void utest_13()
{
  ::std::cerr << "TEST 13" << ::std::endl;
   TrapezoidDecomposition td(enableMerge);
  insertSegment(td,Segment2D(Point2D(2,4),Point2D(2,6)));
  insertSegment(td,Segment2D(Point2D(1,1),Point2D(1,9)));
  ::std::cerr << "Number of trapezoids : " <<td.trapezoidCount() << ::std::endl;
  assert(enableMerge || td.trapezoidCount()==9);
  assert(!enableMerge || td.trapezoidCount()==7);
}

void utest_14()
{
  ::std::cerr << "TEST 14" << ::std::endl;
   TrapezoidDecomposition td(enableMerge);
  insertSegment(td,Segment2D(Point2D(-2,-4),Point2D(-2,-2)));
  insertSegment(td,Segment2D(Point2D(-2,2),Point2D(-2,4)));
  insertSegment(td,Segment2D(Point2D(2,-2),Point2D(2,2)));
  insertSegment(td,Segment2D(Point2D(0,-3),Point2D(0,3)));

  ::std::cerr << "Number of trapezoids : " <<td.trapezoidCount() << ::std::endl;
  assert(enableMerge || td.trapezoidCount()==18);
  assert(!enableMerge || td.trapezoidCount()==13);
}

void utest_15()
{
  ::std::cerr << "TEST 15" << ::std::endl;
  {
     TrapezoidDecomposition td(enableMerge);
    insertSegment(td,Segment2D(Point2D(0,0),Point2D(0,5)));
    insertSegment(td,Segment2D(Point2D(-1,0),Point2D(0,5)));
    insertSegment(td,Segment2D(Point2D(1,0),Point2D(0,5)));
    insertSegment(td,Segment2D(Point2D(-2,0),Point2D(0,5)));
    insertSegment(td,Segment2D(Point2D(2,0),Point2D(0,5)));
    ::std::cerr << "Number of trapezoids : " <<td.trapezoidCount() << ::std::endl;
    assert(enableMerge || td.trapezoidCount()==15);
    assert(!enableMerge || td.trapezoidCount()==12);
  }
  {
     TrapezoidDecomposition td(enableMerge);
    insertSegment(td,Segment2D(Point2D(0,-5),Point2D(0,0)));
    insertSegment(td,Segment2D(Point2D(0,-5),Point2D(-1,0)));
    insertSegment(td,Segment2D(Point2D(0,-5),Point2D(1,0)));
    insertSegment(td,Segment2D(Point2D(0,-5),Point2D(-2,0)));
    insertSegment(td,Segment2D(Point2D(0,-5),Point2D(2,0)));
    ::std::cerr << "Number of trapezoids : " <<td.trapezoidCount() << ::std::endl;
    assert(enableMerge || td.trapezoidCount()==15);
    assert(!enableMerge || td.trapezoidCount()==12);
  }
}


void utest_16()
{
  ::std::cerr << "TEST 16" << ::std::endl;
  {
     TrapezoidDecomposition td(enableMerge);
    insertSegment(td,Segment2D(Point2D(0,0),Point2D(0,5)));
    insertSegment(td,Segment2D(Point2D(-4,-5),Point2D(4,3)));
    ::std::cerr << "Number of trapezoids : " <<td.trapezoidCount() << ::std::endl;
    assert(enableMerge || td.trapezoidCount()==8);
    assert(!enableMerge || td.trapezoidCount()==7);
  }
  {
     TrapezoidDecomposition td(enableMerge);
    insertSegment(td,Segment2D(Point2D(-4,-5),Point2D(4,3)));
    insertSegment(td,Segment2D(Point2D(0,0),Point2D(0,5)));
    ::std::cerr << "Number of trapezoids : " <<td.trapezoidCount() << ::std::endl;
    assert(enableMerge || td.trapezoidCount()==8);
    assert(!enableMerge || td.trapezoidCount()==7);
  }
}

void utest_17()
{
  ::std::cerr << "TEST 17" << ::std::endl;
   TrapezoidDecomposition td(enableMerge);
  insertSegment(td,Segment2D(Point2D(0,0),Point2D(5,5)));
  insertSegment(td,Segment2D(Point2D(10,0),Point2D(5,5)));
  insertSegment(td,Segment2D(Point2D(10,0),Point2D(5,2)));
  insertSegment(td,Segment2D(Point2D(0,0),Point2D(5,2)));
  insertSegment(td,Segment2D(Point2D(5,2),Point2D(5,5)));
  ::std::cerr << "Number of trapezoids : " <<td.trapezoidCount() << ::std::endl;
  assert(enableMerge || td.trapezoidCount()==11);
  assert(!enableMerge || td.trapezoidCount()==10);
}

void utest_18()
{
  ::std::cerr << "TEST 18" << ::std::endl;
   TrapezoidDecomposition td(enableMerge);
  insertSegment(td,Segment2D(Point2D(0,0),Point2D(5,-5)));
  insertSegment(td,Segment2D(Point2D(10,0),Point2D(5,-5)));
  insertSegment(td,Segment2D(Point2D(10,0),Point2D(5,-2)));
  insertSegment(td,Segment2D(Point2D(0,0),Point2D(5,-2)));
  insertSegment(td,Segment2D(Point2D(5,-2),Point2D(5,-5)));
  ::std::cerr << "Number of trapezoids : " <<td.trapezoidCount() << ::std::endl;
  assert(enableMerge || td.trapezoidCount()==11);
  assert(!enableMerge || td.trapezoidCount()==10);
}

