#include <tikal/Region2D.h>
#include <tikal/Polygon2D.h>
#include <tikal/Contour2D.h>
#include <tikal/Area2D.h>
#include <tikal/Path2D.h>
#include <tikal/Path2DBuilder.h>
#include <tikal/BoundingBox2D.h>
#include <tikal/Point2D.h>

#include <cassert>
#include <iostream>

using namespace std;
using namespace timber;
using namespace tikal;

void utest_1()
{
  Point2D pts [] = { 
    Point2D(0,0),
    Point2D(0,1),
    Point2D(1,1),
    Point2D(1,0),
    Point2D(0,0) 
  };
  Pointer<Path2D> path = Path2DBuilder::createPolylinePath(5,pts);

  Point2D x[1];
  for (size_t i = 0; i<path->segmentCount();++i) {
    Int deg = path->segment(i,x);
    if (deg==0) {
      ::std::cerr << "move-to";
    }
    else {
      ::std::cerr << "line-to";
    }
    ::std::cerr << " " << x[0] << ::std::endl;
  }
  Pointer<Region2D> r = Region2D::create(path,VertexOrder::CW);
  Pointer<Contour2D> c = r->area(0)->outsideContour(); 
  ::std::cerr << "Contour vertex order : " << c->vertexOrder() << ::std::endl;
  assert (c->vertexOrder()==VertexOrder::CW);
}

void utest_2()
{
  Point2D pts [] = { 
    Point2D(0,0),
    Point2D(0,1),
    Point2D(1,1),
    Point2D(1,0),
    Point2D(0,0) 
  };
  Pointer<Path2D> path = Path2DBuilder::createPolylinePath(5,pts);

  Pointer<Region2D> r = Region2D::create(path,VertexOrder::CW);
  Pointer<Contour2D> c = r->area(0)->outsideContour(); 
  ::std::cerr << "Contour vertex order : " << c->vertexOrder() << ::std::endl;
  assert (c->vertexOrder()==VertexOrder::CW);
  for (int i=0;i<4;++i) {
    const Point2D& a = pts[i];
    const Point2D& b = pts[(i+1)%4];
    bool found=false;
    for (size_t j=0;j<c->segmentCount();++j) {
      Point2D x[2];
      if (c->segment(j,x)!=1) {
	assert ("Not a linear segment"==0);
      }
      if (x[0].equals(a) && x[1].equals(b)) {
	found = true;
	break;
      }
    }
    assert (found && "Segment not found");
  }
}

/*
  const Point2D p1[] = { 
  Point2D( -148.237055336734357524619554169,  39.8920355473674490554003568832 ),
  Point2D( -153.253922519427732140684383921 , 37.473633763154140297046978958 ),
  Point2D( -158.34891686692699863669986371  ,35.3811930622917074629185663071 ),
  Point2D( -163.517164782957138413621578366,  33.6362480768025662314357759897 ),
  Point2D( -168.749705604685829030131571926 , 32.2577304793799584103908273391 ),
  Point2D( -174.033982153976495510505628772,  31.2612709370208463610651961062 ),
  Point2D( -179.354503018382274603936821222,  30.6585614013369927022267802386 ),
  Point2D( -180 , 30.6341738047820690837852453114 ),
  Point2D( -180 , 90 ),
  Point2D( -4.69368951613824592072887753602,  90 ),
  Point2D( -4.69368951613824592072887753602,  87.9910850697502695538787520491 ),
  Point2D( -74.4420074551560730924393283203,  85.1056147991922671280917711556 ),
  Point2D( -89.9568056485159814883445505984,  80.8708250429850465934578096494 ),
  Point2D( -98.1013219465437771305005298927,  76.5432715529665586018381873146 ),
  Point2D( -104.245056825469021077879006043,  72.2425558669494165542346308939 ),
  Point2D( -109.600462908548237805916869547,  68.0118001563904499562340788543 ),
  Point2D( -114.591888559101803934936469886,  63.8805638469732102180387300905 ),
  Point2D( -119.409413584881562542250321712,  59.8748937314773854723171098158 ),
  Point2D( -124.153190557708185792762378696,  56.0199399659439905008184723556 ),
  Point2D( -128.882329179783255312941037118,  52.340816521056709120784944389 ),
  Point2D( -133.634460611391176598772290163,  48.8628748154044814100416260771 ),
  Point2D( -138.434478312676361611011088826,  45.6117045043851447871929849498 ),
  Point2D( -143.29872188287851031418540515,  42.6129585755849262795891263522 ),
  };
  
  const Point2D p2[] = {
  Point2D( 110.022034552605120438784069847 , 59.8748937314773783668897522148 ),
  Point2D( 105.204509526825361831470218021 , 63.8805638469732102180387300905 ),
  Point2D( 100.213083876271795702450617682 , 68.0118001563904357453793636523 ),
  Point2D( 94.857677793192536341848608572  ,72.2425558669494165542346308939 ),
  Point2D( 88.7139429142672781836154172197 , 76.5432715529665301801287569106 ),
  Point2D( 80.5694266162394399088952923194 , 80.8708250429850465934578096494 ),
  Point2D( 65.0546284228795315129900700413 , 85.1056147991922671280917711556 ),
  Point2D( -4.69368951613834628489030365017,  87.9910850697502695538787520491 ),
  Point2D( -4.69368951613834628489030365017 , 90 ),
  Point2D( 180  ,90 ),
  Point2D( 180  ,30.6341738047820690837852453114 ),
  Point2D( 175.306310483861778948266874067 , 30.4568409751431339316241064807 ),
  Point2D( 169.967123986105860922179999761 , 30.6585614013369927022267802386 ),
  Point2D( 164.646603121700081828748807311 , 31.2612709370208463610651961062 ),
  Point2D( 159.362326572409386926665320061,  32.2577304793799513049634697381 ),
  Point2D( 154.129785750680724731864756905 , 33.6362480768025662314357759897 ),
  Point2D( 148.961537834650556533233611844 , 35.3811930622917074629185663071 ),
  Point2D( 143.86654348715131845892756246 , 37.4736337631541331916196213569 ),
  Point2D( 138.849676304457915421153302304,  39.8920355473674490554003568832 ),
  Point2D( 133.911342850602068210719153285,  42.6129585755849262795891263522 ),
  Point2D( 129.047099280399919507544836961,  45.6117045043851447871929849498 ),
  Point2D( 124.247081579114734495306038298,  48.8628748154044671991869108751 ),
  Point2D( 119.494950147506827420329500455,  52.340816521056694909930229187 ),
  Point2D( 114.765811525431743689296126831,  56.0199399659439833953911147546 ),
  };
*/

namespace test3 {

  class Polygon2D0x80e5740
  {
  public:
    Polygon2D0x80e5740()
    {
      Point2D pts[] = {
	Point2D(-111,90),
	Point2D(-47,90),
	Point2D(-47,87),
	Point2D(-111,85),
      };
      _poly= Polygon2D::create(4,pts);
    }
    Reference<Polygon2D> poly() const throws() { return _poly; }
  private:
    Pointer<Polygon2D> _poly;
  };
  class Polygon2D0x80e54b8
  {
  public:
    Polygon2D0x80e54b8()
    {
      Point2D pts[] = {
	Point2D(15,85),
	Point2D(-47,87),
	Point2D(-47,90),
       	Point2D(15,90),
     };
      _poly= Polygon2D::create(4,pts);
    }
    Reference<Polygon2D> poly() const throws() { return _poly; }
  private:
    Pointer<Polygon2D> _poly;
  };

}

namespace test4 {
  class Polygon2D0x80e5740
  {
  public:
    Polygon2D0x80e5740()
    {
      Point2D pts[] = {
	Point2D(-47.699033091086405989,90),
	Point2D(-47.699033091086405989,87),
	Point2D(-111,85),
      };
      _poly= Polygon2D::create(3,pts);
    }
    Reference<Polygon2D> poly() const throws() { return _poly; }
  private:
    Pointer<Polygon2D> _poly;
  };
  class Polygon2D0x80e54b8
  {
  public:
    Polygon2D0x80e54b8()
    {
      Point2D pts[] = {
	Point2D(15.988934960430553289,85),
	Point2D(-47.699033091086455727,87),
	Point2D(-47.699033091086455727,90),
     };
      _poly= Polygon2D::create(3,pts);
    }
    Reference<Polygon2D> poly() const throws() { return _poly; }
  private:
    Pointer<Polygon2D> _poly;
  };


}

void utest_3()
{
  using namespace test3;

  Pointer<Region2D> r1 =  Polygon2D0x80e5740().poly();
  Pointer<Region2D> r2 = Polygon2D0x80e54b8().poly();
  
  assert(r1->bounds().intersects(r2->bounds()));
  Pointer<Region2D> merged = r1->merge(r2);
  ::std::cerr << "merged : " << merged << ::std::endl;
}

void utest_4()
{
  using namespace test4;

  Pointer<Region2D> r1 =  Polygon2D0x80e5740().poly();
  Pointer<Region2D> r2 = Polygon2D0x80e54b8().poly();
  
  assert(r1->bounds().intersects(r2->bounds()));
  ::std::cerr << "MERGE" << ::std::endl;
  Pointer<Region2D> merged = r1->merge(r2);
  ::std::cerr << "merged : " << merged << ::std::endl;
}

void utest_5()
{
  Point2D outside1[4] = { Point2D(-2,5),Point2D(2,5),Point2D(2,-5),Point2D(-2,-5), };
  Point2D inside1[4] = { Point2D(-1,4), Point2D(1,4), Point2D(1,-4),Point2D(-1,-4) };

  Point2D outside2[4] = { Point2D(-5,2),Point2D(5,2),Point2D(5,-2), Point2D(-5,-2) };
  Point2D inside2[4] = { Point2D(-4,1),Point2D(4,1),Point2D(4,-1),Point2D(-4,-1) };  

  Pointer<Polygon2D> out1 = Polygon2D::create(4,outside1);
  Pointer<Polygon2D> in1 = Polygon2D::create(4,inside1);
  Pointer<Polygon2D> out2 = Polygon2D::create(4,outside2);
  Pointer<Polygon2D> in2 = Polygon2D::create(4,inside2);

  Pointer<Region2D> A = out1->subtract(in1);
  Pointer<Region2D> B = out2->subtract(in2);

  Pointer<Region2D> C = A->merge(B);
  assert(C->areaCount()==1);
  assert(C->area(0)->contourCount()==5);
}

void utest_6()
{
  Point2D outside1[4] = { Point2D(-2,5),Point2D(2,5),Point2D(2,-5),Point2D(-2,-5), };
  Point2D inside1[4] = { Point2D(-1,4), Point2D(1,4), Point2D(1,-4),Point2D(-1,-4) };
  Point2D outside2[4] = { Point2D(-2,4),Point2D(2,4),Point2D(2,-6),Point2D(-2,-6), };
  Point2D inside2[4] = { Point2D(-1,3), Point2D(1,3), Point2D(1,-5),Point2D(-1,-5) };


  Pointer<Polygon2D> out1 = Polygon2D::create(4,outside1);
  Pointer<Polygon2D> in1 = Polygon2D::create(4,inside1);
  Pointer<Polygon2D> out2 = Polygon2D::create(4,outside2);
  Pointer<Polygon2D> in2 = Polygon2D::create(4,inside2);

  Pointer<Region2D> A = out1->subtract(in1);
  Pointer<Region2D> B = out2->subtract(in2);

  Pointer<Region2D> C = A->intersect(B);
  assert(C->areaCount()==2);
  assert(C->area(0)->contourCount()==0);
  assert(C->area(1)->contourCount()==0);
}

void utest_7()
{
  Point2D outside1[4] = { 
    Point2D(-3,5),Point2D(3,5),Point2D(3,-5),Point2D(-3,-5),
  };

  Point2D inside1[4] = { 
    Point2D(-2,4), Point2D(2,4), Point2D(2,-4),
    Point2D(-2,-4)
  };
  Point2D outside2[4] = { 
    Point2D(-3,5),Point2D(3,5),Point2D(3,-5),Point2D(-3,-5),
  };

  Point2D inside2[4] = { 
    Point2D(-2,4), Point2D(2,4), Point2D(2,-4),
    Point2D(-2,-4)
  };
  Pointer<Polygon2D> out1 = Polygon2D::create(4,outside1);
  Pointer<Polygon2D> in1 = Polygon2D::create(4,inside1);
  Pointer<Polygon2D> out2 = Polygon2D::create(4,outside2);
  Pointer<Polygon2D> in2 = Polygon2D::create(4,inside2);

  Pointer<Region2D> A = out1->subtract(in1);
  Pointer<Region2D> B = out2->subtract(in2);

  Pointer<Region2D> C = A->subtract(B);
  assert(!C);
}
void utest_8()
{
  Point2D outside1[5] = { 
    Point2D(-0,0),Point2D(0,5),Point2D(2.5,-1),Point2D(5,5),Point2D(5,0)
  };
  Point2D outside2[4] = { 
    Point2D(-1,-2),Point2D(-1,6),Point2D(4,6),Point2D(4,-2),
  };

  Pointer<Region2D> A=Polygon2D::create(5,outside1);
  Pointer<Region2D> B=Polygon2D::create(4,outside2);
  Pointer<Region2D> C = A->intersect(B);
  assert(C);
  ::std::cerr << "C1 = " << *C << ::std::endl;
  C = B->intersect(A);
  assert(C);
  ::std::cerr << "C2 = " << *C << ::std::endl;
}

void utest_9()
{
  Point2D outside1[4] = { 
    Point2D(-3,5),Point2D(3,5),Point2D(3,-5),Point2D(-3,-5),
  };

  Point2D inside1[4] = { 
    Point2D(-2,4), Point2D(2,4), Point2D(2,-4),
    Point2D(-2,-4)
  };
  Pointer<Polygon2D> out1 = Polygon2D::create(4,outside1);
  Pointer<Polygon2D> in1 = Polygon2D::create(4,inside1);

  Pointer<Region2D> A = out1->subtract(in1);

  Pointer<Region2D> B = A->removeHoles();
  assert(B);
}
