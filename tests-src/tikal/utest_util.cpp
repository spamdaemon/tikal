#include <tikal/util.h>
#include <tikal/Point2D.h>
#include <tikal/Contour2D.h>
#include <tikal/Region2D.h>
#include <tikal/Polygon2D.h>
#include <tikal/Path2D.h>
#include <tikal/Path2DBuilder.h>
#include <cassert>
#include <iostream>

using namespace std;
using namespace timber;
using namespace tikal;

void utest_1()
{
  const Point2D outside[] = { 
    Point2D(-1,1),Point2D(-1,-1), Point2D(1,-1),Point2D(1,1),
    Point2D(2,1),Point2D(2,-2), Point2D(-2,-2),Point2D(-2,2),
  };
  const Point2D other[] = { 
    Point2D(-1,1),Point2D(-1,-1), Point2D(1,-1),Point2D(1,1) 
  };

  Reference<Polygon2D> A=Polygon2D::create (sizeof(outside)/sizeof(Point2D),outside);
  Reference<Polygon2D> B=Polygon2D::create (sizeof(other)/sizeof(Point2D),other);

  assert(!testContainment(A,B));
  assert(!testContainment(B,A));

  assert(!testContainment(A,B));
  assert(!testContainment(B,A));
}

void utest_2()
{
  const Point2D outside[] = { 
    Point2D(-1,1),Point2D(-1,-1), Point2D(1,-1),Point2D(1,1),
    Point2D(2,1),Point2D(2,-2), Point2D(-2,-2),Point2D(-2,2),
  };
  const Point2D path[] = {
    Point2D(1,0),Point2D(-1,0)
  };

  Reference<Polygon2D> A=Polygon2D::create (sizeof(outside)/sizeof(Point2D),outside);
  Pointer<Path2D> p = Path2DBuilder::createPolylinePath(sizeof(path)/sizeof(Point2D),path);
  p = clipPath(p,A);
  ::std::cerr << "Path " << p << ::std::endl;
  assert(p==nullptr);
}

void utest_3()
{
  const Point2D outside[] = { 
    Point2D(-1,1),Point2D(-1,0),Point2D(-1,-1), Point2D(1,-1),Point2D(1,0),Point2D(1,1),
    Point2D(2,1),Point2D(2,-2), Point2D(-2,-2),Point2D(-2,2),
  };
  const Point2D path[] = {
    Point2D(1,0),Point2D(-1,0)
  };

  Reference<Polygon2D> A=Polygon2D::create (sizeof(outside)/sizeof(Point2D),outside);
  Pointer<Path2D> p = Path2DBuilder::createPolylinePath(sizeof(path)/sizeof(Point2D),path);
  p = clipPath(p,A);
  ::std::cerr << "Path " << p << ::std::endl;
  assert(p==nullptr);
}

void utest_4()
{
  const Point2D outside[] = { 
    Point2D(-1,1),Point2D(-1,0),Point2D(-1,-1), Point2D(0,-2),Point2D(1,-1),Point2D(1,0),Point2D(1,1),
    Point2D(2,1),Point2D(2,-3), Point2D(-2,-3),Point2D(-2,3),
  };
  const Point2D path[] = {
    Point2D(-2,-2),Point2D(0,-2),Point2D(2,-2)
  };

  Reference<Polygon2D> A =Polygon2D::create(sizeof(outside)/sizeof(Point2D),outside);
  Pointer<Path2D> p = Path2DBuilder::createPolylinePath(sizeof(path)/sizeof(Point2D),path);
  p = clipPath(p,A);
  ::std::cerr << "Path " << p << ::std::endl;
  assert(p!=nullptr);
}

void utest_5()
{
  const Point2D outside[] = { 
    Point2D(-1,1),Point2D(-1,0),Point2D(-1,-1), Point2D(0,0),Point2D(1,-1),Point2D(1,0),Point2D(1,1),
    Point2D(2,1),Point2D(2,-2), Point2D(-2,-2),Point2D(-2,2),
  };
  const Point2D path[] = {
    Point2D(1,0),Point2D(0,0),Point2D(-1,0)
  };

  Reference<Polygon2D> A=Polygon2D::create (sizeof(outside)/sizeof(Point2D),outside);
  Pointer<Path2D> p = Path2DBuilder::createPolylinePath(sizeof(path)/sizeof(Point2D),path);
  p = clipPath(p,A);
  ::std::cerr << "Path " << p << ::std::endl;
  assert(p==nullptr);
}

