# these are recommended in gnu make manual
SHELL := /bin/sh
COMMA := ,
EMPTY :=
SPACE := $(empty) $(empty)

.DEFAULT_GOAL := all

MODULE_DIR := $(abspath $(CURDIR))
BASE_DIR := ${MODULE_DIR}

# get this module's information as well
ifeq (found,$(shell test -e $(MODULE_DIR)/build/minfo.mk && echo found)) 
  $(info Using module specific info)
  include $(MODULE_DIR)/build/minfo.mk
else
  $(info Using generic module info)
  include $(PROJECT_DIR)/build/new_module/build/minfo.mk
endif

# determine which external libraries we're using
ifeq (found,$(shell test -e $(MODULE_DIR)/build/ext.mk && echo found)) 
  $(info Using module specific extensions)
  include $(MODULE_DIR)/build/ext.mk
else
  $(info Using project-wide extensions)
  include $(PROJECT_DIR)/build/ext.mk
endif

# get the projects on which this project depends
# first, look in the module directory, look in the 
# project directory
ifeq (found,$(shell test -e $(MODULE_DIR)/build/proj.mk && echo found)) 
  $(info Using module specific project dependencies)
  include $(MODULE_DIR)/build/proj.mk
else
  $(info Using project-wide project dependencies)
  include $(PROJECT_DIR)/build/proj.mk
endif

# make sure the basic info about the projects on which
# this project depends are defined
ifndef PROJ_LIB_SEARCH_PATH
	PROJ_LIB_SEARCH_PATH := $(addsuffix /lib,$(PROJ_DIRS))
endif
ifndef PROJ_LINK_LIBS
PROJ_LINK_LIBS := $(addprefix -l,$(PROJ_NAMES))
endif

ifndef PROJ_INC_SEARCH_PATH
PROJ_INC_SEARCH_PATH := $(addsuffix /lib-src,$(PROJ_DIRS))
endif

ifndef PROJ_COMPILER_DEFINES
PROJ_COMPILER_DEFINES :=
endif

# how are we testing
include $(PROJECT_DIR)/build/test.mk

# we need to include the various options for compilation (1 for each supported language)
include $(PROJECT_DIR)/build/opts.*.mk

# the compilation instructions (1 for each supported language)
include $(PROJECT_DIR)/build/compile.*.mk

# the link instructions
include $(PROJECT_DIR)/build/link.mk

# here we're finding all the source in the current project
# let's find all the sources
ifeq (found,$(shell test -e $(MODULE_DIR)/build/sources.mk && echo found)) 
  $(info Using module specific sources)
  include $(MODULE_DIR)/build/sources.mk
else
  $(info Using generic module sources)
  include $(PROJECT_DIR)/build/new_module/build/sources.mk
endif

# collect dependencies
include $(PROJECT_DIR)/build/depends.mk

# here we should make sure that the number of OBJECT_FILES equals the
# number of OBJECT_SOURCE_FILES. They might be different, if there are 
# source whose names only differ in the extensions!

include $(PROJECT_DIR)/build/common-rules.mk
include $(PROJECT_DIR)/build/common-targets.mk


ifeq (found,$(shell test -e $(MODULE_DIR)/build/rules.mk && echo found)) 
  $(info Using module specific rules)
  include $(MODULE_DIR)/build/rules.mk
else
  $(info Using generic module rules)
  include $(PROJECT_DIR)/build/new_module/build/rules.mk
endif

# the targets
ifeq (found,$(shell test -e $(MODULE_DIR)/build/targets.mk && echo found)) 
  $(info Using module specific targets)
  include $(MODULE_DIR)/build/targets.mk
else
  $(info Using generic module targets)
  include $(PROJECT_DIR)/build/new_module/build/targets.mk
endif
